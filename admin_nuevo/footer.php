<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versión</b> 2.0
    </div>
    <strong>Copyright &copy; 2010-2017 <a href="http://distantis.com">Distantis SA</a>.</strong> Todos los derechos reservados.
  </footer>