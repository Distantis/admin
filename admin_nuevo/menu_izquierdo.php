<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/avatar_default.jpeg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p class="nombre_usuario">Prueba</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form >
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Operaciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php"><i class="fa fa-circle-o"></i> OR</a></li>
            <li><a href="nmr_conf.php"><i class="fa fa-circle-o"></i> N° Confirmación</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Reservas Anuladas</a></li>
          </ul>
        </li>
        
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-hotel"></i>
            <span>Hoteles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Hoteles</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Nueva Tarifa</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Empresas</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Editor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="editor_usuarios.php"><i class="fa fa-circle-o text-red"></i> Usuarios</a></li>
            <li><a href="editor_hotelesmerge.php"><i class="fa fa-circle-o text-warning"></i> Merge</a></li>
          </ul>
        </li>
      
      </ul>
    </section>