$(document).on("ready", function(){
     
     
     $('.datepicker').datepicker();
     $("#fono").numeric();
     $("#record").numeric();
    
     $("#mensaje").html("Cargando...<i class='fa fa-spinner fa-spin'></i>");
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_hoteles" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Nombre</th>'
                                +'<th>CTS</th>'
								+'<th>VECI</th>'
                                +'<th>OTSI</th>'
                                +'<th>TURAVION</th>'
                                +'<th>COCHA</th>'
                                +'<th>TRAVEL</th>'
                                +'<th>CADENA</th>'
                                +'<th>OPERA</th>'
                                +'<th>CAPA</th>'
                                +'<th></th>'
                                +'<th></th>'
                                +'<th></th>'
                                +'<th>Agregar faltantes</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:4
                },
                function (data) {

                    data = $.parseJSON(data);
                    var hotel,cts,veci,otsi,turavion,cocha,opera,capacitado = '', tiene_usuario_cts='', tiene_usuario_veci='', tiene_usuario_otsi='', tiene_usuario_turavion='', tiene_usuario_cocha='';
                    $.each(data.hoteles2, function (i, datos) {

                        //console.log(datos.travel);
                        
                        //var x = parseInt(i)+1;
                        
                        if(datos.capacitado == 0){
                            
                            capacitado = "<i class='fa fa-times-circle text-danger'></i>";
                            
                        }else{
                            
                            capacitado = "<i class='fa fa-check-circle text-success'></i>";   
                        }
                        
                        if(datos.opera == null){
                            
                            opera = "<p class='text-danger'>*</p>";
                            
                        }else{
                            
                            opera = datos.opera;   
                            
                            if(opera == 0){
                                opera = "Onrequest";
                            }else{
                                opera = "Plataforma";
                            }
                        }
                        
                        if(datos.nombre_hotel == null || datos.nombre_hotel == "Sin nombre"){
                            hotel = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            hotel = datos.nombre_hotel;
                        }
                        
                        if(datos.cts == null || datos.cts == 0){
                            cts = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            cts = datos.cts;
                        }
                        
                        
                        if(datos.veci == null || datos.veci == 0){
                            veci = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            veci = datos.veci;
                        }
                        
                        if(datos.otsi == null || datos.otsi == 0){
                            otsi = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            otsi = datos.otsi;
                        }
                        
                        if(datos.turavion == null || datos.turavion == 0){
                            turavion = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            turavion = datos.turavion;
                        }
                        
                        if(datos.cocha == null || datos.cocha == 0){
                            cocha = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            cocha = datos.cocha;
                        }


                        if(datos.travel == null || datos.travel == 0){
                            travel = "<i class='fa fa-times text-danger'></i>";
                            
                        }else{
                            
                            travel = datos.travel;
                        }
                        
                        
                        //total = parseInt(datos.cont_cts)+parseInt(datos.cont_veci)+parseInt(datos.cont_otsi)+parseInt(datos.cont_turavion)+parseInt(datos.cont_cocha);
                        
                         if(datos.cont_cts == 0){
                            
                            tiene_usuario_cts = "<a title='No tiene usuario'><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                            
                            tiene_usuario_cts = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }
                        
                        
                        
                        if(datos.cont_veci == 0){
                            
                            tiene_usuario_veci = "<a title='No tiene usuario' ><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                            
                            tiene_usuario_veci = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }
                        
                        
                         if(datos.cont_otsi == 0){
                            
                            tiene_usuario_otsi = "<a title='No tiene usuario' ><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                            
                            tiene_usuario_otsi = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }
                        
                        
                        if(datos.turavion == 0){
                            
                            tiene_usuario_turavion = "<a title='No tiene usuario' ><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                            
                            tiene_usuario_turavion = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }
                        
                        if(datos.cocha == 0){
                            
                            tiene_usuario_cocha = "<a title='No tiene usuario' ><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                            
                            tiene_usuario_cocha = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }

                        if(datos.travel == 0){
                            
                            tiene_usuario_travel = "<a title='No tiene usuario' ><i class='fa fa-times-circle text-danger'></i></a>";
                            
                        }else{
                             
                            tiene_usuario_travel = "<a title='Tiene usuario' ><i class='fa fa-check-circle text-success'></i></a>";   
                        }
                         
                        tabla += "<tr>"
                                    +"<td align='right'>"+datos.id+" <br>  <input type='checkbox' id='activar_"+datos.id+"' name='activar_"+datos.id+"' checked onchange='activar("+datos.id+")'></td>"
                                    +'<td align="left"><a onClick="traer_usupass(\'' + hotel + '\',\'' + datos.bd + '\')" title="Ver Usuarios" class="enviar_usupass" data-modal="form-traer-usupass">'+hotel+'</a> <br><input type="number" id="pkg_'+datos.id+'" name="pkg_'+datos.id+'" style="width: 30px;" value="'+datos.release+'" disabled></td>'
                                    +"<td align='center'><a onClick='traer_tarifa_cts("+datos.cts+")' title='Ver tarifas' class='enviar_tarifas' data-modal='form-traer-tarifas'>"+cts+"</a> &nbsp;"+tiene_usuario_cts+" <br><input type='number' id='pkcts_"+datos.id+"' name='pkcts_"+datos.id+"' style='width: 30px;' value='"+datos.release_cts+"' disabled></td>"
                                   	+"<td align='center'><a onClick='traer_tarifa_veci("+datos.veci+")' title='Ver tarifas' class='enviar_tarifas_veci' data-modal='form-traer-tarifas_veci'>"+veci+"</a>&nbsp;"+tiene_usuario_veci+" <br><input type='number' id='pkveci_"+datos.id+"' name='pkveci_"+datos.id+"'  style='width: 30px;' value='"+datos.release_veci+"'  disabled></td>"
                                    +"<td align='right'><a onClick='traer_tarifa_otsi("+datos.otsi+")' title='Ver tarifas' class='enviar_tarifas_otsi' data-modal='form-traer-tarifas_otsi'>"+otsi+"</a>&nbsp;"+tiene_usuario_otsi+" <br><input type='number'  id='pkotsi_"+datos.id+"' name='pkotsi_"+datos.id+"'  style='width: 30px;' value='"+datos.release_otsi+"'  disabled></td>"
                                    +"<td align='right'><a onClick='traer_tarifa_turavion("+datos.turavion+")' title='Ver tarifas' class='enviar_tarifas_turavion' data-modal='form-traer-tarifas_turavion'>"+turavion+"</a>&nbsp;"+tiene_usuario_turavion+" <br><input type='number'  id='pkturavion_"+datos.id+"' name='pkturavion_"+datos.id+"'  style='width: 30px;' value='"+datos.release_turavion+"'  disabled></td>"
                                    +"<td align='right'><a onClick='traer_tarifa_cocha("+datos.cocha+")' title='Ver tarifas' class='enviar_tarifas_cocha' data-modal='form-traer-tarifas_cocha'>"+cocha+"</a>&nbsp;"+tiene_usuario_cocha+" <br><input type='number'   id='pkcocha_"+datos.id+"' name='pkcocha_"+datos.id+"'  style='width: 30px;' value='"+datos.release_cocha+"'  disabled></td>"
                                    +"<td align='right'><a onClick='traer_tarifa_travel("+datos.travel+")' title='Ver tarifas' class='enviar_tarifas_travel' data-modal='form-traer-tarifas_travel'>"+travel+"</a>&nbsp;"+tiene_usuario_travel+" <br><input type='number'   id='pktravelclub_"+datos.id+"' name='pktravelclub_"+datos.id+"'  style='width: 30px;' value='"+datos.release_travelclub+"'  disabled></td>"
                                    +"<td align='right'>"+datos.nom_cadena+" <br> <center><img src=images/release.png alt='Aplicar Release' onClick='agregar_release("+datos.id+")' width='20pk' height='20pk'><center></td>"
                                    +"<td align='right'><span id='op"+datos.id+"'>"+opera+"</span></td>"
                                    +"<td align='right'><span id='cap"+datos.id+"'>"+capacitado+"</span></td>"
                                    +"<td align='right'><a onClick='traer_observaciones("+datos.id+")' title='Ver Observaciones'><i class='fa fa-comment-o text-primary traer_observacion' data-modal='observaciones'></i></a></td>"
                                     +"<td align='right'><a onClick='traer_contacto("+datos.id+")' title='Ver Contactos'><i class='fa fa-phone text-warning traer_contacto' data-modal='contactos'></i></a></td>"
                                     +"<td align='right'><a onClick='agregar_recordatorio("+datos.id+")' title='Agregar Recordatorio'><i class='fa fa-user text-success enviar_recordatorio' data-modal='form-agregar-recordatorio'></i></a> "+datos.fec_record+"</td>"
                                    +"<td align='center'><a onClick='agregar_operacion("+datos.id+")' title='Agregar Operación'><i class='fa fa-edit text-warning fa-2x enviar_operacion' data-modal='form-agregar-operacion'></i></a>&nbsp;&nbsp;<a onClick='agregar_capacitacion("+datos.id+")' title='Agregar Capacitación'><i class='fa fa-edit text-success fa-2x enviar_capacitacion' data-modal='form-agregar-capacitacion'></i></a>&nbsp;&nbsp;<a onClick='agregar_observacion("+datos.id+")' title='Agregar Observación'><i class='fa  fa-pencil text-info fa-2x enviar_observacion' data-modal='form-agregar-observacion'></i></a>&nbsp;&nbsp;<a onClick='agregar_contacto("+datos.id+")' title='Agregar Contactos'><i class='fa  fa-user-plus text-primary fa-2x enviar_contacto' data-modal='form-agregar-contacto'></i></a></td>"
                                +"</tr>"
                    
                    });
                    
                    
                    tabla += '</tbody></table>';
                    $("#hoteles").html(tabla);
                    $(".enviar_operacion").modalEffects();
                    $(".enviar_capacitacion").modalEffects();
                    $(".enviar_observacion").modalEffects();
                    $(".enviar_contacto").modalEffects();
                    $(".enviar_tarifas").modalEffects();
                    $(".enviar_tarifas_veci").modalEffects();
                    $(".enviar_tarifas_otsi").modalEffects();
                    $(".enviar_tarifas_turavion").modalEffects();
                    $(".enviar_tarifas_cocha").modalEffects();
                    $(".enviar_tarifas_travel").modalEffects();
                    $(".enviar_usupass").modalEffects();
                    $(".traer_observacion").modalEffects();
                    $(".traer_contacto").modalEffects();
                    $(".enviar_recordatorio").modalEffects();
                  
                  
                    
                    var table = $("#tabla_hoteles").DataTable({
                                    "aaSorting": [[0, 'asc']],
                                    "oLanguage": {
                                        "sProcessing": "Procesando...",
                                        "sLengthMenu": "Mostrar _MENU_ ",
                                        "sZeroRecords": "No se han encontrado datos disponibles",
                                        "sEmptyTable": "No se han encontrado datos disponibles",
                                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                        "sInfoPostFix": "",
                                        "sSearch": "Buscar: ",
                                        "sUrl": "",
                                        "sInfoThousands": ",",
                                        "sLoadingRecords": "Cargando...",
                                        "oPaginate": {
                                            "sFirst": "Primero",
                                            "sLast": "Último",
                                            "sNext": "Siguiente",
                                            "sPrevious": "Anterior"
                                        },
                                        "oAria": {
                                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                        }
                                    }
                                });
         
                                 $(".enviar_operacion").modalEffects();
                                 $(".enviar_capacitacion").modalEffects();
                                 $(".enviar_observacion").modalEffects();
                                 $(".enviar_contacto").modalEffects();
                                 $(".enviar_tarifas").modalEffects();
                                 $(".enviar_tarifas_veci").modalEffects();
                                 $(".enviar_tarifas_otsi").modalEffects();
                                 $(".enviar_tarifas_turavion").modalEffects();
                                 $(".enviar_tarifas_cocha").modalEffects();
                                 $(".enviar_tarifas_travel").modalEffects();
                                 $(".enviar_usupass").modalEffects();
                                 $(".enviar_recordatorio").modalEffects();  
         
                                 $(".traer_observacion").modalEffects();
                                 $(".traer_contacto").modalEffects();
         
         
                         /*$('#tabla_hoteles').on('page.dt', function() {
                                var info = table.page.info();
                                var page = info.page+1;
                                alert('changed - page '+page+' out of '+info.pages+' is clicked');
                         });*/
                      
        
                   
                }
             
             ).done(function () {
                  
                    $("#mensaje").hide('slow');
         
             });
    
        
    
        
    
});








$("#enviar").on("click", function(event){
    
    var add_operacion = $("#add_operacion").val();
    var operacion = $("#operacion").val();
    var nombre = '';
    
    if(add_operacion == ""){
        
        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>No dejar el campo vacio.</label>", "danger", "bottom-right");
        
    }else{
        
         $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:1, tipo_operacion : add_operacion, operacion : operacion
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data.respuesta == 1){
                        
                        mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente la ocupación.</label>", "success", "bottom-right");
                        $("#form-agregar-operacion").removeClass("md-show");
                        
                        if(add_operacion == 1){
                            
                            nombre = "Onrequest";
                            
                        }else{
                            
                            nombre = "Plataforma";
                        
                        }
                        
                        $("#op"+operacion).html(nombre);
                        
                        
                    }else{
                        
                        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar editar el campo.</label>", "danger", "bottom-right");
                    }
             
                }
                
                ).done(function (){
             
             
             
                });
        
    }
    
    
    event.preventDefault();
    
});



$("#enviar2").on("click", function(event){
    
    var add_capacitacion = $("#add_capacitacion").val();
    var capacitacion = $("#capacitacion").val();
    var nombre = '';
    
    if(add_capacitacion == ""){
        
        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>No dejar el campo vacio.</label>", "danger", "bottom-right");
        
    }else{
        
         $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:2, tipo_capacitacion : add_capacitacion, capacitado : capacitacion
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data.respuesta == 1){
                        
                        mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente la capacitación.</label>", "success", "bottom-right");
                        $("#form-agregar-capacitacion").removeClass("md-show");
                        
                        if(add_capacitacion == 1){
                            
                            nombre = "<i class='fa fa-check-circle text-success'></i>";
                            
                        }else{
                            
                            nombre = "<i class='fa fa-times-circle text-danger'></i>";
                        
                        }
                        
                        $("#cap"+capacitacion).html(nombre);
                        
                        
                    }else{
                        
                        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar editar el campo.</label>", "danger", "bottom-right");
                    }
             
                }
                
                ).done(function (){
             
             
             
                });
        
    }
    
    
    event.preventDefault();
    
});

$("#enviar3").on("click", function(event){
    
    var obs = $("#obs").val();
    var observacion = $("#observacion").val();
    
    if(obs == ""){
        
        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>No dejar el campo vacio.</label>", "danger", "bottom-right");
        
    }else{
        
         $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:3, obs : obs, observacion : observacion
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data.respuesta == 1){
                        
                        mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha insertado correctamente la observación.</label>", "success", "bottom-right");
                        $("#form-agregar-observacion").removeClass("md-show");
                        $("#obs").val("");
                        
                    }else{
                        
                        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar la observación.</label>", "danger", "bottom-right");
                    }
             
                }
                
                ).done(function (){
             
             
             
                });
        
    }
    
    
    event.preventDefault();
    
});



$("#enviar4").on("click", function(event){
    
    var tipo_usuario = $("#tipo_usuario").val();
    var email = $("#email").val();
    var fono = $("#fono").val();
    var nombre = $("#nombre").val();
    var contacto = $("#contacto").val();
    
    if(tipo_usuario == "" || email == "" || fono == "" || nombre==""){
        
        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>No dejar el campo vacio.</label>", "danger", "bottom-right");
        
    }else{
        
         $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:6, tipo_usuario : tipo_usuario, email : email, fono : fono, contacto : contacto, nombre: nombre
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data.respuesta == 1){
                        
                        mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha insertado correctamente el contacto.</label>", "success", "bottom-right");
                        $("#form-agregar-contacto").removeClass("md-show");
                        $("#tipo_usuario").val("");
                        $("#email").val("");
                        $("#fono").val("");
                        
                    }else{
                        
                        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar el contacto.</label>", "danger", "bottom-right");
                    }
             
                }
                
                ).done(function (){
             
             
             
                });
        
    }
    
    
    event.preventDefault();
    
});



function agregar_operacion(id){
    
    $("#operacion").val(id);
    
}

function agregar_capacitacion(id){
    
    $("#capacitacion").val(id);
    
}

function agregar_observacion(id){
    
    $("#observacion").val(id); 
    
}

function agregar_contacto(id){
    
    $("#contacto").val(id); 
    
}


function traer_tarifa_cts(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_cts" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:8, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_cts").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_cts, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='center'>"+datos.tt+"</td>"
                                    +"<td align='center'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_cts").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}



function traer_tarifa_veci(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_veci" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:9, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_veci").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_veci, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='left'>"+datos.tt+"</td>"
                                    +"<td align='left'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_veci").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}


function traer_tarifa_otsi(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_otsi" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:10, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_otsi").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_otsi, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='left'>"+datos.tt+"</td>"
                                    +"<td align='left'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_otsi").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}



function traer_tarifa_turavion(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_turavion" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:11, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_turavion").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_turavion, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='left'>"+datos.tt+"</td>"
                                    +"<td align='left'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_turavion").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}


function traer_tarifa_cocha(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_cocha" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:12, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_cocha").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_cocha, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='left'>"+datos.tt+"</td>"
                                    +"<td align='left'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_cocha").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}


function traer_tarifa_travel(id){
     
     var id_hotel = id;
    
     var tabla ='<table class="table table-bordered responsive" id="tabla_travel" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Desde</th>'
                                +'<th>Hasta</th>'
                                +'<th>SGL</th>'
                                +'<th>TWN</th>'
                                +'<th>DBL</th>'
                                +'<th>TPL</th>'
                                +'<th>TAR</th>'
                                +'<th>HAB</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:16, id_hotel : id_hotel
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_tarifas_travel").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene tarifas cargadas.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.tarifa_travel, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.desde+"</td>"
                                    +"<td align='left'>"+datos.hasta+"</td>"
                                    +"<td align='left'>"+datos.sgl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.dbl+"</td>"
                                    +"<td align='left'>"+datos.tpl+"</td>"
                                    +"<td align='left'>"+datos.tt+"</td>"
                                    +"<td align='left'>"+datos.th+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_tarifas_travel").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}



function traer_usupass(nombre,bd){
     
     var nombre_hotel = nombre;
   
     var tabla ='<table class="table table-bordered responsive" align="center" id="tabla_usuarios" width="100%"><thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Usuario</th>'
                                +'<th>Password</th>'
                                +'<th>Ciudad</th>'
                            +'</tr></thead><tbody>';
    
     $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:13, nombre_hotel : nombre_hotel, bd: bd
                },
                function (data) {

                    data = $.parseJSON(data);
         
                        if(data.respuesta == 6){
                            
                            $("#informacion_usuarios").html('<div class="alert alert-danger alert-dismissable">'
                                    +'No tiene usuarios asignados.'
                            +'</div>');
                            
                        }else{
                            
                            $.each(data.usuarios, function (i, datos) {
                                 var x = parseInt(i)+1;
                                
                                 tabla += "<tr>"
                                    +"<td align='right'>"+x+"</td>"
                                    +"<td align='left'>"+datos.usuario+"</td>"
                                    +"<td align='left'>"+datos.password+"</td>"
                                    +"<td align='left'>"+datos.ciudad+"</td>"
                         
                                
                            });
                            
                                tabla += '</tbody></table>';
                                $("#informacion_usuarios").html(tabla);
                            
                            
                        }


                    }

            
                    ).done(function(){
         
         
                    });
    
}


function traer_observaciones(id){
    
    
    var valor = id;

            $.post("obtener_hoteles.php", {
                        verificando_sitio: 1, tipo:5, id : valor
                    },
                    function (data) {

                        datax = $.parseJSON(data);
                        
                        if(datax.respuesta == 2){
                            
                             $("#informacion_todas").html("<p class='text-danger'>EL hotel no tiene observación cargada.</p>");
                            
                        }else{

                            $.each(datax.observaciones, function (i, datosx){

                                var datox = '<div class="panel panel-success">'
                                            +'<div class="panel-heading">'
                                                +'<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Observación '+(parseInt(i)+1)+' enviada el '+datosx.fecha+'</h3>'
                                            +'</div>'
                                              +'<div class="panel-body">'
                                                +datosx.obs
                                              +'</div>'


                                $("#informacion_todas").append(datox);

                            });

                        

                           
                        }


                    }

           ).done(function(){
                
                
                    $(".md-overlay").on("click" ,function(){

                       $("#informacion_todas").html(""); 

                    });


                    $(".md-close").on("click" ,function(){

                       $("#informacion_todas").html("");

                    });
                                
                


          });
         
    
    
}


function traer_contacto(id){
    
     var valor = id;
                            
    $.post("obtener_hoteles.php", {
                verificando_sitio: 1, tipo:7, id : valor
            },
            function (data) {

                data = $.parseJSON(data);

                if (data.contacto_comercial.length == 0) {


                     $("#informacion_contacto_comercial").html("<p class='text-danger'>Contacto Comercial sin información de contacto.</p>");

                } else {

                    $.each(data.contacto_comercial, function (i, datox){


                           var dat = '<div class="panel panel-success">'
                                    +'<div class="panel-heading">'
                                        +'<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Contacto Comercial</h3>'
                                    +'</div>'
                                      +'<div class="panel-body">'
                                        +'<i class="fa fa-user"></i>&nbsp;'+datox.nombre
                                        +'&nbsp;&nbsp;<i class="fa fa-at"></i>&nbsp;'+datox.email
                                        +'&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;'+datox.fono
                                      +'</div>'


                        $("#informacion_contacto_comercial").html(dat);


                    });

                }



                 if (data.contacto_operacion.length == 0) {


                     $("#informacion_contacto_operacional").html("<p class='text-danger'>Contacto Operacional sin información de contacto.</p>");



                } else {

                    $.each(data.contacto_operacion, function (i, d){


                           var dats = '<div class="panel panel-warning">'
                                    +'<div class="panel-heading">'
                                        +'<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Contacto Operacional</h3>'
                                    +'</div>'
                                      +'<div class="panel-body">'
                                        +'<i class="fa fa-user"></i>&nbsp;'+d.nombre
                                        +'&nbsp;&nbsp;<i class="fa fa-at"></i>&nbsp;'+d.email
                                        +'&nbsp;&nbsp;<i class="fa fa-phone"></i>&nbsp;'+d.fono
                                      +'</div>'


                        $("#informacion_contacto_operacional").html(dats);



                    });

                }



        }

        ).done(function(){

            $(".md-overlay").on("click" ,function(){

               $("#contactos_todos").removeClass("md-show");

            });


            $(".md-close").on("click" ,function(){

               $("#contactos_todos").removeClass("md-show");

            });


        });

    
}



function agregar_recordatorio(id){
    
    $("#id_pk").val(id);
    //$("#informacion_recordatorio").attr("id","informacion_recordatorio"+id);
    //$("#fecha_de_recordatorio").attr("id","fecha_de_recordatorio"+id);
    
     var valor = id;
                            
    $.post("obtener_hoteles.php", {
                verificando_sitio: 1, tipo:14, id_pk : valor
            },
            function (data) {

                data = $.parseJSON(data);
                
        
                if (data.respuesta == 6) {


                     $("#informacion_recordatorio").html("<p class='text-danger'>Sin recordatorio.</p>");



                } else {
        
                    $.each(data.record, function (i, d){

                        //$("#fecha_de_recordatorio"+d.id).html(d.fecha);
                        //$("#informacion_recordatorio"+d.id).html(d.recordatorio);
                        
                        $("#fecha_de_recordatorio").html(d.fecha);
                        $("#informacion_recordatorio").html(d.recordatorio);


                    });
                    
                    
                }
                
        
        }
           
           
    ).done(function(){
        
        
        
    });
    
}

function activar(pk){
	
	var activo = $('#activar_' + pk).is(":checked");
	
	
	if(activo){
		$("#pkg_"+pk).attr("disabled", "disabled");
		$("#pkcts_"+pk).attr("disabled", "disabled");
		$("#pkveci_"+pk).attr("disabled", "disabled");
		$("#pkotsi_"+pk).attr("disabled", "disabled");
		$("#pkturavion_"+pk).attr("disabled", "disabled");
		$("#pkcocha_"+pk).attr("disabled", "disabled");
		$("#pktravelclub_"+pk).attr("disabled", "disabled");
		alert("Release bloqueado.");
	}else{
	$("#pkg_"+pk).removeAttr("disabled");
	$("#pkcts_"+pk).removeAttr("disabled");
	$("#pkveci_"+pk).removeAttr("disabled");
	$("#pkotsi_"+pk).removeAttr("disabled");
	$("#pkturavion_"+pk).removeAttr("disabled");
	$("#pkcocha_"+pk).removeAttr("disabled");
	$("#pktravelclub_"+pk).removeAttr("disabled");
	alert("Puede modificar Release.");
	}
	
	
}

function agregar_release(pk){
	
	
	
	
		
		$.ajax({
				type: 'POST',
				url: 'http://admin.distantis.com/admin/agrega_release.php',
				data: {
					pk: pk,
					global: $("#pkg_"+pk).val(),
					cts: $("#pkcts_"+pk).val(),
					veci: $("#pkveci_"+pk).val(),
					otsi: $("#pkotsi_"+pk).val(),
					turavion: $("#pkturavion_"+pk).val(),
					cocha: $("#pkcocha_"+pk).val(),
					travelclub: $("#pktravelclub_"+pk).val()
					
					
					},
				async: true,
				success:function(result){
					$("#activar_"+pk).prop("checked", true);
					$("#pkg_"+pk).attr("disabled", "disabled");
					$("#pkcts_"+pk).attr("disabled", "disabled");
					$("#pkveci_"+pk).attr("disabled", "disabled");
					$("#pkotsi_"+pk).attr("disabled", "disabled");
					$("#pkturavion_"+pk).attr("disabled", "disabled");
					$("#pkcocha_"+pk).attr("disabled", "disabled");
					$("#pktravelclub_"+pk).attr("disabled", "disabled");
				    alert("Aplicado");
					
				},
				error:function(){
					alert("Error al cargar el release");
				}
			});
	
}



$("#enviar_recordatorio").on("click", function(event){
    
    var fec_record = $("#record").val();
    var recordatorio = $("#recordatorio_texto").val();
    var id_pk = $("#id_pk").val();
    
    if(fec_record == "" || recordatorio == ""){
        
        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>No dejar el campo vacio.</label>", "danger", "bottom-right");
        
    }else{
        
         $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:15, fec_record : fec_record, recordatorio : recordatorio, id_pk : id_pk
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data.respuesta == 1){
                        
                        mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha agregado correctamente el recordatorio.</label>", "success", "bottom-right");
                        //$("#form-agregar-operacion").removeClass("md-show");
                        
                        $("#record").val("");
                        $("#recordatorio_texto").val("");
                        
                        
                        //$("#informacion_recordatorio"+id_pk).html(recordatorio);
                        //$("#fecha_de_recordatorio"+id_pk).html(fec_record);
                        
                        $("#informacion_recordatorio").html(recordatorio);
                        $("#fecha_de_recordatorio").html(fec_record);
                        
                        
                    }else{
                        
                        mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al guardar el campo.</label>", "danger", "bottom-right");
                    }
                    
             
             
         }
                
                
         ).done(function(){
             
             
         });
        
    }
    
     event.preventDefault();
    
});
