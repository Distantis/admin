
$(document).on("ready", function(){

	$('#desde').datepicker().on('changeDate', function(e){
                $(this).datepicker('hide');
                var newDate = new Date(e.date);

                var dias = traer_dias((newDate.getDate()));
                var mes = traer_meses((newDate.getMonth()+1));

			    secondDate.val(dias+"-"+mes+"-"+newDate.getFullYear());
			    secondDate.datepicker('update');
     });

     
    var secondDate = $('#hasta').datepicker().on('changeDate', function(e){$(this).datepicker('hide');});


    
    armar_confirmaciones(0,0,0,0);
	


  });


 $("#excel").on("click", function(){

     
        window.open("excel.php?tipo=1",'_blank'); 

 });



$("#filtrar").on("click", function(){

		var desde = $("#desde").val();
		var hasta = $("#hasta").val();

	
		if(desde != "" || hasta != "")
			armar_confirmaciones(1,desde,hasta,0);


});

$("#buscar_cot").on("click", function(){

		var cot = $("#id_cot").val();


		if(cot != "")
			armar_confirmaciones(2,0,0,cot);


});


function armar_confirmaciones(verficar,desde,hasta,cot){

$("#nmr_conf_pendiente").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");

var enviando = "";

if(verficar==1)
	enviando = { verificando_sitio: 1, tipo:2 , desde : desde, hasta: hasta};
else if(verficar == 2)
	enviando = { verificando_sitio: 1, tipo:3 , cot : cot};
else
	enviando = { verificando_sitio: 1, tipo:1 };


var tab ='<table class="table table-bordered responsive" id="tabla_informacion" width="100%"><thead>'
                    +'<tr>'
                    	+'<th><center>Operador</center></th>'
						+'<th><center>Cot</center></th>'
						+'<th><center>Desde</center></th>'
						+'<th><center>Hotel</center></th>'
						+'<th>Nemo</th>'
						+'<th><center>Fono</center></th>'
						+'<th><center>Nombre pax</center></th>'
						+'<th><center>Habs</center></th>'
						+'<th><center>Fec In</center></th>'
						+'<th><center>Fec Out</center></th>'
						+'<th><center>Nº Confirmación</center></th>'
					+'</tr></thead><tbody>';


	$.post("sitio/obtener_datos_numconf.php", enviando,

                function (data) {

                	data = $.parseJSON(data);


                	$.each(data, function(j, e){

                		$.each(e, function (i, datos) {

                			var estado = "";
                			var op = "";

                			 if(datos.operador == "touravion_dev")
                			 	op = "turavion";
                			 else if(datos.operador == "distantis")
                			 	op = "cts";
                			 else
                			 	op = datos.operador;

                			 	if(datos.hotel != "JG DISTANTIS (HOTEL PRUEBA NO VALIDO)" && datos.hotel != "HOTEL DE PRUEBA - USO INTERNO OTSI" && datos.hotel!="NB RESORT y SPA(Hotel de Prueba)"){

				                	tab += "<tr id='"+datos.cliente+"_"+datos.cotdes+"'>"
				                					+"<td align='right'>"+op.toUpperCase()+"</td>"
				                                    +"<td align='right'>"+datos.cot+"</td>"
				                                    +"<td align='center'><i class='fa fa-info-circle text-warning revisar_desde' id='"+datos.hace_cuanto+"'></i></td>"
				                                    +'<td align="left">'+datos.hotel+'</td>'
				                                    +'<td align="left"></td>'
				                                    +"<td align='left'>"+datos.fono+"</td>"
				                                    +"<td align='left'>"+datos.pax+"</td>"
				                                    +'<td align="left"><center>S:'+datos.hab1+' - tw:'+datos.hab2+' - ma:'+datos.hab3+' - tpl:'+datos.hab4+'</center></td>'
				                                    +'<td align="left">'+datos.desde+'</td>'
				                                    +'<td align="left">'+datos.hasta+'</td>'
				                                    +'<td align="left">'
				                                    +'<center>'
													   +'<input type="text" name="'+datos.cliente+'_'+datos.cotdes+'_numres" id="'+datos.cliente+'_'+datos.cotdes+'_numres" value='+datos.num_reserva+'><br><br>'
											      	   +'<button name="conf_num" class="btn btn-success" style="width:125px; height:27px"  onClick="conf_cdnum('+datos.cliente+','+datos.cotdes+')">Ingresar Conf</button>'
													  +'</center>'
													+'</td>'
				                                   
				                            +"</tr>"
				                 }


                		});

                	});




					tab += '</tbody></table>';
                    $("#nmr_conf_pendiente").html(tab);

                    $(".revisar_desde").on("click", function(){

			      		var valor = $(this).attr("id");

			      		mostrar_notificacion('Desde', '<b>'+valor+'</b>', 'warning','bottom-left');

	      			});

                 	
                    var table = $("#tabla_informacion").DataTable({
								    "columnDefs": [
								        { "type": "date-euro", "targets": [ 8,9 ] }
								    ],
      								 "oLanguage": {
                                        "sProcessing": "Procesando...",
                                        "sLengthMenu": "Mostrar _MENU_ ",
                                        "sZeroRecords": "No se han encontrado datos disponibles",
                                        "sEmptyTable": "No se han encontrado datos disponibles",
                                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                        "sInfoPostFix": "",
                                        "sSearch": "Buscar: ",
                                        "sUrl": "",
                                        "sInfoThousands": ",",
                                        "sLoadingRecords": "Cargando...",
                                        "oPaginate": {
                                            "sFirst": "Primero",
                                            "sLast": "Último",
                                            "sNext": "Siguiente",
                                            "sPrevious": "Anterior"
                                        },
                                        "oAria": {
                                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                        }
                                    }
                                    /*"aoColumns": [
										null,
										null,
										null,
										null,
										null,
										null,
										null,
										null,
										{ "sType": "date-uk" },
										{ "sType": "date-uk" },
										null
									]*/
                     });


					 



                }

      ).done(function (){






      });


}




function conf_cdnum(idcliente,idcotdes){

    var valor = $("#"+idcliente+"_"+idcotdes+"_numres").val();

    if(valor == ""){

    		mostrar_notificacion('Advertencia', 'Debe ingresar al menos un dato.', 'warning','bottom-right');
    }else{


		$.ajax({
			type: 'POST',
			url: 'nums_conf.php',
			async: false,
			data: {
				id_cliente: idcliente,
				id_cotdes: idcotdes,
				num_conf: valor
			},
			success:function(result){
				//alert(result);
				mostrar_notificacion('Éxito', 'Dato ingresado correctamente.', 'success','bottom-right');
				$("#"+idcliente+"_"+idcotdes).html("");
			},
			error:function(){
				//alert("Error al actualizar");
				mostrar_notificacion('Error', 'Error al actualizar.', 'danger','bottom-right');
			}
		});


	}


}



function traer_dias(dia){

	var nuevo_dia = "";

	switch (dia){

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:

			nuevo_dia = "0"+dia;
		break;

		default:
			nuevo_dia = dia;
		break;

	}


	return nuevo_dia;


}



function traer_meses(mes){

	var nuevo_mes = "";

	switch (mes){

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:

			nuevo_mes = "0"+mes;
		break;

		default:
			nuevo_mes = mes;
		break;

	}


	return nuevo_mes;


}

