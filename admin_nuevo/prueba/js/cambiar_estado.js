$("#formulario4").on("submit", function(event){
    
     var formData = new FormData($("#formulario4")[0]);
     
     $(this).find("#enviar").html("Cambiando estado...  <i class='fa fa-gear fa-spin'></i>");
    
    

    $.ajax({
        url: 'clases/cambiar_estado.php',
        type: 'POST',
        data: formData,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        dataType:'JSON',
        success: function (data) {

                
             if (data.respuesta == 0) {
                mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar los datos.</label>", "danger", "bottom-right");
                 $("#enviar").html("Guardar cambios <i class='fa fa-chevron-circle-right'></i>");
          
            }
            
            
            if (data.respuesta == 2) {
                mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Faltan Campos.</label>", "danger", "bottom-right");
                $("#enviar").html("Guardar cambios <i class='fa fa-chevron-circle-right'></i>");
        
            }
            
            if (data.respuesta == 3) {
                mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Datos actualizados correctamente.</label>", "success", "bottom-right");
                $("#enviar").attr('disabled', true);
                $("#eliminar").attr('disabled', true);
                 setTimeout(function () {
                    window.location.reload(1);
                }, 2000);
              
            }

            
        }
        
        
    });
    
    event.preventDefault();
});
