$(document).on("ready", function(){
    
    //$("#cot").removeClass("form-control");
    $("#cots").html("<i>Cargando Cotizaciones</i>  <i class='fa fa-spinner fa-spin'></i>");
    $("#cots2").hide();
    
    $("#cot").append("<option value=''>Seleccionar cotizacion..</option>");
    
    $.post("obtener_cotizacion.php", {
                    verificando_sitio: 1, tipo:1
                },
                function (data) {

                    data = $.parseJSON(data);
                    var cot = '<option value=\'\'></option>';
                    
                    $.each(data, function (i, datos) {
                        
                      cot += '<option  value="' + datos.id_cot + '">Cotización N° ' + datos.id_cot + '</option>';
                    
                    });
                    
                    $("#cot").html(cot);
                    
                   
                }
             
             ).done(function () {
                 
                  //
                  
                
                  $('select#cot').select2({
                
                        placeholder: "Seleccione Cotización"
                
                  });
        
                  //$("#cots").css("display","none");
                  $("#cots").css("display","none");
                  $("#cots2").show();  

             });
    
});



    

$("#cot").on("change", function(){
   
    var valor = $(this).val();
    
      $.post("obtener_cotizacion.php", {
                    verificando_sitio: 1, tipo:2, idcot: valor
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data == null || data === undefined || data==""){
                        
                        mostrar_notificacion('Advertencia', 'La Cotizacion que ha seleccionado se encuentra vacia.', 'warning','bottom-right');
                     
                    }
          
                    var cotdes = '<option value=\'\'>Seleccionar Cotizacion..</option>';
                    
                    $.each(data, function (i, datos) {
                                         
                            cotdes += '<option  value="' + datos.id_cotdes + '"> (' + datos.id_cotdes + ')</option>';
                         
                    
                    });
                    
                    $("#id_cotdes").html(cotdes);
                    
                    
                   
                }
             
             ).done(function () {
                 
                
                 
                 

             });
    
    
});