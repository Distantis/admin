
$(document).on("ready", function(){



$.post("sitio/modificador_usuarios.php", {
            verificando_sitio: 1, tipo:1
        },
        function (data) {

            data = $.parseJSON(data);
            var hotel = '<option value=\'\'></option>';

            $.each(data.hoteles, function (i, datos) {

              hotel += '<option  value="' + datos.id_pk + '" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">(' + datos.id_pk + ') ' + datos.nombre_hotel + '</option>';

            });

          
            $("#hoteles").html(hotel);


        }

     ).done(function () {

  
          $('select#hoteles').select2({

                placeholder: "Seleccione Hotel"

          });


     });


});








$("#hoteles").on("change", function(){

			$("#respuesta").html("");
			$(".cargando").addClass("fa fa-spinner fa-spin");
			

		var valor = $(this).find(':selected').attr("data-hotel");
		var bd = $(this).find(':selected').attr("data-id");
		var pk = $(this).val();

		$("#hotel").val(valor);
		$("#bd").val(bd);
		$("#pk").val(pk);

		traer_usuarios(pk);



});



function traer_usuarios(pk){

	$("#mostrar").css('display','block');

	$.post("sitio/modificador_usuarios.php", {
            verificando_sitio: 1, tipo: 2, pk : pk
        },
        function (data) {

            data = $.parseJSON(data);

              var tabla ='<table class="table table-bordered stacktable" id="usuarios" width="100%" style="height : 60% !important;"><thead>'
                    +'<tr>'
                        +'<th>Op</th>'
                        +'<th>Nombre</th>'
                        +'<th>Pat</th>'
                        +'<th>Usuario</th>'
                        +'<th>Pass</th>'
                        +'<th>Mail</th>'
                        +'<th>Guardar</th>'
                        +'<th>Replicar</th>'
                        +'<th>Borrar</th>'
                        +'<th>Sesión</th>'
                    +'</tr></thead>'
                    +'<tfoot>'
			            +'<tr>'
			               +'<th>Op</th>'
	                        +'<th>Nombre</th>'
	                        +'<th>Pat</th>'
	                        +'<th>Usuario</th>'
	                        +'<th>Pass</th>'
	                        +'<th>Mail</th>'
	                        +'<th>Guardar</th>'
	                        +'<th>Replicar</th>'
	                        +'<th>Borrar</th>'
	                        +'<th>Sesión</th>'
			            +'</tr>'
			        +'</tfoot><tbody>';

            if(data.usuarios!==undefined){

            	$.each(data.usuarios, function(i,datos){

            		var operador = "";

            		if(datos.cliente == "distantis")
            			operador = "cts";
            		else if(datos.cliente == "touravion_dev")
            			operador = "turavion";
            		else if(datos.cliente == "travelclub")
            			operador = "travel"
            		else
            			operador = datos.cliente
            		

            		if(datos.correo == 1)
            			color = "class='bg-warning'";
            		else
            			color = "";
             
	                    tabla += "<tr id='usu"+datos.id_usuario+datos.cliente+"' "+color+">"
	                    		+"<td align='left'>"+operador.toUpperCase()+"</td>"
                                +"<td align='right'><input type='text' value='"+datos.nombre+"'' class='buscar form-control' id='nombre"+datos.cliente+datos.id_usuario+"'></td>"
                                +"<td align='right'><input type='text' value='"+datos.pat+"'' class='buscar form-control' id='pat"+datos.cliente+datos.id_usuario+"'></td>"   
                                +"<td align='right'><input type='text' value='"+datos.login+"''  class='buscar form-control' id='login"+datos.cliente+datos.id_usuario+"'></td>"
                                +"<td align='left'><input type='text' value='"+datos.pass+"''  class='buscar form-control' id='pass"+datos.cliente+datos.id_usuario+"'></td>"
                                +"<td align='left'><input type='text' value='"+datos.mail+"'' class='buscar form-control' id='mail"+datos.cliente+datos.id_usuario+"'></td>"
                                +"<td align='left'><button  class='btn btn-warning  editar' data-id='"+datos.id_usuario+"' data-operador='"+datos.cliente+"'><i class='fa fa-edit'></button></td>"
                                +"<td align='left'><button id='replicar' class='btn btn-success modales' data-modal='mostrar-informacion'" 
                                	+"data-empresa='"+datos.id_empresa+"' "
                                	+"data-login='"+datos.login+"' "
                                	+"data-pass='"+datos.pass+"' "
                                	+"data-mail='"+datos.mail+"' "
                                	+"data-nombre='"+datos.nombre+"' "
                                	+"data-app='"+datos.pat+"' "
                                	+"data-apm='"+datos.mat+"' "
                                	+"data-idioma='"+datos.idioma+"' "
                                	+">"
                                	+"<i class='fa fa-repeat'></button></td>"
                                +"<td align='left'><button  class='btn btn-danger eliminar' data-id='"+datos.id_usuario+"' data-operador='"+datos.cliente+"'><i class='fa fa-trash'></button></td>"
                                 +"<td align='left'><button  class='btn btn-primary iniciar_session' data-usuario='"+datos.login+"' data-pass='"+datos.pass+"'><i class='fa fa-user'></button></td>"
                              +"</tr>"  

            	});
						
						
        	}


        	tabla += "</thead></tbody>";
        	$("#respuesta").html(tabla);

        	
        	/*var table = $("#usuarios").DataTable({
        		rowReorder: {
		            selector: 'td:nth-child(5)'
		        },
		        responsive: true,
                       "oLanguage": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ ",
                            "sZeroRecords": "No se han encontrado datos disponibles",
                            "sEmptyTable": "No se han encontrado datos disponibles",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar: ",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
            });*/
           
        }


        ).done(function(){

        		
        		$(".modales").modalEffects();

        		$(".modales").on("click", function(){

        			$('#cts').attr('checked',false);
    				$('#otsi').attr('checked',false);
    				$('#veci').attr('checked',false);
    				$('#turavion').attr('checked',false);
    				$('#cocha').attr('checked',false);
    				$('#travel').attr('checked',false);


        			  $("#id_empresa").val($(this).attr("data-empresa"));
        			  $("#usu_login").val($(this).attr("data-login"));
        			  $("#usu_password").val($(this).attr("data-pass"));
        			  $("#usu_nombre").val($(this).attr("data-nombre"));
        			  $("#usu_pat").val($(this).attr("data-app"));
        			  $("#usu_mat").val($(this).attr("data-apm"));
        			  $("#usu_idioma").val($(this).attr("data-idioma"));
        			  $("#usu_mail").val($(this).attr("data-mail"));

        		});


        		$("#guardar_replicar").on("click", function(){


					$(".op:checked").each(function() {
		    			 
						var cliente = $(this).val();
						var id_empresa = $("#id_empresa").val();
						var login = $("#usu_login").val();
						var pass = $("#usu_password").val();
						var nombre = $("#usu_nombre").val();
						var pat = $("#usu_pat").val();
						var mat = $("#usu_mat").val();
						var idioma = $("#usu_idioma").val();
						var mail = $("#usu_mail").val();

						var pk = $("#pk").val();

						if(mail == "" || login == "" || pass == ""){

							mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Mail/Usuario/Password sin información.</label>", "warning", "bottom-right");

						}else{

							$.post("sitio/modificador_usuarios.php", {
			                        verificando_sitio: 1, tipo: 3, cliente:cliente,id_empresa:id_empresa,login:login,pass:pass,nombre:nombre,
			                        pat:pat,mat:mat,idioma:idioma,mail:mail,pk:pk
			                    },
			                    function (data) {

			                        data = $.parseJSON(data);

			                       	switch (data.respuesta){

			                       		case 1:
			                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Replicado en "+data.operador+" Correctamente.</label>", "success", "bottom-right");
			                       			$("#mostrar-informacion").removeClass("md-show");
			                       		break;

			                       		case 2:
			                       			mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>El usuario en "+data.operador+" ya existe.</label>", "warning", "bottom-right");
			                       		break;

			                       		case 3:
			                       			mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No existe hotel del operador en el pk seleccionado.</label>", "warning", "bottom-right");
			                       		break;

			                       		default:
			                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar el usuario.</label>", "danger", "bottom-right");
			                       		break;
			                       	}


			                });

						}


					});

				});


				$(".editar").on("click", function(){

					var operador = $(this).attr("data-operador");
					var id_usuario = $(this).attr("data-id");

					var login = $("#login"+operador+id_usuario).val();
					var pass = $("#pass"+operador+id_usuario).val();
					var mail = $("#mail"+operador+id_usuario).val();
					var nombre = $("#nombre"+operador+id_usuario).val();
					var pat = $("#pat"+operador+id_usuario).val();

					$.post("sitio/modificador_usuarios.php", {
			                        verificando_sitio: 1, tipo: 4, cliente:operador,id_usuario:id_usuario,login:login,pass:pass,mail:mail,nombre:nombre,pat:pat
			                    },
			                    function (data) {

			                        data = $.parseJSON(data);

			                        switch (data.respuesta){

			                       		case 1:
			                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Actualizado Correctamente.</label>", "success", "bottom-right");
			                       		break;

			                       		
			                       		default:
			                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar editar el usuario.</label>", "danger", "bottom-right");
			                       		break;
			                       	}

			                 });

					


				});


				$(".eliminar").on("click", function(){

					var operador = $(this).attr("data-operador");
					var id_usuario = $(this).attr("data-id");

					$.post("sitio/modificador_usuarios.php", {
			                        verificando_sitio: 1, tipo: 5, cliente:operador,id_usuario:id_usuario
			                    },
			                    function (data) {

			                        data = $.parseJSON(data);

			                        switch (data.respuesta){

			                       		case 1:
			                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Eliminado Correctamente.</label>", "success", "bottom-right");
			                       			$("#usu"+id_usuario+operador).html("");
			                       		break;

			                       		
			                       		default:
			                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar eliminar el usuario.</label>", "danger", "bottom-right");
			                       		break;
			                       	}

			                 });

					


				});


				$(".iniciar_session").on("click", function(){

					var usuario = $(this).attr("data-usuario");
					var pass = $(this).attr("data-pass");


					window.open("../../h2o/ingreso.php?u="+usuario+"&p="+pass+"", '_blank');
					


				});



        	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-right");
				$(".cargando").removeClass("fa fa-spinner fa-spin");



        });





}


