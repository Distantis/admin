$(document).on("ready", function(){


$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo:1
        },
        function (data) {

            data = $.parseJSON(data);
            var hotel = '<option value=\'\'></option>';

            $.each(data.hoteles, function (i, datos) {

              hotel += '<option  value="' + datos.id_pk + '">(' + datos.id_pk + ') ' + datos.nombre_hotel + '</option>';

            });

          
            $("#hoteles").html(hotel);


        }

     ).done(function () {

  
          $('select#hoteles').select2({

                placeholder: "Seleccione Hotel"

          });


     });



     $.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo:8
        },
        function (data) {

            data = $.parseJSON(data);
            var cadena = '<option value=\'\'></option>';

            $.each(data.cadenas, function (i, datos) {

              cadena += '<option  value="' + datos.id_cadena + '">(' + datos.id_cadena + ') ' + datos.nombre_cadena + '</option>';

            });

          
            $("#cadena").html(cadena);


        }

     ).done(function () {

  
          $('select#cadena').select2({

                placeholder: "Seleccione Cadena"

          });


     });


});


$("#global").on("click", function(){

//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
var id_hotel = $("#hotel").val();

$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 4, pk: id_hotel
        },
        function (data) {

            data = $.parseJSON(data);

            switch(data.respuesta){

            	case 1:
            		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Hotel ahora es global.</label>", "success", "bottom-right");
            	break;

            	case 2:
            		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>EL hotel ya se encuentra como global.</label>", "warning", "bottom-right");
            	break;

            	default:
            		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-right");
            	break;

            }


         });



});



$("#global_quitar").on("click", function(){

//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
var id_hotel = $("#hotel").val();

$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 5, pk: id_hotel
        },
        function (data) {

            data = $.parseJSON(data);

            switch(data.respuesta){

            	case 1:
            		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>El hotel ya no es global.</label>", "success", "bottom-right");
            	break;

            	default:
            		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-right");
            	break;

            }


         });



});



$("#dispo_normal_agregar").on("click", function(){

//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
var id_hotel = $("#hotel").val();

$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 6, pk: id_hotel
        },
        function (data) {

            data = $.parseJSON(data);

            switch(data.respuesta){

            	case 1:
            		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Pestaña normal agregada.</label>", "success", "bottom-right");
            	break;

            	case 2:
            		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>EL hotel ya se encuentra con la pestaña normal activa.</label>", "warning", "bottom-right");
            	break;

            	default:
            		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-right");
            	break;

            }


         });



});


$("#dispo_normal_quitar").on("click", function(){

//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
var id_hotel = $("#hotel").val();

$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 7, pk: id_hotel
        },
        function (data) {

            data = $.parseJSON(data);

            switch(data.respuesta){

            	case 1:
            		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Pestaña normal eliminada.</label>", "success", "bottom-right");
            	break;

            	default:
            		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-right");
            	break;

            }


         });



});


$("#hoteles").on("change", function(){

			$("#respuesta").html("");
			$(".cargando").addClass("fa fa-spinner fa-spin");
			$("#mostrar_cadena").css("display", "none");


		var valor = $(this).val();

		$("#hotel").val(valor);

		traer_pk(valor);

});


$("#cadena").on("change", function(){

			$("#respuesta_cadena").html("");
			$(".cargando").addClass("fa fa-spinner fa-spin");
			$("#mostrar").css("display", "none");


		var valor = $(this).val();

		$("#cadena").val(valor);


		traer_cadena(valor);

});



function traer_pk(id_hotel){

	var revisar = "";
	var guardar = "";

	$("#mostrar").css('display','block');

	$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 2, pk: id_hotel
        },
        function (data) {

            data = $.parseJSON(data);

              var tabla ='<table class="table table-bordered" id="editor" width="100%" style="height : 60% !important;"><thead>'
                    +'<tr>'
                        +'<th>CTS</th>'
                        +'<th>VECI</th>'
                        +'<th>OTSI</th>'
                        +'<th>TURAVION</th>'
                        +'<th>COCHA</th>'
                        +'<th>TRAVEL</th>'
                        +'<th>Cadena</th>'
                    +'</tr></thead><tbody>';

            if(data.detalle!==undefined){

            	$.each(data.detalle, function(i,datos){
            
             
	                    tabla += "<tr>"
                                +"<td align='right'><input type='text' value='"+datos.cts+"'' class='numeric buscar form-control' id='cts'></td>"
                                +"<td align='right'><input type='text' value='"+datos.veci+"'' class='numeric buscar form-control' id='veci'></td>"
                                +"<td align='left'><input type='text' value='"+datos.otsi+"'' class='numeric buscar form-control' id='otsi'></td>"
                                +"<td align='left'><input type='text' value='"+datos.turavion+"'' class='numeric buscar form-control' id='turavion'></td>"
                                +"<td align='center'><input type='text' value='"+datos.cocha+"'' class='numeric buscar form-control' id='cocha'></td>"
                                +"<td align='center'><input type='text' value='"+datos.travel+"'' class='numeric buscar form-control' id='travelclub'></td>"
                                +"<td align='center'><input type='text' value='"+datos.cadena+"'' class='numeric buscar form-control' id='cadena'></td>"	
                              +"</tr>"  

            	});
						
						
        	}


        	tabla += "</thead></tbody>";
        	$("#respuesta").html(tabla);

           
        }


        ).done(function(){

        		$(".numeric").numeric();

        	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-right");
				$(".cargando").removeClass("fa fa-spinner fa-spin");
				


				$(".buscar").on("blur", function(){


					var id = $(this).attr("id");
					var valor = $(this).val();


						$.post("sitio/modificador_hotelesmerge.php", {
                				verificando_sitio: 1, tipo: 3, id: id, valor:valor,pk: id_hotel
            			},

            			function (data) {

                			data = $.parseJSON(data);


                			if(data.respuesta == 1)
                				mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente.</label>", "success", "bottom-right");
                			else
                				mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar actualizar la información.</label>", "danger", "bottom-right");



                		});





				});



        });

}



function traer_cadena(id_cadena){

	$("#mostrar_cadena").css('display','block');

	$.post("sitio/modificador_hotelesmerge.php", {
            verificando_sitio: 1, tipo: 9, id_cadena: id_cadena
        },
        function (data) {

            data = $.parseJSON(data);

              var tabla ='<table class="table table-bordered" id="editor_cadena" width="100%" style="height : 60% !important;"><thead>'
                    +'<tr>'
                        +'<th>Nombre</th>'
                        +'<th>Modo</th>'
                    +'</tr></thead><tbody>';

            if(data.detalle_cadena!==undefined){

            	$.each(data.detalle_cadena, function(i,datos){
            
             
	                    tabla += "<tr>"
                                +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.nom_cadena+"'' class='buscar form-control' id='nom_cadena'></div></td>"
                                +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.modo+"'' class='numeric buscar form-control' id='modo'></div></td>"
                              +"</tr>"  

            	});
						
						
        	}


        	tabla += "</thead></tbody>";
        	$("#respuesta_cadena").html(tabla);

           
        }


        ).done(function(){

        		$(".numeric").numeric();


        	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-right");
				$(".cargando").removeClass("fa fa-spinner fa-spin");
				


				$(".buscar").on("blur", function(){


					var id = $(this).attr("id");
					var valor = $(this).val();


						$.post("sitio/modificador_hotelesmerge.php", {
                				verificando_sitio: 1, tipo: 10, id: id, valor:valor,id_cadena: id_cadena
            			},

            			function (data) {

                			data = $.parseJSON(data);


                			if(data.respuesta == 1)
                				mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente.</label>", "success", "bottom-right");
                			else
                				mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar actualizar la información.</label>", "danger", "bottom-right");



                		});





				});



        });

}


