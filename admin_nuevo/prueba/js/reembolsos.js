$(document).ready(function () {
  
    
    var url = 'clases/traercreditos.php';
    var verificando_sitio = 1;
    var traer = 2;
    
    function currencyFormat(num, extra) {
             if (isNumber(num) == true)
                 return extra + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
             return "no definido";
    }

     function isNumber(input) {
             return (input - 0) == input && ('' + input).replace(/^\s+|\s+$/g, "").length > 0;
     }

    
    
    var tabla ='<table class="table table-bordered responsive" id="tabla_reembolsos"><thead>'
                    +'<tr>'
                        +'<th>Cuenta</th>'
                        +'<th>Monto</th>'
                        +'<th>Rut</th>'
                        +'<th>Nombre</th>'
                        +'<th>Apellido Paterno</th>'
                        +'<th>Apellido Materno</th>'
                        +'<th>Fecha</th>'
                        +'<th>Solicitud</th>'
                    +'</tr></thead><tbody>';
    


                $.post(url, {
                        verificando_sitio: verificando_sitio,
                        traer : traer
                    },
                    function (data) {
                        data = $.parseJSON(data);
                    
                    
                        if(data.existe == 1){
                            
                            tabla += '</tbody></table>';
                            $("#reembolsos").html(tabla);
                            $("#tabla_reembolsos").DataTable({
                                    "aaSorting": [[0, 'asc']],
                                    "oLanguage": {
                                        "sProcessing": "Procesando...",
                                        "sLengthMenu": "Mostrar _MENU_ ",
                                        "sZeroRecords": "No se han encontrado datos disponibles",
                                        "sEmptyTable": "No se han encontrado datos disponibles",
                                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                        "sInfoPostFix": "",
                                        "sSearch": "Buscar: ",
                                        "sUrl": "",
                                        "sInfoThousands": ",",
                                        "sLoadingRecords": "Cargando...",
                                        "oPaginate": {
                                            "sFirst": "Primero",
                                            "sLast": "Último",
                                            "sNext": "Siguiente",
                                            "sPrevious": "Anterior"
                                        },
                                        "oAria": {
                                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                        }
                                    }
                            });
                            
                        
                            
                        }else{
                    
                        $("#excel_reembolsos").removeClass("disabled");
                        $.each(data.reembolsos, function (i, datos) {
            
        
                            tabla += "<tr>"
                                        +"<td align='right'>4311</td>"
                                        +"<td align='right'>"+currencyFormat(datos.monto,'')+"</td>"
                                        +"<td align='right'>"+datos.rut+""+datos.dv+"</td>"
                                        +"<td>"+datos.nombre+"</td>"
                                        +"<td>"+datos.app+"</td>"
                                        +"<td>"+datos.apm+"</td>"
                                        +"<td>"+datos.fecha+"</td>"
                                        +"<td>"+datos.solicitud+"</td>"
                                    +"</tr>"
                            
                            
                           
                            
                        });
                    
                    
                    
                    tabla += '</tbody></table>';
                    $("#reembolsos").html(tabla);
                    var table = $("#tabla_reembolsos").DataTable({
                            "aaSorting": [[0, 'asc']],
                            "oLanguage": {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ ",
                                "sZeroRecords": "No se han encontrado datos disponibles",
                                "sEmptyTable": "No se han encontrado datos disponibles",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar: ",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                    });
                    
                    
                    
                    $('#tabla_creditos tbody').on( 'click', 'tr', function () {
                        
                        
                              if ( $(this).hasClass('selected') ) {
                                    $(this).removeClass('selected');
                                    
                                }
                                else {
                                    table.$('tr.selected').removeClass('selected');
                                    $(this).addClass('selected');
                                    //$(this).css('background-color','red');
                                }
                        
                        
                         $('#eliminar').click( function () {
                            table.row('.selected').remove().draw( false );
                         } );
                        
                        
                    } );
                
                    
                }

     }); //ajax

    
});




