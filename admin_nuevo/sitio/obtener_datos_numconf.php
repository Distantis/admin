<?php

$traernumconf = new TraerNumConf();


class TraerNumConf{

	//protected $link;
	protected $sql_con;
	protected $datos = array();
	protected $datos_usuario = array();

	
	
	public function __construct(){
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
        //ini_set('memory_limit', '512M');
        session_start();
        require_once('../../Connections/db1.php');
        $this->set_conexion($db1);
        $this->obtener_parametros();
		//$this->revisar_existe();
        //$this->tipo();
		
	}
	
	
	protected function set_conexion($db1){
	
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
	}
    
    
    public function obtener_parametros(){
        
        extract($_POST);
        $this->datos_usuario["sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] =  $tipo;
        $this->datos_usuario["desde"] =  date('Y-m-d',strtotime($desde));
        $this->datos_usuario["hasta"] =  date('Y-m-d',strtotime($hasta));
        $this->datos_usuario["cot"] = $cot;
        $this->quitar_caracteres();
        $this->revisar_tipo();
        
     
    }
    
    
    public function revisar_tipo(){
        
       
        switch($this->datos_usuario["tipo"]){

            case 1:
                
                $this->traer_numconf();
                
            break;


            case 2:
                
                $this->traer_numconf_filtro();
                
            break;


            case 3:
                
                $this->traer_numconf_buscar();
                
            break;
 
         
            
        
        }
        
    }
    
    
    protected function traer_numconf(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          $i=0;
          while (!$clientes->EOF){

          $i+=1;

          $this->datos["cliente$i"] = array();


          if($i==5){

                $agregar_nemo_select = ",n.nemo, pag_dir, plataxplata";
                $agregar_nemo_join = "inner join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
          }else{

                $agregar_nemo_select = "";
                $agregar_nemo_join = "";
          }



                $consulta = "

                        SELECT 
                          c.id_cot,
                          id_cotdes,
                          hot_nombre,
                          hot_fono,
                          c.id_usuario,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_numreserva ".$agregar_nemo_select." 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                          ".$agregar_nemo_join."
                        WHERE cot_estado = 0 
                          AND c.id_seg = 7 
                          AND cd.id_seg = 7 
                          AND (
                            cd_numreserva IS NULL 
                            OR TRIM(cd_numreserva) = ''
                          ) 
                          AND cd.cd_estado = 0 
                          
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";

                //AND cd_fecdesde BETWEEN DATE_ADD(NOW(), INTERVAL - 4 DAY) 
                // AND DATE_ADD(NOW(), INTERVAL 10 DAY) 
                 // echo consulta;
                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = $enviar->Fields("cd_numreserva");

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;


                    if($i==5){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));
                    }else
                        $nuevos_datos = "";


                    $a = array(

                            "cot"  => $enviar->Fields("id_cot"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono" => $telefono,
                            "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                            "desde"  => $enviar->Fields("fec_in"),
                            "hasta"  => $enviar->Fields("fec_out"),
                            "hab1"  => $enviar->Fields("cd_hab1"),
                            "hab2"  => $enviar->Fields("cd_hab2"),
                            "hab3"  => $enviar->Fields("cd_hab3"),
                            "hab4"  => $enviar->Fields("cd_hab4"),
                            "cotdes" => $enviar->Fields("id_cotdes"),
                            "operador" =>$operador,
                            "num_reserva"=>$reserva,
                            "cliente"=>$i,
                            "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                            $nuevos_datos

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }


    protected function calcular_fecha($bd,$cot){

      if(isset($this->datos_usuario["sitio"])){

          /*$consulta = "select TIMEDIFF(NOW(),cot_fec) as diferencia from $bd.cot where id_cot = $cot";
          $traer = $this->sql_con->SelectLimit($consulta);
          $diferencia = $traer->Fields("diferencia");

          return $diferencia;*/

          $consulta = "select cot_fec from $bd.cot where id_cot = $cot";
          //echo $consulta."<br>";
          $traer = $this->sql_con->SelectLimit($consulta);

          $fecha_bd = $traer->Fields("cot_fec");
          $fecha_del_dia = date('Y-m-d H:i:s',strtotime("-2 hour"));

          //if($cot == 354313)
             //echo $fecha_bd."<=>".date('Y-m-d H:i:s',strtotime("-2 hour"))."<br>";


          $fecha1 = new DateTime("$fecha_bd");
          $fecha2 = new DateTime("$fecha_del_dia");
          $fecha = $fecha1->diff($fecha2);

          $ano = $fecha->y;
          $meses = $fecha->m;
          $dias = $fecha->d;
          $horas = $fecha->h;
          $minutos = $fecha->i;


          $diferencia = "A:".$ano." M:".$meses." D:".$dias." Hrs:".$horas." Min:".$minutos;

          return $diferencia;

      }



    }

    protected function traer_numconf_filtro(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          $i=0;
          while (!$clientes->EOF){

          $i+=1;

          $this->datos["cliente$i"] = array();



                $consulta = "

                        SELECT 
                          c.id_cot,
                          id_cotdes,
                          hot_nombre,
                          hot_fono,
                          c.id_usuario,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_numreserva 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                        WHERE cot_estado = 0 
                          AND c.id_seg = 7 
                          AND cd.id_seg = 7 
                          AND (
                            cd_numreserva IS NULL 
                            OR TRIM(cd_numreserva) = ''
                          ) 
                          AND cd.cd_estado = 0 
                          AND (cd_fecdesde  BETWEEN '".$this->datos_usuario["desde"]."'
                                  AND '".$this->datos_usuario["hasta"]."'
                                  OR cd_fechasta  BETWEEN '".$this->datos_usuario["desde"]."'
                                  AND '".$this->datos_usuario["hasta"]."'
                                  OR '".$this->datos_usuario["desde"]."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                                  OR '".$this->datos_usuario["hasta"]."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                                )
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";


                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = $enviar->Fields("cd_numreserva");

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;

                    if($i==5){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));
                    }else
                        $nuevos_datos = "";


                    $a = array(

                            "cot"  => $enviar->Fields("id_cot"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono" => $telefono,
                            "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                            "desde"  => $enviar->Fields("fec_in"),
                            "hasta"  => $enviar->Fields("fec_out"),
                            "hab1"  => $enviar->Fields("cd_hab1"),
                            "hab2"  => $enviar->Fields("cd_hab2"),
                            "hab3"  => $enviar->Fields("cd_hab3"),
                            "hab4"  => $enviar->Fields("cd_hab4"),
                            "cotdes" => $enviar->Fields("id_cotdes"),
                            "operador" =>$operador,
                            "num_reserva"=>$reserva,
                            "cliente"=>$i,
                            "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                             $nuevos_datos

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }



      protected function traer_numconf_buscar(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          $i=0;
          while (!$clientes->EOF){

          $i+=1;

          $this->datos["cliente$i"] = array();



                $consulta = "

                        SELECT 
                          c.id_cot,
                          id_cotdes,
                          hot_nombre,
                          hot_fono,
                          c.id_usuario,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_numreserva 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                           WHERE cot_estado = 0 
                           AND c.id_cot = ".$this->datos_usuario["cot"]." 
                           AND cd_estado = 0 
                           AND c.id_seg = 7
                           AND cd.id_seg = 7
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";

              
                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = $enviar->Fields("cd_numreserva");

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;

                    if($i==5){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));
                    }else
                        $nuevos_datos = "";


                    $a = array(

                            "cot"  => $enviar->Fields("id_cot"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono" => $telefono,
                            "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                            "desde"  => $enviar->Fields("fec_in"),
                            "hasta"  => $enviar->Fields("fec_out"),
                            "hab1"  => $enviar->Fields("cd_hab1"),
                            "hab2"  => $enviar->Fields("cd_hab2"),
                            "hab3"  => $enviar->Fields("cd_hab3"),
                            "hab4"  => $enviar->Fields("cd_hab4"),
                            "cotdes" => $enviar->Fields("id_cotdes"),
                            "operador" =>$operador,
                            "num_reserva"=>$reserva,
                            "cliente"=>$i,
                            "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                             $nuevos_datos

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }
    
   
    
    
    
    protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["desde"]);
        $this->mssql_escape($this->datos_usuario["hasta"]);
        $this->mssql_escape($this->datos_usuario["cot"]);
    }
    

   
    protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }
    
    
    function __destruct(){
         echo json_encode($this->datos);
        
    }
    

}

?>