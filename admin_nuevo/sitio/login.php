<?php

$login = new Login();


class Login{

	//protected $link;
	protected $sql_con;
	protected $datos = array();
	protected $datos_usuario = array();

	
	
	public function __construct(){
        error_reporting(0);
        session_start();
        require_once('../../Connections/db1.php'); 
        $this->set_conexion($db1);
      
        $this->obtener_parametros();
        //$this->revisar_modo_global();
		
	}

    
	protected function set_conexion($db1){
	
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
	}


    
    
    protected function obtener_parametros(){
        
        extract($_POST);
        $this->datos_usuario["verificando_sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] = $tipo;
        $this->datos_usuario["user"] = $user;
        $this->datos_usuario["pass"] = $pass;

        $this->revisar_tipo();
    }

    protected function revisar_tipo(){
        
       
        switch($this->datos_usuario["tipo"]){


        	case 1:

                $this->verificar_session();
                
            break;

            case 2:
              
                $this->verificar_usuario();
                
            break;

            case 3:
              
                $this->cerrar_session();
                
            break;


        }

    }


    protected function cerrar_session(){

    	session_destroy();
    	$this->datos["respuesta"] = 1;

    }



    protected function verificar_session(){

    	if(isset($_SESSION["entro"]) and $_SESSION["entro"] != ""){
    		$this->datos["respuesta"] = 1;
    		$this->datos["usuario"]  = trim($_SESSION['usrname']);
    		$this->traer_empresa();
    		$this->totales();
    	}else
    		$this->datos["respuesta"] = 0;

    }


    protected function totales(){

    	$con = "select * from hoteles.clientes";
    	$cliente = $this->sql_con->SelectLimit($con) or $this->errores(__LINE__);

    	$desde = date('Y-m-d');
    	$hasta = date('Y-m-d',strtotime('+3 day'));

    	$or = 0;
    	$nmr_conf = 0;
    	$anuladas = 0;
    	while(!$cliente->EOF){

    		$bd = $cliente->Fields("bd");

    		$consulta = "SELECT 
						  COUNT(*)AS total
						FROM
						  $bd.cotdes cd 
						  INNER JOIN $bd.cot c 
						    ON cd.id_cot = c.id_cot 
						  JOIN $bd.hotel ho 
						    ON ho.id_hotel = cd.id_hotel
						WHERE cd.id_seg IN (13) 
						  AND cd.cd_estado = 0 
						  AND c.cot_estado = 0 
						  AND c.id_seg IN (13)";

			$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

			$or += $traer->Fields("total");


			$consulta2 = "SELECT 
						  COUNT(*) AS total
						FROM
						  $bd.cot c 
						  INNER JOIN $bd.cotdes cd 
						    ON c.id_cot = cd.id_cot 
						  INNER JOIN $bd.hotel h 
						    ON cd.id_hotel = h.id_hotel
						WHERE cot_estado = 0 
						  AND c.id_seg = 7 
						  AND cd.id_seg = 7 
						  AND (
						    cd_numreserva IS NULL 
						    OR TRIM(cd_numreserva) = ''
						  ) 
						  AND cd.cd_estado = 0";

			$traer2 = $this->sql_con->SelectLimit($consulta2) or $this->errores(__LINE__);
			
			$nmr_conf += $traer2->Fields("total");


			$consulta3 = "SELECT 
						  COUNT(*) AS total 
						FROM
						  $bd.cot c 
						  INNER JOIN $bd.cotdes cd 
						    ON c.id_cot = cd.id_cot 
						  INNER JOIN $bd.hotel h 
						    ON h.id_hotel = cd.id_hotel 
						WHERE (
						    cd.`cd_estado` = 1 
						    OR c.cot_estado = 1
						  ) 
						  AND c.id_seg = 7 
						  AND cd.`cd_fecdesde` BETWEEN '$desde' 
						  AND '$hasta' ";

			$traer3 = $this->sql_con->SelectLimit($consulta3) or $this->errores(__LINE__);

			$anuladas += $traer3->Fields("total");;
			  
    		$cliente->MoveNext();
    	}


    	$this->datos["total_or"] = $or;
    	$this->datos["total_nmr"] = $nmr_conf;
    	$this->datos["total_anuladas"] = $anuladas;



    }


    protected function verificar_usuario(){


    	if($this->datos_usuario["verificando_sitio"] == 1){

		    	$consulta = "SELECT id_usuario, id_empresa, usu_login, usu_password ,usu_nombre, usu_pat , usu_mat, id_oficina, id_tipo, usu_idioma, usu_mail, id_area
		    				 FROM distantis.usuarios WHERE usu_login='".$this->datos_usuario["user"]."' 
		    				 AND usu_password='".$this->datos_usuario["pass"]."' 
		    				 and usu_estado = 0 
		    				 AND id_usuario IN (2443,2444,3296,816,2158,3361,4016,4189,4344, 4902, 4972, 5216, 5941)
		    				";

		    	$traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

		    	if(!$traer->EOF) {

					$_SESSION['entro'] = $traer->Fields("id_usuario");
					$_SESSION['m']=1;
					$_SESSION['logname']=$traer->Fields("usu_login");
					$_SESSION['usrname']=$traer->Fields("usu_nombre")." ".$traer->Fields("usu_pat")." ".$traer->Fields("usu_mat");
					$_SESSION['id']=$traer->Fields("id_usuario");
					$_SESSION['id_tipo']=$traer->Fields("id_tipo");
					$_SESSION['id_oficina']=$traer->Fields("id_oficina");
					$_SESSION['comp']="Distantis";
					$_SESSION['id_empresa']=$traer->Fields("id_empresa");
					$_SESSION['idioma']=$traer->Fields("usu_idioma");
					$_SESSION['mailuser']=$traer->Fields("usu_mail");
					$_SESSION['id_area']=$traer->Fields("id_area");

					$this->datos["respuesta"] = 1;

				}else
					$this->datos["respuesta"] = 0;

		}

    }


    protected function traer_empresa(){

    	$consulta = "select hot_nombre as nombre from distantis.hotel where id_hotel = ".$_SESSION["id_empresa"]." ";
    	$traer = $this->sql_con->SelectLimit($consulta);

    	$this->datos["empresa"] = $traer->Fields("nombre");

    }



    protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["id_pk"]);
    }
    

   
    protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }


    protected function errores($linea){

        die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
    }
    
    
    function __destruct(){
         echo json_encode($this->datos);
        
    }

}
   
