<?php

$traer_or = new TraerOr();


class TraerOr{

	//protected $link;
	protected $sql_con;
	protected $datos = array();
	protected $datos_usuario = array();

	
	
	public function __construct(){
        error_reporting(0);
        //ini_set("display_errors", 1);
        //ini_set('memory_limit', '512M');
        session_start();
        require_once('../../Connections/db1.php');
        $this->set_conexion($db1);
        $this->obtener_parametros();
		//$this->revisar_existe();
        //$this->tipo();
		
	}
	
	
	protected function set_conexion($db1){
	
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
	}
    
    
    public function obtener_parametros(){
        
        extract($_POST);
        $this->datos_usuario["sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] =  $tipo;
        $this->datos_usuario["id_cot"] =  $id_cot;
        $this->datos_usuario["cliente"] =  $operador;
        $this->datos_usuario["id_cotdes"] = $cotdes;
        $this->datos_usuario["motivo"] = $motivo;
        $this->datos_usuario["comentario"] = $comentario;
        $this->datos_usuario["usuario"] = $_SESSION["usrname"];
        $this->datos_usuario["observacion"] = $obs;
        $this->datos_usuario["nombre_bd"] = $nombre;
        $this->quitar_caracteres();
        $this->revisar_tipo();
        
     
    }
    
    
    public function revisar_tipo(){
        
       
        switch($this->datos_usuario["tipo"]){

            case 1:
                
                $this->traer_or();
                
            break;
 
            case 2:
                
                $this->traer_pax();
                
            break;


            case 3:
                
                $this->traer_detalle_or();
                
            break;

            case 4:
                
                $this->guardar_log_or();
                
            break;

            case 5:
                
                $this->rechazar_log_or();
                
            break;


            case 6:
                
                $this->traer_observacion();
                
            break;

            case 7:
                
                $this->guardar_observacion();
                
            break;
            
            case 8:
                
                $this->traer_or_unico();
                
            break;
        
        }
        
    }


    protected function traer_or_unico(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where nombre ='".$this->datos_usuario["nombre_bd"]."' and estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          $i=0;
          while (!$clientes->EOF){

          $i+=1;

          $this->datos["cliente$i"] = array();

               if($i==3){

                    $agregar_select = ",hv.hot_nombre as nombre_hv";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hv on c.id_opcts = hv.id_hotel";

               }elseif($i==4){

                    $agregar_select = ",hcc.hot_nombre as nombre_opcts";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hcc on c.id_opcts = hcc.codigo_cliente";

               }elseif($i==5){

                    $agregar_select = ",n.razon_social";
                    $join = "left join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
                    
               }else{

                    $agregar_select = "";
                    $join = "";
                    
               }


                $bd = $clientes->Fields("bd");

                $consulta = "
                              SELECT 
                                  DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS desde,
                                  DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS hasta,
                                  c.*,
                                  ho.hot_nombre,
                                  ho.id_hotel,
                                  cd.id_cotdes,
                                  TIMEDIFF(NOW(),t1.fecha) AS dif ".$agregar_select."
                                FROM
                                  $bd.cotdes cd 
                                  INNER JOIN $bd.cot c 
                                    ON cd.id_cot = c.id_cot 
                                  LEFT JOIN 
                                    (SELECT 
                                      * 
                                    FROM
                                      $bd.fechas_conf 
                                    GROUP BY id_cot) t1 
                                    ON t1.id_cot = c.id_cot 
                                  JOIN $bd.hotel ho 
                                    ON ho.id_hotel = cd.id_hotel
                                    ".$join." 
                                WHERE cd.id_seg IN (13) 
                                  AND cd.cd_estado = 0 
                                  AND c.cot_estado = 0 
                                  AND c.id_seg IN (13)
                                  AND c.id_cot in ".$this->datos_usuario["id_cot"]." 
                                GROUP BY cd.id_cotdes   ";
                //echo $consulta."<br><br>"; 
                //
                $enviar = $this->sql_con->SelectLimit($consulta);


                
                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){

                    if($i==3){

                        $arreglo = array(
                            "nombre_hv"=>utf8_encode(trim($enviar->Fields("nombre_hv")))
                            );

                    }elseif($i==4){
                        $arreglo = array(
                            "nombre_opcts"=>utf8_encode(trim($enviar->Fields("nombre_opcts")))
                            );

                    }elseif($i==5){
                         
                        $arreglo = array(
                                "razon_social"=>utf8_encode(trim($enviar->Fields("razon_social")))
                                );
                    }else{

                        $arreglo = "";
                    }


                    $verificar_observacion = $this->verificar_si_observacion($enviar->Fields("id_cot"));


                    $a = array(

                            "cot"  => utf8_encode($enviar->Fields("id_cot")),
                            "cotdes"=> $enviar->Fields("id_cotdes"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "id_hotel" => $enviar->Fields("id_hotel"),
                            "pax"  => $enviar->Fields("cot_numpas"),
                            "desde"  => $enviar->Fields("desde"),
                            "hasta"  => $enviar->Fields("hasta"),
                            "dif"  => $enviar->Fields("dif"),
                            "id_seg"  => $enviar->Fields("id_seg"),
                            "operador" =>$operador,
                            "cliente"=>$i,
                            "verificar_obs"=>$verificar_observacion,
                            $arreglo

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }


    protected function guardar_observacion(){


        if(isset($this->datos_usuario["sitio"])){

            $insrt = "insert into obs_onrequest(id_cot,operador,usuario,observacion,creacion) 
                      values('".$this->datos_usuario["id_cot"]."','".$this->datos_usuario["cliente"]."','".$this->datos_usuario["usuario"]."',
                             '".utf8_decode($this->datos_usuario["observacion"])."',NOW())";


            $guardar = $this->sql_con->Execute($insrt);

            if($guardar)
                $this->datos["respuesta"] = 1;
            else
                $this->datos["respuesta"] = 0;

        }

    }


    protected function traer_observacion(){


        if(isset($this->datos_usuario["sitio"])){

            $consulta = "select * from obs_onrequest where id_cot = '".$this->datos_usuario["id_cot"]."' order by creacion desc";
            $traer = $this->sql_con->SelectLimit($consulta);

            $this->datos["obs"] = array();  

            while(!$traer->EOF){

                $datos = array(
                                "cot" => $traer->Fields("id_cot"),
                                "operador" => $traer->Fields("operador"),
                                "usuario" => $traer->Fields("usuario"),
                                "observacion" => utf8_encode($traer->Fields("observacion")),
                                "creacion" => date('d-m-Y H:i:s',strtotime($traer->Fields("creacion")."-4 hours"))
                            );


                array_push($this->datos["obs"], $datos);

                $traer->MoveNext();

            }  

        }

    }

    protected function guardar_log_or(){

        if(isset($this->datos_usuario["sitio"])){

                $sql = "select * from clientes where nombre = '".$this->datos_usuario["cliente"]."' and estado = 0";
                $traer = $this->sql_con->SelectLimit($sql);
                $bd = $traer->Fields("bd");

                $query_cot = "select id_cot, id_seg from $bd.cotdes where id_cotdes = ".$this->datos_usuario["id_cotdes"];
                $cot = $this->sql_con->SelectLimit($query_cot);
                $despues = $cot->Fields("id_seg");

                $inser_log = "INSERT INTO hoteles.log_h2o(id_usuario, bd_usuario, per_codigo, fecha, id_cambio, bd_cambio, antes, despues, url, nota) 
                    VALUES(".$_SESSION['id'].", '".$_SESSION['comp']."', 5, NOW(), ".$this->datos_usuario["id_cotdes"].", 
                    '".$bd."', '', $despues, '".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."', '' )";

                
                $guardar = $this->sql_con->Execute($inser_log);


        }


    }
    

     protected function rechazar_log_or(){

        if(isset($this->datos_usuario["sitio"])){

                $sql = "select * from clientes where nombre = '".$this->datos_usuario["cliente"]."' and estado = 0";
                $traer = $this->sql_con->SelectLimit($sql);
                $bd = $traer->Fields("bd");

                $query_cot = "select id_cot, id_seg from $bd.cotdes where id_cotdes = ".$this->datos_usuario["id_cotdes"];
                $cot = $this->sql_con->SelectLimit($query_cot);
                $despues = $cot->Fields("id_seg");

                $inser_log = "INSERT INTO hoteles.log_h2o(id_usuario, bd_usuario, per_codigo, fecha, id_cambio, bd_cambio, antes, despues, url, nota) 
                    VALUES(".$_SESSION['id'].", '".$_SESSION['comp']."', 6, NOW(), ".$this->datos_usuario["id_cotdes"].", 
                    '".$bd."', '', $despues, '".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."', '".$this->datos_usuario["motivo"]."/".$this->datos_usuario["comentario"]."'  )";

                
                $guardar = $this->sql_con->Execute($inser_log);


        }


    }
    
    protected function traer_or(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          $i=0;
          while (!$clientes->EOF){

          $i+=1;

          $this->datos["cliente$i"] = array();

               if($i==3){

                    $agregar_select = ",hv.hot_nombre as nombre_hv";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hv on c.id_opcts = hv.id_hotel";

               }elseif($i==4){

                    $agregar_select = ",hcc.hot_nombre as nombre_opcts";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hcc on c.id_opcts = hcc.codigo_cliente";

               }elseif($i==5){

                    $agregar_select = ",n.razon_social";
                    $join = "left join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
                    
               }else{

                    $agregar_select = "";
                    $join = "";
                    
               }


                $bd = $clientes->Fields("bd");

                $consulta = "
                              SELECT 
                                  DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS desde,
                                  DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS hasta,
                                  c.*,
                                  ho.hot_nombre,
                                  ho.id_hotel,
                                  cd.id_cotdes,
                                  TIMEDIFF(NOW(),t1.fecha) AS dif ".$agregar_select."
                                FROM
                                  $bd.cotdes cd 
                                  INNER JOIN $bd.cot c 
                                    ON cd.id_cot = c.id_cot 
                                  LEFT JOIN 
                                    (SELECT 
                                      * 
                                    FROM
                                      $bd.fechas_conf 
                                    GROUP BY id_cot) t1 
                                    ON t1.id_cot = c.id_cot 
                                  JOIN $bd.hotel ho 
                                    ON ho.id_hotel = cd.id_hotel
                                    ".$join." 
                                WHERE cd.id_seg IN (13) 
                                  AND cd.cd_estado = 0 
                                  AND c.cot_estado = 0 
                                  AND c.id_seg IN (13)
                                  -- AND ho.id_hotel not in (4321,3973,7303,6048,6448,6448)  
                                GROUP BY cd.id_cotdes   ";
				//echo $consulta."<br><br>"; 
                //
                $enviar = $this->sql_con->SelectLimit($consulta);


                
                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){

                    if($i==3){

                        $arreglo = array(
                            "nombre_hv"=>utf8_encode(trim($enviar->Fields("nombre_hv")))
                            );

                    }elseif($i==4){
                        $arreglo = array(
                            "nombre_opcts"=>utf8_encode(trim($enviar->Fields("nombre_opcts")))
                            );

                    }elseif($i==5){
                         
                        $arreglo = array(
                                "razon_social"=>utf8_encode(trim($enviar->Fields("razon_social")))
                                );
                    }else{

                        $arreglo = "";
                    }


                    $verificar_observacion = $this->verificar_si_observacion($enviar->Fields("id_cot"));


                    $a = array(

                            "cot"  => utf8_encode($enviar->Fields("id_cot")),
                            "cotdes"=> $enviar->Fields("id_cotdes"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "id_hotel" => $enviar->Fields("id_hotel"),
                            "pax"  => $enviar->Fields("cot_numpas"),
                            "desde"  => $enviar->Fields("desde"),
                            "hasta"  => $enviar->Fields("hasta"),
                            "dif"  => $enviar->Fields("dif"),
                            "id_seg"  => $enviar->Fields("id_seg"),
                            "operador" =>$operador,
                            "cliente"=>$i,
                            "verificar_obs"=>$verificar_observacion,
                            $arreglo

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }



      protected function verificar_si_observacion($id_cot){


            $consulta = "select * from obs_onrequest where id_cot = $id_cot ";
            $verificar = $this->sql_con->SelectLimit($consulta);

            if($verificar->RecordCount() > 0)
                $respuesta = 1;
            else
                $respuesta = 0;

            return $respuesta;

      }
    
   
    
    protected function traer_pax(){
        
        if(isset($this->datos_usuario["sitio"])){

            $consulta="
                            
                       select * from ".$this->datos_usuario["cliente"].".cotpas where id_cot = '".$this->datos_usuario["id_cot"]."'
            
                    ";
            $enviar = $this->sql_con->SelectLimit($consulta);


           $this->datos["pasajeros"] = array();
             

                while (!$enviar->EOF){
          
                    $a = array(
                      
                            "nombre"  => utf8_encode($enviar->Fields("cp_nombres")),
                            "apellido"  => utf8_encode($enviar->Fields("cp_apellidos"))
                        
                    
                        );

                        

                    array_push($this->datos["pasajeros"], $a);
                    
                    $enviar->MoveNext();


                }
            
            
        }
        
    }


    protected function traer_detalle_or(){

      $consulta = "SELECT 
                     DATE_FORMAT(cd_fecdesde,'%d-%m-%Y') AS desde,
                    DATE_FORMAT(cd_fechasta,'%d-%m-%Y') AS hasta,
                    cd.id_hotel as TheHotel,
                    cd.*,
                    c.*,
                    h.*,
                    th.*,
                    ho.*,
                    hd.*,
                    tt.* 
                          FROM
                            ".$this->datos_usuario["cliente"].".cotdes cd
                          LEFT JOIN ".$this->datos_usuario["cliente"].".ciudad c
                          ON cd.`id_ciudad` = c.`id_ciudad`
                          LEFT JOIN ".$this->datos_usuario["cliente"].".hotel h
                          ON cd.`id_hotel` = h.`id_hotel`
                          LEFT JOIN ".$this->datos_usuario["cliente"].".tipohabitacion th
                          ON cd.`id_tipohabitacion` = th.`id_tipohabitacion`
                          INNER JOIN ".$this->datos_usuario["cliente"].".hotocu ho
                            ON cd.`id_cot` = ho.`id_cot`
                            INNER JOIN ".$this->datos_usuario["cliente"].".hotdet hd
                            ON ho.`id_hotdet` = hd.`id_hotdet`
                            INNER JOIN ".$this->datos_usuario["cliente"].".tipotarifa tt
                            ON hd.`id_tipotarifa` = tt.`id_tipotarifa`        
                          WHERE cd.id_cot = ".$this->datos_usuario["id_cot"]."
                          and cd_estado = 0
                          AND cd.id_seg = 13 
                        
                 GROUP BY cd.id_cotdes";

        $enviar = $this->sql_con->SelectLimit($consulta);

        $this->datos["detalle_or"] = array();

        while (!$enviar->EOF){
          
                    $a = array(
                      
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono"  => trim($enviar->Fields("hot_fono")),
                            "desde" => $enviar->Fields("desde"),
                            "hasta" => $enviar->Fields("hasta"),
                            "hab1" => $enviar->Fields("cd_hab1"),
                            "hab2" => $enviar->Fields("cd_hab2"),
                            "hab3" => $enviar->Fields("cd_hab3"),
                            "hab4" => $enviar->Fields("cd_hab4"),
                            "va1" => $enviar->Fields("hd_sgl"),
                            "va2" => $enviar->Fields("hd_dbl")*2,
                            "va3" => $enviar->Fields("hd_dbl")*2,
                            "va4" => $enviar->Fields("hd_tpl")*3,
                            "va4" => $enviar->Fields("hd_tpl")*3,
                            "habitacion" => $enviar->Fields("th_nombre"),
                            "tarifa" => $enviar->Fields("tt_nombre")

                         
                    
                        );

                        

                    array_push($this->datos["detalle_or"], $a);
                    $this->traer_usuarios($enviar->Fields("TheHotel"));
                    
                    $enviar->MoveNext();


                }


       



    }


     protected function traer_usuarios($hotel){

        $consulta = "SELECT * FROM ".$this->datos_usuario["cliente"].".usuarios WHERE id_empresa =".$hotel."  AND usu_estado = 0";

        $enviar = $this->sql_con->SelectLimit($consulta);

        $this->datos["usuarios"] = array();

        while (!$enviar->EOF){
          
                    $b = array(
                      
                            "usuario"  => trim($enviar->Fields("usu_login")),
                            "pass"  => trim($enviar->Fields("usu_password")),
                            "mail" => trim($enviar->Fields("usu_mail"))

                    
                        );

                        

                    array_push($this->datos["usuarios"], $b);
                    
                    $enviar->MoveNext();


                }



    }        

    
    
    
    protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["id_hot"]);
        $this->mssql_escape($this->datos_usuario["cliente"]);
    }
    

   
    protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }
    
    
    function __destruct(){
         echo json_encode($this->datos);
        
    }
    

}

?>