<?php

$modificador_hotelesmerge = new ModificadorHotelesmerge();

class ModificadorHotelesmerge{
    
    private $datos,$datos_usuario = array();
    private $markup;

    public function __construct(){
        
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
        //include_once("secure.php");
        include('../../Connections/db1.php');
        $this->set_conectar($db1);
        $this->post();
        

        
    }
    
    protected function set_conectar($valor){
  
        $this->sql_con = $valor;
   }

    


   protected function post(){

      extract($_POST);

      $this->datos_usuario["tipo"] = $tipo;
      $this->datos_usuario["pk"] = $pk;
      $this->datos_usuario["valor"] = $valor;
      $this->datos_usuario["id"] = $id;
      $this->datos_usuario["id_cadena"] = $id_cadena;
      $this->traer($tipo);

   }
    

    
    public function traer($tipo){
        
        
        switch($tipo){
            
            
            case 1:
                 
              $this->traer_hoteles();
            
            break;

            case 2:
                 
              $this->traer_detalle_pk();
            
            break;

            case 3:
                 
              $this->actualizar_datos_hotelesmerge();
            
            break;


            case 4:
                 
              $this->pasar_global();
            
            break;


            case 5:
                 
              $this->quitar_global();
            
            break;

            case 6:
                 
              $this->agregar_pestana_normal();
            
            break;

            case 7:
                 
              $this->quitar_pestana_normal();
            
            break;


            case 8:
                 
              $this->traer_cadenas();
            
            break;

            case 9:
                 
              $this->traer_detalle_cadena();

            break;

            case 10:
                 
              $this->actualizar_datos_cadena();

            break;


            
        }
        
   
        
    }


    protected function traer_cadenas(){


      $consulta = "select * from hoteles.cadena";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["cadenas"] = array();

      while(!$traer->EOF){


             $datos = array(
                    
                    "nombre_cadena" => utf8_encode($traer->Fields("nom_cadena")),
                    "id_cadena"=>$traer->Fields("id_cadena")

                  );            


            array_push($this->datos["cadenas"], $datos);

        

        $traer->MoveNext();
      }


    }


    protected function quitar_pestana_normal(){

      $actualizar = "update hoteles.hotelesmerge set mira = 1 where id_pk = '".$this->datos_usuario["pk"]."' ";
      $act = $this->sql_con->Execute($actualizar);



        if($act)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function agregar_pestana_normal(){

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' and mira = 0 ";
      $revisar = $this->sql_con->SelectLimit($consulta);

      if($revisar->RecordCount() > 0)
          $this->datos["respuesta"] = 2;
      else{

          $actualizar = "update hoteles.hotelesmerge set mira = 0 where id_pk = '".$this->datos_usuario["pk"]."' ";
          $act = $this->sql_con->Execute($actualizar);

        

            if($act)
              $this->datos["respuesta"] = 1;
            else
              $this->datos["respuesta"] = 0;

      }

    }


    protected function quitar_global(){

      $actualizar = "update hoteles.hotelesmerge set ver = 1 where id_pk = '".$this->datos_usuario["pk"]."' ";
      $act = $this->sql_con->Execute($actualizar);

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' ";
      $revisar = $this->sql_con->SelectLimit($consulta);

       $goblal_hotelbeds = "UPDATE
                              distantis.hotelbeds_merge_hoteles hmh 
                              JOIN distantis.hotelbeds_merge_tarifas hmt 
                                ON hmt.id_tarifa_hb = hmh.id_hotel_hb 
                              SET hmh.usa_global = 0, hmt.usa_global = 0  
                            WHERE id_hotel_cts = ".$revisar->FIelds("id_hotel_cts")."
                            ";

        $this->sql_con->Execute($goblal_hotelbeds);

        if($act)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function pasar_global(){

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' and ver = 0 ";
      $revisar = $this->sql_con->SelectLimit($consulta);

      if($revisar->RecordCount() > 0)
          $this->datos["respuesta"] = 2;
      else{

          $actualizar = "update hoteles.hotelesmerge set ver = 0 where id_pk = '".$this->datos_usuario["pk"]."' ";
          $act = $this->sql_con->Execute($actualizar);

          $goblal_hotelbeds = "UPDATE
                              distantis.hotelbeds_merge_hoteles hmh 
                              JOIN distantis.hotelbeds_merge_tarifas hmt 
                                ON hmt.id_tarifa_hb = hmh.id_hotel_hb 
                              SET hmh.usa_global = 1, hmt.usa_global = 1  
                            WHERE id_hotel_cts = ".$revisar->FIelds("id_hotel_cts")."
                            ";

          $this->sql_con->Execute($goblal_hotelbeds);

            if($act)
              $this->datos["respuesta"] = 1;
            else
              $this->datos["respuesta"] = 0;

      }

    }


    protected function actualizar_datos_hotelesmerge(){


       if($this->datos_usuario["id"] != "cadena"){

           $actualizar = "update hoteles.hotelesmerge set 
                              id_hotel_".$this->datos_usuario["id"]." = ".$this->datos_usuario["valor"]." 
                              where id_pk = '".$this->datos_usuario["pk"]."' 
                         ";
      
           $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          

      }else{

         $actualizar = "update hoteles.hotelesmerge set id_cadena = ".$this->datos_usuario["valor"]." 
                              where id_pk = '".$this->datos_usuario["pk"]."' 
                         ";

         $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          
        

      }




    }


     protected function actualizar_datos_cadena(){


       if($this->datos_usuario["id"] != "nom_cadena"){

           $actualizar = "update hoteles.cadena set modo = ".$this->datos_usuario["valor"]." 
                              where id_cadena = '".$this->datos_usuario["id_cadena"]."' 
                         ";

           $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          

      }else{

         $actualizar = "update hoteles.cadena set nom_cadena = '".$this->datos_usuario["valor"]."'
                              where id_cadena = '".$this->datos_usuario["id_cadena"]."' 
                         ";


         $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          
        

      }




    }


    protected function traer_detalle_pk(){


      $consulta = "select * from hoteles.hotelesmerge where id_pk =  '".$this->datos_usuario["pk"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["detalle"] = array();

      while(!$traer->EOF){


        $datos = array(
                          "cts" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "turavion" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travel" => $traer->Fields("id_hotel_travelclub"),
                          "cadena" =>$traer->Fields("id_cadena")
                  );


        array_push($this->datos["detalle"], $datos);


        $traer->MoveNext();



      }



    }

    protected function traer_detalle_cadena(){


      $consulta = "select * from hoteles.cadena where id_cadena =  '".$this->datos_usuario["id_cadena"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["detalle_cadena"] = array();

      while(!$traer->EOF){


        $datos = array(
                          "nom_cadena" => utf8_encode($traer->Fields("nom_cadena")),
                          "modo" => $traer->Fields("modo")
                  );


        array_push($this->datos["detalle_cadena"], $datos);


        $traer->MoveNext();



      }



    }

    protected function traer_hoteles(){


      $consulta = "select * from hoteles.hotelesmerge";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hoteles"] = array();

      while(!$traer->EOF){

      $hoteles = array(
                          "distantis" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travelclub" => $traer->Fields("id_hotel_travelclub")
                  );


        $nombre_hotel = $this->nombre_hotel($hoteles);



        if($nombre_hotel != "NADA"){

             $datos = array(
                    
                    "nombre_hotel" => utf8_encode($nombre_hotel),
                    "id_pk"=>$traer->Fields("id_pk")

                  );            


            array_push($this->datos["hoteles"], $datos);

        }


        $traer->MoveNext();
      }


    }


    protected function nombre_hotel($hoteles){
        
        $bd = "";
        $id_hotel = "";
        $revisar = 0;
        foreach($hoteles as $key=>$datos){
         
            if($datos != 0 and $revisar == 0){
              $bd = $key;
              $id_hotel = $datos;
              $revisar = 1;
            }

        }


       if($bd!= "" and $id_hotel != ""){
        
         $consulta = "select hot_nombre from $bd.hotel where id_hotel = $id_hotel ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $nombre_hotel = $traer->Fields("hot_nombre");

       }else
          $nombre_hotel = "NADA";


       return $nombre_hotel;
       
    }




   

    function __destruct(){
         echo json_encode($this->datos); 
    }
    


}


?>