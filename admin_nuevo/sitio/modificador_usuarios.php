<?php

$modificador_usuarios = new ModificadorUsuarios();

class ModificadorUsuarios{
    
    private $datos,$datos_usuario = array();
    private $markup;

    public function __construct(){
        
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
       
        include('../../Connections/db1.php');
        $this->set_conectar($db1);
        $this->post();
        

        
    }
    
    protected function set_conectar($valor){
  
        $this->sql_con = $valor;
   }

    


   protected function post(){

      extract($_POST);

      $this->datos_usuario["tipo"] = $tipo;
      $this->datos_usuario["id_hotel"] = $id_hotel;
      $this->datos_usuario["bd"] = $bd;
      $this->datos_usuario["pk"] = $pk;
      $this->datos_usuario["cliente"] = $cliente;
      $this->datos_usuario["id_empresa"] = $id_empresa;
      $this->datos_usuario["login"] = $login;
      $this->datos_usuario["pass"] = $pass;
      $this->datos_usuario["nombre"] = $nombre;
      $this->datos_usuario["pat"] = $pat;
      $this->datos_usuario["mat"] = $mat;
      $this->datos_usuario["idioma"] = $idioma;
      $this->datos_usuario["mail"] = $mail;
      $this->datos_usuario["id_usuario"] = $id_usuario;
      $this->datos_usuario["nombre"] = $nombre;

      $this->traer($tipo);

   }
    

    
    public function traer($tipo){
        
        
        switch($tipo){
            
            
            case 1:
                 
              $this->traer_hoteles();
            
            break;


            case 2:
                 
              $this->traer_detalle_usuarios();
            
            break;

            case 3:
                 
              $this->replicar_usuario();
            
            break;

            case 4:
                 
              $this->editar_usuario();
            
            break;

            case 5:
                 
              $this->eliminar_usuario();
            
            break;


            
        }
        
   
        
    }


     protected function eliminar_usuario(){


        $delete = "update ".$this->datos_usuario["cliente"].".usuarios set 
                  usu_estado = 1
                  where id_usuario = ".$this->datos_usuario["id_usuario"]." ";
        
        $eliminar = $this->sql_con->Execute($delete);


        if($eliminar)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function editar_usuario(){


        $act = "update ".$this->datos_usuario["cliente"].".usuarios set
                  usu_nombre = '".$this->datos_usuario["nombre"]."',
                  usu_login = '".$this->datos_usuario["login"]."',
                  usu_password = '".$this->datos_usuario["pass"]."', 
                  usu_mail = '".$this->datos_usuario["mail"]."',
                  usu_pat= '".$this->datos_usuario["pat"]."' 
                  where id_usuario = ".$this->datos_usuario["id_usuario"]." ";
        
        $actualizar = $this->sql_con->Execute($act);


        if($actualizar)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function replicar_usuario(){


       $con = "select * from hoteles.clientes where id_cliente = ".$this->datos_usuario["cliente"]." ";
       $cliente = $this->sql_con->SelectLimit($con);

       $bd = $cliente->Fields("bd");
       $nombre_cliente = $cliente->Fields("nombre");

       $consulta = "select * from $bd.usuarios where usu_login = '".$this->datos_usuario["login"]."' 
                      and usu_password = '".$this->datos_usuario["pass"]."'
                      and usu_mail = '".$this->datos_usuario["mail"]."'
                      and usu_estado = 0
                    ";
       
       $verificar = $this->sql_con->SelectLimit($consulta);

       if($verificar->RecordCount() == 0){


         $con = "select id_hotel_$nombre_cliente as hotel from hoteles.hotelesmerge where id_pk = ".$this->datos_usuario["pk"]." ";
         $ver = $this->sql_con->SelectLimit($con);

         $hotel = $ver->Fields("hotel");

             if($hotel != 0){

                  $inser = "insert into $bd.usuarios (id_empresa,usu_login,usu_password,usu_nombre,usu_pat,usu_mat,id_tipo,usu_idioma,usu_mail,usu_enviar_correo)
                            values ('$hotel',
                                    '".$this->datos_usuario["login"]."',
                                    '".$this->datos_usuario["pass"]."',
                                    '".$this->datos_usuario["nombre"]."',
                                    '".$this->datos_usuario["pat"]."',
                                    '".$this->datos_usuario["mat"]."',
                                    '2',
                                    '".$this->datos_usuario["idioma"]."',
                                    '".$this->datos_usuario["mail"]."',
                                    1
                                    )
                            ";

                  $insertar = $this->sql_con->Execute($inser);

                  if($insertar){

                      $this->datos["respuesta"] = 1;
                      $this->datos["operador"] = $nombre_cliente;

                  }else
                      $this->datos["respuesta"] = 0;

            }else
               $this->datos["respuesta"] = 3;



       }else{
           $this->datos["operador"] = $nombre_cliente;
           $this->datos["respuesta"] = 2;
        }


    }


    protected function traer_detalle_usuarios(){


      $consulta = "select * from hoteles.hotelesmerge where id_pk =  '".$this->datos_usuario["pk"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["usuarios"] = array();

      while(!$traer->EOF){


          $user = array(
                            "distantis" => $traer->Fields("id_hotel_cts"),
                            "veci" => $traer->Fields("id_hotel_veci"),
                            "otsi" => $traer->Fields("id_hotel_otsi"),
                            "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                            "cocha" => $traer->Fields("id_hotel_cocha"),
                            "travelclub" => $traer->Fields("id_hotel_travelclub")
                    );


        $this->buscar_usuarios($user);


        $traer->MoveNext();



      }



      $unicos = array_unique($this->datos_usuario["usuarios"], SORT_REGULAR);


        foreach($unicos as $datos){


            array_push($this->datos["usuarios"], $datos);
          
        }




    }


    protected function buscar_usuarios($usuarios){

      $this->datos_usuario["usuarios"] = array();
      foreach($usuarios as $key=>$datos){
         
            if($datos != 0){

              
               $consulta = "select * from $key.usuarios where id_empresa = $datos and usu_estado = 0 and id_tipo  = 2";
               $traer = $this->sql_con->SelectLimit($consulta);

               while(!$traer->EOF){

                 $nombre = "";
                 $app = "";
                 $apm = "";
                 $mail = "";

                 if($traer->Fields("usu_nombre") == "null" or $traer->Fields("usu_nombre") == null or $traer->Fields("usu_nombre") == "")
                      $nombre = "";
                 else
                      $nombre = $traer->Fields("usu_nombre");

                 if($traer->Fields("usu_pat") == "null" or $traer->Fields("usu_pat") == null or $traer->Fields("usu_pat") == "")
                      $app = "";
                 else
                      $app = $traer->Fields("usu_pat");

                 if($traer->Fields("usu_mat") == "null" or $traer->Fields("usu_mat") == null or $traer->Fields("usu_mat") == "")
                      $apm = "";
                 else
                      $apm = $traer->Fields("usu_mat");

                 if($traer->Fields("usu_mail") == "null" or $traer->Fields("usu_mail") == null or $traer->Fields("usu_mail") == "")
                      $mail = "";
                 else
                      $mail = $traer->Fields("usu_mail");


                 if($traer->Fields("usu_enviar_correo") == 0)
                      $enviar_correo = 1;
                 else
                      $enviar_correo = 0;   


                  $datos = array(
                                  "nombre"=>utf8_encode($traer->Fields("usu_nombre")." ".$traer->Fields("usu_pat")),
                                  "login"=>trim(utf8_encode($traer->Fields("usu_login"))),
                                  "pass"=>trim($traer->Fields("usu_password")),
                                  "mail"=>trim($mail),
                                  "cliente"=>$key,
                                  "id_empresa"=>$traer->Fields("id_empresa"),
                                  "nombre"=>utf8_encode($nombre),
                                  "pat"=>utf8_encode($app),
                                  "mat"=>utf8_encode($apm),
                                  "idioma"=>$traer->Fields("usu_idioma"),
                                  "id_usuario"=>$traer->Fields("id_usuario"),
                                  "correo"=>$enviar_correo
                                );

                  array_push($this->datos_usuario["usuarios"], $datos);

                  $traer->MoveNext();

               }



               

            }

        }



    }


   

    protected function traer_hoteles(){


      $consulta = "select * from hoteles.hotelesmerge";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hoteles"] = array();

      while(!$traer->EOF){

      $hoteles = array(
                          "distantis" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travelclub" => $traer->Fields("id_hotel_travelclub")
                  );


        $nombre_hotel = $this->nombre_hotel($hoteles);



        if($nombre_hotel != "NADA"){

             $datos = array(
                    
                    "nombre_hotel" => utf8_encode($nombre_hotel),
                    "id_pk"=>$traer->Fields("id_pk"),
                    "bd" =>$this->datos_usuario["bd"],
                    "id_hotel"=>$this->datos_usuario["id_hotel"]

                  );            


            array_push($this->datos["hoteles"], $datos);

        }


        $traer->MoveNext();
      }


    }


    protected function nombre_hotel($hoteles){
        
        $bd = "";
        $id_hotel = "";
        $revisar = 0;
        foreach($hoteles as $key=>$datos){
         
            if($datos != 0 and $revisar == 0){
              $bd = $key;
              $id_hotel = $datos;
              $revisar = 1;
            }

        }


       if($bd!= "" and $id_hotel != ""){
        
         $consulta = "select hot_nombre from $bd.hotel where id_hotel = $id_hotel ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $nombre_hotel = $traer->Fields("hot_nombre");

         $this->datos_usuario["bd"] = $bd;
         $this->datos_usuario["id_hotel"] = $id_hotel;

       }else
          $nombre_hotel = "NADA";


       return $nombre_hotel;
       
    }




   

    function __destruct(){
         echo json_encode($this->datos); 
    }
    


}


?>