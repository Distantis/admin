<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DISTANTIS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


  <!-- CSS ANTIGUO ADMIN -->
  <link href="prueba/css/jquery.gritter.css" rel="stylesheet">
  <link href="prueba/css/style_gritter.css"  rel="stylesheet">
  <link href="../ws/pnotify/pnotify.core.css" rel="stylesheet" type="text/css" />
  <link href="../ws/pnotify/pnotify.buttons.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="prueba/css/jquery.mCustomScrollbar.css" type="text/css" />
  <link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
  <link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
  <link href="prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
  <link href="prueba/css/stacktable.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet">

  


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DISTANTIS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="dist/img/logo-distantis.png" width="85px;" height="50px;" /></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <?php include_once("menu_derecho_superior.php");?>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php include_once("menu_izquierdo.php");?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
         	
         	<div class="alert alert-warning alert-dismissable overlay"><center><strong>Editor Usuarios</strong></center></div>
         	<br>

         	  <div class="col-md-12">
                <div class="col-md-12 table-responsive">
						       
                   <select id="hoteles" name="hotel" class="form-control select2" style="width:50%;"></select>
                       
           
                </div>

                <br><br>


                <div class="col-md-12">
                      
                        <div class="panel panel-success" id="mostrar" style="display:none;">
                               <div class="panel-heading" ><center><b>Detalle usuarios</b><i class="fa fa-spinner fa-spin pull-right cargando"></i></center>

                           </div>
                      

                            <div class="panel-body">

                                <div class='col-md-12'>
                                        <div id="respuesta"></div>
                                        <br><br>
                                 </div>

                            </div>
                       </div>

                </div>


            </div>

            <input type="hidden" id="hotel">
            <input type="hidden" id="bd">
            <input type="hidden" id="pk">

        </section>
      
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("footer.php");?>
  <?php //include_once("sidebar.php");?>

</div>


<!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="mostrar-informacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Operadores</center></h3><br>
                                <small>Replicar correos para los siguientes operadores.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                               
                                <br><br>
            
                                        <div class="row" style="height:70% !important;">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <div class='responsive'>

                                                        <input type="hidden" id="id_empresa">
                                                        <input type="hidden" id="usu_login">
                                                        <input type="hidden" id="usu_password">
                                                        <input type="hidden" id="usu_nombre">
                                                        <input type="hidden" id="usu_pat">
                                                        <input type="hidden" id="usu_mat">
                                                        <input type="hidden" id="usu_idioma">
                                                        <input type="hidden" id="usu_mail">

                                                      
                                                      CTS <input type="checkbox" class="op" id="cts" value="1">
                                                      OTSI <input type="checkbox" class="op" id="otsi" value="2">
                                                      VECI <input type="checkbox" class="op" id="veci" value="3">
                                                      TURAVION <input type="checkbox" class="op" id="turavion" value="4">
                                                      COCHA <input type="checkbox" class="op" id="cocha" value="5">
                                                      TRAVEL <input type="checkbox" class="op" id="travel" value="6"><br><br><br>

                                                      <center><button class="btn btn-success pull-right" id="guardar_replicar">Guardar Información</button></center>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <br><br>
                                      

                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>




<!-- jQuery 2.2.3 -->
<script src="dist/js/jquery-1.9.1.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="dist/js/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="dist/js/raphael-min.js"></script>
<!--script src="plugins/morris/morris.min.js"></script-->
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="dist/js/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<!-- ADMIN ANTIGUO -->

<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
<script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script type="text/javascript" src="../ws/pnotify/pnotify.core.js"></script>
<script type="text/javascript" src="../ws/pnotify/pnotify.buttons.js"></script>
<script type="text/javascript" src="prueba/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="prueba/js/jquery.datatables.sorting-plugins.js"></script>
<script src="prueba/js/blockui.min.js" type="text/javascript"></script>
<script src="prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>

<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>



<script src="prueba/js/stacktable.js"></script>
<script src="prueba/js/editor_usuarios.js" type="text/javascript"></script>
<script src="prueba/js/prueba.js"></script>
 

<script src="prueba/js/login.js" type="text/javascript"></script>
<script type="text/javascript" src="../ws/pnotify/pnotify.core.js"></script>
<script type="text/javascript" src="../ws/pnotify/pnotify.buttons.js"></script>
<script src="prueba/js/tiempo_real.js" type="text/javascript"></script>




  
</body>
</html>
