
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Hotelería 2.0</title>
		<!--meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"-->
        <link href="prueba/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
        <link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="prueba/css/jquery.gritter.css" rel="stylesheet">
        <link href="prueba/css/style_gritter.css"  rel="stylesheet">
        <link href="prueba/css/datepicker.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
        <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
		<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
        <link href="prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
        <link rel="stylesheet" href="prueba/css/jquery.mCustomScrollbar.css" type="text/css" />
        <link rel="stylesheet" href="prueba/css/protip.min.css">
        
       
      </head>
    <body>


    <div class="container">

             <div class="row">
        
               
               
                   <div class="row"><br><br></div>
                             
                    <div align='center' class="row">

                        <div class="col-md-12">


                            <div class="col-md-3">
                                <select id="hoteles" name="hoteles" class="form-control select2 pull-left desactivados" ></select>
                            </div>
                          
                        </div>


                        <div class="row">
                        
                             <div class="col-md-12 centered">
                              <br><br><br>


                                <div class="col-md-10 col-md-offset-1">

                                        <div class="col-md-6">
                                            <div class="alert alert-warning alert-dismissable"><small class='pull-left'><strong>Seleccione habitación donde el hotel actualizará el valor</strong></small><br></div>


                                             <div class="col-md-2">
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading"><strong><center><small>Twin</small></center></strong></div>
                        
                                                    <div class="panel-body"><center><input type="checkbox" class="habitaciones" id="twn" value='0'></center></div>

                                                </div>
                                            </div>


                                             <div class="col-md-2">
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading"><strong><center><small>Matri.</small></center></strong></div>
                        
                                                    <div class="panel-body"><center><input type="checkbox" class="habitaciones" id="dbl" value='0'></center></div>

                                                </div>
                                            </div>

                                            <div class="col-md-2 pull-right">
                                                <button id="guardar" class="btn btn-success pull-right">Guardar</button>
                                            </div>  

                                        </div> 



                                </div>

                             </div>

                        </div>


                    </div>
   
            </div>


        </div>

        <script src="prueba/js/jquery.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
        <script src="prueba/js/notificaciones.js" type="text/javascript"></script>
        <script src="prueba/js/jquery.numeric.js"></script>
        <script src="prueba/js/protip.min.js"></script>
        <script type="text/javascript" src="prueba/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="prueba/js/dobles.js"></script>
        <script src="prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
        <script src="prueba/js/bootstrap-datepicker.js" type="text/javascript"></script>
        
    </body>
</html>