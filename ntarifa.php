<?php	 	  
include ("secure.php");  
require_once ('includes/mailing.php');
require_once('Connections/db1.php');
$clientes_query = "select * from clientes where estado = 0";
$clientes=$db1->Execute($clientes_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total = $clientes->RecordCount();


$query_cadenas = "SELECT 
					  * 
					FROM
					  (SELECT 
						* 
					  FROM
						cadena 
					  WHERE cad_estado = 0 
					  ORDER BY nom_cadena ASC) tb1 
					  INNER JOIN (SELECT 
					  IFNULL(hm.`id_hotel_cts`, 0) AS id_hotel_cts,
					  hcts.`hot_nombre` AS cts_nom,
					  IFNULL(hm.`id_hotel_otsi`, 0) AS id_hotel_otsi,
					  hotsi.`hot_nombre` AS otsi_nom,
					  IFNULL(hm.`id_hotel_veci`, 0) AS id_hotel_veci,
					  hveci.`hot_nombre` AS veci_nom,
					  IFNULL(hm.`id_hotel_turavion`, 0) AS id_hotel_turavion,
					  hturavion.`hot_nombre` AS turavion_nom,
					  IFNULL(hm.`id_hotel_cocha`, 0) AS id_hotel_cocha,
					  hcocha.`hot_nombre` AS cocha_nom,
					  IFNULL(hm.`id_hotel_travelclub`, 0) AS id_hotel_travelclub,
				      htravelclub.`hot_nombre` AS travelclub_nom,
					  hm.`id_cadena`,
					  hm.id_pk 
					FROM
					  hotelesmerge hm 
					  LEFT JOIN distantis.hotel hcts 
						ON hm.`id_hotel_cts` = hcts.`id_hotel` 
					  LEFT JOIN otsi.hotel hotsi 
						ON hm.`id_hotel_otsi` = hotsi.`id_hotel` 
					  LEFT JOIN veci.hotel hveci 
						ON hm.`id_hotel_veci` = hveci.`id_hotel` 
					  LEFT JOIN touravion_dev.hotel hturavion 
						ON hm.`id_hotel_turavion` = hturavion.`id_hotel`
					LEFT JOIN cocha.hotel hcocha 
						ON hm.`id_hotel_cocha` = hcocha.`id_hotel`
						 LEFT JOIN travelclub.hotel htravelclub 
                                                ON hm.`id_hotel_travelclub` = htravelclub.`id_hotel`
						) tb2 
						ON tb1.id_cadena = tb2.id_cadena
						order by nom_cadena";
//echo $query_cadenas;
$cadenas=$db1->Execute($query_cadenas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_th = "SELECT * FROM tipohabitacion WHERE th_estado=0 ORDER BY th_nombre";
$th=$db1->Execute($query_th) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$fecha = date('d-m-Y');
	$nueva = strtotime('+7 day' , strtotime($fecha));
	$hasta = date('d-m-Y', $nueva);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<style>th {
text-align: center;
}
td {
	text-align: center;
}
</Style>
	<script>
			$(function(){
			$("#desde").datepicker({
				dateFormat: 'dd-mm-yy'
			});
			$("#hasta").datepicker({
				dateFormat: 'dd-mm-yy'
			});
		});		
			
		function crear(){
			
			var clientes = new Array();
				//recorremos todos los checkbox seleccionados con .each
				$('input[name="clientes[]"]:checked').each(function() {
				//$(this).val() es el valor del checkbox correspondiente
				clientes.push($(this).val());
				});
				
			
			alert(clientes);
			var desde = $("#desde").val();
			var hasta = $("#hasta").val();
			var cadena = $("#cadena").val();
			var habitacion = $("#habitacion").val();
			//alert(desde);
			//alert(hasta);
			//alert(cadena);
			//alert(habitacion);
			$.ajax({
					type: 'POST',
					url: 'solicitar_ntarifa.php',
					//async: false,
					data: {
						flag: "agregar",
						des: desde,
						has: hasta,
						cad: cadena,
						hab: habitacion,
						cliente: clientes
					},
					success:function(result){
					//$("#calendario").html(result);
						//alert(result);
						alert(result);
					},
					error:function(){
						alert("Error 1!!")
					}
			    });
			
			
			}	
		</script>
	</head>



<body onLoad="document.form.txt_numpas.focus();">

    <div id="container" class="inner">

        <div id="header">
			
			
		<?php	  include ("bienvenida.php");?>



            <ul id="nav">

                <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
								
		   </ul>

            <ol id="pasos">
            	
            </ol>													    

        </div>

        <!-- INICIO Contenidos principales-->

        <div style="margin-left:auto;margin-right:auto;width: 1024px;">
			<table>
				<tr>
					<?
					while (!$clientes->EOF){
						echo "<th>".$clientes -> Fields('nombre')."<input type='checkbox' name='clientes[]' value='".$clientes -> Fields('id_cliente')."' checked></th>";
					$clientes -> MoveNext();
						} ?>
				</tr>
			</table>
			<table>
				<tr>
					<th colspan="4">
						Nueva Tarifa Global
					</th>
				</tr>
				<tr>
					<th>Hotel</th>
					<td>
					<select id="cadena">
						<option value ="0">Selecciona un Hotel</option>
						<?
						while (!$cadenas->EOF){

							if($cadenas -> Fields('cts_nom')!=""){
							$hotel_nom = $cadenas -> Fields('cts_nom');
							}else{
							if($cadenas -> Fields('otsi_nom')!=""){
							$hotel_nom = $cadenas -> Fields('otsi_nom');
							}else{
							if($cadenas -> Fields('veci_nom')!=""){
							$hotel_nom = $cadenas -> Fields('veci_nom');
							}else{
							if($cadenas -> Fields('turavion_nom')!=""){
							$hotel_nom = $cadenas -> Fields('turavion_nom');
							}else{
							if($cadenas -> Fields('cocha_nom')!=""){
							$hotel_nom = $cadenas -> Fields('cocha_nom');
							}else{
							if($cadenas -> Fields('travelclub_nom')!=""){
							$hotel_nom = $cadenas -> Fields('travelclub_nom');
							}	
							}
							}
							}
							}
							}						
						?>
						<option value="<?=$cadenas -> Fields('id_pk')?>"><?=$cadenas -> Fields('nom_cadena')."-".$hotel_nom?></option>
						<?
						$cadenas -> MoveNext();
						} 
						?>
					</select>
					</td>
					<th>Tipo Habitación</th>
					<td><select id="habitacion">
						<option value ="0">Selecciona una Habitación</option>
						<?
						while (!$th->EOF){ 
						?>
						<option value="<?=$th -> Fields('id_tipohabitacion')?>"><?=$th -> Fields('th_nombre')?></option>
						<? $th -> MoveNext();
						} 
						?>
					</select></td>
				</tr>
				<tr>
					<th>Desde</th>
					<td><input type="text" value="<?php	echo $fecha;?>" id="desde"></td>
					<th>Hasta</th>
					<td><input type="text" value="<?php	echo $hasta;?>" id="hasta"></td>
				</tr>
				<tr>
					<th colspan="4"><button onclick="crear()">Crear</button></th>
				</tr>
			</table>
		
        

        </div>

        <!-- FIN Contenidos principales -->





        

    

    </div>

    <!-- FIN container -->

</body>

</html>

