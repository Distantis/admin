
<!DOCTYPE html>
<html>
<head>
	<title>CONFIRMACIONES OR</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--meta content="60" http-equiv="REFRESH"> </meta-->
	<link href="prueba/css/bootstrap.css" rel="stylesheet">
	<link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="prueba/css/jquery.gritter.css" rel="stylesheet">
	<link href="prueba/css/style_gritter.css"  rel="stylesheet">
	<link href="prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
    <link rel="stylesheet" href="prueba/css/protip.min.css">
</head>
<body>

<br><br>
<div class="col-md-12">

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3">
					<select id="hoteles" name="hotel" class="form-control select2 hoteles" ></select>
			</div>

			<div class="col-md-3">
					<select id="set_competitivo" name="hotel" multiple class="form-control select2 hoteles oculto" style='display:none;' ></select>
			</div>

			<div class="col-md-1">
					<button class="btn btn-success oculto" style='display:none;' id="buscar_set">Buscar set competitivo</button>
			</div>

		</div>

	</div>

</div>

<div class="col-md-12">
	<div class="row">
	<br>
	              <div class="col-md-12">
		              	<div class="col-md-10"> 
				               <div class="panel panel-success" id="mostrar" style="display:none;">
				                   <div class="panel-heading" ><center><b>Detalle </b><i class="fa fa-spinner fa-spin pull-right cargando"></i></center>

				                   </div>
				  				<div class="panel-body">
				  					<div class='col-md-12'>
				                		<div id="respuesta" class='table-responsive'></div>
				                		<br><br>


				                		<div class="panel panel-warning">
							                   <div class="panel-heading" ><center><b>TOTAL AÑO Y MES ANTERIOR</center>

							                   </div>
							  				<div class="panel-body">
							  				   <div class='col-md-12'>
							                		<div class='col-md-5'>
							                			<div id="anual"></div>
							                		</div>

							                		<div class='col-md-5'>
							                			<div id="mes_anterior"></div>
							                		</div>
							                	</div>
						                	</div>

						              	 </div>

				                		<br><br>
				                	</div>

				                	<div class="row"></div>

				                	
				                	<div class="panel panel-warning competitivo" style="display:none;">
							                   <div class="panel-heading" ><center><b>TOTAL AÑO Y MES ANTERIOR SET COMPETITIVO</center>

							                   </div>
							  				<div class="panel-body">
							  				   <div class='col-md-12'>
							                		<br><br>	
							                		<div class='col-md-5'>
							                			<div id="anual_set"></div>
							                		</div>

							                		<div class='col-md-5'>
							                			<div id="mes_anterior_set"></div>
							                		</div>


							                		<br><br>
							                	</div>

						                	</div>
						                	
						              	 </div>

						              	 <br><br>
						              	 <div id="respuesta2" class='table-responsive'></div>
				                		
				                </div>
				        	</div>



			        </div>
	            </div>
	  </div>     
</div> 


<input type="hidden" id="hotel">
<input type="hidden" id="bd">
<input type="hidden" id="pk">



<input type="hidden" id="total_or">
<input type="hidden" id="total_confirmaciones">
<input type="hidden" id="total_or_anterior">
<input type="hidden" id="total_confirmaciones_anterior">

<script src="prueba/js/jquery.js" type="text/javascript"></script>
<script src="prueba/js/protip.min.js"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
<script src="prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>
<script src="prueba/js/jquery.numeric.js" type="text/javascript"></script>
<script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
<script src="code/highcharts.js"></script>
<script src="code/modules/exporting.js"></script>
<script>
		
		$(document).on("ready", function(){

			$.protip();

			$.post("modificador_usuarios.php", {
                        verificando_sitio: 1, tipo:1
                    },
                    function (data) {

                        data = $.parseJSON(data);
                        var hotel = '<option value=\'\'></option>';

                        $.each(data.hoteles, function (i, datos) {

                          hotel += '<option  value="' + datos.id_pk + '" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">(' + datos.id_pk + ') ' + datos.nombre_hotel + '</option>';

                        });

                      
                        $(".hoteles").html(hotel);


                    }).done(function () {

              
                      $('select#hoteles').select2({

                            placeholder: "Seleccione Hotel"

                      });

                 });


		});



		 $("#hoteles").on("change", function(){

		 			$("#respuesta").html("");
		 			$(".cargando").addClass("fa fa-spinner fa-spin");
		 			

        			var valor = $(this).find(':selected').attr("data-hotel");
        			var bd = $(this).find(':selected').attr("data-id");
        			var pk = $(this).val();

        			$("#hotel").val(valor);
        			$("#bd").val(bd);
        			$("#pk").val(pk);

        			traer_confirmaciones_or(pk);

    		});


		 $("#buscar_set").on("click", function(){

		 			
        			var valor = $("#set_competitivo").val();

        			$.post("modificador_usuarios.php", {
                        verificando_sitio: 1, tipo:11, set_competitivo : valor
                    },
                    function (data) {

                    	data = $.parseJSON(data);

                    	var total_or = $("#total_or").val();
                        var total_confirmaciones = $("#total_confirmaciones").val();
                        var total_or_anterior = $("#total_or_anterior").val();
                        var total_confirmaciones_anterior = $("#total_confirmaciones_anterior").val();

                        //console.log(total_or,total_confirmaciones,total_or_anterior,total_confirmaciones_anterior);
                        //console.log("===================== BD ===================================");

                        var total_or_set = data.total_or_set;
                        var total_confirmaciones_set = data.total_confirmaciones_set;
                        var total_or_anterior_set = data.total_or_anterior_set;
                        var total_confirmaciones_anterior_set = data.total_confirmaciones_anterior_set;

                        //console.log(total_or_set,total_confirmaciones_set,total_or_anterior_set,total_confirmaciones_anterior_set);
                        //console.log("===================== TTOAL ===================================");

                        var final_total_or = parseInt(total_or) + parseInt(total_or_set);
                        var final_total_confirmaciones = parseInt(total_confirmaciones) + parseInt(total_confirmaciones_set);
                        var final_total_or_anterior = parseInt(total_or_anterior) + parseInt(total_or_anterior_set);
                        var final_total_confirmaciones_anterior = parseInt(total_confirmaciones_anterior) + parseInt(total_confirmaciones_anterior_set);

                        //console.log(final_total_or,final_total_confirmaciones,final_total_or_anterior,final_total_confirmaciones_anterior);



                          var tabla ='<table class="table table-bordered table-striped" id="tabla_or_conf_2" width="100%"><thead>'
                            +'<tr>'
                            	+'<th align="center">PK</th>'
                                +'<th align="center">Total OR</th>'
                                +'<th align="center">Total Confirmaciones</th>'
                                +'<th align="center">Total OR mes anterior</th>'
                                +'<th align="center">Total Confirmaciones mes anterior</th>'
                            +'</tr></thead>'
                            +'<tbody>';

                            $.each(data.informacion, function(i,datos){

		                            tabla += "<tr>"
		                            			+"<td align='center'>"+datos.pk+"</td>"
			                                    +"<td align='center'>"+datos.total_or+"</td>"
			                                    +"<td align='center'>"+datos.total_confirmaciones+"</td>"
			                                    +"<td align='center'>"+datos.total_or_anterior+"</td>"
			                                    +"<td align='center'>"+datos.total_confirmaciones_anterior+"</td>"
			                                +"</tr>"

			                });

	                        tabla += '</tbody></table>';
                            $("#respuesta2").html(tabla);




                        armar_grafico(3,total_or_set,total_confirmaciones_set,null,null,"anual_set");  
                        armar_grafico(4,null,null,total_or_anterior_set,total_confirmaciones_anterior_set,"mes_anterior_set");  


                   	}).done(function(){
                   		$(".competitivo").css('display','block');		
                   	});


    		});



		 function traer_confirmaciones_or(pk){

		 		$("#mostrar").css('display','block');
		 		$.post("modificador_usuarios.php", {
                        verificando_sitio: 1, tipo:10, pk : pk
                    },
                    function (data) {

                    	data = $.parseJSON(data);

                    	 var tabla ='<table class="table table-bordered table-striped" id="tabla_or_conf" width="100%"><thead>'
                            +'<tr>'
                                +'<th align="center">Total OR</th>'
                                +'<th align="center">Total Confirmaciones</th>'
                                +'<th align="center">Total OR mes anterior</th>'
                                +'<th align="center">Total Confirmaciones mes anterior</th>'
                            +'</tr></thead>'
                            +'<tbody>';



                            tabla += "<tr>"
	                                    +"<td align='center'>"+data.total_or+"</td>"
	                                    +"<td align='center'>"+data.total_confirmaciones+"</td>"
	                                    +"<td align='center'>"+data.total_or_anterior+"</td>"
	                                    +"<td align='center'>"+data.total_confirmaciones_anterior+"</td>"
	                                +"</tr>"

	                        tabla += '</tbody></table>';
                            $("#respuesta").html(tabla);

                            $("#total_or").val(data.total_or);
                            $("#total_confirmaciones").val(data.total_confirmaciones);
                            $("#total_or_anterior").val(data.total_or_anterior);
                            $("#total_confirmaciones_anterior").val(data.total_confirmaciones_anterior);


                            armar_grafico(1,data.total_or,data.total_confirmaciones,null,null,"anual");  
                            armar_grafico(2,null,null,data.total_or_anterior,data.total_confirmaciones_anterior,"mes_anterior");  

                    }).done(function(){
						$(".cargando").removeClass("fa fa-spinner fa-spin");
						$(".oculto").css('display','block');

						$('select#set_competitivo').select2({

                            placeholder: "Seleccione Set"

                      	});

                    });	



		 }


function armar_grafico(tipo,total_or,total_confirmaciones,total_or_anterior,total_confirmaciones_anterior,id){

	var titulo = "";
	var color_1 = "";
	var color_2 = "";
    var valor_1 = 0;
    var valor_2 = 0;


	switch(tipo){

		case 1:
			titulo = "TOTAL AÑO";
			total = (parseInt(total_or)+parseInt(total_confirmaciones));
			valor_1 = (total_or/total)*100;
			valor_2 = (total_confirmaciones/total)*100;
			color_1 = "#088A08";
			color_2 = "#FE9A2E";

		break;

		case 2:
			titulo = "TOTAL MES ANTERIOR";
			total = (parseInt(total_or_anterior)+parseInt(total_confirmaciones_anterior));
			valor_1 = (total_or_anterior/total)*100;
			valor_2 = (total_confirmaciones_anterior/total)*100;
			color_1 = "#088A08";
			color_2 = "#FE9A2E";
		break;

		case 3:
			titulo = "TOTAL AÑO SET";
			total = (parseInt(total_or)+parseInt(total_confirmaciones));
			valor_1 = (total_or/total)*100;
			valor_2 = (total_confirmaciones/total)*100;
			color_1 = "#FFFF00";
			color_2 = "#013ADF";
		break;

		case 4:
			titulo = "TOTAL MES ANTERIOR SET";
			total = (parseInt(total_or_anterior)+parseInt(total_confirmaciones_anterior));
			valor_1 = (total_or_anterior/total)*100;
			valor_2 = (total_confirmaciones_anterior/total)*100;
			color_1 = "#FFFF00";
			color_2 = "#013ADF";
		break;
	}




			Highcharts.chart(''+id+'', {
			    chart: {
			        plotBackgroundColor: null,
			        plotBorderWidth: null,
			        plotShadow: false,
			        type: 'pie'
			    },
			    title: {
			        text: ''+titulo+''
			    },
			    tooltip: {
			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			    },
			    plotOptions: {
			        pie: {
			            allowPointSelect: true,
			            cursor: 'pointer',
			            dataLabels: {
			                enabled: true,
			                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			                style: {
			                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                }
			            }
			        }
			    },
				    series: [{
				        name: 'Porcentaje',
				        colorByPoint: true,
				        data: [{
				            name: 'On Request',
				            y: parseFloat(valor_1.toFixed(2)),
				            color : ""+color_1+""
				        }, {
				            name: 'Confirmaciones',
				            y: parseFloat(valor_2.toFixed(2)),
				            color : ""+color_2+""
				        }]
				    }]
				});

}



</script>

</body>
</html>