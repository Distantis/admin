<?php	 	 
include ("secure.php");
require_once('Connections/db1.php');
//$clientes_query = "select * from clientes where estado = 0";
//$clientes=$db1->Execute($clientes_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	//JGAETE 28-03-2014
	//Se muestran solo detalles con hotel.hot_or_operador = N
	//S=el OR lo gestiona el operador; N=el OR lo gestiona Distantis
	//Query agregada = AND h.hot_or_operador = 'N'
//$db1->debug = true;
				
$query_datos = "SELECT 
				  	DATE_FORMAT(cd_fecdesde,'%d-%m-%Y') AS desde,
	DATE_FORMAT(cd_fechasta,'%d-%m-%Y') AS hasta,
	cd.id_hotel as TheHotel,
  cd.*,
  c.*,
  h.*,
  th.*,
  ho.*,
  hd.*,
  tt.* 
				FROM
				  ".$_GET['lugar'].".cotdes cd
				LEFT JOIN ".$_GET['lugar'].".ciudad c
				ON cd.`id_ciudad` = c.`id_ciudad`
				LEFT JOIN ".$_GET['lugar'].".hotel h
				ON cd.`id_hotel` = h.`id_hotel`
				LEFT JOIN ".$_GET['lugar'].".tipohabitacion th
				ON cd.`id_tipohabitacion` = th.`id_tipohabitacion`
				INNER JOIN ".$_GET['lugar'].".hotocu ho
				  ON cd.`id_cot` = ho.`id_cot`
				  INNER JOIN ".$_GET['lugar'].".hotdet hd
				  ON ho.`id_hotdet` = hd.`id_hotdet`
				  INNER JOIN ".$_GET['lugar'].".tipotarifa tt
				  ON hd.`id_tipotarifa` = tt.`id_tipotarifa`				
				WHERE cd.id_cot = ".$_GET['cot']."
				and cd_estado = 0
				AND cd.id_seg = 13 
			
				GROUP BY cd.id_cotdes";   // 	AND h.hot_or_operador = 'N'
echo $query_datos;
//$datos=$db1->Execute($query_datos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$datos = $db1->SelectLimit($query_datos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$nombre = "SELECT nombre FROM clientes WHERE bd = '".$_GET['lugar']."'";

//die($nombre);
//$nom=$db1->Execute($nombre) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$nom = $db1->SelectLimit($nombre) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	
	<link rel="stylesheet" href="/resources/demos/style.css" />
	

  <link rel="stylesheet" href="/resources/demos/style.css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 

	<script>
		
	</script>
	</head>



<body onLoad="document.form.txt_numpas.focus();">

    <div id="container" class="inner">

        <div id="header">
			
		<?php	 	 include ("bienvenida.php");?>


            <ul id="nav">

                <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="datos_cot.php" title="<? echo $servind_tt;?>" class="tooltip">Datos Hotel Cot</a></li>
				
				
		   </ul>

            <ol id="pasos">

            </ol>													    

        </div>

        <!-- INICIO Contenidos principales-->

        <div style="margin-left:auto;margin-right:auto;width: 1024px;">

		<div id="nueva" width = '1024px'  >
			<table class="programa" align="center" width = '892px'>
				<tr>
					<th width="892px">
						<center>Cliente 
						
						<?php	 	
						while (!$nom->EOF){
							echo $nom -> Fields('nombre');
						$nom -> MoveNext();
						}
						?></center>
					</th>
				</tr>
			</table>
			
			<?php	 	 while (!$datos->EOF){ ?>
			<table class="programa" align="center" width = '892px'>
				<tr>
					<th colspan='4' width="892px">
						<center> <font size=4>Cot <?= $datos -> Fields('id_cot') ?> - Destino <?= $datos -> Fields('ciu_nombre') ?></font></center>
					</th>
					
				</tr>
			</table>
			<table class="programa" align="center" width = '892px'>
				<tr>
					<th colspan='4' width = '892px'>
						<center>Datos Hotel</center>
					</th>
				</tr>
				<tr>
					<th>Hotel:
					</th>
					<td><?= $datos -> Fields('hot_nombre') ?>
					</td>
					<th>Telefono:
					</th>
					<td><?= $datos -> Fields('hot_fono') ?>
					</td>
				<tr>
				<tr>
					<th>Desde:
					</th>
					<td><?= $datos -> Fields('desde') ?>
					</td>
					<th>Hasta:
					</th>
					<td><?= $datos -> Fields('hasta') ?>
					</td>
				<tr>
				<tr>
					<th>Single:
					</th>
					<td><?= $datos -> Fields('cd_hab1') ?> a <?= $datos -> Fields('hd_sgl') ?>
					</td>
					<th>Doble Twin:
					</th>
					<td><?= $datos -> Fields('cd_hab2') ?> a <?= $datos -> Fields('hd_dbl')*2 ?>
					</td>
				<tr>
				<tr>
					<th>Doble Matrimonial:
					</th>
					<td><?= $datos -> Fields('cd_hab3') ?> a <?= $datos -> Fields('hd_tpl')*2 ?>
					</td>
					<th>Triple:
					</th>
					<td><?= $datos -> Fields('cd_hab4') ?> a <?= $datos -> Fields('hd_qua')*3 ?>
					</td>
				<tr>
				<tr>
					<th>Tipo Habitacion:
					</th>
					<td><?= $datos -> Fields('th_nombre') ?>
					</td>
					<th>Tipo Tarifa:
					</th>
					<td><?= $datos -> Fields('tt_nombre') ?>
					</td>
				</tr>
				</table>
				<table class="programa" align="center" width = '892px'>
				<tr>
					<th colspan='3' width = '892px'><center>Usuarios</center>
					</th>
				</tr>
				<tr>
					<th><center>Usuario</center>
					</th>
					<th><center>Password</center>
					</th>
					<th><center>e-mail</center>
					</th>
				</tr>
				<?php	 	
					$query_usuarios = "SELECT * FROM ".$_GET['lugar'].".usuarios WHERE id_empresa =".$datos -> Fields('TheHotel')."  AND usu_estado = 0";
					//die($query_usuarios);
					//echo $query_usuarios;
					$usuarios = $db1->SelectLimit($query_usuarios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					while (!$usuarios->EOF){
					?>
				<tr>
					<td><center><?= $usuarios -> Fields('usu_login')?></center>
					</td>
					<td><center><?= $usuarios -> Fields('usu_password')?></center>
					</td>
					<td><center><?= $usuarios -> Fields('usu_mail')?></center>
					</td>
				</tr>				
				<?php	 	 $usuarios -> MoveNext();
						}
						?>
			</table>
			<?php	 	 $datos -> MoveNext();
						}
						?>
		</div>
		<div id="editar">
		</div>
        

        </div>

        <!-- FIN Contenidos principales -->





        

    

    </div>

    <!-- FIN container -->

</body>

</html>