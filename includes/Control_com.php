<?
//Markup
$markup_hotel = MarkupHoteleria($db1,$cot->Fields('id_cont'));
$markup_trans = MarkupTransporte($db1,$cot->Fields('id_cont'));
$markup_programa = MarkupPrograma($db1,$cot->Fields('id_cont'));

//Comisiones del operador
if(PerteneceCTS($_SESSION['id_empresa'])){
	if($debug and isset($pack)){
		echo "id_comdet: ".$pack->Fields('id_comdet')." | id_grupo: ".$cot->Fields('id_grupo')."<br>";
	}
	
	$opcomtrapro = ComisionOP2($db1,"LIB",$cot->Fields('id_grupo'));
		$opcomtra = ComisionOP2($db1,"TRF",$cot->Fields('id_grupo'));
	
	$opcomexc = ComisionOP2($db1,"EXC",$cot->Fields('id_grupo'));
	
	$opcomhtl = ComisionOP2($db1,"HTL",$cot->Fields('id_grupo'));
		$opcomnie = ComisionOP2($db1,"TRN",$cot->Fields('id_grupo'));
		$opcomesp = $db1->SelectLimit("SELECT hot_comesp FROM hotel WHERE id_hotel = ".$cot->Fields('id_mmt'))->Fields('hot_comesp');
	
	if(isset($pack))$opcompro = ComisionOP($db1,$pack->Fields('id_comdet'),$cot->Fields('id_grupo'));
}else{
	if($debug and isset($pack)){
		echo "id_comdet: ".$pack->Fields('id_comdet')." | id_grupo: ".$_SESSION['id_grupo']."<br>";
	}
	//echo $_SESSION['id_grupo'];
	$opcomtrapro = ComisionOP2($db1,"LIB",$_SESSION['id_grupo']);
	$opcomtra = ComisionOP2($db1,"TRF",$_SESSION['id_grupo']);
	$opcomexc = ComisionOP2($db1,"EXC",$_SESSION['id_grupo']);
	$opcomhtl = ComisionOP2($db1,"HTL",$_SESSION['id_grupo']);
	$opcomnie = ComisionOP2($db1,"TRN",$_SESSION['id_grupo']);
	$opcomesp = $db1->SelectLimit("SELECT hot_comesp FROM hotel WHERE id_hotel = ".$cot->Fields('id_mmt'))->Fields('hot_comesp');
// 	echo $pack->Fields('id_pack');die();
	if(isset($pack))$opcompro = ComisionOP($db1,$pack->Fields('id_comdet'),$_SESSION['id_grupo']);
}
if(PerteneceEuropa($cot->Fields('id_cont'))){
	$opcomtrapro = 0;
	$opcomtra = 0;
	$opcomexc = 0;
	$opcomhtl = 0;
	$opcomnie = 0;
	$opcomesp = 0;
}
if($debug){
	echo "TRAPRO: $opcomtrapro | TRA: $opcomtra | EXC: $opcomexc | HTL: $opcomhtl | PRO: $opcompro | MARK-UP HOTEL: $markup_hotel | MARK-UP TRANS: $markup_trans | MARK-UP PRO: $markup_programa<br>";
}
?>