<?
//ID de la tabla hotel que representa a CTS
$id_cts = array(1138,3229,4300,4301,4302,4303,4304);
$id_europa = array(1,3,4,5,6);

function PerteneceCTS($id_empresa){
	global $id_cts;
	$id_cts = array(1138,3229,4300,4301,4302,4303,4304);
	return in_array($id_empresa,$id_cts);
}

function PerteneceEuropa($id_cont){
	global $id_europa;
	$id_europa = array(1,3,4,5,6);
	return in_array($id_cont,$id_europa);
}



function getClientes($db1,$cliente=0){
		$sql="SELECT * FROM clientes WHERE estado = 0";
		if($cliente!=0) $sql.= " AND id_cliente = $cliente";
		$rpt = $db1->SelectLimit($sql) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
		$cont=0;
		while(!$rpt->EOF){
			$clientes[$rpt->Fields('id_cliente')]['nombre'] = $rpt->Fields('nombre');
			$clientes[$rpt->Fields('id_cliente')]['bd']=$rpt->Fields('bd');
			$rpt->MoveNext();
		}
		return $clientes;
	}







/*
 * ConsultaCotizacion
 *
 * Funcion para obtener la informacion de la cotizacion
 *
 */

 // JGaete
function ValorTransporteConMarkupEspecial($db1, $tra_codigo, $tra_pas1, $tra_pas2){
	// echo "Entrando";
	$sql="	SELECT round(neto / ((100-mkup)/100), 2) AS precioventa
			FROM serv_markup_especial
			WHERE codigo = ".$tra_codigo." 
			AND pax_i = ".$tra_pas1." AND pax_f = ".$tra_pas2;
	
	$trans = $db1->SelectLimit($sql) or die("Control - ValorTransporteConMarkupEspecial : ".__LINE__." ".$db1->ErrorMsg());

	$cantRows = $trans->RecordCount();
	
	if($cantRows>0){
		$trans->MoveFirst();
		return $trans->Fields("precioventa");
	}else{
		return 0;
	}
}
 
function ConsultaCotizacion($db1,$id_cot){
	global $id_cts;
	$query_cot = "SELECT *,
			DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
			DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta,
			DATE_FORMAT(c.cot_fecdesde, '%Y-%m-%d') as cot_fecdesde1,
			DATE_FORMAT(c.cot_fechasta, '%Y-%m-%d') as cot_fechasta1,
			DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y 23:59:59') as cot_fecanula,
			YEAR(c.cot_fecdesde) as anod, MONTH(c.cot_fecdesde) as mesd, DAY(c.cot_fecdesde) as diad,
			YEAR(c.cot_fechasta) as anoh, MONTH(c.cot_fechasta) as mesh, DAY(c.cot_fechasta) as diah,
			DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fecd,
			DATE_FORMAT(c.cot_fec, '%H:%i:%s') as cot_fech,
			if(c.id_operador not in (".implode(",",$id_cts).") , o.hot_comhot , h.hot_comhot) as comhot,
			if(c.id_opcts is null,c.id_operador,c.id_opcts) as id_mmt,
			if(c.id_opcts is null,o.hot_comesp,h.hot_comesp) as hot_comesp,
			if(c.id_opcts is null,o.id_continente,h.id_continente) as id_cont,
			IF(c.id_opcts IS NULL,o.id_grupo,IF(h.id_grupo IS NULL, o.id_grupo, h.id_grupo)) AS id_grupo,
			if(c.id_opcts is null,o.id_ciudad,h.id_ciudad) as op_ciudad,
			if(c.id_opcts is null,o.hot_diasrestriccion_conf,h.hot_diasrestriccion_conf) as hot_diasrestriccion_conf,
			o.hot_nombre as op,
			h.hot_nombre as op2,
			now() as ahora,
			c.cot_numpas as cot_numpas,
			c.id_tipopack as id_tipopack,
			c.id_area as id_area
		FROM cot c
		INNER JOIN seg s ON s.id_seg = c.id_seg
		INNER JOIN hotel o ON o.id_hotel = c.id_operador
		LEFT JOIN hotel h ON h.id_hotel = c.id_opcts
		INNER JOIN tipopack tp ON tp.id_tipopack = c.id_tipopack WHERE c.id_cot = ".GetSQLValueString($id_cot,"int");
	$cot = $db1->SelectLimit($query_cot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo $query_cot;
	return $cot;
}

/*
 * ConsultaDestinos
 *
 * Funcion para obtener los destinos de la cotizacion
 *
 */
function ConsultaDestinos($db1,$id_cot,$join_hotel){
	$query_destinos = "SELECT *,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%Y') as ano1,DATE_FORMAT(c.cd_fecdesde, '%m') as mes1,DATE_FORMAT(c.cd_fecdesde, '%d') as dia1,
		DATE_FORMAT(c.cd_fechasta, '%Y') as ano2,DATE_FORMAT(c.cd_fechasta, '%m') as mes2,DATE_FORMAT(c.cd_fechasta, '%d') as dia2,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		c.id_ciudad as id_ciudad,c.cd_hab1,c.cd_hab2,c.cd_hab3,c.cd_hab4 
	FROM cotdes c
	LEFT JOIN seg s ON s.id_seg = c.id_seg";
	if($join_hotel){
		$query_destinos.= " INNER JOIN hotel h ON h.id_hotel = c.id_hotel
							INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion";
	}else{
		$query_destinos.= " LEFT JOIN hotel h ON h.id_hotel = c.id_hotel";
	}
	$query_destinos.= " INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 and id_cotdespadre = 0";
	//echo $query_destinos;
	$destinos = $db1->SelectLimit($query_destinos) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos;
}

function ConsultaNoxAdi($db1,$id_cot,$id_cotdes,$confirmadas=true){
	$query_nochead = "SELECT *,
		DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		DATEDIFF(cd_fecdesde,cd_fechasta) as dias
	FROM cotdes WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND id_cotdespadre = ".GetSQLValueString($id_cotdes,"int")." AND cd_estado = 0";
	if($confirmadas)$query_nochead.=" and id_seg=7";
	$nochead = $db1->SelectLimit($query_nochead) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $nochead;
}

function ConsultaNoxAdiXcot($db1,$id_cot,$confirmadas=true){
	$query_destinos2 = "SELECT c.*,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		h.id_hotel as hot_padre, h.id_tipohabitacion as th_padre
	FROM cotdes c
	LEFT JOIN cotdes h ON h.id_cotdes = c.id_cotdespadre
	WHERE c.id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 AND c.id_cotdespadre !=0";
	if($confirmadas)$query_destinos2.=" and c.id_seg=7";
	$destinos2 = $db1->SelectLimit($query_destinos2) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos2;
}

function ConsultaPackDetalle($db1,$id_packdetalle){
	$hq = "SELECT p.*, h1.hot_nombre as hot_nombre1, h2.hot_nombre as hot_nombre2, h3.hot_nombre as hot_nombre3, h4.hot_nombre as hot_nombre4,
				h1.hot_diasrestriccion_conf as hot_diasrestriccion_conf1 ,h2.hot_diasrestriccion_conf as hot_diasrestriccion_conf2 , h3.hot_diasrestriccion_conf as hot_diasrestriccion_conf3 , h4.hot_diasrestriccion_conf as hot_diasrestriccion_conf4 ,
				h1.hot_direccion as hot_direccion1,
				h2.hot_direccion as hot_direccion2,
				h3.hot_direccion as hot_direccion3,
				h4.hot_direccion as hot_direccion4
		FROM packdetalle p
		LEFT JOIN hotel h1 ON p.id_hotel1 = h1.id_hotel
		LEFT JOIN hotel h2 ON p.id_hotel2 = h2.id_hotel
		LEFT JOIN hotel h3 ON p.id_hotel3 = h3.id_hotel
		LEFT JOIN hotel h4 ON p.id_hotel4 = h4.id_hotel
		WHERE p.id_packdetalle = ".GetSQLValueString($id_packdetalle,"int");
	$h = $db1->SelectLimit($hq) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $h;
}

function ConsultaPack($db1, $id_pack){
	$query_pack = "SELECT *,
		(p.pac_cantdes-1) as cantdes,
		DATE_FORMAT(p.pac_fecdesde,'%d-%m-%Y') as pac_fecdesde1,
		DATE_FORMAT(p.pac_fechasta,'%d-%m-%Y') as pac_fechasta1,
		DATE_FORMAT(p.pac_fecdesde,'%d') as idia,
		DATE_FORMAT(p.pac_fecdesde,'%m') as imes,
		DATE_FORMAT(p.pac_fecdesde,'%Y') as iano,
		DATE_FORMAT(p.pac_fechasta,'%d') as fdia,
		DATE_FORMAT(p.pac_fechasta,'%m') as fmes,
		DATE_FORMAT(p.pac_fechasta,'%Y') as fano
	FROM pack as p
	LEFT JOIN packdet d ON p.id_pack = d.id_pack and d.pd_estado = 0
	WHERE p.id_pack = ".GetSQLValueString($id_pack,"int");
	$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pack;
}

function PuedeModificar($db1,$id_cot){
	$mod_sql = 'SELECT DATE_FORMAT(ha_hotanula,"%Y-%m-%d %H:%m:%s") AS anulacion, DATE_FORMAT(NOW(),"%Y-%m-%d %H:%m:%s") as ahora FROM cot WHERE id_cot = '.GetSQLValueString($id_cot,"int");
	$mod = $db1->SelectLimit($mod_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$fec1 = new DateTime($mod->Fields('ahora'));
	$fec2 = new DateTime($mod->Fields('anulacion'));
	if($fec1<$fec2){
		return true;
	}else{
		return false;	
	}
}

function ValidarValorTransportes($id_cot,$id_continente){
	global $db1,$cot,$markup_trans,$opcomtrapro,$opcomtra,$opcomexc,$opcomhtl,$opcomnie,$opcomesp;
	
	$query_pasajeros = "
		SELECT *, 
				c.id_pais as id_pais,
				DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCION__." : ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
	
	
	while(!$pasajeros->EOF){
		$id_cotpas = $pasajeros->Fields('id_cotpas');
		$servicios = ConsultaServiciosXcotpas($db1,$id_cotpas);
		
		while (!$servicios->EOF) {
			$id_cotser = $servicios->Fields('id_cotser');
			$id_ttagrupa = $servicios->Fields('id_ttagrupa');
			$cs_fecped = $servicios->Fields('cs_fecped');
				
			$trans_array[$id_cotpas][$cs_fecped][$id_ttagrupa] = $id_cotser;
				
			$servicios->MoveNext();
		}
		$pasajeros->MoveNext();
	}
	$pasajeros->MoveFirst();
	
	if(isset($trans_array)){
		foreach($trans_array as $id_cotpas1=>$a1){
			foreach($trans_array as $id_cotpas2=>$a2){
				if($id_cotpas1!=$id_cotpas2){
					foreach($a1 as $cs_fecped1=>$b1){
						foreach($a2 as $cs_fecped2=>$b2){
							foreach($b1 as $id_ttagrupa1=>$id_cotser1){
								foreach($b2 as $id_ttagrupa2=>$id_cotser2){
									if(($cs_fecped1==$cs_fecped2)and($id_ttagrupa1==$id_ttagrupa2)){
										$serv_iguales[$id_ttagrupa1][$cs_fecped1][$id_cotpas1]=$id_cotser1;
										$serv_iguales[$id_ttagrupa2][$cs_fecped2][$id_cotpas2]=$id_cotser2;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(count($serv_iguales)>0){
			foreach($serv_iguales as $id_ttagrupa=>$datos1){
				foreach($datos1 as $cs_fecped=>$datos2){
					$count = count($datos2);
					$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = $id_ttagrupa and tra_pas1 <= $count and tra_pas2 >= $count";
					$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
					
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser in (".implode(",",$datos2).") and id_cot=".GetSQLValueString($id_cot, "int")." and DATE_FORMAT(cs_fecped, '%d-%m-%Y') = '".$cs_fecped."'";
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
		}
		
		$sql_trans1 = "SELECT cs.id_cotser,tr.id_ttagrupa,cs.id_trans,tr.tra_codigo,count(*) as cant
			FROM cotser as cs
			INNER JOIN trans as tr ON tr.id_trans = cs.id_trans
			WHERE cs.id_cot = ".GetSQLValueString($id_cot, "int")." and cs.cs_estado = 0 GROUP BY cs.id_trans, cs.cs_fecped";
		$trans1 = $db1->Execute($sql_trans1) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
		
		while(!$trans1->EOF){
			if($trans1->Fields('cant')==1){
				$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = ".$trans1->Fields('id_ttagrupa')." and tra_pas1 <= 1 and tra_pas2 >= 1";
				$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				
				if($id_trans->RecordCount()>0){
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}else{
					$update_id_trans="UPDATE cotser SET cs_estado = 1 WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
			$trans1->MoveNext();
		}
		
		$id_trans = ConsultaServiciosXcot($db1,$id_cot);
		
		while(!$id_trans->EOF){
			if(PerteneceEuropa($id_continente)){
				$cs_valor = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
			}else{
					
					// if($id_trans->Fields('id_trans')==2595||$id_trans->Fields('id_trans')==493){
					if($id_trans->Fields('tra_codigoprocesado')==0){
						$trans_neto = $id_trans->Fields('tra_valor');
					}else{
						$valTransporte= ValorTransporteConMarkupEspecial($db1, $id_trans->Fields('tra_codigo'), $id_trans->Fields('tra_pas1'), $id_trans->Fields('tra_pas2'));
						if($valTransporte<>0)
							$trans_neto=$valTransporte;
						else
							$trans_neto = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
					}
/*						
					if(($id_trans->Fields('id_trans')==2595)
							||($id_trans->Fields('tra_codigo')==19542)
							||($id_trans->Fields('tra_codigo')==19543)
							||($id_trans->Fields('tra_codigo')==19587)
							||($id_trans->Fields('tra_codigo')==19589)
						)
						$trans_neto = $id_trans->Fields('tra_valor');
					else
						$trans_neto = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
*/					
				
				if($id_trans->Fields('id_tipotrans') == 15){
					// echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$opcomtrapro;
					
					$ser_trans2 = ($trans_neto*$opcomtrapro)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$trans_neto."-".$opcomtrapro."-".$cs_valor."<br>";
					//$cs_valor = round($trans_neto/(1+($opcomtrapro/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == 0){
					$ser_trans2 = ($trans_neto*$opcomtra)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomtra/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == $cot->Fields('id_mmt')){
					$ser_trans2 = ($trans_neto*$opcomesp)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomesp/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 14){
					$ser_trans2 = ($trans_neto*$opcomnie)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomnie/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 4){
					$ser_trans2 = ($trans_neto*$opcomexc)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomexc/100)),1);
				}
			}
			
			$update_id_trans="UPDATE cotser SET cs_valor = ".GetSQLValueString($cs_valor, "double")." WHERE id_cotser = ".$id_trans->Fields('id_cotser');
			$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
			$id_trans->MoveNext();
		}
	}
}

function CalcularTransTotal($db1,$id_cot){
	$cs_valorSQL = "SELECT SUM(cs_valor) as cs_valor FROM cotser WHERE cs_estado = 0 and id_cot = ".GetSQLValueString($id_cot,"int");
	$cs_valor = $db1->SelectLimit($cs_valorSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $cs_valor->Fields('cs_valor');
}

function CalcularValorCot($db1,$id_cot,$guardar,$estado){
	$valorcs=0;
	$valorhc2=0;
	
	$query_cotser1 = "SELECT *
					FROM cotser
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$cotser1 = $db1->SelectLimit($query_cotser1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$cotser1->EOF){
		$valorcs+= $cotser1->Fields('cs_valor');
		$cotser1->MoveNext();
	}
	$query_hotocu1 = "SELECT *
					FROM hotocu
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND hc_estado = $estado";
	$hotocu1 = $db1->SelectLimit($query_hotocu1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$hotocu1->EOF){
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor1');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor2');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor3');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor4');
		$hotocu1->MoveNext();
	}
	if(count($valorhc)>0){
		foreach($valorhc as $id_cotdes=>$cd_valor){
			if($guardar){
				$query = sprintf("update cotdes set cd_valor=%s where id_cotdes=%s",GetSQLValueString($cd_valor, "double"),GetSQLValueString($id_cotdes, "int"));
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$valorhc2+=$cd_valor;
		}
	}
	$valor_tot = $valorcs+$valorhc2;

	if($guardar){
		$query = sprintf("update cot set cot_valor=%s where id_cot=%s",GetSQLValueString($valor_tot, "double"),GetSQLValueString($id_cot, "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	return $valor_tot;
}

function InsertarLog($db1,$id_cot,$id_accion,$id_user){
	$log_q = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", GetSQLValueString($id_user, "int"), GetSQLValueString($id_accion, "int"), GetSQLValueString($id_cot, "int"));					
	$log = $db1->Execute($log_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
}

function OperadoresActivos($db1){
	$query_opcts = "SELECT * FROM hotel WHERE id_tipousuario = 3 and hot_activo = 0 and id_area = 1 ORDER BY hot_nombre";
	$rsOpcts = $db1->SelectLimit($query_opcts) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsOpcts;
}

function Categorias($db1){
	$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
	$categoria = $db1->SelectLimit($query_categoria) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $categoria;
}

function ConsultaServiciosXcotdes($db1,$id_cotdes,$groupby=false){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped";
	
	if($groupby)$query_servicios.=", count(*) as cuenta, sum(c.cs_valor) as cs_valor";
	$query_servicios.=" FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0";
	if($groupby)$query_servicios.=" GROUP BY c.id_trans,c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}
function ConsultaServiciosXcot($db1,$id_cot){
	$query_servicios = "SELECT *,ifnull(t.tra_codigo, 0) as tra_codigoprocesado,
				DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,
				DATE_FORMAT(c.cs_fecped, '%Y-%m-%d') as cs_fecped1
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServXcotdesGrupo($db1,$id_cotdes){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,count(*) as cuenta
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0 GROUP BY c.id_trans";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServiciosXcotpas($db1,$id_cotpas){
	$query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
			FROM cotser c 
			INNER JOIN trans t on t.id_trans = c.id_trans
			WHERE c.cs_estado=0 and c.id_cotpas = ".GetSQLValueString($id_cotpas,"int")."
			ORDER BY c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaListaTransportes($db1,$id_operador,$id_ciudad=96,$fec_entrada=NULL,$fec_salida=NULL){
	$query_tipotrans = "SELECT * 
		FROM trans t
		INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
		WHERE tra_estado = 0 AND i.tpt_estado = 0 and t.id_hotel IN (0,$id_operador) and t.id_area = 1 and t.id_ciudad = $id_ciudad";
	if(isset($fec_entrada) and isset($fec_salida))$query_tipotrans.= " and t.tra_facdesde <= '$fec_entrada' and t.tra_fachasta >= '$fec_salida'";
	$query_tipotrans.= " GROUP BY id_ttagrupa ORDER BY tra_orderhot desc, tpt_nombre";
	$rstipotrans = $db1->SelectLimit($query_tipotrans) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $rstipotrans;
}

function ConsultaPasajeros($db1,$id_cot){
	$query_pasajeros = "
		SELECT *,c.id_pais as id_pais,DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pasajeros;
}

function MarkupPrograma($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuppro');
}

function MarkupHoteleria($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuphtl');
}

function MarkupTransporte($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuptra');
}

//Funcion para obtener la comision del operador segun el id_comdet y el id_grupo
function ComisionOP($db1,$id_comdet,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE id_comdet = $id_comdet and id_grupo = $id_grupo";
// 	echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

//Funcion para obtener la comision del operador segun el tipo de comision y el id_grupo
function ComisionOP2($db1,$com_tipo,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
	//echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

function ordenar_matriz_nxn($m,$ordenar,$direccion) {
	if(count($m)==0) return $m;
	// 	$m //es vuestra matriz
	// 	$ordenar //es el campo a ordenar
	// 	$direccion //aquí podéis elegir de forma ASC o DESC

	usort($m, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	return $m;
}

function ordenar_matriz_multidimensionada($m) {
    if(count($m)==0) return $m;
    $ordenar='valor';
    $direccion = 'ASC';
    for($x=0; $x<count($m);$x++){
        $matriz[$x]['num']=$m[$x][0]['num'];
        $matriz[$x]['valor']=$m[$x][0]['valorsss'];
    }
	
	usort($matriz, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	
	for($x=0; $x<count($m);$x++){
		$matrizFinal[$x]=$m[$matriz[$x]['num']]; 
	}
	
	return $matrizFinal;
}

//Funcion para obtener la disponibilidad
function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack){
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
	/*
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
		*/
		
		$query_pack = "select id_tipotarifa,pac_n,id_tipoprog from pack where id_pack = ".$id_pack;
		$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
		if($pack->Fields('id_tipoprog')== 1){
		
		if($pack->Fields('id_tipotarifa') == 0){
			$condtarifa= " AND id_tipotarifa in (2,127,128,129,130,131,132,133,4)";
		}
		if($pack->Fields('id_tipotarifa') == 2){
			
			if($pack->Fields('pac_n') == 1) $condtarifa= " AND id_tipotarifa in (2)";
			if($pack->Fields('pac_n') == 2) $condtarifa= " AND id_tipotarifa in (133)";
			if($pack->Fields('pac_n') == 3) $condtarifa= " AND id_tipotarifa in (132)";
			if($pack->Fields('pac_n') == 4) $condtarifa= " AND id_tipotarifa in (127)";
			if($pack->Fields('pac_n') == 5) $condtarifa= " AND id_tipotarifa in (128)";
			if($pack->Fields('pac_n') == 6) $condtarifa= " AND id_tipotarifa in (129)";
			if($pack->Fields('pac_n') == 7) $condtarifa= " AND id_tipotarifa in (130)";
			if($pack->Fields('pac_n') == 8) $condtarifa= " AND id_tipotarifa in (131)";
			//$condtarifa= " AND id_tipotarifa in (2,127,128,129,130,131,132,133)";
			/*
			echo "<br>".$id_pack."<br>";
			echo $pack->Fields('id_tipotarifa')."<br>";
			echo $cant_noches."<br>";
			echo $condtarifa;
			*/
		}
		else{
			$condtarifa= " AND id_tipotarifa =".$pack->Fields('id_tipotarifa');
		}
		}
		
		if($pack->Fields('id_tipoprog')== 2 or $pack->Fields('id_tipoprog')== 4){
		if($pack->Fields('id_tipotarifa') == 0){
			$condtarifa= " AND id_tipotarifa in (2,4)";
		}
		if($pack->Fields('id_tipotarifa') == 2){
			$condtarifa= " AND id_tipotarifa in (2)";
		}

		if($pack->Fields('id_tipotarifa') == 4){
			$condtarifa= " AND id_tipotarifa in (4)";
		}		
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND a.id_area = 1
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();
	//echo $sqlDisp;
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			//echo "-->".$agregaralvalor;
			
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		// TARIFA GEN SUITE
		/*
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=89.6;
			if($doble>0) $thab3=53.2;
			if($twin>0) $thab2=53.2;
			if($triple>0) $thab4=0;
		}
		*/
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}
 

//MATRIZ DISPONIBILIDAD PERO EXEPTUANDO UNA COT EN HOTOCU POR PARAMETRO ENTRANTE
function matrizDisponibilidad_exeptionCot($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$id_cot_ex,$id_cotdes){
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	if(isset($id_pack)){
		//Si esta seteado el ID Pack, quiere decir que la cotizacion es un programa - promocion, etc...
		// en ese caso se va a buscar el tipo de tarifa seteado en el programa
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}

		if(isset($tarifas)){
			//Si el programa tiene seteado tipo(s) de tarifa entonces se usan esos, en caso contrario se usan
			//las que estan en la linea 728, que son Convenio, Especial y Promocional respectivamente, no la tarifa
			//usada en el pack
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}else{
			//JG - 15-OCT-2013 (DESDE EL ELSE DE LA LINEA 746)
			//Se agrega query para ir a buscar ESPECIFICAMENTE el tipo de tarifa utilizado por la cotizacion al momento
			//de confirmarlo. Si y solo si la Cotizacion viene como parametro
			if(isset($id_cot_ex)){
				$pack_tar_q = "select distinct h.id_tipotarifa
								from 
									cotdes c
									inner join hotdet h on h.id_hotdet = c.id_hotdet
								where c.id_cot = ".$id_cot_ex."
								and c.cd_estado = 0
								and h.hd_estado = 0";
				$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				while(!$pack_tar->EOF){
					if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
					$pack_tar->MoveNext();
				}

				if(isset($tarifas)){
					//de ser encontrados los tipos de tarifa de la cotizacion, se utilizan para calcular la disponibilidad
					$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
				}								
			}
		}//FIN JG - 15-OCT-2013
	}
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}

	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
	FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
	FROM hotdet a, stock b, hotel hot
	WHERE a.id_hotdet=b.id_hotdet
	AND b.sc_fecha>='$fechaInicio'
	AND b.sc_fecha<='$fechaFin'
	AND a.id_area = 1
	AND hot.hot_barco = 0
	AND hot.id_hotel=a.id_hotel
	$condtarifa
	AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1
	LEFT JOIN
	(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4
	FROM hotocu
	WHERE hc_fecha>='$fechaInicio'
	AND hc_fecha<='$fechaFin' AND hc_estado = 0
	AND id_cot <> $id_cot_ex
	GROUP BY hc_fecha, id_hotdet) AS t2
	ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();
// 	echo $sqlDisp;die('aqui');
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	if(isset($id_cotdes)){
		$query_cotdes = "SELECT *, DATE_FORMAT(cd_fecdesde,'%d-%m-%y') as cd_fecdesde1,DATE_FORMAT(cd_fechasta,'%d-%m-%y') as cd_fechasta1 FROM cotdes WHERE id_cotdes = $id_cotdes";
		$cotdes = $db1->SelectLimit($query_cotdes) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
		$cd_fecdesde = new DateTime($cotdes->Fields('cd_fecdesde1'));
		$cd_fechasta = new DateTime($cotdes->Fields('cd_fechasta1'));
	}

	for($ll=0;$ll<$totalRows_listado1;++$ll){
	$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');

		$singlexdoble=0;

		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if(isset($id_cotdes)){
			$fec_actual = new DateTime($fecha);
			
			if($cd_fecdesde<=$fec_actual and $cd_fechasta>=$fec_actual){
				if ($stocksingle<0) $stocksingle=$cotdes->Fields('cd_hab1');
				if ($stockdoble<0) $stockdoble=$cotdes->Fields('cd_hab2');
				if ($stocktwin<0) $stocktwin=$cotdes->Fields('cd_hab3');
				if ($stocktriple<0) $stocktriple=$cotdes->Fields('cd_hab4');
			}else{
				if ($stocksingle<0) $stocksingle=0;
				if ($stockdoble<0) $stockdoble=0;
				if ($stocktwin<0) $stocktwin=0;
				if ($stocktriple<0) $stocktriple=0;
			}
		}else{
			if ($stocksingle<0) $stocksingle=0;
			if ($stockdoble<0) $stockdoble=0;
			if ($stocktwin<0) $stocktwin=0;
			if ($stocktriple<0) $stocktriple=0;
		}

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
		$tienedisp=1;
		$singlexdoble=$single-$stocksingle;
	}

		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
		if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false;
		}
		/////////////////////////////////////////////////

	$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
	$tipo=$disp->Fields('id_tipotarifa');

	$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;

		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;

		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;

		if ($tienedisp) $dispaux="I"; else $dispaux="R";

		if (!$bloqueado) {
		if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
		$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
		$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
		$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
		$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
		$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
		$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
		$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
		$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
		$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
		}else{
		if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
				if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
							$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
							$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
							$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
							$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
							$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
							$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
							$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
							$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
							$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
							}
							}
							}
							}
							$disp->MoveNext();
							}
							return $arreglo;
		}
		

function matrizDisponibilidadPromocion($db1,$cant_noches,$fechaInicio,$fechaFin,$single, $twin, $doble , $triple,$id_pack = 0){
	
	
	if($id_pack==0){
	if($cant_noches==1) $condtarifa="2";
	if($cant_noches==2) $condtarifa="2";
	if($cant_noches==3) $condtarifa="119";
	if($cant_noches==4) $condtarifa="120,137";
	if($cant_noches==5) $condtarifa="121";
	}else{
		$query_pack = "select id_tipotarifa from pack where id_pack = ".$id_pack;
		$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
		if($pack->Fields('id_tipotarifa') == 2){
			$condtarifa= "2";
		}
		if($pack->Fields('id_tipotarifa') == 4){
			if($cant_noches==1) $condtarifa="2";
			if($cant_noches==2) $condtarifa="2";
			if($cant_noches==3) $condtarifa="119";
			if($cant_noches==4) $condtarifa="120,137";
			if($cant_noches==5) $condtarifa="121";
			//echo "<br>".$id_pack."<br>";
			//echo $pack->Fields('id_tipotarifa')."<br>";
			//echo $cant_noches."<br>";
			//echo $condtarifa;
		}
		if($pack->Fields('id_tipotarifa') == 0){
			if($cant_noches==1) $condtarifa="2";
			if($cant_noches==2) $condtarifa="2";
			if($cant_noches==3) $condtarifa="2,119";
			if($cant_noches==4) $condtarifa="2,120,137";
			if($cant_noches==5) $condtarifa="2,121";
		}		
	}
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND a.id_area = 1
						AND hot.id_hotel=a.id_hotel 
						and a.id_tipotarifa in ($condtarifa)
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	//echo $cant_noches."<br><br>";		
	//echo $sqlDisp;
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		/*
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=80;
			if($doble>0) $thab3=44.88;
			if($twin>0) $thab2=44.88;
			if($triple>0) $thab4=0;
		}*/
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}

/////////////////////////////////////////////////////////////////////////////

/*function matrizDisponibilidadPromocion($db1,$cant_noches,$fechadesde,$fechahasta,$hab1,$hab2,$hab3,$hab4)
{
//echo $cant_noches." | ".$fechadesde." | ".$fechahasta." | ".$hab1." | ".$hab2." | ".$hab3." | ".$hab4."<br>";
	
// $cant_noches =3;
$tarifa=-1;
if($cant_noches==1) $tarifa=2;
if($cant_noches==2) $tarifa=2;
if($cant_noches==3) $tarifa=119;
if($cant_noches==4) $tarifa=120;
if($cant_noches==5) $tarifa=121;

// $id_hotel=127;
// $fecha= '2012-02-09';


$hab1= $hab1==''? '0' : $hab1;
$hab2= $hab2==''? '0' : $hab2;
$hab3= $hab3==''? '0' : $hab3;
$hab4= $hab4==''? '0' : $hab4;

$consulta_sql="select a.id_hotel,a.sc_fecha,a.id_hotdet,b.hc_fecha ,b.sohab1,b.sohab2,b.sohab3,b.sohab4, a.sc_hab1,a.sc_hab2,a.sc_hab3,a.sc_hab4, a.hd_sgl,a.hd_dbl,a.hd_tpl,a.hd_qua , a.id_tipotarifa, a.id_tipohabitacion
from 
	(select hotdet.id_hotel,hotdet.id_hotdet , stock.sc_fecha,stock.sc_hab1,stock.sc_hab2,stock.sc_hab3,stock.sc_hab4, hotdet.hd_sgl,hotdet.hd_dbl,hotdet.hd_tpl,hotdet.hd_qua,hotdet.id_tipotarifa,hotdet.id_tipohabitacion
		from hotdet 
		inner join stock on stock.id_hotdet = hotdet.id_hotdet and hotdet.hd_estado =0 and stock.sc_estado=0
		where hotdet.id_tipotarifa = $tarifa 
			AND hotdet.id_area = 1
			and stock.sc_fecha>='$fechadesde' 
			and stock.sc_fecha<='$fechahasta'
		) a
left join 
	(select id_hotdet,hc_fecha, sum(hc_hab1) as sohab1 , sum(hc_hab2) as sohab2 ,sum(hc_hab3) as sohab3 ,sum(hc_hab4) as sohab4
		from hotocu 
		where hc_estado =0
			and hc_fecha >= '$fechadesde' 
			and hc_fecha <= '$fechahasta'
		group by hc_fecha , id_hotdet
		) b 
on a.id_hotdet = b.id_hotdet and a.sc_fecha = b.hc_fecha";
echo $consulta_sql."";
$consulta_promo = $db1->SelectLimit($consulta_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

//DISP HABITACION SINGLE, EN CASO DE QUE NO TENGA SE BUSCA DOBLE MAT
//0 = NO
//1 = SI

for ($i=0; $i <$consulta_promo->RecordCount() ; $i++) {
	$bloqueado=0;
		$sohab1=$consulta_promo->Fields('sohab1')==''? 0: $consulta_promo->Fields('sohab1');
		$sohab2=$consulta_promo->Fields('sohab2')==''? 0: $consulta_promo->Fields('sohab2');
		$sohab3=$consulta_promo->Fields('sohab3')==''? 0: $consulta_promo->Fields('sohab3');
		$sohab4=$consulta_promo->Fields('sohab4')==''? 0: $consulta_promo->Fields('sohab4');
	 
		$dispHab1=$consulta_promo->Fields('sc_hab1') - $sohab1;
		$dispHab2=$consulta_promo->Fields('sc_hab2') - $sohab2;
		$dispHab3=$consulta_promo->Fields('sc_hab3') - $sohab3;
		$dispHab4=$consulta_promo->Fields('sc_hab4') - $sohab4;
	 
	 
	 
	 	if($hab1>0){
		
			if($consulta_promo->Fields('sc_hab1') <0){$bloqueado = 1;}
	
			if($dispHab1>= $hab1){ $disponible1 = 1;	}
			else{
				if($dispHab3>= ($hab1+$hab3)){ $disponible1 = 1;	}
				else{$disponible1 = 0;}
			}
	}
	 
	 
	 
	if($hab2>0){
				if($consulta_promo->Fields('sc_hab2') <0){$bloqueado = 1;}
			if($dispHab2>= $hab2){ $disponible2 = 1;	}
			else{$disponible2=0;}
		
	}
	
	
		 
	if($hab3>0){
				if($consulta_promo->Fields('sc_hab3') <0){$bloqueado = 1;}
			if($dispHab3>= $hab3){ $disponible3 = 1;	}
			else{$disponible3=0;}
	}
		 
		 
		
	if($hab4>0){
				if($consulta_promo->Fields('sc_hab4') <0){$bloqueado = 1;}
			if($dispHab4>= $hab4){ $disponible4 = 1;	}
			else{$disponible4=0;}
		
	} 
	
	
	
			if($bloqueado=='1'){
				$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="N";
				
				
			}
			else if($disponible1!='0' && $disponible2!='0' && $disponible3!='0' && $disponible4!='0'){
						// echo 'SI';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="I";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						//echo $consulta_promo->Fields('hd_dbl')*$hab2." | ".$consulta_promo->Fields('hd_dbl')*$hab3."<br>";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
						
					}
					else{
						// echo 'NO';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="R";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
					}
	
	
	
$consulta_promo->MoveNext();}





return $array;

		
}*/

function matrizDisponibilidadCruceLagos($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack){
	//echo "Matiz: $fechaInicio - $fechaFin - $single - $single - $twin - $doble - $triple";
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND a.id_area = 1
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		/*
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=77;
			if($doble>0) $thab3=44;
			if($twin>0) $thab2=44;
			if($triple>0) $thab4=36.66;
		}*/
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}


//Funcion para obtener la url correspondiente, segun el id_tipopack y al id_seg
function get_url($db1,$id_seg,$id_tipopack,$url_tipo){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url
		WHERE id_seg = $id_seg and id_tipopack = $id_tipopack and url_tipo = $url_tipo";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg;
}

function get_url2($db1,$url){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url	WHERE url_direccion = '$url'";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg;
}

//Funcion para verificar si esta en la direccion correcta
function v_url($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot,$estricto = false){
	require('lan/idiomas.php');
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$tempurl2 = get_url2($db1,$url);
	$url_direccion = $tempurl->Fields('url_direccion');
	$url_confirmado = $tempurl->Fields('url_confirmado');
	$url_direccion2 = $tempurl2->Fields('url_direccion');
	$url_confirmado2 = $tempurl2->Fields('url_confirmado');
	
	if($url_confirmado==$url_confirmado2){
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}else{
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.alert('$programaconfirmado.');window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}
}

//Funcion para verificar si esta la cotizacion esta en disponibilidad inmediata o on-request y redirigir si ingreso mal la direccion
function v_or($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$url_direccion = $tempurl->Fields('url_direccion');
	if((substr($url,-2,2)!=substr($url_direccion,-2,2))and(isset($url_direccion))){
		if(stristr($url,"_or")){
			$url = str_replace("_or","",$url);
		}else{
			$url.="_or";
		}
		
		die("<script>window.location='$url.php?id_cot=$id_cot';</script>");
	}
}

//Funcion para redirigir cuando ya esta confirmada una cotizacion
function redir($dir, $seg, $pconf, $pfinal){
	require('lan/idiomas.php');
	if(($seg == 17)or($seg == 20)or($seg == 23)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 18){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if(($seg == 7)or($seg == 19)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 13){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	return false;
}

function mail_disponibilidad($db1,$id_cot){
	$hotocu_q = "SELECT
			hc.id_hotel,
			hc.id_hotdet,
			hc.hc_fecha,
			DATE_FORMAT(hc.hc_fecha, '%d-%m-%Y') AS hc_fecha2,
			tt.tt_nombre,
			th.th_nombre,
			hot.hot_nombre
		FROM
			hotocu AS hc
		INNER JOIN hotdet AS hd ON hd.id_hotdet = hc.id_hotdet
		INNER JOIN tipotarifa AS tt ON tt.id_tipotarifa = hd.id_tipotarifa
		INNER JOIN tipohabitacion AS th ON th.id_tipohabitacion = hd.id_tipohabitacion
		INNER JOIN hotel as hot ON hot.id_hotel = hc.id_hotel
		WHERE
			hc.id_cot = $id_cot
		AND hc.hc_estado = 0";
	$hotocu = $db1->SelectLimit($hotocu_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$id_hotel = 0;
	
	while(!$hotocu->EOF){
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$id_hotel = $hotocu->Fields("id_hotel");
			$send = false;
			$hotel_mail = '
			<table cellpadding="2" cellspacing="0" border="1" align="center">
				<tr>
					<td align="center" valign="middle"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>ALERTA DE DISPONIBILIDAD<br>'.$hotocu->Fields('hot_nombre').'</td>
					<td align="center" colspan="12">Informe de las habitaciones que cuentan con 1 o menos de disponibilidad<br><br>Para modificar la disponibilidad visite este <a href="http://cts.distantis.com/distantis/hot_disponible.php">vinculo</a></td>
				</tr>';
		}
		
		$disp_q = "SELECT sc.sc_hab1, sc.sc_hab2, sc.sc_hab3, sc.sc_hab4, sum(hc.hc_hab1) AS hc_hab1, sum(hc.hc_hab2) AS hc_hab2, sum(hc.hc_hab3) AS hc_hab3, sum(hc.hc_hab4) AS hc_hab4
		FROM stock AS sc
		INNER JOIN hotocu AS hc ON hc.hc_estado = 0 AND hc.id_hotdet = sc.id_hotdet AND hc.hc_fecha = sc.sc_fecha
		WHERE sc.id_hotdet = ".$hotocu->Fields("id_hotdet")." AND sc.sc_estado = 0 AND sc_fecha = '".$hotocu->Fields("hc_fecha")."' GROUP BY hc.hc_fecha";
		$disp = $db1->SelectLimit($disp_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
		
		if($disp->Fields('hc_hab1')==''){$ocu_hab1 = 0;}else{$ocu_hab1 = $disp->Fields('hc_hab1');};
		if($disp->Fields('hc_hab2')==''){$ocu_hab2 = 0;}else{$ocu_hab2 = $disp->Fields('hc_hab2');};
		if($disp->Fields('hc_hab3')==''){$ocu_hab3 = 0;}else{$ocu_hab3 = $disp->Fields('hc_hab3');};
		if($disp->Fields('hc_hab4')==''){$ocu_hab4 = 0;}else{$ocu_hab4 = $disp->Fields('hc_hab4');};
		
		$tot_hab1 = $disp->Fields('sc_hab1')-$ocu_hab1;
		if($tot_hab1<0) $tot_hab1 = 0;
		if($tot_hab1<1){$color1="red";
		}elseif($tot_hab1==1){$color1="yellow";
		}else{$color1="#EEE";}
		
		$tot_hab2 = $disp->Fields('sc_hab2')-$ocu_hab2;
		if($tot_hab2<0) $tot_hab2 = 0;
		if($tot_hab2<1){$color2="red";
		}elseif($tot_hab2==1){$color2="yellow";
		}else{$color2="#EEE";}
		
		$tot_hab3 = $disp->Fields('sc_hab3')-$ocu_hab3;
		if($tot_hab3<0) $tot_hab3 = 0;
		if($tot_hab3<1){$color3="red";
		}elseif($tot_hab3==1){$color3="yellow";
		}else{$color3="#EEE";}
		
		$tot_hab4 = $disp->Fields('sc_hab4')-$ocu_hab4;
		if($tot_hab4<0) $tot_hab4 = 0;
		if($tot_hab4<1){$color4="red";
		}elseif($tot_hab4==1){$color4="yellow";
		}else{$color4="#EEE";}
		
		if($tot_hab1<=1 or $tot_hab2<=1 or $tot_hab3<=1 or $tot_hab4<=1){
			$hotel_mail.= ' 
			<tr>
				<td align="center" valign="middle" rowspan="3" width="200">'.$hotocu->Fields('hc_fecha2')."<br>".$hotocu->Fields('tt_nombre')." - ".$hotocu->Fields('th_nombre').'</td>
				<td align="center" colspan="3" width="100">Single</td>
				<td align="center" colspan="3" width="100">Doble Twin</td>
				<td align="center" colspan="3" width="100">Doble Matrimonial</td>
				<td align="center" colspan="3" width="100">Triple</td>
			</tr>
			<tr>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
			</tr>
			<tr>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab1').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab1').'</td>
                <td align="center" valign="middle" bgcolor="'.$color1.'">'.$tot_hab1.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab2').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab2').'</td>
                <td align="center" valign="middle" bgcolor="'.$color2.'">'.$tot_hab2.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab3').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab3').'</td>
                <td align="center" valign="middle" bgcolor="'.$color3.'">'.$tot_hab3.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab4').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab4').'</td>
                <td align="center" valign="middle" bgcolor="'.$color4.'">'.$tot_hab4.'</td>
			</tr>
            ';
			$send = true;
		}
		
		$hotocu->MoveNext();
		
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$hotel_mail.='</table><center>Este mail fue generado automáticamente por CTS ONLINE, En caso de cualquier duda o consulta, escríbenos un mail a <a href="mailto:cts@distantis.com">cts@distantis.com</a></center>.';
			if($send){
				$hotocu->MoveLast();
				$message_alt='Si ud no puede ver este email por favor contactese con CTS-ONLINE &copy; 2011-2012';
				
				$mail_to = 'ctsonline@vtsystems.cl';
				
				//$mail_to = 'sguzman@vtsystems.cl';
	
				$opSup="SELECT id_usuario,usu_mail FROM usuarios
					WHERE usu_enviar_correo = 1 and not usu_mail = '' and id_empresa = ".$id_hotel;
				$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
				if($rs_opSup->RecordCount()>0){
					while (!$rs_opSup->EOF){
						$mail_to.=";".$rs_opSup->Fields('usu_mail');
						$rs_opSup->MoveNext(); 
					}
					$subject = 'CTS-ONLINE - ALERTA DE DISPONIBILIDAD '.$hotocu->Fields('hot_nombre');
					$ok = envio_mail($mail_to,$copy_to,$subject,$hotel_mail,$message_alt);
				}
				$hotocu->MoveNext();
			}
		}
	}
}

function Cmb_Pais($db1){
	$sqlPais = "
		SELECT id_pais, 
			CASE '".$_SESSION['idioma']."' 
			WHEN 'sp' THEN pai_nombre
			WHEN 'po' THEN pai_noming
			WHEN 'en' THEN pai_noming
			END as pai_nombre 
		FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
	$rsPais = $db1->SelectLimit($sqlPais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $rsPais;
}

function MonedaNombre($db1,$id_mon){
	$mon_q = "SELECT * FROM mon WHERE id_mon = $id_mon";
	$mon = $db1->SelectLimit($mon_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $mon->Fields('mon_nombre');
}

function ErrorPhp($db1,$id_cot,$linea,$error,$url){

	$err = str_replace("'","-",$error);
	$insertSQL = sprintf("INSERT INTO phperror (id_cot, phe_desc, phe_fecha, phe_linea, php_url) VALUES (%s, %s, now(), %s, %s)",
			GetSQLValueString($id_cot, "int"),
			GetSQLValueString($err, "text"),
			//now()
			GetSQLValueString($linea, "int"),
			GetSQLValueString($url, "text")
	);
	//echo "<hr>".$insertSQL."<hr>";
	$insert = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $linea." - ".$error;
}

function addServInit(){
	global $db1,$cot,$ciudad;
	
	$query_ciudad = "SELECT c.ciu_nombre,c.id_ciudad
	FROM trans as t
	inner join transcont tc on tc.id_trans = t.id_trans
	INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
	WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now()
	GROUP BY t.id_ciudad ORDER BY ciu_nombre";
	$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$query_transportes = "SELECT * FROM trans as t
						inner join transcont tc on tc.id_trans = t.id_trans
						INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
						WHERE t.id_area = 1 and tra_estado = 0 AND i.tpt_estado = 0 and t.id_mon = 1 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now() GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
	$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$transportes->EOF){
		if($_SESSION['idioma'] == 'sp'){
			$tra_obs = $transportes->Fields('tra_obs');
		}
		if($_SESSION['idioma'] == 'po'){
			$tra_obs = $transportes->Fields('tra_obspor');
		}
		if($_SESSION['idioma'] == 'en'){
			$tra_obs = $transportes->Fields('tra_obsing');
		}
		$temp[$transportes->Fields('id_ciudad')][] = array('id_trans'=>$transportes->Fields('id_trans'),'id_hotel'=>$transportes->Fields('id_hotel'),'tra_nombre'=>utf8_encode($transportes->Fields('tra_nombre')),'tra_obs'=>utf8_encode($tra_obs));
		$transportes->MoveNext();
	}
	?> 
	<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }
	var transportes = <?= json_encode($temp) ?>;
	function trans(pas){
		var ciudad = $("#id_ciudad_"+pas+" option:selected").val();
		$('#id_ttagrupa_'+pas).empty();
		transportes[ciudad].forEach(function(fn, scope) {
			if(fn['id_hotel']==0){
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'">'+fn['tra_nombre']+'</option>');
			}else{
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'" style="background-color:#FFFF00">'+fn['tra_nombre']+'</option>');
			}
		});
	};
</script>
	<?
}

function addServProc(){
	global $db1,$cot;
	global $advertenciaserv,$noexisteserv;
	
	foreach ($_POST as $keys => $values){    //Search all the post indexes 
		if(strpos($keys,"=")){              //Only edit specific post fields 
			$vars = explode("=",$keys);     //split the name variable at your delimiter
			$_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
			unset($_POST[$keys]);           //unset the temporary post index. 
		} 
	}
	
	if ((isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
		for($v=1;$v<=$_POST['c'];$v++){
				$query = sprintf("
				update cotpas
				set
				cp_nombres=%s,
				cp_apellidos=%s,
				cp_dni=%s,
				id_pais=%s,
				cp_numvuelo=%s
				where
				id_cotpas=%s",
				GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
				GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
				GetSQLValueString($_POST['txt_dni_'.$v], "text"),
				GetSQLValueString($_POST['id_pais_'.$v], "int"),
				GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
				GetSQLValueString($_POST['id_cotpas_'.$v], "int")
				);
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
		
		//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
		if($_POST['pax_max'] == '1'){
			//partimos del c = 1 hasta donde llege el c por POST
			$contador = $_POST['c'];
			$insertarReg=true;
			$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
			$dateasdf = new DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
			$id_trans_sql ="select*
				from trans 
				where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
				and  tra_fachasta >= '".$dateasdf->format('Y-m-d')."' 
				and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
			//echo $id_trans_sql;
			$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
	
			//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
			if($totalRows_id_trans_rs > 0){	
				//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
				for ($x=1; $x <=$contador ; $x++) {
					$verifica_sql = "select*from trans t 
		inner join cotser cs on t.id_trans = cs.id_trans 
		where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
					$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
					if($verifica->RecordCount()>0){
						$insertarReg=false;break;
					}
				}
				if($insertarReg===false){
					echo '<script>
							alert("'.html_entity_decode($advertenciaserv).'");
					</script>';
				}
				for($i = 1 ; $i<= $contador ; $i++){
					$insertSQL = sprintf("INSERT INTO cotser (id_cotpas,id_cotdes, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s,%s, %s, %s, %s, %s, %s, %s ,%s, 0)",
						GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
						GetSQLValueString($_POST['id_cotdes_'.$i], "int"),
						1,
						GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
						GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
						GetSQLValueString($_GET['id_cot'], "int"),
						GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
						GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
						GetSQLValueString($seg, "int"));
						$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				}
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("-'.html_entity_decode($noexisteserv).'.");
				</script>';
			}
		}else{
			$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
			$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
				
			$id_trans_sql ="select*
				from trans 
				where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
				and  tra_fachasta>= '".$dateasdf->format('Y-m-d')."'
				and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
			$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
	
			//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
			if($totalRows_id_trans_rs > 0){
				//validamos de que antes no esté ingresado el mismo servicio
				$verifica_sql = "select*from trans t 
		inner join cotser cs on t.id_trans = cs.id_trans 
		where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
				$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						
				if($verifica->RecordCount() >0){
					echo '<script type="text/javascript">
							alert("'.html_entity_decode($advertenciaserv).'");
					</script>';
				}
				
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas,id_cotdes, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s,%s, %s, %s, %s ,%s, %s, %s ,%s, 0)",
										GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
										GetSQLValueString($_POST['id_cotdes_'.$_POST["agrega"]], "int"),
										1,
										GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
										GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
										GetSQLValueString($_GET['id_cot'], "int"),
										GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
										GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
										GetSQLValueString($seg, "int")							
										);
				$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("-'.html_entity_decode($noexisteserv).'.");
				</script>';
	
			}
		}
		
		ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	}
}

function addServHTML($num_pas){
	global $db1,$destinos,$pasajeros,$ciudad;
	global $crea_serv,$nomserv,$todos_pax,$fechaserv,$destino,$numtrans,$observa,$agregar;
	?>
	<script>
	$(function(){
		$("#datepicker_<?=$num_pas?>").datepicker({
			minDate: new Date(),
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...',
     onSelect: function( selectedDate ) {
      //var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      //dates.not( this ).datepicker( "option", option, date );
      validate("true",this.id);
     }
		});
		
		$('#id_ttagrupa_<?=$num_pas?>').change(function(){
			var selected = $('#id_ttagrupa_<?=$num_pas?> option:selected').attr('title');
			
			if(selected){
				$('#label_ttagrupa_<?=$num_pas?>').show().html('*'+selected);
				alert(selected);
			}else{
				$('#label_ttagrupa_<?=$num_pas?>').hide();
			}
		})
		
		$("#id_ciudad_<?=$num_pas?>").change(function(e) {
			trans(<?=$num_pas?>);
		});
		
		trans(<?=$num_pas?>);
	});
	</script>
	
	<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="2">
                      <select name="id_ttagrupa_<?= $num_pas ?>" id="id_ttagrupa_<?= $num_pas ?>" style="width:480px;"></select>
                      <label id="label_ttagrupa_<?= $num_pas ?>" for="id_ttagrupa_<?= $num_pas ?>" style="color:red;"></label>
                      </td>
                      <td><button name="agrega=<?= $num_pas ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $num_pas==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>
                  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?= $num_pas ?>" name="datepicker_<?= $num_pas ?>" value="<?= $destinos->Fields('cd_fecdesde1') ?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $destino ?> :</td>
                    <td width="316"><select name="id_ciudad_<?= $num_pas ?>" id="id_ciudad_<?= $num_pas ?>" >
                    <? while(!$ciudad->EOF){ ?>
                    <option value="<?= $ciudad->Fields('id_ciudad') ?>" <? if ($ciudad->Fields('id_ciudad') == $destinos->Fields('id_ciudad')) {echo "SELECTED";} ?>><?= $ciudad->Fields('ciu_nombre') ?></option>
                    <? $ciudad->MoveNext(); } $ciudad->MoveFirst(); ?>
                  </select></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $numtrans ?> :</td>
                    <td colspan="3"><input type="text" name="txt_numtrans_<?= $num_pas ?>" id="txt_numtrans_<?= $num_pas ?>" value="" onchange="M(this)" /></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="3"><input type="text" name="txt_obs_<?= $num_pas ?>" id="txt_obs_<?= $num_pas ?>" value="" onchange="M(this)" style="width:500px;" /></td>
                  </tr>
                </table>
<? }

?>