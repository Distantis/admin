<?php
include("secure.php");
require_once("includes/mailing.php");
require_once("Connections/db1.php");

error_reporting(E_ALL);
ini_set('display_errors', '1');
if (isset($_POST['id_cliente'])) {

   $qcliente = "SELECT * FROM clientes WHERE estado = 0 and id_cliente = " . $_POST['id_cliente'];

   $cliente = $db1->SelectLimit($qcliente) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
   $bd = $cliente->Fields('bd');

   $id_usuario = $_SESSION["id"];
   $bd_usuario = $_SESSION["comp"];
   $per_codigo = 4;
   $despues = $_POST['num_conf'];
   $bd_cambio = $bd;
   $id_cambio = $_POST['id_cotdes'];
   $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

   $insertar = "INSERT INTO hoteles.log_h2o(id_usuario, bd_usuario, per_codigo, fecha, id_cambio, bd_cambio, antes, despues, url, nota) 
				 VALUES($id_usuario, '$bd_usuario', $per_codigo, NOW(), $id_cambio,'$bd_cambio','NULL', '$despues', '$url', 'NULL' )";

   $db1->Execute($insertar);


   $qupdate = "UPDATE $bd.cotdes SET cd_numreserva = '" . $_POST['num_conf'] . "' WHERE id_cotdes = " . $_POST['id_cotdes'];

   $db1->Execute($qupdate) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg() . " - <br>$qupdate");
   if ($db1->Affected_Rows() > 0) {
      $qcotdes = " SELECT * FROM $bd.cotdes WHERE id_cotdes =" . $_POST['id_cotdes'];
      $rcotdes = $db1->SelectLimit($qcotdes) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
      $qcot = " SELECT * FROM $bd.cot WHERE id_cot =" . $rcotdes->Fields("id_cot");
      $rcot = $db1->SelectLimit($qcot) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
      if ($_POST['id_cliente'] == 5 && $rcot->Fields("id_usuario") == 4316) {
         require_once('/var/www/cocha/xmlWebServices/Travelio/confirma_reserva.php');
         $setconf = confirmaReserva($db1, $rcotdes->Fields("id_cot"), $_POST['num_conf'], "set_conf");
         function chantalo($este, $i)
         {
            $texto = "";
            foreach ($este as $wea1 => $wea2) {
               if (is_object($wea2)) {
                  chantalo($wea2, $i + 1);
               } else {
                  $texto .= "<$wea1>$wea2</$wea1>";
               }
            }
            return $texto;
         }

         $texto = chantalo($setconf, 0);
         $insert = ' INSERT INTO cocha.log_xml(xml, id_cot, fecha, operador) 
			values("' . $texto . '", ' . $rcot->Fields("id_cot") . ', now(), ' . $rcot->Fields("id_operador") . ')';

         $db1->Execute($insert) or die($db1->ErrorMsg());
      }
      die("Correcto");
   } else {
      die("Error: " . $qupdate);
   }
}


$qclientes = "SELECT * FROM clientes WHERE estado = 0";
$clientes = $db1->SelectLimit($qclientes) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());

if (!isset($_POST['filtrar'])) {
   $qdates = "SELECT DATE_FORMAT(NOW(),'%Y') as ano, date_format(NOW(),'%m') as mes";
   $dates = $db1->SelectLimit($qdates) or die($_SERVER['REQUEST_URI'] . " - " . __LINE__ . " : " . $db1->ErrorMsg());
   $ano = $dates->Fields('ano');
   $mes = $dates->Fields('mes');
} else {
   if (strlen($_POST['mes']) == 1) {
      $mes = '0' . $_POST['mes'];
   } else {
      $mes = $_POST['mes'];
   }
   $ano = $_POST['ano'];
}
$cont_html = "";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!--meta content="90" http-equiv="REFRESH"> </meta!-->
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css"/>
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css"/>
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <link href="prueba/css/bootstrap.css" rel="stylesheet">
    <link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css"/>
    <link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
    <link href="prueba/css/datepicker.css" rel="stylesheet" type="text/css"/>
    <link href="prueba/css/jquery.gritter.css" rel="stylesheet">
    <link href="prueba/css/style_gritter.css" rel="stylesheet">
    <link rel="stylesheet" href="prueba/css/protip.min.css">
    <link rel="stylesheet" href="prueba/css/jquery.mCustomScrollbar.css" type="text/css"/>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>


<body>
<div id="container" class="inner">
    <div id="header">
       <? include("bienvenida.php"); ?>
        <ul id="nav">
            <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt; ?>" class="tooltip">On
                    Request</a></li>
            <li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt; ?>" class="tooltip">Nueva
                    Tarifa</a></li>
            <li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt; ?>" class="tooltip">Empresas</a>
            </li>
            <li class="servicios activo"><a href="nums_conf.php" title="<? echo $servind_tt; ?>" class="tooltip">N Conf
                    Pendiente</a></li>
            <li class="servicios activo"><a href="anulados.php" title="<? echo $servind_tt; ?>" class="tooltip">Reservas
                    anuladas</a></li>
        </ul>

        <ol id="pasos">

        </ol>
    </div>

    <div style="margin-left:auto;margin-right:auto;width: 930px;">
        <div id="nueva" width='930px'>

            <br><br>
            <div class="alert alert-warning alert-dismissable">
                <center><strong>Números Confirmación Pendientes</strong></center>
            </div>
            <br>

            <table class="" align="center">

                <tr>

                    <td>
                        <input type='text' name='desde' placeholder="Fecha desde" data-date-format="dd-mm-yyyy"
                               class='datepicker' id='desde' size='10'>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <input type='text' name='hasta' placeholder="Fecha hasta" data-date-format="dd-mm-yyyy"
                               class='datepicker' id='hasta' size='10'>
                    </td>
                </tr>

                <tr>
                    <td><br></td>
                </tr>

                <tr>
                    <td>
                        <button id="filtrar" class="btn btn-success">Filtrar</button>
                    </td>
                </tr>

            </table>


            <br>

            <a href='#' title='Descargar Excel'><i id='excel'
                                                   class='fa fa-file-excel-o fa-2x text-success pull-left'></i></a>


            <br>

            <div class='pull-right'><input type='text' id="id_cot" placeholder="Ingrese N° cot">&nbsp;&nbsp;&nbsp;<button
                        id="buscar_cot" class="btn btn-warning">Buscar
                </button>
            </div>
            <br><br><br>


            <div><input type="checkbox" class="sobresiete">
                <small>Reservas sobre 7 días</small>
            </div>
            <br><br>

            <div id="nmr_conf_pendiente"></div>
            <br>


        </div>
    </div>
</div>


<!-- Nifty Modal -->
<div class="md-modal  custom-width md-effect-5" id="form-observacion">
    <div class="md-content" style="background-color:#ffffff !important; color: #555;">
        <div class="modal-header colored-header">
            <div class="modal-title">
                <h3>
                    <center>Observación</center>
                </h3>
                <br>
                <small> Agregar Observación.</small>
                <br>
            </div>
        </div>
        <div class="modal-body form mCustomScrollbar" data-mcs-theme="dark" id="form-agregar-promocion-body">
            <br><br>

            <div class="col-md-12">
                <div class="col-md-10">

                    <textarea placeholder="Ingresar Observación" class="form-control" id="obs"></textarea>
                    <br>
                    <input type="hidden" id="cot">
                    <input type="hidden" id="operador">
                    <center>
                        <button class="btn btn-success" id="guardar_obs">Guardar</button>
                    </center>
                    <br>

                    <i class="fa fa-info-circle text-warning pull-left"></i> <font size="2px" class="pull-left">Observaciones</font><br>
                    <div id="observaciones"></div>

                    <br><br><br><br>

                </div>
            </div>
            <br><br>
        </div>
    </div>

</div>

<div class="md-overlay"></div>

</body>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="prueba/js/protip.min.js"></script>
<script type="text/javascript" charset="utf-8"
        src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script src="prueba/js/jquery.datatables.sorting-plugins.js" type="text/javascript"></script>
<script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
<script type="text/javascript" src="prueba/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="prueba/js/traer_num_conf.js?<?= strtotime(date("YmdHis")) ?>" type="text/javascript"></script>
<script src="prueba/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>


</html>