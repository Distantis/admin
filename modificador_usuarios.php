<?php

include('includes/funciones.distantis.php');

$modificador_usuarios = new ModificadorUsuarios();

class ModificadorUsuarios{
    
    private $datos,$datos_usuario = array();
    private $markup,$otas;

    public function __construct(){
        
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
        require_once('/var/www/otas/clases/tarifa.php');
        require_once('/var/www/admin/Connections/db1.php');
        $this->set_conectar($db1);
        $this->post();
        

        
    }
    
    protected function set_conectar($valor){
  
        $this->sql_con = $valor;
        $this->otas = new Tarifa();
   }

    


   protected function post(){

      extract($_POST);

      $this->datos_usuario["tipo"] = $tipo;
      $this->datos_usuario["id_hotel"] = $id_hotel;
      $this->datos_usuario["bd"] = $bd;
      $this->datos_usuario["pk"] = $pk;
      $this->datos_usuario["cliente"] = $cliente;
      $this->datos_usuario["id_empresa"] = $id_empresa;
      $this->datos_usuario["login"] = $login;
      $this->datos_usuario["pass"] = $pass;
      $this->datos_usuario["nombre"] = $nombre;
      $this->datos_usuario["pat"] = $pat;
      $this->datos_usuario["mat"] = $mat;
      $this->datos_usuario["idioma"] = $idioma;
      $this->datos_usuario["mail"] = $mail;
      $this->datos_usuario["id_usuario"] = $id_usuario;
      $this->datos_usuario["nombre"] = $nombre;
      $this->datos_usuario["porcentaje"] = $porcentaje;
      $this->datos_usuario["tipo_habitacion"] = $tipo_habitacion;
      $this->datos_usuario["habitacion_quitar_dispo"] = $habitacion_quitar_dispo;
      $this->datos_usuario["habitaciones_porcentaje"] = $habitaciones_porcentaje;
      $this->datos_usuario["habitacion"] = $habitacion;
      $this->datos_usuario["set_competitivo"] = $set_competitivo;
      $this->datos_usuario["habitacion"] = $habitacion;
      $this->datos_usuario["cerrar_abrir"] = $cerrar_abrir;
      $this->datos_usuario["existe"] = $existe;
      $this->datos_usuario["desde"] = $desde;
      $this->datos_usuario["hasta"] = $hasta;
      $this->datos_usuario["id"] = $id;


      $this->traer($tipo);

   }
    

    
    public function traer($tipo){
        
        
        switch($tipo){
            
            
            case 1:
                 
              $this->traer_hoteles();
            
            break;


            case 2:
                 
              $this->traer_detalle_usuarios();
            
            break;

            case 3:
                 
              $this->replicar_usuario();
            
            break;

            case 4:
                 
              $this->editar_usuario();
            
            break;

            case 5:
                 
              $this->eliminar_usuario();
            
            break;

            case 6:
                 
              $this->buscar_tipo_habitacion();
            
            break;

             case 7:
                 
              $this->traer_hoteles_global();
            
            break;

            case 8:
              
              $this->guardar_porcentaje_channel();
            
            break;

            case 9:
                 
              $this->guardar_doble_precio_channel();
            
            break;
            
            case 10:
                 
              $this->traer_confirmaciones_or();
            
            break;
            
            case 11:
                 
              $this->traer_confirmaciones_or_set_competitivo();
            
            break;

            case 12:
                 
              $this->traer_habitacion_channel();
            
            break;

            case 13:
                 
              $this->guardar_habitacion_channel();
            
            break;

            case 14:
                 
              $this->crear_hotdet_paridad();
            
            break;

             case 15:
                 
              $this->traer_porcentaje_hotel();
            
            break;

            case 16:
                 
              $this->eliminar_porcentaje_hotel();
            
            break;


        }
        
   
        
    }


    protected function eliminar_porcentaje_hotel(){

      $actualizar = "update hoteles.porcentaje_dispo set estado = 1 where id = ".$this->datos_usuario["id"]." ";
      $actualizando = $this->sql_con->Execute($actualizar) or $this->errores(__LINE__);


      if($actualizando)
        $this->datos["respuesta"] = 1;
      else
        $this->datos["respuesta"] = 0;

    }

    protected function traer_porcentaje_hotel(){

        $this->datos["informacion"] = array();

        $consulta = "select * from hoteles.porcentaje_dispo where id_pk = ".$this->datos_usuario["pk"]." and id_tipohabitacion = ".$this->datos_usuario["tipo_habitacion"]." and estado = 0";
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

       while(!$traer->EOF){

          $hab_quitar = $traer->Fields("hab_quitar_disp");
          $porcentaje_quitar = $traer->Fields("porcentaje");

          $hab_para = $traer->fields("hab_para");


          foreach(unserialize($hab_para) as $info){

             $datos = array(
                            "hab"=>$info["hab"],
                            "porcentaje"=>$info["porcentaje"],
                            "hab_quitar"=>$hab_quitar,
                            "porcentaje_quitar"=>$porcentaje_quitar,
                            "id"=>$traer->Fields("id")
                            );  

            array_push($this->datos["informacion"], $datos);           

          }

           $traer->MoveNext();

       }


    }

    protected function crear_hotdet_paridad(){

        $hoy = date("Y-m-d",strtotime($this->datos_usuario["desde"]));
        $despues = date("Y-m-d",strtotime($this->datos_usuario["hasta"]));


        $consulta_cliente = "select * from hoteles.clientes";
        $traer_cliente = $this->sql_con->SelectLimit($consulta_cliente) or $this->errores(__LINE__);

        $this->datos_usuario["hotdet_creado"] = 0;
        $this->datos_usuario["stock_creado"] = 0;

        while(!$traer_cliente->EOF){

          $id_cliente = $traer_cliente->Fields("id_cliente");
          $nombre = $traer_cliente->Fields("nombre");
          $bd = $traer_cliente->Fields("bd");


          $consulta_hotel = "select id_hotel_$nombre as hotel from hoteles.hotelesmerge where id_pk = ".$this->datos_usuario["pk"]." ";
          $traer_hotel = $this->sql_con->SelectLimit($consulta_hotel) or $this->errores(__LINE__);

          $hotel = $traer_hotel->Fields("hotel");
          $tipotarifa = $this->traer_idtarifa_paridad($bd);
          $hab = $this->traer_idhabitacion_operador($bd);


            if($hotel != 0){

                if( $hab!= ""){

                    for($i=$hoy;$i<=$despues;$i=date("Y-m-d", strtotime($i ."+ 1 days"))){

                        $consulta = "select * from $bd.hotdet where id_hotel = $hotel and hd_fecdesde = '$i' and hd_fechasta = '$i' and id_tipotarifa = $tipotarifa and id_tipohabitacion = $hab  ";
                        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

                        if($traer->RecordCount() == 0){

                          $estado = 1;

                          if($bd=="distantis");
                              $estado = 0;
                          
                          $insertar = "insert into $bd.hotdet(id_hotel,hd_fecdesde,hd_fechasta,hd_sgl,hd_dbl,hd_tpl,hd_mon,id_tipotarifa,id_tipohabitacion,hd_estado)";
                          $insertar.= "values($hotel,'$i','$i','999.0','999.0','0.0',1,$tipotarifa,$hab,$estado)";  

                          $guardar = $this->sql_con->Execute($insertar);

                          if($guardar){ 

                            $buscar_hotdet = $this->buscar_hotdet($bd);
                            $this->ingresar_stock($bd,$buscar_hotdet,$i);

                            $this->otas->TarHandler($this->sql_con,$id_cliente,$buscar_hotdet);

                            $this->datos_usuario["hotdet_creado"]+=1;
                          }

                        }

                    }
                }

            }    

          $traer_cliente->MoveNext();
        }


        if($this->datos_usuario["hotdet_creado"] == 0 or $this->datos_usuario["stock_creado"] == 0)
          $this->datos["respuesta"] = 2;
        elseif($this->datos_usuario["hotdet_creado"] == $this->datos_usuario["stock_creado"])
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;

    }

    protected function traer_idhabitacion_operador($bd){

      $consulta = "select id_tipohabitacion from $bd.tipohabitacion where id_tipohab = ".$this->datos_usuario["tipo_habitacion"]." ";
      $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

      return $traer->Fields("id_tipohabitacion");

    }


    protected function traer_idtarifa_paridad($bd){

      $consulta = "select id_tipotarifa from $bd.tipotarifa where tt_nombre = 'PARIDAD' ";
      $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

      return $traer->Fields("id_tipotarifa");

    }

    protected function ingresar_stock($bd,$hotdet,$fecha){

      $insertar = "insert into $bd.stock(id_hotdet,sc_fecha,sc_hab1,sc_hab2,sc_hab3,sc_hab4,sc_cerrado)";
      $insertar .= "values($hotdet,'$fecha',0,0,0,0,1)";

      $guardar = $this->sql_con->Execute($insertar);

      if($guardar)
        $this->datos_usuario["stock_creado"] += 1;

    }


    protected function buscar_hotdet($bd){

      $consulta = "select max(id_hotdet) as ultimo from $bd.hotdet";
      $traer = $this->sql_con->SelectLimit($consulta);


      return $traer->Fields("ultimo");


    }

    protected function traer_habitacion_channel(){

      $consulta = "select * from hoteles.dispo_por_ocup where id_pk = ".$this->datos_usuario["pk"]."  and id_tipohab =  ".$this->datos_usuario["tipo_habitacion"]." ";
      $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

      $this->datos["habitacion"] = array();

        while(!$traer->EOF){  

            $datos = array("sgl"=>$traer->Fields("ocup_sgl"),
                          "twn"=>$traer->Fields("ocup_twn"),
                          "dbl"=>$traer->Fields("ocup_dbl"),
                          "tpl"=>$traer->Fields("ocup_tpl")
                          );
            array_push($this->datos["habitacion"], $datos);

            $traer->MoveNext();
        }

    }


    protected function guardar_habitacion_channel(){


        if($this->datos_usuario["existe"] == 1){

            $act = "update hoteles.dispo_por_ocup set ocup_".$this->datos_usuario["habitacion"]." = ".$this->datos_usuario["cerrar_abrir"]." where id_pk = ".$this->datos_usuario["pk"]." and id_tipohab = ".$this->datos_usuario["tipo_habitacion"]." ";
            $actualizar = $this->sql_con->Execute($act) or $this->errores(__LINE__);

            if($actualizar)
              $this->datos["respuesta"] = 1;
            else  
              $this->datos["respuesta"] = 0;

        }else{
         

            $guardar = "insert into hoteles.dispo_por_ocup (id_pk,id_tipohab,ocup_".$this->datos_usuario["habitacion"].")
                    values (".$this->datos_usuario["pk"].",".$this->datos_usuario["tipo_habitacion"].",1)";

            $guardando = $this->sql_con->Execute($guardar) or $this->errores(__LINE__);


            $actualizar_modo_pk = "update hoteles.hotelesmerge set modo_pk = 1 where id_pk = ".$this->datos_usuario["pk"]." ";
            $ac_modo_pk = $this->sql_con->Execute($actualizar_modo_pk) or $this->errores(__LINE__);

            if($guardando and $ac_modo_pk){
              $this->datos["respuesta"] = 1;
              $this->datos["existe"] = 1;
            }else  
              $this->datos["respuesta"] = 0;



        }

    }

    protected function traer_confirmaciones_or_set_competitivo(){


        $fecha = new DateTime();
        $ano_anterior = "2018";
        $ano = date("Y");
        $mes_anterior = date("m",strtotime("-1 month"));
        $mes = date("F",strtotime("-1 month"));

        $fecha->modify('last day of '.$mes.' '.$ano.'');

        $cantidad_confirmaciones = 0;
        $cantidad_or = 0;


        $cantidad_confirmaciones_mes_anterior = 0;
        $cantidad_or_mes_anterior = 0;

        $this->datos["informacion"] = array();
        foreach($this->datos_usuario["set_competitivo"] as $hotel_pk){

            $pk = $hotel_pk;

            $total_or_pk = 0;
            $total_confirmaciones_pk = 0;

            $total_or_pk_anterior = 0;
            $total_confirmaciones_pk_anterior = 0;

            $consulta_cliente = "select * from hoteles.clientes";
            $traer_clientes = $this->sql_con->SelectLimit($consulta_cliente);

            while(!$traer_clientes->EOF){

              $nombre = $traer_clientes->Fields("nombre");
              $bd = $traer_clientes->Fields("bd");


              

                $consulta = "select id_hotel_$nombre as hotel from hoteles.hotelesmerge where id_pk = ".$pk." ";
                $traer_hotel = $this->sql_con->SelectLimit($consulta);
               

                 /*$consulta_or = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16 and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20170101' and '20171231' ";
       

                $traer_or = $this->sql_con->SelectLimit($consulta_or);

                $consulta_conf = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20170101' and '20171231' ";*/

               
                  
                  $consulta_or = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16 and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano."0101' and '".$fecha->format('Y-m-d')."' ";
       

                $traer_or = $this->sql_con->SelectLimit($consulta_or);

                $consulta_conf = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano."0101' and '".$fecha->format('Y-m-d')."' ";

          

                $traer_conf = $this->sql_con->SelectLimit($consulta_conf);

                $cantidad_or+=$traer_or->Fields("total");
                $cantidad_confirmaciones+=$traer_conf->Fields("total");

                $total_or_pk +=$traer_or->Fields("total");
                $total_confirmaciones_pk+=$traer_conf->Fields("total");

                // =================================== SOLO MES ANTERIOR =========================================




                /*$consulta_or_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20171201' and '20171231' ";

             

                $traer_or_anterior = $this->sql_con->SelectLimit($consulta_or_anterior);

                $consulta_conf_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20171201' and '20171231' ";*/


                $consulta_or_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior.$mes_anterior."01' and '".$fecha->format('Y-m-d')."' ";

             

                $traer_or_anterior = $this->sql_con->SelectLimit($consulta_or_anterior);

                $consulta_conf_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior.$mes_anterior."01' and '".$fecha->format('Y-m-d')."' ";

              


                $traer_conf_anterior = $this->sql_con->SelectLimit($consulta_conf_anterior);

                $cantidad_or_mes_anterior+=$traer_or_anterior->Fields("total");
                $cantidad_confirmaciones_mes_anterior+=$traer_conf_anterior->Fields("total");

                $total_or_pk_anterior +=$traer_or_anterior->Fields("total");
                $total_confirmaciones_pk_anterior+=$traer_conf_anterior->Fields("total");

              $traer_clientes->MoveNext();
            }


            $datos = array("pk"=>$pk,
                          "total_or"=>$total_or_pk,
                          "total_confirmaciones"=>$total_confirmaciones_pk,
                          "total_or_anterior"=>$total_or_pk_anterior,
                          "total_confirmaciones_anterior"=>$total_confirmaciones_pk_anterior  
                          );
            array_push($this->datos["informacion"], $datos);
        }


        $this->datos["total_or_set"] = $cantidad_or;
        $this->datos["total_confirmaciones_set"] = $cantidad_confirmaciones;

        $this->datos["total_or_anterior_set"] = $cantidad_or_mes_anterior;
        $this->datos["total_confirmaciones_anterior_set"] = $cantidad_confirmaciones_mes_anterior;


    }


    protected function traer_confirmaciones_or(){

        $consulta_cliente = "select * from hoteles.clientes";
        $traer_clientes = $this->sql_con->SelectLimit($consulta_cliente);


        $fecha = new DateTime();
        $ano_anterior = "2018";
        $ano = date("Y");
        $mes_anterior = date("m",strtotime("-1 month"));
        $mes = date("F",strtotime("-1 month"));

        $fecha->modify('last day of '.$mes.' '.$ano_anterior.'');

        $cantidad_confirmaciones = 0;
        $cantidad_or = 0;


        $cantidad_confirmaciones_mes_anterior = 0;
        $cantidad_or_mes_anterior = 0;

        while(!$traer_clientes->EOF){

          $nombre = $traer_clientes->Fields("nombre");
          $bd = $traer_clientes->Fields("bd");

          $consulta = "select id_hotel_$nombre as hotel from hoteles.hotelesmerge where id_pk = ".$this->datos_usuario["pk"]." ";
          $traer_hotel = $this->sql_con->SelectLimit($consulta);

          /*$consulta_or = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and cd_estado  = 0 and cd.id_seg = 13 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano."0101' and '".$fecha->format('Y-m-d')."' ";

          $consulta_or = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16 and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20170101' and '20171231' ";

          $traer_or = $this->sql_con->SelectLimit($consulta_or);

          $consulta_conf = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20170101' and '20171231' ";*/



       

          $consulta_or = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16 and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior."0101' and '".$fecha->format('Y-m-d')."' ";


          $traer_or = $this->sql_con->SelectLimit($consulta_or);

          $consulta_conf = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior."0101' and '".$fecha->format('Y-m-d')."' ";

      

          $traer_conf = $this->sql_con->SelectLimit($consulta_conf);

          $cantidad_or+=$traer_or->Fields("total");
          $cantidad_confirmaciones+=$traer_conf->Fields("total");

          // =================================== SOLO MES ANTERIOR =========================================




          /*$consulta_or_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20171201' and '20171231' ";


          $traer_or_anterior = $this->sql_con->SelectLimit($consulta_or_anterior);

          $consulta_conf_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '20171201' and '20171231' ";*/


         
            
            $consulta_or_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 1 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior.$mes_anterior."01' and '".$fecha->format('Y-m-d')."' ";


          $traer_or_anterior = $this->sql_con->SelectLimit($consulta_or_anterior);

          $consulta_conf_anterior = "select count(*) as total from $bd.cot c join $bd.cotdes cd on cd.id_cot = c.id_cot where cot_estado = 0 and c.id_seg != 16  and cd_estado  = 0 and c.cot_or = 0 and id_hotel = ".$traer_hotel->Fields("hotel")." and cot_fecconf between '".$ano_anterior.$mes_anterior."01' and '".$fecha->format('Y-m-d')."' ";


         


          $traer_conf_anterior = $this->sql_con->SelectLimit($consulta_conf_anterior);

          $cantidad_or_mes_anterior+=$traer_or_anterior->Fields("total");
          $cantidad_confirmaciones_mes_anterior+=$traer_conf_anterior->Fields("total");



          $traer_clientes->MoveNext();
        }


        $this->datos["total_or"] = $cantidad_or;
        $this->datos["total_confirmaciones"] = $cantidad_confirmaciones;

        $this->datos["total_or_anterior"] = $cantidad_or_mes_anterior;
        $this->datos["total_confirmaciones_anterior"] = $cantidad_confirmaciones_mes_anterior;


    }




     protected function guardar_doble_precio_channel(){

       if($this->datos_usuario["habitacion"] == "twn")
        $tipo = 2;
       else
        $tipo = 3;

        $fecha = date("Y-m-d H:i:s");

        $agregar = "insert into hoteles.precio_dbl_channel(id_channel,id_pk,tipo,creacion) values ('1','".$this->datos_usuario["pk"]."','$tipo','$fecha') ";
        $guardar = $this->sql_con->Execute($agregar);

        if($guardar)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


     }


    protected function guardar_porcentaje_channel(){


        $consulta = "
                    select * from hoteles.porcentaje_dispo 
                    where id_pk = ".$this->datos_usuario["pk"]."
                    and id_tipohabitacion = ".$this->datos_usuario["tipo_habitacion"]." 
                    and hab_quitar_disp = '".$this->datos_usuario["habitacion_quitar_dispo"]."'
                    and hab_para = '".serialize($this->datos_usuario["habitaciones_porcentaje"])."'
                    and estado = 0
                  ";

        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        if($traer->RecordCount() == 0){

            $act = "update hoteles.porcentaje_dispo set estado = 1 where id_pk = ".$this->datos_usuario["pk"]." and id_tipohabitacion = ".$this->datos_usuario["tipo_habitacion"]." and hab_quitar_disp = '".$this->datos_usuario["habitacion_quitar_dispo"]."' ";
            $actualizar = $this->sql_con->Execute($act) or $this->errores(__LINE__);

            if($actualizar){

                  $guardar = "insert into hoteles.porcentaje_dispo (id_pk,id_tipohabitacion,porcentaje,hab_quitar_disp,hab_para)
                              values (".$this->datos_usuario["pk"].",".$this->datos_usuario["tipo_habitacion"].",'".$this->datos_usuario["porcentaje"]."','".$this->datos_usuario["habitacion_quitar_dispo"]."','".serialize($this->datos_usuario["habitaciones_porcentaje"])."')
                              ";
                  
                  $insertar = $this->sql_con->Execute($guardar) or $this->errores(__LINE__);

                  if($insertar)
                    $this->datos["respuesta"] = 1;
                  else
                    $this->datos["respuesta"] = 0;

            }else
              $this->datos["respuesta"] = 0;

      }else
        $this->datos["respuesta"] = 2;

    }


    protected function buscar_tipo_habitacion(){

      $consulta  = "select hd.id_tipohabitacion,th_nombre from hoteles.hotdet hd join hoteles.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion where id_pk = ".$this->datos_usuario["pk"]." and hd_estado = 0 group by th.id_tipohabitacion";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["tipo_habitacion"] = array();

      while(!$traer->EOF){

        $hab = $traer->Fields("id_tipohabitacion");
        $th_nombre = $traer->Fields("th_nombre");

        $dato = array("hab"=>$hab,"nombre"=>$th_nombre);
        array_push($this->datos["tipo_habitacion"], $dato);


        $traer->MoveNext();
      }
 

    }


     protected function eliminar_usuario(){


        $delete = "update ".$this->datos_usuario["cliente"].".usuarios set 
                  usu_estado = 1
                  where id_usuario = ".$this->datos_usuario["id_usuario"]." ";
        
        $eliminar = $this->sql_con->Execute($delete);


        if($eliminar)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function editar_usuario(){


        $act = "update ".$this->datos_usuario["cliente"].".usuarios set
                  usu_nombre = '".$this->datos_usuario["nombre"]."',
                  usu_login = '".$this->datos_usuario["login"]."',
                  usu_password = '".$this->datos_usuario["pass"]."', 
                  usu_mail = '".$this->datos_usuario["mail"]."',
                  usu_pat= '".$this->datos_usuario["pat"]."' 
                  where id_usuario = ".$this->datos_usuario["id_usuario"]." ";
        
        $actualizar = $this->sql_con->Execute($act);


        if($actualizar)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function replicar_usuario(){


       $con = "select * from hoteles.clientes where id_cliente = ".$this->datos_usuario["cliente"]." ";
       $cliente = $this->sql_con->SelectLimit($con);

       $bd = $cliente->Fields("bd");
       $nombre_cliente = $cliente->Fields("nombre");


       $co = "select id_hotel_$nombre_cliente as hotel from hoteles.hotelesmerge where id_pk = ".$this->datos_usuario["pk"]." ";
       $ve = $this->sql_con->SelectLimit($co);

       $hot = $ve->Fields("hotel");

       $consulta = "select * from $bd.usuarios where usu_login = '".$this->datos_usuario["login"]."' 
                      and usu_password = '".$this->datos_usuario["pass"]."'
                      and usu_mail = '".$this->datos_usuario["mail"]."'
                      and usu_estado = 0
                      and id_empresa = $hot
                    ";
       
       $verificar = $this->sql_con->SelectLimit($consulta);

       if($verificar->RecordCount() == 0){


         $con = "select id_hotel_$nombre_cliente as hotel from hoteles.hotelesmerge where id_pk = ".$this->datos_usuario["pk"]." ";
         $ver = $this->sql_con->SelectLimit($con);

         $hotel = $ver->Fields("hotel");

             if($hotel != 0){

                  $inser = "insert into $bd.usuarios (id_empresa,usu_login,usu_password,usu_nombre,usu_pat,usu_mat,id_tipo,usu_idioma,usu_mail,usu_enviar_correo)
                            values ('$hotel',
                                    '".$this->datos_usuario["login"]."',
                                    '".$this->datos_usuario["pass"]."',
                                    '".$this->datos_usuario["nombre"]."',
                                    '".$this->datos_usuario["pat"]."',
                                    '".$this->datos_usuario["mat"]."',
                                    '2',
                                    '".$this->datos_usuario["idioma"]."',
                                    '".$this->datos_usuario["mail"]."',
                                    1
                                    )
                            ";

                  $insertar = $this->sql_con->Execute($inser);

                  if($insertar){

                      $this->datos["respuesta"] = 1;
                      $this->datos["operador"] = $nombre_cliente;

                  }else
                      $this->datos["respuesta"] = 0;

            }else
               $this->datos["respuesta"] = 3;



       }else{
           $this->datos["operador"] = $nombre_cliente;
           $this->datos["respuesta"] = 2;
        }


    }


    protected function traer_detalle_usuarios(){


      $consulta = "select * from hoteles.hotelesmerge where id_pk =  '".$this->datos_usuario["pk"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["usuarios"] = array();

      while(!$traer->EOF){


          $user = array(
                            "distantis" => $traer->Fields("id_hotel_cts"),
                            "veci" => $traer->Fields("id_hotel_veci"),
                            "otsi" => $traer->Fields("id_hotel_otsi"),
                            "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                            "cocha" => $traer->Fields("id_hotel_cocha"),
                            "travelclub" => $traer->Fields("id_hotel_travelclub"),
                            "carlson" => $traer->Fields("id_hotel_carlson"),
                            "mundotour" => $traer->Fields("id_hotel_mundotour"),
                            "tourmundial" => $traer->Fields("id_hotel_tourmundial"),
                            "euroandino" => $traer->Fields("id_hotel_euroandino")
                    );

     
        $this->buscar_usuarios($user);


        $traer->MoveNext();



      }



      $unicos = array_unique($this->datos_usuario["usuarios"], SORT_REGULAR);


        foreach($unicos as $datos){


            array_push($this->datos["usuarios"], $datos);
          
        }




    }


    protected function buscar_usuarios($usuarios){

      $this->datos_usuario["usuarios"] = array();
      foreach($usuarios as $key=>$datos){
         
            if($datos != 0){

               $consulta = "select * from $key.usuarios where id_empresa = $datos and usu_estado = 0 and id_tipo  = 2";
               $traer = $this->sql_con->SelectLimit($consulta);

               while(!$traer->EOF){

                 $nombre = "";
                 $app = "";
                 $apm = "";
                 $mail = "";
                 $correos_todos = "";

                 if($traer->Fields("usu_nombre") == "null" or $traer->Fields("usu_nombre") == null or $traer->Fields("usu_nombre") == "")
                      $nombre = "";
                 else
                      $nombre = $traer->Fields("usu_nombre");

                 if($traer->Fields("usu_pat") == "null" or $traer->Fields("usu_pat") == null or $traer->Fields("usu_pat") == "")
                      $app = "";
                 else
                      $app = $traer->Fields("usu_pat");

                 if($traer->Fields("usu_mat") == "null" or $traer->Fields("usu_mat") == null or $traer->Fields("usu_mat") == "")
                      $apm = "";
                 else
                      $apm = $traer->Fields("usu_mat");

                 if($traer->Fields("usu_mail") == "null" or $traer->Fields("usu_mail") == null or $traer->Fields("usu_mail") == ""){
                      $mail = "";
                      $correos_todos = "Sin correo<br>";
                 }else{
                      $mail = $traer->Fields("usu_mail");

                      $todos = explode(";",$traer->Fields("usu_mail"));
                      $correos_todos = "";
                      foreach($todos as $correos){
                        $correos_todos .= $correos."<br>";
                      }
                 }


                 if($traer->Fields("usu_enviar_correo") == 0)
                      $enviar_correo = 1;
                 else
                      $enviar_correo = 0;

                   $user = trim(utf8_encode($traer->Fields("usu_login")));
                   $pass = trim($traer->Fields("usu_password"));

                  $datos = array(
                                  "nombre"=>utf8_encode($traer->Fields("usu_nombre")." ".$traer->Fields("usu_pat")),
                                  "login"=> $user,
                                  "pass"=> $pass,
                                  "mail"=>trim($mail),
                                  "cliente"=>$key,
                                  "id_empresa"=>$traer->Fields("id_empresa"),
                                  "nombre"=>utf8_encode($nombre),
                                  "pat"=>utf8_encode($app),
                                  "mat"=>utf8_encode($apm),
                                  "idioma"=>$traer->Fields("usu_idioma"),
                                  "id_usuario"=>$traer->Fields("id_usuario"),
                                  "correo"=>$enviar_correo,
                                  "correos_todos"=>$correos_todos,
                                  "url_credenciales_codificados" => encode_get('user='.$user.'&pass='.$pass)
                                );

                  array_push($this->datos_usuario["usuarios"], $datos);

                  $traer->MoveNext();

               }



               

            }

        }



    }


    protected function traer_hoteles_global(){


      $consulta = "select * from hoteles.hotelesmerge where ver = 0";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hoteles"] = array();

      while(!$traer->EOF){

      $hoteles = array(
                          "distantis" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travelclub" => $traer->Fields("id_hotel_travelclub"),
                          "carlson" => $traer->Fields("id_hotel_carlson"),
                          "mundotour" => $traer->Fields("id_hotel_mundotour"),
                          "tourmundial" => $traer->Fields("id_hotel_tourmundial"),
                          "euroandino" => $traer->Fields("id_hotel_euroandino")
                  );


        $nombre_hotel = $this->nombre_hotel($hoteles);



        if($nombre_hotel != "NADA"){

             $datos = array(
                    
                    "nombre_hotel" => utf8_encode($nombre_hotel),
                    "id_pk"=>$traer->Fields("id_pk"),
                    "bd" =>$this->datos_usuario["bd"],
                    "id_hotel"=>$this->datos_usuario["id_hotel"],
                    "channel"=>$this->verificar_si_existe_channel($traer->Fields("id_pk"))

                  );            


            array_push($this->datos["hoteles"], $datos);

        }


        $traer->MoveNext();
      }


    }


     protected function verificar_si_existe_channel($pk){

        $consulta = "select channel from hoteles.chandet de join hoteles.channel cha on cha.id_channel = de.id_channel where id_pk = ".$pk." ";
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $retornar = "";

        if($traer->RecordCount() > 0)
            $retornar = $traer->Fields("channel");

        return $retornar;
    }



   

    protected function traer_hoteles(){


      $consulta = "select * from hoteles.hotelesmerge";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hoteles"] = array();

      while(!$traer->EOF){

      $hoteles = array(
                          "distantis" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travelclub" => $traer->Fields("id_hotel_travelclub"),
                          "carlson" => $traer->Fields("id_hotel_carlson"),
                          "mundotour" => $traer->Fields("id_hotel_mundotour"),
                          "tourmundial" => $traer->Fields("id_hotel_tourmundial"),
                          "euroandino" => $traer->Fields("id_hotel_euroandino")
                  );


        $nombre_hotel = $this->nombre_hotel($hoteles);



        if($nombre_hotel != "NADA"){

             $datos = array(
                    
                    "nombre_hotel" => utf8_encode($nombre_hotel),
                    "id_pk"=>$traer->Fields("id_pk"),
                    "bd" =>$this->datos_usuario["bd"],
                    "id_hotel"=>$this->datos_usuario["id_hotel"],
                    "channel"=>$this->verificar_si_existe_channel($traer->Fields("id_pk"))

                  );            


            array_push($this->datos["hoteles"], $datos);

        }


        $traer->MoveNext();
      }


    }


    protected function nombre_hotel($hoteles){
        
        $bd = "";
        $id_hotel = "";
        $revisar = 0;
        foreach($hoteles as $key=>$datos){
         
            if($datos != 0 and $revisar == 0){
              $bd = $key;
              $id_hotel = $datos;
              $revisar = 1;
            }

        }


       if($bd!= "" and $id_hotel != ""){
        
         $consulta = "select hot_nombre from $bd.hotel where id_hotel = $id_hotel ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $nombre_hotel = $traer->Fields("hot_nombre");

         $this->datos_usuario["bd"] = $bd;
         $this->datos_usuario["id_hotel"] = $id_hotel;

       }else
          $nombre_hotel = "NADA";


       return $nombre_hotel;
       
    }


     protected function errores($linea){
        
        die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
    }

   

    function __destruct(){
         $this->sql_con->close();
         //ob_end_clean();

         echo json_encode($this->datos); 
    }
    


}


?>