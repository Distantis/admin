$(document).on("ready", function(){
    
    
    
   $('.datepicker').datepicker();
   $("#monto").numeric();
   $("#nmr").numeric();
   
    
   $("#eliminar").on("click", function(){
        
       $("#nombre_socio").html("");
       $("#rut").attr('disabled', false);
       $("#tipo").val(1);
       
   });
    
    
});



$("#formulario").on("submit", function(event){
    
     var formData = new FormData($("#formulario")[0]);
     
     $(this).find("#enviar").html("Creando certificado...  <i class='fa fa-gear fa-spin'></i>");
    
    

    $.ajax({
        url: 'clases/crear_certificado.php',
        type: 'POST',
        data: formData,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        dataType:'JSON',
        success: function (data) {

                
             if (data.respuesta == 0) {
                mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar los datos.</label>", "danger", "bottom-right");
                 $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
          
            }
            
            
            if (data.respuesta == 2) {
                mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Faltan Campos.</label>", "danger", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
        
            }
            
            if (data.respuesta == 3) {
                mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>El rut ingresado no existe.</label>", "warning", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
                $("#tipo").val('1');
              
            }
            
            
            
            if (data.respuesta == 5) {
               
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
                $("#tipo").val('2');
                $("#rut").attr('disabled', true);
                $("#rut2").val(data.rut);
                $("#socio").val(data.nombre_usuario);
                $("#nombre_usu").val(data.nombre_usuario);
            }
            
            
            
            
            if (data.respuesta == 6) {
                mostrar_notificacion("Exito", "<label style='color:white !important;font-size:13px'>Datos guardados correctamente.</label>", "success", "bottom-right");
                $("#enviar").attr('disabled', true);
                setTimeout(function () {
                    window.location.reload(1);
                }, 2000);
            }
            
            
            if (data.respuesta == 7) {
                mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Faltan Campos.</label>", "danger", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
           
            }
            
            
            
            if (data.respuesta == 8) {
                mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Fecha Vigencia formato incorrecto.</label>", "warning", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
           
            }
            
            if (data.respuesta == 9) {
                mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Fecha Emisión formato incorrecto.</label>", "warning", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
           
            }
            
            
             if (data.respuesta == 4) {
                mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>El nmr ingresado ya existe.</label>", "warning", "bottom-right");
                $("#enviar").html("Crear certificado <i class='fa fa-chevron-circle-right'></i>");
           
            }
            
            
            
            
            
        }
        
        
    });
    
    event.preventDefault();
});





