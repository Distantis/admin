$("#texto_cantidad").hide();
var menos = 0;
var todo = [];

$(document).on("ready", function () {


   $(".md-modal").mCustomScrollbar({
      setHeight: 600,
      theme: "minimal-dark"
   });

   $.protip();

   $('#desde').datepicker().on('changeDate', function (e) {
      $(this).datepicker('hide');
      var newDate = new Date(e.date);

      var dias = traer_dias((newDate.getDate() + 1));
      var mes = traer_meses((newDate.getMonth() + 1));

      secondDate.val(dias + "-" + mes + "-" + newDate.getFullYear());
      secondDate.datepicker('update');
   });


   var secondDate = $('#hasta').datepicker().on('changeDate', function (e) {
      $(this).datepicker('hide');
   });


   armar_confirmaciones(0, 0, 0, 0);


});


$("#excel").on("click", function () {


   window.open("excel.php?tipo=1", '_blank');

});


$("#filtrar").on("click", function () {

   var desde = $("#desde").val();
   var hasta = $("#hasta").val();


   if (desde != "" || hasta != "")
      armar_confirmaciones(1, desde, hasta, 0);


});

$("#buscar_cot").on("click", function () {

   var cot = $("#id_cot").val();


   if (cot != "")
      armar_confirmaciones(2, 0, 0, cot);


});


function armar_confirmaciones(verficar, desde, hasta, cot) {

   todo.length = 0;
   $("#nmr_conf_pendiente").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");

   var enviando = "";

   if (verficar == 1)
      enviando = {verificando_sitio: 1, tipo: 2, desde: desde, hasta: hasta};
   else if (verficar == 2)
      enviando = {verificando_sitio: 1, tipo: 3, cot: cot};
   else
      enviando = {verificando_sitio: 1, tipo: 1};


   $.post("obtener_datos_numconf.php", enviando,

      function (data) {

         data = $.parseJSON(data);

         todo.push(data);
         //$.each(data, function(j, e){

         /*$.each(e, function (i, datos) {

            var estado = "";
            var op = "";

             if(datos.operador == "touravion_dev")
                op = "turavion";
             else if(datos.operador == "distantis")
                op = "cts";
             else
                op = datos.operador;

                if(datos.hotel != "JG DISTANTIS (HOTEL PRUEBA NO VALIDO)" && datos.hotel != "HOTEL DE PRUEBA - USO INTERNO OTSI" && datos.hotel!="NB RESORT y SPA(Hotel de Prueba)"){


                   if(datos.fono == "")
                      info = "<span class='agregar_fono"+datos.cadena+"'><input type='text' placeholder='Ingresa Fono' id='fono"+datos.cadena+"' class='quitar_fono"+datos.cadena+"'><i class='fa fa-arrow-circle-right text-danger pull-right ingresar_fono quitar_fono"+datos.cadena+"' id='"+datos.cadena+"'></i></span>";
                   else
                      info = datos.fono;




                   var color = "class='estado_dia"+datos.estado_dia+"'";

                   if(datos.estado_dia == 1)
                      color = "class='bg-danger estado_dia"+datos.estado_dia+"'";


                  tab += "<tr id='"+datos.cliente+"_"+datos.cotdes+"' "+color+">"
                              +"<td align='right'>"+op.toUpperCase()+"</td>"
                                   +"<td align='right'>"+datos.cot+"</td>"
                                   +"<td align='center'><i class='fa fa-info-circle text-warning protip' id='"+datos.hace_cuanto+"' data-pt-position='bottom' data-pt-title='"+datos.hace_cuanto+"' data-pt-scheme='blue'></i></td>"
                                   +'<td align="left">'+datos.hotel+'</td>'
                                   +'<td align="left"></td>'
                                   +"<td align='left'>"+info+"</td>"
                                   +"<td align='left'>"+datos.pax+"</td>"
                                   +'<td align="left"><center><i class="fa fa-group text-warning protip" data-pt-position="bottom" data-pt-title="SINGLE: '+datos.hab1+'<br> TWIN: '+datos.hab2+'<br> DBL: '+datos.hab3+'<br> TRIPLE: '+datos.hab4+'"></i></center></td>'
                                   +'<td align="left">'+datos.desde+'</td>'
                                   +'<td align="left">'+datos.hasta+'</td>'
                                   +'<td align="left">'
                                   +'<center>'
                             +'<input type="text" name="'+datos.cliente+'_'+datos.cotdes+'_numres" id="'+datos.cliente+'_'+datos.cotdes+'_numres" value='+datos.num_reserva+'><br><br>'
                                +'<button name="conf_num" class="btn btn-success" style="width:125px; height:27px"  onClick="conf_cdnum('+datos.cliente+','+datos.cotdes+')">Ingresar Conf</button>'
                            +'</center>'
                          +'</td>'

                           +"</tr>"
                }


         });*/

         //});


         armar_tabla(1);


         /*tab += '</tbody></table>';
              $("#nmr_conf_pendiente").html(tab);

              $(".revisar_desde").on("click", function(){

               var valor = $(this).attr("id");

               mostrar_notificacion('Desde', '<b>'+valor+'</b>', 'warning','bottom-left');

            });


              var table = $("#tabla_informacion").DataTable({
                      "columnDefs": [
                          { "type": "date-euro", "targets": [ 8,9 ] }
                      ],
                         "oLanguage": {
                                  "sProcessing": "Procesando...",
                                  "sLengthMenu": "Mostrar _MENU_ ",
                                  "sZeroRecords": "No se han encontrado datos disponibles",
                                  "sEmptyTable": "No se han encontrado datos disponibles",
                                  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                  "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                  "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                  "sInfoPostFix": "",
                                  "sSearch": "Buscar: ",
                                  "sUrl": "",
                                  "sInfoThousands": ",",
                                  "sLoadingRecords": "Cargando...",
                                  "oPaginate": {
                                      "sFirst": "Primero",
                                      "sLast": "Último",
                                      "sNext": "Siguiente",
                                      "sPrevious": "Anterior"
                                  },
                                  "oAria": {
                                      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                  }
                              }
                              /*"aoColumns": [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        { "sType": "date-uk" },
                        { "sType": "date-uk" },
                        null
                     ]*/
         //});


      }
   ).done(function () {


      $(document).on("click", ".ingresar_fono", function () {

         var cadena = $(this).attr("id");
         var fono = $("#fono" + cadena).val();

         if (fono != "") {


            $.post("obtener_datos_numconf.php", {verificando_sitio: 1, tipo: 4, cadena: cadena, fono: fono},

               function (data) {

                  data = $.parseJSON(data);

                  if (data.respuesta == 1) {

                     $(".quitar_fono" + cadena).remove();
                     $(".agregar_fono" + cadena).text(fono);

                     mostrar_notificacion('Éxito', 'Teléfono agregado correctamente', 'success', 'bottom-left');

                  } else
                     mostrar_notificacion('ERROR', 'Hubo un error al ingresar la información', 'danger', 'bottom-left');

               });


         } else
            mostrar_notificacion('Advertencia', 'Debe agregar un teléfono', 'warning', 'bottom-left');

      });


   });


}


function armar_tabla(tipo) {

   $("#cargar").addClass("fa-spinner fa-spin");

   var tab = '<table class="table table-bordered responsive" id="tabla_informacion" width="100%"><thead>'
      + '<tr>'
      + '<th><center>Operador</center></th>'
      + '<th><center>Cot</center></th>'
      + '<th><center>ID Compr.</center></th>'
      + '<th><center>Compr.</center></th>'
      + '<th><center>Desde</center></th>'
      + '<th><center>Hotel</center></th>'
      + '<th>Nemo</th>'
      + '<th>TMA</th>'
      + '<th><center>Fono</center></th>'
      + '<th><center>Nombre pax</center></th>'
      + '<th><center>Habs</center></th>'
      + '<th><center>Fec In</center></th>'
      + '<th><center>Fec Out</center></th>'
      + '<th><center>Nº Confirmación</center></th>'
      + '<th><center>obs</center></th>'
      + '</tr></thead><tbody>';

   $.each(todo, function (j, e) {
      $.each(e, function (m, d) {
         $.each(d, function (i, datos) {


            var estado = "";
            var op = "";

            if (datos.operador == "touravion_dev")
               op = "turavion";
            else if (datos.operador == "distantis")
               op = "cts";
            else
               op = datos.operador;

            //if(datos.hotel != "JG DISTANTIS (HOTEL PRUEBA NO VALIDO)" && datos.hotel != "HOTEL DE PRUEBA - USO INTERNO OTSI" && datos.hotel!="NB RESORT y SPA(Hotel de Prueba)"){


            if (datos.fono == "")
               info = "<span class='agregar_fono" + datos.cadena + "'><input type='text' placeholder='Ingresa Fono' id='fono" + datos.cadena + "' class='quitar_fono" + datos.cadena + "'><i class='fa fa-arrow-circle-right text-danger pull-right ingresar_fono quitar_fono" + datos.cadena + "' id='" + datos.cadena + "'></i></span>";
            else
               info = datos.fono;


            var color = "class='estado_dia" + datos.estado_dia + "'";

            if (datos.estado_dia == 1)
               color = "class='bg-danger estado_dia" + datos.estado_dia + "'";

            var empresa = "";

            if (datos.empresa != "") {
               empresa = datos.empresa["nemo"];
            }

            var comprador = "<td align='right'></td>";
            if (datos.id_comprador !== null)
               comprador = "<td align='right'>" + datos.id_comprador + "</td>";

            var agencia = "<td align='right'></td>";
            if (datos.agencia !== null)
               agencia = "<td align='right'>" + datos.agencia + "</td>";


            var obs = "";

            if (datos.verificar_obs == 0)
               obs = "<i  class='fa fa-envelope traer_observaciones ver_obs text-primary' id='obs_" + datos.cot + "' data-id='" + datos.cot + "' data-operador = '" + datos.nombre + "' data-modal='form-observacion'></i>";
            else
               obs = "<i  class='fa fa-envelope traer_observaciones ver_obs text-danger' id='obs_" + datos.cot + "' data-id='" + datos.cot + "' data-operador = '" + datos.nombre + "' data-modal='form-observacion'></i>";


            switch (tipo) {

               case 1:
                  tab += "<tr id='" + datos.cliente + "_" + datos.cotdes + "' " + color + ">"
                     + "<td align='right'>" + op + "</td>"
                     + "<td align='right'>" + datos.cot + "</td>"
                     + comprador
                     + agencia
                     + "<td align='center'><i class='fa fa-info-circle text-warning protip' id='" + datos.hace_cuanto + "' data-pt-position='bottom' data-pt-title='" + datos.hace_cuanto + "' data-pt-scheme='blue'></i>"+datos.cd_fecdesde+"</td>"
                     + '<td align="left">' + datos.hotel + '</td>'
                     + '<td align="left">' + empresa + '</td>'
                     + '<td align="center"><small>' + datos.tma + '</small></td>'
                     + "<td align='left'>" + info + "</td>"
                     + "<td align='left'>" + datos.pax + "</td>"
                     + '<td align="left"><center><i class="fa fa-group text-warning protip" data-pt-position="bottom" data-pt-title="SINGLE: ' + datos.hab1 + '<br> TWIN: ' + datos.hab2 + '<br> DBL: ' + datos.hab3 + '<br> TRIPLE: ' + datos.hab4 + '"></i></center></td>'
                     + '<td align="left">' + datos.desde + '</td>'
                     + '<td align="left">' + datos.hasta + '</td>'
                     + '<td align="left">'
                     + '<center>'
                     + '<input type="text" name="' + datos.cliente + '_' + datos.cotdes + '_numres" id="' + datos.cliente + '_' + datos.cotdes + '_numres" value=' + datos.num_reserva + '><br><br>'
                     + '<button name="conf_num" class="btn btn-success" style="width:125px; height:27px"  onClick="conf_cdnum(' + datos.cliente + ',' + datos.cotdes + ')">Ingresar Conf</button>'
                     + '</center>'
                     + '</td>'
                     + "<td align='center'>" + obs + "</td>"

                     + "</tr>"
                  break;

               case 2:

                  if (datos.estado_dia == 1) {

                     tab += "<tr id='" + datos.cliente + "_" + datos.cotdes + "' " + color + ">"
                        + "<td align='right'>" + op + "</td>"
                        + "<td align='right'>" + datos.cot + "</td>"
                        + "<td align='right'>" + datos.id_comprador + "</td>"
                        + "<td align='right'>" + datos.agencia + "</td>"
                        + "<td align='center'><i class='fa fa-info-circle text-warning protip' id='" + datos.hace_cuanto + "' data-pt-position='bottom' data-pt-title='" + datos.hace_cuanto + "' data-pt-scheme='blue'></i></td>"
                        + '<td align="left">' + datos.hotel + '</td>'
                        + '<td align="left"></td>'
                        + '<td align="center"><small>' + datos.tma + '</small></td>'
                        + "<td align='left'>" + info + "</td>"
                        + "<td align='left'>" + datos.pax + "</td>"
                        + '<td align="left"><center><i class="fa fa-group text-warning protip" data-pt-position="bottom" data-pt-title="SINGLE: ' + datos.hab1 + '<br> TWIN: ' + datos.hab2 + '<br> DBL: ' + datos.hab3 + '<br> TRIPLE: ' + datos.hab4 + '"></i></center></td>'
                        + '<td align="left">' + datos.desde + '</td>'
                        + '<td align="left">' + datos.hasta + '</td>'
                        + '<td align="left">'
                        + '<center>'
                        + '<input type="text" name="' + datos.cliente + '_' + datos.cotdes + '_numres" id="' + datos.cliente + '_' + datos.cotdes + '_numres" value=' + datos.num_reserva + '><br><br>'
                        + '<button name="conf_num" class="btn btn-success" style="width:125px; height:27px"  onClick="conf_cdnum(' + datos.cliente + ',' + datos.cotdes + ')">Ingresar Conf</button>'
                        + '</center>'
                        + '</td>'
                        + "<td align='center'>" + obs + "</td>"

                        + "</tr>"

                  }

                  break;


            }


            //}


         });
      });
   });


   tab += '</tbody></table>';
   $("#nmr_conf_pendiente").html(tab);

   $(".revisar_desde").on("click", function () {

      var valor = $(this).attr("id");

      mostrar_notificacion('Desde', '<b>' + valor + '</b>', 'warning', 'bottom-left');

   });

   $(".ver_obs").modalEffects();
   var table = $("#tabla_informacion").DataTable({
      "columnDefs": [
         {"type": "date-euro", "targets": [11, 12]}
      ],
      "oLanguage": {
         "sProcessing": "Procesando...",
         "sLengthMenu": "Mostrar _MENU_ ",
         "sZeroRecords": "No se han encontrado datos disponibles",
         "sEmptyTable": "No se han encontrado datos disponibles",
         "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
         "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
         "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
         "sInfoPostFix": "",
         "sSearch": "Buscar: ",
         "sUrl": "",
         "sInfoThousands": ",",
         "sLoadingRecords": "Cargando...",
         "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
         },
         "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
         }
      },
      "initComplete": function (settings, json) {
         //$("#cargar").removeClass("fa-spinner fa-spin");
      }
      /*"aoColumns": [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    { "sType": "date-uk" },
    { "sType": "date-uk" },
    null
 ]*/
   });
   $(".ver_obs").modalEffects();


}


$(document).on("click", '.traer_observaciones', function () {

   var operador = $(this).attr("data-operador");
   var cot = $(this).attr("data-id");


   $("#cot").val(cot);
   $("#operador").val(operador);

   $("#observaciones").html("<i class='fa fa-spinner fa-spin'></i>");


   /*var tabla ='<table class="table table-bordered" id="tabla_observacion" width="100%"><thead>'
                           +'<tr>'
                               +'<th>Cot</th>'
                               +'<th>Operador</th>'
                               +'<th>Obs</th>'
                               +'<th>Fecha</th>'
                           +'</tr></thead><tbody>';*/


   $.post("obtener_datos_or.php", {
         verificando_sitio: 1, tipo: 6, id_cot: cot, guardar_tipo: 2
      },
      function (data) {

         data = $.parseJSON(data);
         var informacion = "";


         if (data.obs == "") {

            informacion += "<div class='alert alert-danger alert-dismissable'>"
               + "<center>Sin observaciones</center>"
               + "</div><br>";

         }


         $.each(data.obs, function (i, datos) {

            /*tabla += "<tr>"
                            +"<td align='right'>"+datos.cot+"</td>"
                            +'<td align="left">'+datos.operador+'</td>'
                            +'<td align="left">'+datos.observacion+'</td>'
                            +'<td align="left">'+datos.creacion+'</td>'
                    +"</tr>"*/


            if (datos.observacion != "") {


               informacion += "<div class='alert alert-warning alert-dismissable'>"
                  + "<center>" + datos.observacion + "</center>"
                  + "<br>"
                  + "<p class='pull-right'><font size='1px'>" + datos.usuario + " " + datos.creacion + "</font></p><br>"
                  + "</div><br>";
            }


         });


         $("#observaciones").html(informacion);

      }
   ).done(function () {


   });

});


$("#guardar_obs").on("click", function () {

   var operador = $("#operador").val();
   var cot = $("#cot").val();
   var obs = $("#obs").val();

   if (obs == "") {
      mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No dejar campo vacio.</label>", "warning", "bottom-right");
   } else {

      $.post("obtener_datos_or.php", {
            verificando_sitio: 1, tipo: 7, operador: operador, id_cot: cot, obs: obs, guardar_tipo: 2
         },
         function (data) {

            data = $.parseJSON(data);

            if (data.respuesta == 1) {
               mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Dato guardado correctamente.</label>", "success", "bottom-right");
               $("#obs").val("");
               $("#obs_" + cot).removeClass("text-primary").addClass("text-danger");
               $("#form-observacion").removeClass("md-show");
            } else
               mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar guardar la información.</label>", "danger", "bottom-right");


         });


   }

});


$(document).on("change", ".sobresiete", function () {

   if ($(this).is(':checked')) {

      /*$("#texto_cantidad").show();
      var rows = table.$("tr");
      var cantidad = 0;
      $(rows).each(function(i,datos){
         var clase = $(this).attr("class");
         var sumar = clase.indexOf("estado_dia1");

         if(sumar >= 0)
            cantidad += 1;

      });

      $("#cantidad").html(cantidad-menos);*/
      $("#nmr_conf_pendiente").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");
      armar_tabla(2);


   } else {

      //$("#texto_cantidad").hide();
      $("#nmr_conf_pendiente").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");
      armar_tabla(1);
   }

});


function conf_cdnum(idcliente, idcotdes) {

   var valor = $("#" + idcliente + "_" + idcotdes + "_numres").val();

   if (valor == "") {

      mostrar_notificacion('Advertencia', 'Debe ingresar al menos un dato.', 'warning', 'bottom-right');
   } else {


      $.ajax({
         type: 'POST',
         url: 'nums_conf.php',
         async: false,
         data: {
            id_cliente: idcliente,
            id_cotdes: idcotdes,
            num_conf: valor
         },
         success: function (result) {
            //alert(result);
            mostrar_notificacion('Éxito', 'Dato ingresado correctamente.', 'success', 'bottom-right');
            $("#" + idcliente + "_" + idcotdes).html("");
            menos += 1;
            var valor_cantidad = $("#cantidad").text();
            $("#cantidad").html(valor_cantidad - 1);


            var eliminar = idcotdes;
            var id_cliente = idcliente;

            $.each(todo, function (j, e) {
               $.each(e, function (m, d) {
                  $.each(d, function (i, datos) {

                     if (datos.cotdes == eliminar && datos.cliente == id_cliente) {

                        todo[j][m][i] = $.grep(todo[j][m][i], function (data, index) {
                           return data.cotdes != eliminar
                        });

                     }
                  });
               });
            });


         },
         error: function () {
            //alert("Error al actualizar");
            mostrar_notificacion('Error', 'Error al actualizar.', 'danger', 'bottom-right');
         }
      });


   }


}


function traer_dias(dia) {

   var nuevo_dia = "";

   switch (dia) {

      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:

         nuevo_dia = "0" + dia;
         break;

      default:
         nuevo_dia = dia;
         break;

   }


   return nuevo_dia;


}


function traer_meses(mes) {

   var nuevo_mes = "";

   switch (mes) {

      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:

         nuevo_mes = "0" + mes;
         break;

      default:
         nuevo_mes = mes;
         break;

   }


   return nuevo_mes;


}

