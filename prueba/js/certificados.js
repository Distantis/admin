$(document).ready(function () {
  
    
    var url = 'clases/traercertificados.php';
    var verificando_sitio = 1;
    
    function currencyFormat(num, extra) {
             if (isNumber(num) == true)
                 return extra + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
             return "no definido";
    }

     function isNumber(input) {
             return (input - 0) == input && ('' + input).replace(/^\s+|\s+$/g, "").length > 0;
     }

    
    
    var tabla ='<table class="table table-bordered responsive" id="tabla_certificados"><thead>'
                    +'<tr>'
                        +'<th>Nº</th>'
                        +'<th>Rut</th>'
                        +'<th>Nombre</th>'
                        +'<th>Fecha Prepago</th>'
                        +'<th>Fecha Emisión</th>'
                        +'<th>Monto</th>'
                        +'<th>Estado</th>'
                        +'<th>Cambiar</th>'
                    +'</tr></thead><tbody>';
    


                $.post(url, {
                        verificando_sitio: verificando_sitio
                    },
                    function (data) {
                        data = $.parseJSON(data);
                    
                    
                        var estados;
                            
                        $.each(data.certificados, function (i, datos) {
                            
                            
                            
                            if(datos.estado == 1){
                                
                                estados = "<span class='text-default'><i class='fa fa-ticket'></i> EMITIDA</span>";
                                
                            }else if(datos.estado == 2){
                                
                                
                                estados = "<span class='text-danger'><i class='fa fa-times'></i> IMPAGO</span>";
                                
                            }else if(datos.estado == 3){
                                
                                
                                estados = "<span class='text-warning'><i class='fa fa-warning'></i> ABONADO</span>";
                                
                            }else if(datos.estado == 4){
                                
                                
                                estados = "<span class='text-success'><i class='fa fa-check'></i> PAGADO</span>";
                                
                            }
                         
        
                            tabla += "<tr>"
                                        +"<td align='right'>"+datos.nmr+"</td>"
                                        +"<td align='right'>"+datos.rut+"</td>"
                                        +"<td align='right'>"+datos.nombre+"</td>"
                                        +"<td align='center'>"+datos.fecha1+"</td>"
                                        +"<td align='center'>"+datos.fecha2+"</td>"
                                        +"<td align='right'>"+currencyFormat(datos.monto,'$')+"</td>"
                                        +"<td align='center'>"+estados+"</td>"
                                        +"<td align='center'><a onClick='agregar_usu("+datos.id+")'><i class='text-warning fa fa-edit fa-2x traer_certificado' data-modal='form-editar-certificado'></i></a></td>"
                                    +"</tr>"
                            
                            
                           
                            
                        });
                    
                    
                    
                    tabla += '</tbody></table>';
                    $("#certificados").html(tabla);
                    var table = $("#tabla_certificados").DataTable({
                            "aaSorting": [[0, 'asc']],
                            "oLanguage": {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ ",
                                "sZeroRecords": "No se han encontrado datos disponibles",
                                "sEmptyTable": "No se han encontrado datos disponibles",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar: ",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                    });
                            
                            
                    $(".traer_certificado").modalEffects();
                    
                    
                    
                    $('#tabla_creditos tbody').on( 'click', 'tr', function () {
                        
                        
                              if ( $(this).hasClass('selected') ) {
                                    $(this).removeClass('selected');
                                    
                                }
                                else {
                                    table.$('tr.selected').removeClass('selected');
                                    $(this).addClass('selected');
                                    //$(this).css('background-color','red');
                                }
                        
                        
                         $('#eliminar').click( function () {
                            table.row('.selected').remove().draw( false );
                         } );
                        
                        
                    } );
                
                    
                }

     }); //ajax

    
});

function agregar_usu(usu){
    
    $("#usuario").val(usu);   
}


