$(document).on("ready", function(){
    
    //$("#hoteles_seleccion").hide();
    $("#hote").html("<i>Cargando hoteles</i>  <i class='fa fa-spinner fa-spin'></i>");
    $("#hotels").hide();
    
    $("#id_hotdet").append("<option value=''>Seleccionar hotdet..</option>");
    
    $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:1
                },
                function (data) {

                    data = $.parseJSON(data);
                    var hotel = '<option value=\'\'></option>';
                    
                    $.each(data.hoteles1, function (i, datos) {
                        
                      hotel += '<option  value="' + datos.id_hotel + '">' + datos.nombre_hotel + '</option>';
                    
                    });
                    
                    $("#hotel").html(hotel);
                    $("#hoteles").html(hotel);
        
                   
                }
             
             ).done(function () {
                  
                  $("#hotels").show();
                  $("#hote").hide();
                  $('select#hotel').select2({
                
                        placeholder: "Seleccione Hotel"
                
                  });
        
                  $('select#hoteles').select2({
                
                        placeholder: "Seleccione Hotel"
                
                  });
                 

             });
    
});



    

$("#hotel").on("change", function(){
   
    var valor = $(this).val();
    
      $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:2, idhotel: valor
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data == null || data === undefined || data==""){
                        
                        mostrar_notificacion('Advertencia', 'El hotel que ha seleccionado no ha cargado tarifas.', 'warning','bottom-right');
                     
                    }
          
                    var idhotdet = '<option value=\'\'>Seleccionar hotdet..</option>';
                    
                    $.each(data, function (i, datos) {
                                         
                            idhotdet += '<option  value="' + datos.id_hotdet + '"> (' + datos.id_hotdet + ')</option>';
                         
                    
                    });
                    
                    $("#id_hotdet").html(idhotdet);
                    
                    
                   
                }
             
             ).done(function () {
                 
                
                 
                 

             });
    
    
});



$("#hoteles").on("change", function(){
   
    var valor = $(this).val();
    
      $.post("obtener_hoteles.php", {
                    verificando_sitio: 1, tipo:3, idhotel: valor
                },
                function (data) {

                    data = $.parseJSON(data);
                    
                    if(data == null || data === undefined || data==""){
                        
                        mostrar_notificacion('Advertencia', 'El hotel que ha seleccionado no ha cargado tarifas.', 'warning','bottom-right');
                     
                    }
          
                    if(data.porcentaje == null){
                        
                        $("#p_hb").val(0);
                      
                    }else{
                        
                        $("#p_hb").val(data.porcentaje);
                      
                    }
                    
                    
                   
                }
             
             ).done(function () {
                 
                
                 
                 

             });
    
    
});




$("#p_hb").on("keyup", function(){
    
   
    var valor = $(this).val();

    if(valor>100){
         
        mostrar_notificacion('Advertencia', 'El porcentaje no puede superar el 100%.', 'warning','bottom-right');
        $("#p_hb").val("");
    }
    
    
});