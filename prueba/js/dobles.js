

$(document).on("ready",function(){

		$(".habitaciones_porcentaje").attr("disabled",true);
		$(".p_activar").attr("disabled",true).numeric();
		$(".desactivados").attr("disabled",true).addClass("disabled");
		$("#guardar").addClass("disabled");
		$('select#hoteles').select2({

                    placeholder: "Seleccione Hotel"

        });

        $('select#tipo_habitacion').select2({

                    placeholder: "Seleccione Tipo Habitación"

        });

		$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:7
            },
            function (data) {

                data = $.parseJSON(data);
                var hotel = '<option value=\'\'></option>';

                $.each(data.hoteles, function (i, datos) {

                  hotel += '<option  value="'+datos.id_pk+'" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">' + datos.nombre_hotel + '</option>';

                });

              
                $("#hoteles").html(hotel);


            }).done(function () {

      		  $(".desactivados").attr("disabled",false).removeClass("disabled");
           
         });

});



$(".habitaciones").on("change", function(){

	var hotel = $("#hoteles").val();

	if($(this).is(":checked")){

		var id = $(this).attr("id");
		$(".habitaciones_porcentaje").attr("disabled",false);
		

		$(".habitaciones").each(function(){
			if(!$(this).is(":checked"))
				$(this).attr("disabled",true);
		});


		if(hotel != "")
			$("#guardar").removeClass("disabled");

		
	}else{
		$(".habitaciones").attr("disabled",false);
		$("#guardar").addClass("disabled");
	}
	



});





$("#guardar").on("click",function(){

	var habitacion = "";
	var hotel = $("#hoteles").val();

	
	if($(".habitaciones").is(":checked"))
		habitacion = $(".habitaciones:checked").attr("id");


	if(habitacion != ""){

			$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:9,pk : hotel, habitacion : habitacion
            },
            function (data) {

                data = $.parseJSON(data);
                
              	if(data.respuesta == 1){
              		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Habitacion agregada correctamente.</label>", "success", "bottom-left");
              		$(".habitaciones").prop('checked', false);
              		$("#guardar").addClass("disabled");
              	}else
              		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al guardar la información.</label>", "danger", "bottom-left");


            }).done(function () {

      		  	
           
         	});


	}else
		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Debe completar los campos.</label>", "warning", "bottom-left");


});


