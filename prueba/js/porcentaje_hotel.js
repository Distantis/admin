

$(document).on("ready",function(){

		$(".habitaciones_porcentaje").attr("disabled",true);
		$(".p_activar").attr("disabled",true).numeric();
		$(".desactivados").attr("disabled",true).addClass("disabled");
		$("#guardar").addClass("disabled");
		$('select#hoteles').select2({

                    placeholder: "Seleccione Hotel"

        });

        $('select#tipo_habitacion').select2({

                    placeholder: "Seleccione Tipo Habitación"

        });

		$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:7
            },
            function (data) {

                data = $.parseJSON(data);
                var hotel = '<option value=\'\'></option>';

                $.each(data.hoteles, function (i, datos) {

                  hotel += '<option  value="'+datos.id_pk+'" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">' + datos.nombre_hotel + '</option>';

                });

              
                $("#hoteles").html(hotel);


            }).done(function () {

      		  $(".desactivados").attr("disabled",false).removeClass("disabled");
           
         });

});

$("#hoteles").on("change", function(){

	var pk = $(this).val();
	$("#tipo_habitacion").select2("val", "");

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:6,pk:pk
            },
            function (data) {

                data = $.parseJSON(data);
                var habitacion = '<option value=\'\'></option>';

                $.each(data.tipo_habitacion, function (i, datos) {

                  habitacion += '<option  value="' + datos.hab + '" >' + datos.nombre + '</option>';

                });

              
                $("#tipo_habitacion").html(habitacion);


            });

	

});


$(".habitaciones").on("change", function(){

	if($(this).is(":checked")){

		var id = $(this).attr("id");
		$("#tipo_"+id).removeClass("habitaciones_porcentaje").addClass("habitaciones_porcentaje_sacar");
		$(".habitaciones_porcentaje").attr("disabled",false);
		

		$(".habitaciones").each(function(){
			if(!$(this).is(":checked"))
				$(this).attr("disabled",true);
		});

		
	}else{
		$(".habitaciones_porcentaje_sacar").removeClass("habitaciones_porcentaje_sacar").addClass("habitaciones_porcentaje");
		$(".habitaciones_porcentaje").attr("disabled",true).attr("checked",false);
		$(".habitaciones").attr("disabled",false);
		$(".p_activar").attr("disabled",true).val("");
	}



});


$("#tipo_habitacion").on("change",function(){

	$("#guardar").removeClass("disabled");

	var hotel = $("#hoteles").val();
	var tipo_habitacion = $(this).val();

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:15,pk : hotel, tipo_habitacion : tipo_habitacion
            },
            function (data) {

                data = $.parseJSON(data);


                var tabla ='<table class="table table-bordered table-striped table-responsive" id="tabla_porcentaje" width="100%"><thead>'
                          +'<tr>'
                              +'<td align="center"><strong>Hab quitar</strong></td>'
                              +'<td align="center"><strong>Porcentaje quitar</strong></td>'	
                              +'<td align="center"><strong>Hab</strong></td>'
                              +'<td align="center"><strong>%</strong></td>'
                              +'<td align="center"></td>'
                          +'</tr></thead><tbody>';


                $.each(data.informacion, function(i, datos){

                	tabla+='<tr id="eliminar_'+datos.id+'">'
                              +'<td align="center">'+datos.hab_quitar+'</td>'
                              +'<td align="center">'+datos.porcentaje_quitar+'</td>'	
                              +'<td align="center">'+datos.hab+'</td>'
                              +'<td align="center">'+datos.porcentaje+'</td>'
                              +'<td align="center"><i class="fa fa-trash text-danger eliminar" id="'+datos.id+'"></i></td>'
                          +'</tr>'

                });


                tabla += '</tbody></table>';

                if(data.informacion != "")
                	$("#informacion").html(tabla);
                else
                	$("#informacion").html("");


     });

});


$(document).on("click", ".eliminar", function(){

	var id = $(this).attr("id");

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:16,id : id
            },
            function (data) {

                data = $.parseJSON(data);

                if(data.respuesta == 1){
              		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Porcentaje eliminado correctamente.</label>", "success", "bottom-left");
                	 $("#eliminar_"+id).html("");
                }else
              		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar eliminar la información.</label>", "danger", "bottom-left");

               	


    });



});

$(".habitaciones_porcentaje").on("change",function(){

	var id = $(this).attr("data-id");

	if($(this).is(":checked"))
		$("#porcentaje_"+id).attr("disabled",false);
	else
		$("#porcentaje_"+id).attr("disabled",true);

});


$(document).on("keyup",'.p_activar', function () { 
    	$(this).val($(this).val().replace(/[^0-9]/g, ''));
});



$("#guardar").on("click",function(){

	var habitaciones_porcentaje = [];
	var hotel = $("#hoteles").val();
	var tipo_habitacion = $("#tipo_habitacion").val();
	var habitacion_quitar_dispo = "";

	$(".habitaciones").each(function(){

		if($(this).is(":checked"))
			habitacion_quitar_dispo = $(this).attr("id");

	});

	var sum = 0;
	$(".habitaciones_porcentaje").each(function(){

		var obj = {};
		var id = $(this).attr("data-id");
		if($(this).is(":checked")){
			var porcentaje = $("#porcentaje_"+id).val();
			obj["hab"] = id;
			obj["porcentaje"] = porcentaje;


			if(porcentaje!=""){
			   habitaciones_porcentaje.push(obj);
			   sum = parseInt(porcentaje) + parseInt(sum); 
			   
			}else
			   habitaciones_porcentaje.length=0;


		}

	});


	if(sum > 100){

		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>El porcentaje entre las habitaciones no puede superar el 80%</label>", "warning", "bottom-left");
		habitaciones_porcentaje.length=0;
		return false;
	}
	

	//console.log(habitaciones_porcentaje,sum);
	//return false;


	if(!$.isEmptyObject(habitaciones_porcentaje)){

		porcentaje = 100 - sum;


			$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:8,pk : hotel, tipo_habitacion : tipo_habitacion,habitacion_quitar_dispo : habitacion_quitar_dispo, habitaciones_porcentaje : habitaciones_porcentaje, porcentaje : porcentaje
            },
            function (data) {

                data = $.parseJSON(data);
                
              	if(data.respuesta == 1){
              		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Porcentaje agregado correctamente.</label>", "success", "bottom-left");
              		$('.p_activar').val('');
              		//$('option').attr('selected', false);
              		$(".habitaciones_porcentaje").attr("disabled",true);
					$(".p_activar").attr("disabled",true).numeric();
              	}else if(data.respuesta == 2)
              		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Ya se ha agregado el mismo porcentaje para la habitación seleccionada.</label>", "warning", "bottom-left");
              	else
              		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al guardar la información.</label>", "danger", "bottom-left");


            }).done(function () {

      		  	
           
         	});



		
	}else
		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Debe completar los campos.</label>", "warning", "bottom-left");


});


