$(document).on("ready", function(){

	$(".md-modal").mCustomScrollbar({
		setHeight:600,
		theme:"minimal-dark"
	});

	abrir_or();

});


function abrir_or(){

	$("#informacion_or").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");

	var tab ='<table class="table table-bordered responsive" id="tabla_informacion" width="100%"><thead>'
                    +'<tr>'
                    	+'<th><center>Empresa</center></th>'
						+'<th><center>Cot</center></th>'
						+'<th><center>Hotel</center></th>'
						+'<th><center>Num Pas</center></th>'
						+'<th><center>Desde</center></th>'
						+'<th><center>Hasta</center></th>'
						+'<th><center>Estado</center></th>'
                        +'<th></th>'
                        +'<th></th>'
					+'</tr></thead><tbody>';


	$.post("../admin/obtener_datos_or.php", {
                    verificando_sitio: 1, tipo:9
                },
                function (data) {

                	data = $.parseJSON(data);

                	$.each(data, function(j, e){



                        $.each(e, function (i, datos) {

                            //console.log(datos[0].nombre_hv,datos[0].nombre_opcts,datos[0].razon_social);

                            var estado = "";
                            var op = "";

                             if(datos.operador == "touravion_dev")
                                op = "turavion";
                             else if(datos.operador == "distantis")
                                op = "cts";
                             else
                                op = datos.operador;

                             if(datos.id_seg = 13)
                                estado = "On Request";
                             else
                                estado = "Expirado";


                            var empresa = ""
                            if(op == "veci")
                                empresa = datos[0].nombre_hv;
                            else if(op == "turavion")
                                empresa = datos[0].nombre_opcts;
                            else if(op == "cocha")
                                empresa = datos[0].razon_social;


                            var color = "";

                            if(datos.verificar_obs == 0)
                            	color = "<i  class='fa fa-envelope traer_observaciones ver_obs text-primary' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'>";
                            else
                            	color = "<i  class='fa fa-envelope traer_observaciones ver_obs text-danger' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'>";



                                tab += "<tr id='info_"+datos.cotdes+datos.id_hotel+"'>"
                                                +"<td align='right'>"+empresa+"</td>"
                                                +"<td align='right'>"+datos.cot+"</td>"
                                                +'<td align="left">'+datos.hotel+'</td>'
                                                +"<td align='left'><div id='"+datos.cot+"' data-operador = '"+datos.operador+"' class='traer_pax buscar_pax' data-modal='form-traer-pax'>"+datos.pax+"</div></center></td>"
                                                +'<td align="left">'+datos.desde+'</td>'
                                                +'<td align="left">'+datos.hasta+'</td>'
                                                +'<td align="left">'+estado+'</td>'
                                                +'<td align="center"><button id="confirmado'+datos.cotdes+'" class="btn btn-success" onclick="abrir_form_confirmar('+datos.cotdes+',\'' + op + '\','+datos.id_hotel+')">Confirmar</button> </td>'
                                                +'<td align="center"><button class="btn btn-warning" onclick="abrir_form_rechazar('+datos.cotdes+',\'' + op + '\','+datos.id_hotel+')"">Rechazar</button><!--button id="rechazar'+datos.cotdes+'" class="btn btn-warning" onclick="rechazar('+datos.cotdes+',\'' + op + '\','+datos.id_hotel+')">Rechazar</button--></td>'
                                        +"</tr>"


                        });

						

                    });


                	tab += '</tbody></table>';
                    $("#informacion_or").html(tab);

                 	$(".buscar_pax").modalEffects();
					$(".buscar_or").modalEffects();	

                    var table = $("#tabla_informacion").DataTable({
                                    "columnDefs": [
										{ "type": "date-euro", "targets": [ 5,6 ] }
									],
									"iDisplayLength": 50,
                                    "oLanguage": {
                                        "sProcessing": "Procesando...",
                                        "sLengthMenu": "Mostrar _MENU_ ",
                                        "sZeroRecords": "No se han encontrado datos disponibles",
                                        "sEmptyTable": "No se han encontrado datos disponibles",
                                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                        "sInfoPostFix": "",
                                        "sSearch": "Buscar: ",
                                        "sUrl": "",
                                        "sInfoThousands": ",",
                                        "sLoadingRecords": "Cargando...",
                                        "oPaginate": {
                                            "sFirst": "Primero",
                                            "sLast": "Último",
                                            "sNext": "Siguiente",
                                            "sPrevious": "Anterior"
                                        },
                                        "oAria": {
                                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                        }
                                    }
                     });

					

                    $(".buscar_pax").modalEffects();
					$(".buscar_or").modalEffects();


                }

                ).done(function (){


                	$("#tabla_informacion").on("click",'.traer_pax', function(){

						var valor = $(this).attr("id");
						var operador = $(this).attr("data-operador");

						var tabla ='<table class="table table-bordered" id="tabla_pasajeros" width="100%"><thead>'
					                            +'<tr>'
					                                +'<th>Nombre</th>'
					                                +'<th>Apellido</th>'
					                            +'</tr></thead><tbody>';


					     $.post("../admin/obtener_datos_or.php", {
					                    verificando_sitio: 1, tipo:2, id_cot : valor, operador: operador
					                },
					                function (data) {

					                	data = $.parseJSON(data);

					                	$.each(data.pasajeros, function (i, datos) {

					                	tabla += "<tr>"
					                                    +"<td align='right'>"+datos.nombre+"</td>"
					                                    +'<td align="left">'+datos.apellido+'</td>'
					                                   
					                            +"</tr>"

					                   });


					                	tabla += '</tbody></table>';
					                    $("#pasajeros").html(tabla);

					                }
					                
					                ).done(function (){
					             
					             
					             
					                });

					});

			});

}




function confirmar(id,clien,hotel){
                var contenido = "confirmar";

                $("#confirmar"+id).html("<i class='fa fa-spinner fa-spin'></i>").attr("disabled",'true');

                var nmr_confirmacion = $("#nmr_confirmacion_"+id).val();

                $.ajax({
                    type: 'POST',
                    url: '../h2o/solicitar_or_prueba.php',
                    data: {
                        flag: contenido,
                        cotdes: id,
                        cliente: clien,
                        hotel: hotel,
                        nmr_confirmacion : nmr_confirmacion,
                        per : 9
                    },
                    success:function(result){
                      
                        /*$.post("../admin/obtener_datos_or.php", {
                                        verificando_sitio: 1, tipo:4, cotdes : id, operador: clien
                                    },
                                    function (data) {

                                        data = $.parseJSON(data);


                            }

                            ).done(function(){*/


                                alert("Cotización Confirmada");
                                //parent.document.location.href = 'or_main.php';
                                $("#form-traer-confirmar").removeClass("md-show");
                                $("#info_"+id+hotel).html("");


                            //});
                        
                    
                        
                    },
                    error:function(){
                        alert("Error 1!!")
                    }
                });

            
            }


function abrir_form_rechazar(id,clien,hotel){

    var datos = "";

    datos+='<select name="mot_rechazo_'+id+'" id="mot_rechazo_'+id+'">'
            +'<option value="DUPLICIDAD">DUPLICIDAD</option>'  
            +'<option value="SIN DISPONIBILIDAD" selected="">SIN DISPONIBILIDAD</option>'
            +'<option value="SIN DISPONIBILIDAD OCUPACION REQUERIDA">SIN DISPONIBILIDAD OCUPACION REQUERIDA</option>' 
            +'<option value="SIN DISPONIBILIDAD EN EL TIPO DE HABITACION">SIN DISPONIBILIDAD EN EL TIPO DE HABITACION</option>' 
          +'</select>'
          +'&nbsp;&nbsp;&nbsp;<input type="text" name="com_rechazo_'+id+'" id="com_rechazo_'+id+'" >'
          +'<br><br><center><button class="btn btn-warning" id="rechazar'+id+'" onclick="rechazar('+id+',\'' + clien + '\','+hotel+')">Enviar</button></center>'

       $("#form-traer-rechazar").addClass("md-show");

       $("#eliminar").on("click",function(){
           
           $("#form-traer-rechazar").removeClass("md-show");
          
       });
    
       $(".md-overlay").on("click", function(){
           
           $("#form-traer-rechazar").removeClass("md-show");
          
       });


        $("#formulario_rechazar").html(datos);


}


function abrir_form_confirmar(id,clien,hotel){

	    var datos = "";

	    datos+='<center><input type="text" id="nmr_confirmacion_'+id+'" placeholder="N° confirmación" class="form-control"></center>'
	          +'<br><br><center><button class="btn btn-success" id="confirmar'+id+'" onclick="confirmar('+id+',\'' + clien + '\','+hotel+')">Guardar y Enviar</button></center>'

	       $("#form-traer-confirmar").addClass("md-show");

	       $("#eliminar").on("click",function(){
	           
	           $("#form-traer-confirmar").removeClass("md-show");
	          
	       });
	    
	       $(".md-overlay").on("click", function(){
	           
	           $("#form-traer-confirmar").removeClass("md-show");
	          
	       });


	        $("#formulario_confirmacion").html(datos);


}



function rechazar(id,clien,hotel){
            var contenido = "rechazo";

            $("#rechazar"+id).html("<i class='fa fa-spinner fa-spin'></i>").attr("disabled",'true');

            var motivo = $("#mot_rechazo_"+id).val();
            var coment = $("#com_rechazo_"+id).val();
            
                $.ajax({
                    type: 'POST',
                    url: '../h2o/solicitar_or_prueba.php',
                    data: {
                        flag: contenido,
                        cotdes: id,
                        cliente: clien,
                        moti: motivo,
                        comentario: coment,
                        per: 10
                    },
                    success:function(result){
                        

                         /*$.post("../admin/obtener_datos_or.php", {
                                        verificando_sitio: 1, tipo:5, cotdes : id, operador: clien, motivo : motivo, comentario: coment
                                    },
                                    function (data) {

                                        data = $.parseJSON(data);


                            }

                            ).done(function(){*/

                                alert("Rechazado");
                                //parent.document.location.href = 'or_main.php';
                                $("#form-traer-rechazar").removeClass("md-show");
                                $("#info_"+id+hotel).html("");

                            //});
                        

                        
                    },
                    error:function(){
                        alert("Error 1!!")
                    }
                });
            } 