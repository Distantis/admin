$(document).on("ready", function(){
    
    $("#solicitud").numeric();

    
});



$("#formulario").on("submit", function (event) {

    var formData = new FormData($("#formulario")[0]);
   
    $(this).find("#enviar").html("Recuperando  <i class='fa fa-spinner fa-spin'></i>");
    $("#enviar").addClass('disabled');
    
    $.ajax({
        url: 'clases/eliminar.php',
        type: 'POST',
        data: formData,
        async: true,
        cache: false,
        processData: false,
        contentType: false,
        dataType:'JSON',
        success: function (data) {
          
            
            switch(data.respuesta){
                    
                
                case 1:
                    
                    mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>El campo no puede quedar en blanco.</label>", "danger", "bottom-left");
                    $("#enviar").html("Recuperar <i class='fa fa-send'></i>");
                    $("#enviar").removeClass('disabled');
                    
                break;
                    
                    
                case 2:
                    
                    mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>La solicitud ingresada no existe.</label>", "warning", "bottom-left");
                    $("#enviar").html("Recuperar <i class='fa fa-send'></i>");
                    $("#enviar").removeClass('disabled');
                    
                break;
                    
                case 3:
                    
                    mostrar_notificacion("Exito", "<label style='color:white !important;font-size:13px'>Solicitud recuperada correctamente.</label>", "success", "bottom-left");
                    setTimeout(function () {
                        window.location.reload(1);
                    }, 2000);
                    
                break;
                    
                    
                case 4:
                    
                    mostrar_notificacion("Error", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar recuperar el registro.</label>", "danger", "bottom-left");
                    $("#enviar").html("Recuperar <i class='fa fa-send'></i>");
                    $("#enviar").removeClass('disabled');
                    
                break;
                    
            }


        }
    });

    event.preventDefault();
});