$(document).on("ready", function(){

	$(".md-modal").mCustomScrollbar({
		setHeight:600,
		theme:"minimal-dark"
	});


	/*exampleSocket = new WebSocket("ws://199.89.54.134:8081");
	exampleSocket.onopen = function (event) {

	    /*var msg = {
				mensaje: "Hola",
				nombre: "asdasd",
				tipo : "on_request"
		};
		//convert and send data to server
		//console.log(msg);
		exampleSocket.send(JSON.stringify(msg));
	};
	exampleSocket.onmessage = function (event) {
	    
		var msg = JSON.parse(event.data); //PHP sends Json data

		if(msg.type!=null){
       
				
				var tipo = msg["type"].tipo; //message type
				var mensaje = msg["type"].mensaje; //message text
				var nombre = msg["type"].nombre;

				if(tipo!==undefined || mensaje!=undefined || nombre!=undefined){
				
					switch(tipo){

						case "on_request":
							$(function(){
							        new PNotify({
							            title: 'Nuevo On request',
							            text: nombre+" "+mensaje,
							            type: 'warning'
							        });
							    });

							or_unico(nombre,mensaje);
							//sonido();
							break;
				     }

				}

	    }

	 };*/


	abrir_or();



});




function abrir_or(){

	$("#informacion_or").html("<center><i class='fa fa-circle-o-notch fa-spin fa-2x'></i></center>");

	var tab ='<table class="table table-bordered responsive" id="tabla_informacion" width="100%"><thead>'
                    +'<tr>'
                    	+'<th><center>Operador</center></th>'
                    	+'<th><center>Empresa</center></th>'
						+'<th><center>Cot</center></th>'
						+'<th><center>Hotel</center></th>'
						+'<th><center>Num Pas</center></th>'
						+'<th><center>Desde</center></th>'
						+'<th><center>Hasta</center></th>'
						+'<th><center>Tiempo Activo</center></th>'
						+'<th><center>Estado</center></th>'
                        +'<th></th>'
                        +'<th></th>'
						+'<th><center>ver</center></th>'
                        +'<th><center>obs</center></th>'
					+'</tr></thead><tbody>';


	$.post("obtener_datos_or.php", {
                    verificando_sitio: 1, tipo:1
                },
                function (data) {

                	data = $.parseJSON(data);

                	$.each(data, function(j, e){



                        $.each(e, function (i, datos) {

                            //console.log(datos[0].nombre_hv,datos[0].nombre_opcts,datos[0].razon_social);

                            var estado = "";
                            var op = "";

                             if(datos.operador == "touravion_dev")
                                op = "turavion";
                             else if(datos.operador == "distantis")
                                op = "cts";
                             else
                                op = datos.operador;

                             if(datos.id_seg = 13)
                                estado = "On Request";
                             else
                                estado = "Expirado";


                            var empresa = ""
                            if(op == "veci")
                                empresa = datos[0].nombre_hv;
                            else if(op == "turavion")
                                empresa = datos[0].nombre_opcts;
                            else if(op == "cocha")
                                empresa = datos[0].razon_social;


                            var color = "";

                            if(datos.verificar_obs == 0)
                            	color = "<i  class='fa fa-envelope traer_observaciones ver_obs text-primary' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'>";
                            else
                            	color = "<i  class='fa fa-envelope traer_observaciones ver_obs text-danger' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'>";



                                tab += "<tr>"
                                                +"<td align='right'>"+op.toUpperCase()+"</td>"
                                                +"<td align='right'>"+empresa+"</td>"
                                                +"<td align='right'>"+datos.cot+"</td>"
                                                +'<td align="left">'+datos.hotel+'</td>'
                                                +"<td align='left'><div id='"+datos.cot+"' data-operador = '"+datos.operador+"' class='traer_pax buscar_pax' data-modal='form-traer-pax'>"+datos.pax+"</div></center></td>"
                                                +'<td align="left">'+datos.desde+'</td>'
                                                +'<td align="left">'+datos.hasta+'</td>'
                                                +'<td align="left">'+datos.dif+'</td>'
                                                +'<td align="left">'+estado+'</td>'
                                                +'<td align="center"><button id="confirmado'+datos.cotdes+'" class="btn btn-success" onclick="abrir_form_confirmar('+datos.cotdes+',\'' + op + '\','+datos.id_hotel+')">Confirmar</button> </td>'
                                                +'<td align="center"><button class="btn btn-warning" onclick="abrir_form_rechazar('+datos.cotdes+',\'' + op + '\')"">Rechazar</button><!--button id="rechazar'+datos.cotdes+'" class="btn btn-warning" onclick="rechazar('+datos.cotdes+',\'' + op + '\')">Rechazar</button--></td>'
                                                +"<td align='center'><i  class='fa fa-info-circle traer_detalle_or buscar_or' id='"+datos.cot+"'  data-operador = '"+datos.operador+"' data-modal='form-traer-or'></td>"
                                                +"<td align='center'>"+color+"</td>"

                                        +"</tr>"


                        });

						

                    });


                	tab += '</tbody></table>';
                    $("#informacion_or").html(tab);

                 	$(".buscar_pax").modalEffects();
					$(".buscar_or").modalEffects();	
                    $(".ver_obs").modalEffects(); 
                    var table = $("#tabla_informacion").DataTable({
                                    "columnDefs": [
										{ "type": "date-euro", "targets": [ 5,6 ] }
									],
									"iDisplayLength": 50,
                                    "oLanguage": {
                                        "sProcessing": "Procesando...",
                                        "sLengthMenu": "Mostrar _MENU_ ",
                                        "sZeroRecords": "No se han encontrado datos disponibles",
                                        "sEmptyTable": "No se han encontrado datos disponibles",
                                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                        "sInfoEmpty": "Mostrando  del 0 al 0 de un total de 0 ",
                                        "sInfoFiltered": "(filtrado de un total de _MAX_ datos)",
                                        "sInfoPostFix": "",
                                        "sSearch": "Buscar: ",
                                        "sUrl": "",
                                        "sInfoThousands": ",",
                                        "sLoadingRecords": "Cargando...",
                                        "oPaginate": {
                                            "sFirst": "Primero",
                                            "sLast": "Último",
                                            "sNext": "Siguiente",
                                            "sPrevious": "Anterior"
                                        },
                                        "oAria": {
                                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                        }
                                    }
                     });

					

                    $(".buscar_pax").modalEffects();
					$(".buscar_or").modalEffects();
                    $(".ver_obs").modalEffects();	





                }

                ).done(function (){


                	$("#tabla_informacion").on("click",'.traer_pax', function(){

								var valor = $(this).attr("id");
								var operador = $(this).attr("data-operador");

								console.log(valor,operador);
								

								var tabla ='<table class="table table-bordered" id="tabla_pasajeros" width="100%"><thead>'
							                            +'<tr>'
							                                +'<th>Nombre</th>'
							                                +'<th>Apellido</th>'
							                            +'</tr></thead><tbody>';


							     $.post("obtener_datos_or.php", {
							                    verificando_sitio: 1, tipo:2, id_cot : valor, operador: operador
							                },
							                function (data) {

							                	data = $.parseJSON(data);

							                	$.each(data.pasajeros, function (i, datos) {

							                	tabla += "<tr>"
							                                    +"<td align='right'>"+datos.nombre+"</td>"
							                                    +'<td align="left">'+datos.apellido+'</td>'
							                                   
							                            +"</tr>"

							                   });


							                	tabla += '</tbody></table>';
							                    $("#pasajeros").html(tabla);

							                }
							                
							                ).done(function (){
							             
							             
							             
							                });

							});





					$("#tabla_informacion").on("click",'.traer_detalle_or', function(){

						var valor = $(this).attr("id");
						var operador = $(this).attr("data-operador");

                        window.stop();
						
						$("#detalle").html("<i class='fa fa-spinner fa-spin'></i>");
                        $("#usuarios").html("<i class='fa fa-spinner fa-spin'></i>");

						var tabla2 = "<table class='table table-bordered' align='center'>"
										+"<tr>"
											+"<th colspan='3' width = '892px'><center>Usuarios</center>"
											+"</th>"
										+"</tr>"
										+"<tr>"
											+"<th><center>Usuario</center>"
											+"</th>"
											+"<th><center>Password</center>"
											+"</th>"
											+"<th><center>e-mail</center>"
											+"</th>"
										+"</tr>";


					     $.post("obtener_datos_or.php", {
					                    verificando_sitio: 1, tipo:3, id_cot : valor, operador: operador
					                },
					                function (data) {

					                	data = $.parseJSON(data);

					                	$.each(data.detalle_or, function (i, datos) {

					                		var tabla = "<table class='programa table table-bordered' align='center' width = '892px'>"
														+"<tr><th colspan='4' width = '892px'><center>Datos Hotel</center></th></tr>"
														+"<tr>"
															+"<th>Hotel:"
															+"</th>"
															+"<td>"+datos.hotel+""
															+"</td>"
															+"<th>Telefono:"
															+"</th>"
															+"<td>"+datos.fono+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Desde:"
															+"</th>"
															+"<td>"+datos.desde+""
															+"</td>"
															+"<th>Hasta:"
															+"</th>"
															+"<td>"+datos.hasta+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Single:"
															+"</th>"
															+"<td>"+datos.hab1+" a "+datos.va1+""
															+"</td>"
															+"<th>Doble Twin:"
															+"</th>"
															+"<td>"+datos.hab2+" a "+datos.va2+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Doble Matrimonial:"
															+"</th>"
															+"<td>"+datos.hab3+" a "+datos.va3+""
															+"</td>"
															+"<th>Triple:"
															+"</th>"
															+"<td>"+datos.hab4+" a "+datos.va4+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Tipo Habitacion:"
															+"</th>"
															+"<td>"+datos.habitacion+""
															+"</td>"
															+"<th>Tipo Tarifa:"
															+"</th>"
															+"<td>"+datos.tarifa+""
															+"</td>"
														+"</tr>"
													+"</table>"

											 $("#detalle").html(tabla);

					                   });


										$.each(data.usuarios, function (m, datos1) {
									
															
												tabla2 += "<tr>"
															+"<td><center>"+datos1.usuario+"</center>"
															+"</td>"
															+"<td><center>"+datos1.pass+"</center>"
															+"</td>"
															+"<td><center>"+datos1.mail+"</center>"
															+"</td>"
														 +"</tr>"			
										});

										tabla2 += "</table>";
										$("#usuarios").html(tabla2);


					    
					                   

					                }
					                
					                ).done(function (){
					             
					             
					             
					                });

					});



                     $("#tabla_informacion").on("click",'.traer_observaciones', function(){

                             var operador = $(this).attr("data-operador");
                             var cot = $(this).attr("data-id");

                             $("#cot").val(cot);
                             $("#operador").val(operador);

                             $("#observaciones").html("<i class='fa fa-spinner fa-spin'></i>");
                             window.stop();

                            
                                /*var tabla ='<table class="table table-bordered" id="tabla_observacion" width="100%"><thead>'
                                                        +'<tr>'
                                                            +'<th>Cot</th>'
                                                            +'<th>Operador</th>'
                                                            +'<th>Obs</th>'
                                                            +'<th>Fecha</th>'
                                                        +'</tr></thead><tbody>';*/


                                 $.post("obtener_datos_or.php", {
                                                verificando_sitio: 1, tipo:6, id_cot:cot, guardar_tipo:1
                                            },
                                            function (data) {

                                                data = $.parseJSON(data);
                                                var informacion = "";


                                                if(data.obs == ""){

                                                       informacion  += "<div class='alert alert-danger alert-dismissable'>"
                                                                            +"<center>Sin observaciones</center>"
                                                                            +"</div><br>";

                                                }

                                               
                                                $.each(data.obs, function (i, datos) {

                                                /*tabla += "<tr>"
                                                                +"<td align='right'>"+datos.cot+"</td>"
                                                                +'<td align="left">'+datos.operador+'</td>'
                                                                +'<td align="left">'+datos.observacion+'</td>'
                                                                +'<td align="left">'+datos.creacion+'</td>'
                                                        +"</tr>"*/


                                                  if(datos.observacion != ""){


                                                        informacion  += "<div class='alert alert-warning alert-dismissable'>"
                                                                            +"<center>"+datos.observacion+"</center>"
                                                                            +"<br>"
                                                                                +"<p class='pull-right'><font size='1px'>"+datos.usuario+" "+datos.creacion+"</font></p><br>"
                                                                            +"</div><br>";
                                                }
                                                                   

                                                    

                                               });


                                                
                                               $("#observaciones").html(informacion); 

                                            }
                                            
                                            ).done(function (){
                                         		

                                         		
                                         
                                            });

                            });


                            $("#guardar_obs").on("click", function(){

                                    var operador = $("#operador").val();
                                    var cot = $("#cot").val();
                                    var obs = $("#obs").val();

                                    if(obs == ""){
                                        mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No dejar campo vacio.</label>", "warning", "bottom-right");
                                    }else{

                                         $.post("obtener_datos_or.php", {
                                                verificando_sitio: 1, tipo:7, operador : operador, id_cot:cot, obs:obs, guardar_tipo:1
                                            },
                                            function (data) {

                                                data = $.parseJSON(data);

                                                if(data.respuesta == 1){
                                                    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Dato guardado correctamente.</label>", "success", "bottom-right");
                                                    $("#obs").val("");
                                                    $("#obs_"+cot).removeClass("text-primary").addClass("text-danger");
                                                    $("#form-observacion").removeClass("md-show");  
                                                }else
                                                    mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar guardar la información.</label>", "danger", "bottom-right");
                                         

                                            });



                                    }

                            });



             
             
          });



}


function or_unico(nombre,id_cot){

	$("#nuevo_or").html("Agregando nuevo <i class='fa fa-spinner fa-spin fa-2x'></i><br><br>");

	var tab = "";

	$.post("obtener_datos_or.php", {
                    verificando_sitio: 1, tipo:8, nombre : nombre, id_cot : id_cot
                },
                function (data) {

                	data = $.parseJSON(data);
                	var t = $('#tabla_informacion').DataTable();

                	$.each(data, function(j, e){



                        $.each(e, function (i, datos) {

                            //console.log(datos[0].nombre_hv,datos[0].nombre_opcts,datos[0].razon_social);

                            var estado = "";
                            var op = "";

                             if(datos.operador == "touravion_dev")
                                op = "turavion";
                             else if(datos.operador == "distantis")
                                op = "cts";
                             else
                                op = datos.operador;

                             if(datos.id_seg = 13)
                                estado = "On Request";
                             else
                                estado = "Expirado";


                            var empresa = ""
                            if(op == "veci")
                                empresa = datos[0].nombre_hv;
                            else if(op == "turavion")
                                empresa = datos[0].nombre_opcts;
                            else if(op == "cocha")
                                empresa = datos[0].razon_social;
                            


                            var color = "";

                            if(datos.verificar_obs == 0)
                            	color = "<center><i  class='fa fa-envelope traer_observaciones ver_obs text-primary' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'></center>";
                            else
                            	color = "<center><i  class='fa fa-envelope traer_observaciones ver_obs text-danger' id='obs_"+datos.cot+"' data-id='"+datos.cot+"' data-operador = '"+datos.operador+"' data-modal='form-observacion'></center>";

                            if(empresa===undefined)
                            	empresa = null;


		                    t.row.add( [
					            op.toUpperCase(),
					            empresa,
					            datos.cot,
					            datos.hotel,
					            "<div id='"+datos.cot+"' data-operador = '"+datos.operador+"' class='traer_pax buscar_pax' data-modal='form-traer-pax'>"+datos.pax+"</div></center>",
					            datos.desde,
					            datos.hasta,
					            datos.dif,
					            estado,
					            '<button id="confirmar'+datos.cotdes+'" class="btn btn-success" onclick="confirmar('+datos.cotdes+',\'' + op + '\','+datos.id_hotel+')">Confirmar</button>',
					            '<button class="btn btn-warning" onclick="abrir_form_rechazar('+datos.cotdes+',\'' + op + '\')"">Rechazar</button><!--button id="rechazar'+datos.cotdes+'" class="btn btn-warning" onclick="rechazar('+datos.cotdes+',\'' + op + '\')">Rechazar</button-->',
					            "<center><i  class='fa fa-info-circle traer_detalle_or buscar_or' id='"+datos.cot+"'  data-operador = '"+datos.operador+"' data-modal='form-traer-or'></center>",
					            color

					        ] ).draw( false );


                             


                        });

						

                    });


                 	$(".buscar_pax").modalEffects();
					$(".buscar_or").modalEffects();	
                    $(".ver_obs").modalEffects(); 


                }

                ).done(function (){

                	$("#nuevo_or").html("");

                	$("#tabla_informacion").on("click",'.traer_pax', function(){

                                window.stop();

								var valor = $(this).attr("id");
								var operador = $(this).attr("data-operador");
								

								var tabla ='<table class="table table-bordered" id="tabla_pasajeros" width="100%"><thead>'
							                            +'<tr>'
							                                +'<th>Nombre</th>'
							                                +'<th>Apellido</th>'
							                            +'</tr></thead><tbody>';


							     $.post("obtener_datos_or.php", {
							                    verificando_sitio: 1, tipo:2, id_cot : valor, operador: operador
							                },
							                function (data) {

							                	data = $.parseJSON(data);

							                	$.each(data.pasajeros, function (i, datos) {

							                	tabla += "<tr>"
							                                    +"<td align='right'>"+datos.nombre+"</td>"
							                                    +'<td align="left">'+datos.apellido+'</td>'
							                                   
							                            +"</tr>"

							                   });


							                	tabla += '</tbody></table>';
							                    $("#pasajeros").html(tabla);

							                }
							                
							                ).done(function (){
							             
							             
							             
							                });

							});






					$("#tabla_informacion").on("click",'.traer_detalle_or', function(){

						var valor = $(this).attr("id");
						var operador = $(this).attr("data-operador");

                        window.stop();
						
						$("#detalle").html("<i class='fa fa-spinner fa-spin'></i>");
                        $("#usuarios").html("<i class='fa fa-spinner fa-spin'></i>");

						var tabla2 = "<table class='programa table table-bordered' align='center' width = '892px'>"
										+"<tr>"
											+"<th colspan='3' width = '892px'><center>Usuarios</center>"
											+"</th>"
										+"</tr>"
										+"<tr>"
											+"<th><center>Usuario</center>"
											+"</th>"
											+"<th><center>Password</center>"
											+"</th>"
											+"<th><center>e-mail</center>"
											+"</th>"
										+"</tr>";


					     $.post("obtener_datos_or.php", {
					                    verificando_sitio: 1, tipo:3, id_cot : valor, operador: operador
					                },
					                function (data) {

					                	data = $.parseJSON(data);

					                	$.each(data.detalle_or, function (i, datos) {

					                		var tabla = "<table class='programa table table-bordered' align='center' width = '892px'>"
														+"<tr><th colspan='4' width = '892px'><center>Datos Hotel</center></th></tr>"
														+"<tr>"
															+"<th>Hotel:"
															+"</th>"
															+"<td>"+datos.hotel+""
															+"</td>"
															+"<th>Telefono:"
															+"</th>"
															+"<td>"+datos.fono+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Desde:"
															+"</th>"
															+"<td>"+datos.desde+""
															+"</td>"
															+"<th>Hasta:"
															+"</th>"
															+"<td>"+datos.hasta+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Single:"
															+"</th>"
															+"<td>"+datos.hab1+" a "+datos.va1+""
															+"</td>"
															+"<th>Doble Twin:"
															+"</th>"
															+"<td>"+datos.hab2+" a "+datos.va2+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Doble Matrimonial:"
															+"</th>"
															+"<td>"+datos.hab3+" a "+datos.va3+""
															+"</td>"
															+"<th>Triple:"
															+"</th>"
															+"<td>"+datos.hab4+" a "+datos.va4+""
															+"</td>"
														+"<tr>"
														+"<tr>"
															+"<th>Tipo Habitacion:"
															+"</th>"
															+"<td>"+datos.habitacion+""
															+"</td>"
															+"<th>Tipo Tarifa:"
															+"</th>"
															+"<td>"+datos.tarifa+""
															+"</td>"
														+"</tr>"
													+"</table>"

											 $("#detalle").html(tabla);

					                   });


										$.each(data.usuarios, function (m, datos1) {
									
															
												tabla2 += "<tr>"
															+"<td><center>"+datos1.usuario+"</center>"
															+"</td>"
															+"<td><center>"+datos1.pass+"</center>"
															+"</td>"
															+"<td><center>"+datos1.mail+"</center>"
															+"</td>"
														 +"</tr>"			
										});

										tabla2 += "</table>";
										$("#usuarios").html(tabla2);


					    
					                   

					                }
					                
					                ).done(function (){
					             
					             
					             
					                });

					});



                     $("#tabla_informacion").on("click",'.traer_observaciones', function(){

                             var operador = $(this).attr("data-operador");
                             var cot = $(this).attr("data-id");

                             $("#cot").val(cot);
                             $("#operador").val(operador);

                             $("#observaciones").html("<i class='fa fa-spinner fa-spin'></i>");
                             window.stop();

                            
                                /*var tabla ='<table class="table table-bordered" id="tabla_observacion" width="100%"><thead>'
                                                        +'<tr>'
                                                            +'<th>Cot</th>'
                                                            +'<th>Operador</th>'
                                                            +'<th>Obs</th>'
                                                            +'<th>Fecha</th>'
                                                        +'</tr></thead><tbody>';*/


                                 $.post("obtener_datos_or.php", {
                                                verificando_sitio: 1, tipo:6, id_cot:cot
                                            },
                                            function (data) {

                                                data = $.parseJSON(data);
                                                var informacion = "";


                                                if(data.obs == ""){

                                                       informacion  += "<div class='alert alert-danger alert-dismissable'>"
                                                                            +"<center>Sin observaciones</center>"
                                                                            +"</div><br>";

                                                }

                                               
                                                $.each(data.obs, function (i, datos) {

                                                /*tabla += "<tr>"
                                                                +"<td align='right'>"+datos.cot+"</td>"
                                                                +'<td align="left">'+datos.operador+'</td>'
                                                                +'<td align="left">'+datos.observacion+'</td>'
                                                                +'<td align="left">'+datos.creacion+'</td>'
                                                        +"</tr>"*/


                                                  if(datos.observacion != ""){


                                                        informacion  += "<div class='alert alert-warning alert-dismissable'>"
                                                                            +"<center>"+datos.observacion+"</center>"
                                                                            +"<br>"
                                                                                +"<p class='pull-right'><font size='1px'>"+datos.usuario+" "+datos.creacion+"</font></p><br>"
                                                                            +"</div><br>";
                                                }
                                                                   

                                                    

                                               });


                                                
                                               $("#observaciones").html(informacion); 

                                            }
                                            
                                            ).done(function (){
                                         		

                                         		
                                         
                                            });

                            });


                            $("#guardar_obs").on("click", function(){

                                    var operador = $("#operador").val();
                                    var cot = $("#cot").val();
                                    var obs = $("#obs").val();

                                    if(obs == ""){
                                        mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No dejar campo vacio.</label>", "warning", "bottom-right");
                                    }else{

                                         $.post("obtener_datos_or.php", {
                                                verificando_sitio: 1, tipo:7, operador : operador, id_cot:cot, obs:obs
                                            },
                                            function (data) {

                                                data = $.parseJSON(data);

                                                if(data.respuesta == 1){
                                                    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Dato guardado correctamente.</label>", "success", "bottom-right");
                                                    $("#obs").val("");
                                                    $("#obs_"+cot).removeClass("text-primary").addClass("text-danger");
                                                    $("#form-observacion").removeClass("md-show");  
                                                }else
                                                    mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar guardar la información.</label>", "danger", "bottom-right");
                                         

                                            });



                                    }

                            });
             
             
          });

}







function confirmar(id,clien,hotel){
                var contenido = "confirmar";


                $("#confirmar"+id).html("<i class='fa fa-spinner fa-spin'></i>").attr("disabled",'true');

                var nmr_confirmacion = $("#nmr_confirmacion_"+id).val();

                $.ajax({
                    type: 'POST',
                    url: '../h2o/solicitar_or.php',
                    data: {
                        flag: contenido,
                        cotdes: id,
                        cliente: clien,
                        hotel: hotel,
                        nmr_confirmacion : nmr_confirmacion
                    },
                    success:function(result){
                      
                        $.post("obtener_datos_or.php", {
                                        verificando_sitio: 1, tipo:4, cotdes : id, operador: clien
                                    },
                                    function (data) {

                                        data = $.parseJSON(data);


                            }

                            ).done(function(){

                                alert("Cotización Confirmada");
                                parent.document.location.href = 'or_main.php';


                            });
                        
                    
                        
                    },
                    error:function(){
                        alert("Error 1!!")
                    }
                });

            
            }


function abrir_form_rechazar(id,clien){

    var datos = "";

    datos+='<select name="mot_rechazo_'+id+'" id="mot_rechazo_'+id+'">'
            +'<option value="DUPLICIDAD">DUPLICIDAD</option>'  
            +'<option value="SIN DISPONIBILIDAD" selected="">SIN DISPONIBILIDAD</option>'
            +'<option value="SIN DISPONIBILIDAD OCUPACION REQUERIDA">SIN DISPONIBILIDAD OCUPACION REQUERIDA</option>' 
            +'<option value="SIN DISPONIBILIDAD EN EL TIPO DE HABITACION">SIN DISPONIBILIDAD EN EL TIPO DE HABITACION</option>' 
          +'</select>'
          +'&nbsp;&nbsp;&nbsp;<input type="text" name="com_rechazo_'+id+'" id="com_rechazo_'+id+'" >'
          +'<br><br><center><button class="btn btn-warning" id="rechazar'+id+'" onclick="rechazar('+id+',\'' + clien + '\')">Enviar</button></center>'

       $("#form-traer-rechazar").addClass("md-show");

       $("#eliminar").on("click",function(){
           
           $("#form-traer-rechazar").removeClass("md-show");
          
       });
    
       $(".md-overlay").on("click", function(){
           
           $("#form-traer-rechazar").removeClass("md-show");
          
       });


        $("#formulario_rechazar").html(datos);


}


function abrir_form_confirmar(id,clien,hotel){

	    var datos = "";

	    datos+='<center><input type="text" id="nmr_confirmacion_'+id+'" placeholder="N° confirmación" class="form-control"></center>'
	          +'<br><br><center><button class="btn btn-success" id="confirmar'+id+'" onclick="confirmar('+id+',\'' + clien + '\','+hotel+')">Guardar y Enviar</button></center>'

	       $("#form-traer-confirmar").addClass("md-show");

	       $("#eliminar").on("click",function(){
	           
	           $("#form-traer-confirmar").removeClass("md-show");
	          
	       });
	    
	       $(".md-overlay").on("click", function(){
	           
	           $("#form-traer-confirmar").removeClass("md-show");
	          
	       });


	        $("#formulario_confirmacion").html(datos);


}



function rechazar(id,clien){
            var contenido = "rechazo";

            $("#rechazar"+id).html("<i class='fa fa-spinner fa-spin'></i>").attr("disabled",'true');

            var motivo = $("#mot_rechazo_"+id).val();
            var coment = $("#com_rechazo_"+id).val();
            
                $.ajax({
                    type: 'POST',
                    url: '../h2o/solicitar_or.php',
                    data: {
                        flag: contenido,
                        cotdes: id,
                        cliente: clien,
                        moti: motivo,
                        comentario: coment
                    },
                    success:function(result){
                        

                         $.post("obtener_datos_or.php", {
                                        verificando_sitio: 1, tipo:5, cotdes : id, operador: clien, motivo : motivo, comentario: coment
                                    },
                                    function (data) {

                                        data = $.parseJSON(data);


                            }

                            ).done(function(){

                                alert("Rechazado");
                                parent.document.location.href = 'or_main.php';


                            });
                        

                        
                    },
                    error:function(){
                        alert("Error 1!!")
                    }
                });
            } 



		function sonido(){

					var audioElement = document.createElement('audio');
			        audioElement.setAttribute('src', 'notificacion.mp3');
			        audioElement.setAttribute('autoplay', 'autoplay');
			        //audioElement.load()
			        $.get();
			        audioElement.addEventListener("load", function() {
			        audioElement.play();
			        }, true);

			         audioElement.play();

		}
