

$(document).on("ready",function(){

		$(".desactivados").attr("disabled",true).addClass("disabled");
		$("#paridad").attr("disabled",true);

		$('select#hoteles').select2({

                    placeholder: "Seleccione Hotel"

        });

        $('select#tipo_habitacion').select2({

                    placeholder: "Seleccione Tipo Habitación"

        });

		$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:7
            },
            function (data) {

                data = $.parseJSON(data);
                var hotel = '<option value=\'\'></option>';

                $.each(data.hoteles, function (i, datos) {

                  hotel += '<option  value="'+datos.id_pk+'" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">' + datos.nombre_hotel + '</option>';

                });

              
                $("#hoteles").html(hotel);


            }).done(function () {

      		  $(".desactivados").attr("disabled",false).removeClass("disabled");
           
         });



       	  var now = new Date();
	      var nueva_fecha = sumarDias(now, -1);

	      var checkin = $('.desde').datepicker({
	        onRender: function(date) {
	           return date.valueOf() < nueva_fecha.valueOf() ? 'disabled' : '';
	        }
	      }).on('changeDate', function(ev) {

	        if (ev.date.valueOf() > checkout.date.valueOf()) {
	          var newDate = new Date(ev.date)
	          newDate.setDate(newDate.getDate() + 1);
	          checkout.setValue(newDate);
	        }
	        checkin.hide();
	        $('.hasta')[0].focus();
	      }).data('datepicker');
	      var checkout = $('.hasta').datepicker({
	        onRender: function(date) {
	          return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
	        }
	      }).on('changeDate', function(ev) {
	        checkout.hide();
	      }).data('datepicker');


});


function sumarDias(fecha, dias){
	fecha.setDate(fecha.getDate() + dias);
	return fecha;
}

$("#hoteles").on("change", function(){

	var pk = $(this).val();

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:6,pk:pk
            },
            function (data) {

                data = $.parseJSON(data);
                var habitacion = '<option value=\'\'></option>';

                $.each(data.tipo_habitacion, function (i, datos) {

                  habitacion += '<option  value="' + datos.hab + '" >' + datos.nombre + '</option>';

                });

              
                $("#tipo_habitacion").html(habitacion);


            });

	

});



$("#tipo_habitacion").on("change",function(){

	$("#paridad").attr("disabled",false);


	var hotel = $("#hoteles").val();
	var tipo_habitacion = $(this).val();

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:12,pk : hotel, tipo_habitacion : tipo_habitacion
            },
            function (data) {

                data = $.parseJSON(data);

                $.each(data.habitacion, function(i,datos){

                	if(datos.sgl == 1) 
                		$("#sgl").prop("checked",true);
                	else
                		$("#sgl").prop("checked",false);

                	if(datos.twn == 1) 
                		$("#twn").prop("checked",true);
                	else
                		$("#twn").prop("checked",false);

                	if(datos.dbl == 1) 
                		$("#dbl").prop("checked",true);
                	else
                		$("#dbl").prop("checked",false);

                	if(datos.tpl == 1) 
                		$("#tpl").prop("checked",true);
                	else
                		$("#tpl").prop("checked",false);

                });	

                if(data.habitacion != "")
                	$("#existe").val(1);
                else{
                	$(".habitaciones").prop("checked",false);
                	$("#existe").val(0);
                }


     });


});

$(".habitaciones").on("change",function(){

	var habitacion = $(this).attr("id");
	var tipo = "";
	var hotel = $("#hoteles").val();
	var tipo_habitacion = $("#tipo_habitacion").val();
	var existe = $("#existe").val();

	if($(this).is(":checked"))
		tipo = 1;	
	else
		tipo = 0;


	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:13,pk : hotel, tipo_habitacion : tipo_habitacion,habitacion : habitacion,cerrar_abrir:tipo,existe:existe
            },
            function (data) {

            data = $.parseJSON(data);

            var mensaje = "Desactivada";

            if(tipo == 1)
            	mensaje = "Activada";

                
          	if(data.respuesta == 1){
          		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Habitación "+habitacion+" "+mensaje+" Correctamente.</label>", "success", "bottom-left");

          		if(existe == 0)
          			$("#existe").val(1);

          	}else
              	mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al guardar la información.</label>", "danger", "bottom-left");
	
    });


});


$("#paridad").on("click",function(){

	$("#paridad").html("<i class='fa fa-spinner fa-spin'></i>").attr("disabled",true);

	var hotel = $("#hoteles").val();
	var tipo_habitacion = $("#tipo_habitacion").val();
	var desde = $("#desde").val();
	var hasta = $("#hasta").val();

	$.post("modificador_usuarios.php", {
                verificando_sitio: 1, tipo:14,pk : hotel, tipo_habitacion : tipo_habitacion, desde:desde,hasta:hasta
            },
            function (data) {

            data = $.parseJSON(data);

            switch(data.respuesta){

            	case 1:
            		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Paridad creada correctamente.</label>", "success", "bottom-left");
            		$("#paridad").html("Caragar tarifa paridad").attr("disabled",false);
            		$("#desde").val("");
            		$("#hasta").val("");
            	break;

            	case 2:
            		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>La tarifa paridad ya esta creada para el período seleccionado.</label>", "warning", "bottom-left");
            		$("#paridad").html("Caragar tarifa paridad").attr("disabled",false);
            		$("#desde").val("");
            		$("#hasta").val("");
            	break;

            	default:
            		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al guardar la información.</label>", "danger", "bottom-left");
            		$("#paridad").html("Caragar tarifa paridad").attr("disabled",false);
            		$("#desde").val("");
            		$("#hasta").val("");
            	break;

            }


     });


});




