
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="../logo-distantis.ico" />   
<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link href="prueba/css/bootstrap.css" rel="stylesheet">
<link href="prueba/css/jquery.gritter.css" rel="stylesheet">
<link href="prueba/css/style_gritter.css"  rel="stylesheet">
<link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
<link href="ws/pnotify/pnotify.core.css" rel="stylesheet" type="text/css" />
<link href="ws/pnotify/pnotify.buttons.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="prueba/css/jquery.mCustomScrollbar.css" type="text/css" />


<style type="text/css">

   

</style>




	</head>



<body>

    <div id="container" class="inner">

        <div id="header">
			
			
		<?php	 	 include ("bienvenida.php");?>



            <ul id="nav">

                <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
				<li class="servicios activo"><a href="nums_conf.php" title="<? echo $servind_tt;?>" class="tooltip">N Conf Pendiente</a></li>
				<li class="servicios activo"><a href="anulados.php" title="<? echo $servind_tt;?>" class="tooltip">Reservas anuladas</a></li> 
                <li class="servicios activo"><a href="ver_hoteles.php" title="<? echo $servind_tt;?>" class="tooltip">Hoteles</a></li> 
                <!--li class="servicios activo"><a href="editor_usuarios.php" title="<? echo $servind_tt;?>" class="tooltip">Edit. Usuarios</a></li> 
                <li class="servicios activo"><a href="editor_hotelesmerge.php" title="<? echo $servind_tt;?>" class="tooltip">Edit. HotelesMerge</a></li--> 
		   </ul>

            <ol id="pasos">

            </ol>													    

        </div>

        <!-- INICIO Contenidos principales-->

        <div style="margin-left:auto;margin-right:auto;width: 920px;">

		<div id="nueva" class="">
		<br>
			<div class="alert alert-warning alert-dismissable"><center><strong>Onrequest Pendientes</strong></center></div>
			<br>
			
            <div class="col-md-12">
                <div class="col-md-12">
						<div id="nuevo_or"></div>	
						<div id="informacion_or"></div><br>
                </div>
            </div>
			
		</div>
		<div id="editar">
		</div>
        

        </div>

        <!-- FIN Contenidos principales -->


       


        

    

    </div>
    <!-- FIN container -->


    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-traer-rechazar">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Rechazar</center></h3><br>
                                <small> Ingrese motivo del rechazo.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                   
                                    

                                <div id='formulario_rechazar'></div>
                                  

                           </div>
                        </div>
                         
                    </div>
                
                    <div class="md-overlay"></div>


                    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-traer-confirmar">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Confirmar</center></h3><br>
                                <small> Ingrese número de confirmación.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                   
                                    

                                <div id='formulario_confirmacion'></div>
                                  

                           </div>
                        </div>
                         
                    </div>
                
                    <div class="md-overlay"></div>



     <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-traer-pax">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Pasajeros</center></h3><br>
                                <small> Detalle por cotización de pasajeros.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                   
                                   <center><div id="pasajeros"></div></center>

                           </div>
                        </div>
                         
                    </div>
                
                    <div class="md-overlay"></div>



                   <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-traer-or">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Detalle</center></h3><br>
                                <small> Detalle del Onrequest.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                   
                                   
                                    <div id="detalle"></div>
                                    <br>
                                    <div id="usuarios"></div>
                                  

                         </div>
                    </div>
                         
    </div>
                
                    <div class="md-overlay"></div>



                    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-observacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Observación</center></h3><br>
                                <small> Agregar Observación.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form mCustomScrollbar" data-mcs-theme="dark" id="form-agregar-promocion-body">
                                <br><br>
                                
                                <div class="col-md-12">
                                    <div class="col-md-10">

                                        <textarea placeholder="Ingresar Observación" class="form-control" id="obs"></textarea>
                                        <br>
                                        <input type="hidden" id="cot">
                                        <input type="hidden" id="operador">
                                        <center><button class="btn btn-success" id="guardar_obs">Guardar</button></center>
                                        <br>
                                       
                                        <i class="fa fa-info-circle text-warning pull-left"></i> <font size="2px" class="pull-left">Observaciones</font><br> 
                                        <div id="observaciones" ></div>

                                        <br><br><br><br>

                                    </div>
                                </div>   
                                <br><br>
                         </div>
                    </div>
                         
                    </div>
                
            <div class="md-overlay"></div>




</body>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="prueba/js/bootstrap.js" type="text/javascript"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
<script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script type="text/javascript" src="ws/pnotify/pnotify.core.js"></script>
<script type="text/javascript" src="ws/pnotify/pnotify.buttons.js"></script>
<script type="text/javascript" src="prueba/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="prueba/js/jquery.datatables.sorting-plugins.js"></script>


<script src="prueba/js/traer_datos.js?<?=strtotime(date("YmdHis"))?>" type="text/javascript"></script>



</html>

