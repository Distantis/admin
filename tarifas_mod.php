<?php
include("secure.php");
require_once("Connections/db1.php");
//$db1->debug=true;
error_reporting(E_ALL);
ini_set('display_errors', 1);
foreach($_POST as $id=>$value){
	//echo "$id -> $value </br>";
}
function savelog($db1, $usuario, $permiso, $hotdet, $desde1, $hasta1, $desde2, $hasta2){
	$nota = "Antes: $desde1 al $hasta1 // Ahora $desde2 al $hasta2";
	$insert="insert into hoteles.log_h2o (id_usuario, bd_usuario, per_codigo, fecha, id_cambio, url, nota)
		VALUES($usuario, 'distantis',$permiso, now(), $hotdet, '".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."', '".$nota."')";
	$db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return true;
}
function ModGlobal($db1,$hotdet, $desde, $hasta){
	//datediff($mayor, $menor) = numero positivo
	//$db1->debug=true;
	$get_hotdet = "SELECT hd.*, DATE_FORMAT(hd_fecdesde, '%Y-%m-%d') AS desde2,
		DATE_FORMAT(hd_fechasta, '%Y-%m-%d') AS hasta2 FROM hoteles.hotdet hd WHERE id_hotdet =".$hotdet;
	$hotdet_rs= $db1->SelectLimit($get_hotdet) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($hotdet_rs->RecordCount()>0){
		$actualizar_hotdet=false;
		$diff_sql = "SELECT MIN(sc_fecha) AS menor, MAX(sc_fecha) AS mayor, datediff(MIN(sc_fecha), '$desde') as diffdesde,
				datediff(MAX(sc_fecha), '$hasta') as diffhasta, id_pk, id_tipohab, IFNULL(ano, '0') AS ano,  
				IFNULL(mes,'0') AS mes,  IFNULL( dia, '0') AS dia, sc_minnoche
					FROM hoteles.`stock_global` WHERE id_hotdet = ".$hotdet;
		$diff_rs= $db1->SelectLimit($diff_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$pk=$diff_rs->Fields("id_pk");
		$hab=$diff_rs->Fields("id_tipohab");
		$min=$diff_rs->Fields("sc_minnoche");
		$ano=$diff_rs->Fields("ano");
		$mes=$diff_rs->Fields("mes");
		$dia=$diff_rs->Fields("dia");
		if($diff_rs->Fields("diffdesde")>0){
			$actualizar_hotdet=true;
			for($i=0;$i<$diff_rs->Fields("diffdesde");$i++){
				$dias_sql = "SELECT DATE_ADD('$desde', INTERVAL $i DAY) as fecha";
				$dias_rs= $db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$diaux=$dias_rs->Fields("fecha");
				$insert="insert into hoteles.stock_global (id_pk, id_tipohab, sc_fecha, sc_minnoche, id_hotdet, ano, mes, dia)
						VALUES($pk, $hab, '".$diaux."', $min, $hotdet, $ano, $mes, $dia)";
				//echo "31-".$insert;
				$db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
		}elseif($diff_rs->Fields("diffdesde")<0){
			$actualizar_hotdet=true;
			$update="update hoteles.stock_global set sc_estado = 1 where id_hotdet = $hotdet and sc_fecha < '".$desde."'";
			//echo "36-".$update;
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		if($diff_rs->Fields("diffhasta")<0){
			$actualizar_hotdet=true;
			$aux=$diff_rs->Fields("diffhasta")*-1;
			for($i=1;$i<$aux;$i++){
				$dias_sql = "SELECT DATE_ADD('".$diff_rs->Fields("mayor")."', INTERVAL $i DAY) as fecha";
				$dias_rs= $db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$diaux=$dias_rs->Fields("fecha");
				$insert="insert into hoteles.stock_global (id_pk, id_tipohab, sc_fecha, sc_minnoche, id_hotdet, ano, mes, dia)
						VALUES($pk, $hab, '".$diaux."', $min, $hotdet, $ano, $mes, $dia)";
				//echo "47-".$insert;		
				$db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
		}elseif($diff_rs->Fields("diffhasta")>0){
			$actualizar_hotdet=true;
			$update="update hoteles.stock_global set sc_estado = 1 where id_hotdet = $hotdet and sc_fecha >= '".$hasta."'";
			//echo "52-".$update;			
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		if($actualizar_hotdet){
			$desde_aux=$desde." 00:00:00";
			$hasta_aux=$hasta." 00:00:00";
			$update = "update hoteles.hotdet set hd_fecdesde = '".$desde_aux."' ,
				hd_fechasta = '".$hasta_aux."' where id_hotdet = ".$hotdet;
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$x=savelog($db1,$_SESSION['id'], 7, $hotdet, $hotdet_rs->Fields("desde2"), $hotdet_rs->Fields("hasta2"),$desde, $hasta);			
		}		
		return "exito";
	}else{
		return "El hotdet no existe en global";
	}
}
function ModNormal($db1,$bd, $hotdet, $desde, $hasta){
	//datediff($mayor, $menor) = numero positivo
	//$db1->debug=true;
	$get_hotdet = "select hd.*, DATE_FORMAT(hd_fecdesde, '%Y-%m-%d') AS desde2,
		DATE_FORMAT(hd_fechasta, '%Y-%m-%d') AS hasta2 FROM $bd.hotdet hd where id_hotdet = ".$hotdet;
	$hotdet_rs= $db1->SelectLimit($get_hotdet) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($hotdet_rs->RecordCount()>0){
		$actualizar_hotdet=false;
		$diff_sql = "SELECT MIN(sc_fecha) AS menor, MAX(sc_fecha) AS mayor, datediff(MIN(sc_fecha), '$desde') as diffdesde,
				datediff(MAX(sc_fecha), '$hasta') as diffhasta, sc_minnoche
					FROM $bd.`stock` WHERE id_hotdet = ".$hotdet;
		$diff_rs= $db1->SelectLimit($diff_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$min=$diff_rs->Fields("sc_minnoche");
		if($diff_rs->Fields("diffdesde")>0){
			$actualizar_hotdet=true;
			for($i=0;$i<$diff_rs->Fields("diffdesde");$i++){
				$dias_sql = "SELECT DATE_ADD('$desde', INTERVAL $i DAY) as fecha";
				$dias_rs= $db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$diaux=$dias_rs->Fields("fecha");
				$insert="insert into $bd.stock (sc_fecha, sc_minnoche, id_hotdet)
						VALUES('".$diaux."', $min, $hotdet)";
				//echo "84-".$insert;
				$db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
		}elseif($diff_rs->Fields("diffdesde")<0){
			$actualizar_hotdet=true;
			$update="update $bd.stock set sc_estado = 1 where id_hotdet = $hotdet and sc_fecha < '".$desde."'";
			//echo "89-".$update;
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		if($diff_rs->Fields("diffhasta")<0){
			$actualizar_hotdet=true;
			$aux=$diff_rs->Fields("diffhasta")*-1;
			for($i=1;$i<$aux;$i++){
				$dias_sql = "SELECT DATE_ADD('".$diff_rs->Fields("mayor")."', INTERVAL $i DAY) as fecha";
				$dias_rs= $db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$diaux=$dias_rs->Fields("fecha");
				$insert="insert into $bd.stock (sc_fecha, sc_minnoche, id_hotdet)
						VALUES('".$diaux."', $min, $hotdet)";
				//echo "100-".$insert;		
				$db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
		}elseif($diff_rs->Fields("diffhasta")>0){
			$actualizar_hotdet=true;
			$update="update $bd.stock set sc_estado = 1 where id_hotdet = $hotdet and sc_fecha >= '".$hasta."'";
			//echo "105-".$update;			
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		if($actualizar_hotdet){
			$desde_aux=$desde." 00:00:00";
			$hasta_aux=$hasta." 00:00:00";
			$update = "update $bd.hotdet set hd_fecdesde = '".$desde_aux."' ,
				hd_fechasta = '".$hasta_aux."' where id_hotdet = ".$hotdet;
			$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$x=savelog($db1,$_SESSION['id'], 8, $hotdet, $hotdet_rs->Fields("desde2"), $hotdet_rs->Fields("hasta2"),$desde, $hasta);	
		}		
		return "exito";
	}else{
		return "El hotdet no existe en $bd";
	}
}
if(isset($_POST["Modificar"])){
	if($_POST["cb_clientes"]=='0'){
		echo "<script> alert('Debe seleccionar un cliente'); </script>";
	}else{
		$x = explode('-', $_POST["desde"] );
		$desde= $x[2]."-".$x[1]."-".$x[0];
		$x = explode('-', $_POST["hasta"] );
		$hasta= $x[2]."-".$x[1]."-".$x[0];
		if($_POST["cb_clientes"]=='global'){
			$result= ModGlobal($db1, $_POST["hotdet"], $desde, $hasta);
		}else{
			$result = ModNormal($db1, $_POST["cb_clientes"], $_POST["hotdet"], $desde, $hasta);
		}
		echo "<script> alert('".$result."'); </script>";		
	}	
}
$clientes_sql= "select * from hoteles.clientes where estado = 0";
$clientes = $db1->SelectLimit($clientes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$combo_clientes="";
if($clientes->RecordCount()>0){
	$combo_clientes="<select name='cb_clientes' id='cb_clientes' >";
	$combo_clientes.="<option value='0'>Seleccione</option>";
	while(!$clientes->EOF){
		$combo_clientes.="<option value='".$clientes->Fields("bd")."'>".$clientes->Fields("nombre")." </option>";
		$clientes->MoveNext();
	}
	$combo_clientes.="<option value='global'>Global</option>";
	$combo_clientes.="</select>";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--meta content="90" http-equiv="REFRESH"> </meta!-->
	<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
	<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
	<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
	<link href="prueba/css/bootstrap.css" rel="stylesheet">
	<link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
	<link href="prueba/css/datepicker.css" rel="stylesheet" type="text/css"/>
	<link href="prueba/css/jquery.gritter.css" rel="stylesheet">
	<link href="prueba/css/style_gritter.css"  rel="stylesheet">
	<link rel="stylesheet" href="prueba/css/protip.min.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>


<body>
	<div id="container" class="inner">
		<div id="header">
			<? include("bienvenida.php");?>
			<ul id="nav">
				<li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
				<li class="servicios activo"><a href="nums_conf.php" title="<? echo $servind_tt;?>" class="tooltip">N Conf Pendiente</a></li>
				<li class="servicios activo"><a href="anulados.php" title="<? echo $servind_tt;?>" class="tooltip">Reservas anuladas</a></li> 
			</ul>
			<ol id="pasos">

            </ol>
		</div>
        <div style="margin-left:auto;margin-right:auto;width: 930px;">
			<div id="nueva" width = '930px' >
				<div class="alert alert-warning alert-dismissable">
					<center><strong>Editor de tarifas para majo</strong></center>
			</div>				
			<table class="" align="center">
				<form method='POST'>
				<tr>
					<td>
						<?php echo $combo_clientes; ?>
					</td>
					<td>
						<input type="text" name="hotdet" id="hotdet" placeholder="Id hotdet" />
					</td>
					<td>
						<input type='text' name='desde'  placeholder="Fecha desde" data-date-format="dd-mm-yyyy" class='datepicker' id='desde' size='10'>
					</td>
					<td>
						<input type='text' name='hasta'  placeholder="Fecha hasta" data-date-format="dd-mm-yyyy" class='datepicker'  id='hasta' size='10'>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" id="Modificar" name="Modificar" class="btn btn-success" value='Modificar' />
					</td>
				</tr>
				</form>
			</table>
			</div>
		</div>
	</div>
</body>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="prueba/js/protip.min.js"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script src="prueba/js/jquery.datatables.sorting-plugins.js" type="text/javascript"></script>
<script src="prueba/js/trae_datepicker.js" type="text/javascript"></script>
<script src="prueba/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
</html>