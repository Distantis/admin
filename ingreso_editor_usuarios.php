<?php

if(isset($_POST['login']) && isset($_POST['pass'])) {
    extract($_POST);
    if($login == 'distantis' && $pass == 'cts2018distantis') {
        session_start();
        $_SESSION['ses_editor_usuario_activo'] = true;

        header('location: editor_usuarios.php?estado=ok');
        exit;
    }

    header('location: ?estado=error');
    exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CTS Turismo</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="../logo-distantis.ico" />
    <meta name="keywords" content=""></meta>
    <meta name="description" content=""></meta>
    <meta http-equiv="imagetoolbar" content="no" />
    <!-- 	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->

    <!-- hojas de estilo -->
    <link rel="stylesheet" href="css/screen-sm.css" media="screen" />

    <link rel="stylesheet" href="css/easy.css" media="screen" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" />

    <link rel="stylesheet" href="css/screen-jf.css" media="screen" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

</head>
<BODY onLoad="document.getElementById('<? if (isset($_COOKIE['CookieErp'])) {?>pass<? } else { ?>login<? } ?>').focus();" >

<div id="container" class="login">

    <div id="header">
        <h1>CTS Turismo</h1>
    </div>

    <div class="content">

        <div class="main">

            <form method="post" class="login" name="form">

                <fieldset class="cols"><legend>Login form</legend>
                    <div class="col first">
                        <label for="usuario">Usuario</label>
                        <INPUT name="login" id="login" value="" size="20" maxlength="30" onChange="M(this)" tabindex="1" class="field required">
                    </div>
                    <div class="col">
                        <label for="pass">Password</label>
                        <input type="password" name="pass" id="pass" size="20" class="field" />
                    </div>
                    <!--<div class="col first">
                        <label><input type="checkbox" name="mantenermeconectado" value="1" checked="checked" class="required" /> Mantenerme conectado</label>
                    </div>
                    <div class="col">
                        <a href="pass_recupera.php" title="Siga estas instrucciones para <br>recuperar una contraseña perdida u olvidada" class="tooltip">¿Olvidó su contraseña?</a>
                    </div>-->
                    <div class="submit"><button type="submit">Entrar</button></div>
                    <br />
                    <center>
                        Sitio Optimzado para <a href="http://www.apple.com/es/safari/" title="Descarga Safari">Safari</a> - <a href="https://www.google.com/chrome?hl=es" title="Descargar Chrome">Chrome</a> - <a href="http://www.opera.com/download/" title="Descarga Opera">Opera</a> - <a href="http://windows.microsoft.com/es-ES/internet-explorer/products/ie/home" title="Descarga IE9">IE9</a> en 1024x768.
                    </center>
                </fieldset>

            </form>


        </div>

    </div>
    <div class="content">



    </div>

    <!--div id="footer">
    </div-->
    <!-- INICIO NavAuxiliar para usuario y perfil -->
    <div class="main">
        <ul class="navAux fixed">
            <li class="first"><a href="registrarse.php">Registrarse</a></li>
            <li><a href="./">Home</a></li>
        </ul>
    </div>
    <!-- FIN NavAuxiliar para usuario y perfil -->

</div>

</body>
</html>