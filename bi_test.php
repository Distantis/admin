<?
require_once('Connections/db3.php');
require_once('Connections/db3.php');
require('secure.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);


$respuesta = "<table border=1>";
$respuesta.="<tr>";
$respuesta.="<td>Cliente</td>";
$respuesta.="<td>hotel</td>";
$respuesta.="<td>mes</td>";
$respuesta.="<td>ano</td>";
$respuesta.="<td>roomnights</td>";
$respuesta.="<td>booking</td>";
$respuesta.="<td>revenue</td>";
$respuesta.="</tr>";


$mesinicial = 1;
$mesfinal = 8;
$ano = 2016;
$pk = 56;
$tcambio = 660;

while($mesinicial <= $mesfinal){
	
	
	
	$consulta="SELECT 
				  dc.id_cliente,
				  mes AS fechat,
				  SUM(
				    (
				      hc_hab1 + hc_hab2 + hc_hab3 + hc_hab4
				    )
				  ) AS total,
				SUM(
				    (
				      hc_hab1 * IFNULL(IF(hd_mon = 2, hd_sgl, hd_sgl * $tcambio), 0)
				    ) + (
				      hc_hab2 * IFNULL(IF(hd_mon = 2, hd_dbl, hd_dbl * $tcambio), 0) * 2
				    ) + (
				      hc_hab3 * IFNULL(IF(hd_mon = 2, hd_dbl, hd_dbl * $tcambio), 0) * 2
				    ) + (
				      hc_hab4 * IFNULL(IF(hd_mon = 2, hd_tpl, hd_tpl *$tcambio), 0) * 3
				    )
				  ) AS revenue ,
				  bookings2,
				  CONCAT('SC-',$pk) AS hotel
				FROM
				  bi.data_central dc 
				  INNER JOIN 
				    (SELECT 
				      id_cliente,
				      COUNT(*) AS bookings2 
				    FROM
				      (SELECT 
					* 
				      FROM
					bi.data_central 
				      WHERE ano = $ano 
					AND mes = $mesinicial 
					AND id_pk IN 
					(SELECT 
					  id_pkset 
					FROM
					  hoteles.`setcompetitivo` sc 
					WHERE sc.`id_pk` = $pk 
					  AND sc.`id_pkset` != $pk) 
					AND cot_seg = 7 
					AND cot_estado = 0 
					AND hc_estado = 0 
				      GROUP BY id_cliente,
					id_cot) tb1 
				    GROUP BY id_cliente) tb1 
				    ON dc.`id_cliente` = tb1.id_cliente 
				WHERE cot_seg = 7 
				  AND cot_estado = 0 
				  AND hc_estado = 0 
				  AND id_pk IN 
				  (SELECT 
				    id_pkset 
				  FROM
				    hoteles.`setcompetitivo` sc 
				  WHERE sc.`id_pk` = $pk 
				    AND sc.`id_pkset` != $pk) 
				  AND mes = $mesinicial 
				  AND ano = $ano 
				GROUP BY id_cliente 
				ORDER BY mes";
	
	//$consulta="call roomnights(".$ano.",".$mesinicial.",".$pk.",1,100,1,660);";
          //echo $consulta."<br>";
		 // $response = $db3->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());	
	$response = $db3->SelectLimit($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
	//echo $response->RecordCount();
	while(!$response->EOF){
		$consulta_cliente = "select * from hoteles.clientes where id_cliente = ".$response->Fields("id_cliente");
		//echo $consulta_cliente."<br>";
		//$response = $db3->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());	
		$cliente = $db3->SelectLimit($consulta_cliente) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
		
		$respuesta.="<tr>";
		$respuesta.="<td>".$cliente->Fields("nombre")."</td>";
		$respuesta.="<td>".$response->Fields("hotel")."</td>";
		$respuesta.="<td>".$mesinicial."</td>";
		$respuesta.="<td>".$ano."</td>";
		$respuesta.="<td>".$response->Fields("total")."</td>";
		$respuesta.="<td>".$response->Fields("bookings2")."</td>";
		$respuesta.="<td>".$response->Fields("revenue")."</td>";
		$respuesta.="</tr>";
		
		
		
	$response->MoveNext();
						}
	
    $consulta = "SELECT 
				  dc.id_cliente,
				  mes AS fechat,
				  SUM(
				    (
				      hc_hab1 + hc_hab2 + hc_hab3 + hc_hab4
				    )
				  ) AS total,
				  SUM(
				    (
				      hc_hab1 * IFNULL(IF(hd_mon = 2, hd_sgl, hd_sgl * $tcambio), 0)
				    ) + (
				      hc_hab2 * IFNULL(IF(hd_mon = 2, hd_dbl, hd_dbl * $tcambio), 0) * 2
				    ) + (
				      hc_hab3 * IFNULL(IF(hd_mon = 2, hd_dbl, hd_dbl * $tcambio), 0) * 2
				    ) + (
				      hc_hab4 * IFNULL(IF(hd_mon = 2, hd_tpl, hd_tpl * $tcambio), 0) * 3
				    )
				  ) AS revenue ,
				  bookings2,
				  CONCAT('SB-',$pk) AS hotel
				FROM
				  bi.data_central dc 
				  INNER JOIN 
				    (SELECT 
				      id_cliente,
				      COUNT(*) AS bookings2 
				    FROM
				      (SELECT 
					* 
				      FROM
					bi.data_central 
				      WHERE ano = $ano 
					AND mes = $mesinicial 
					AND id_pk IN 
					(SELECT 
					  id_pkset 
					FROM
					  hoteles.`setcompetitivo` sc 
					WHERE sc.`id_pk` = $pk 
					  AND sc.`id_pkset` = $pk) 
					AND cot_seg = 7 
					AND cot_estado = 0 
					AND hc_estado = 0 
				      GROUP BY id_cliente,
					id_cot) tb1 
				    GROUP BY id_cliente) tb1 
				    ON dc.`id_cliente` = tb1.id_cliente 
				WHERE cot_seg = 7 
				  AND cot_estado = 0 
				  AND hc_estado = 0 
				  AND id_pk IN 
				  (SELECT 
				    id_pkset 
				  FROM
				    hoteles.`setcompetitivo` sc 
				  WHERE sc.`id_pk` = $pk 
				    AND sc.`id_pkset` = $pk) 
				  AND mes = $mesinicial 
				  AND ano = $ano 
				GROUP BY id_cliente -- GROUP BY mes 
				ORDER BY mes";


	
	//$consulta="CALL roomnights(".$ano.",".$mesinicial.",".$pk.",0,100,1,660)";
         // echo $consulta;
		 // $response = $db3->Execute($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());	
	$response = $db3->SelectLimit($consulta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
	
	while(!$response->EOF){
		$consulta_cliente = "select * from hoteles.clientes where id_cliente = ".$response->Fields("id_cliente");
		$cliente = $db3->SelectLimit($consulta_cliente) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db3->ErrorMsg());
		
		$respuesta.="<tr>";
		$respuesta.="<td>".$cliente->Fields("nombre")."</td>";
		$respuesta.="<td>".$response->Fields("hotel")."</td>";
		$respuesta.="<td>".$mesinicial."</td>";
		$respuesta.="<td>".$ano."</td>";
		$respuesta.="<td>".$response->Fields("total")."</td>";
		$respuesta.="<td>".$response->Fields("bookings2")."</td>";
		$respuesta.="<td>".$response->Fields("revenue")."</td>";
		$respuesta.="</tr>";
		
		
		
	$response->MoveNext();
						}					
	
	
	
	$mesinicial++;
	
	
}
$respuesta.="</table>";





header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=bi_test.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
echo $respuesta; 
die();	
//echo $respuesta;

?>
<html>
	<head></head>
	<body><?=$respuesta;?></body>
</html>