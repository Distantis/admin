<?php

include ("secure.php");
require_once ('includes/mailing.php');
require_once('Connections/db1.php');
$clientes_query = "select * from clientes where estado = 0";
$clientes=$db1->Execute($clientes_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$sonando = false;
 
?>


<!DOCTYPE html> 
<head> 
    <title>Hoteles</title>
    <meta charset="utf-8">
    <link href="prueba/css/bootstrap.css" rel="stylesheet">
    <link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="prueba/css/jquery.gritter.css" rel="stylesheet">
    <link href="prueba/css/style_gritter.css"  rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
    <link href="prueba/css/datepicker.css" rel="stylesheet" type="text/css"/>
    
    
</head> 

<body>

    <div id="container" class="inner">

        <div id="header">
			
			
		<?php	 	 include ("bienvenida.php");?>



            <ul id="nav">

                <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
				<li class="servicios activo"><a href="nums_conf.php" title="<? echo $servind_tt;?>" class="tooltip">N Conf Pendiente</a></li>
				<li class="servicios activo"><a href="anulados.php" title="<? echo $servind_tt;?>" class="tooltip">Reservas anuladas</a></li> 
                <li class="servicios activo"><a href="ver_hoteles.php" title="<? echo $servind_tt;?>" class="tooltip">Hoteles</a></li> 
		   </ul>

            <ol id="pasos">

            </ol>													    

        </div>

        <!-- INICIO Contenidos principales-->
             <div class='col-md-12'>
                 <br><br>
                <hr>
                <div id="mensaje"></div>
                <div class="container-fluid table-responsive" id="hoteles"></div>
                
                
                
            </div>
 
        <!-- FIN Contenidos principales -->

    

    </div>
    
    
    
     <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-agregar-operacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Agregar Operación</center></h3><br>
                                <small>Agrega el estado de la operación (Onrequest - Plataforma).</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                    <form class="formulario4" id="formulario4" autocomplete="off"> 

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select name="add_operacion" id="add_operacion" class="form-control">
                                                    
                                                        <option value="">Seleccionar tipo de operación</option>
                                                        <option value="1">Onrequest</option>
                                                        <option value="2">Plataforma</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <input type="hidden" id="operacion" name="operacion">
                                        <br><br>
                                        <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="eliminar">Cancelar</button>
                                        <button type="submit" class="btn btn-success btn-flat" id="enviar">Guardar cambios <i class='fa fa-chevron-circle-right'></i></button>
                                </center>

                                    </form>
                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    
         <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-agregar-capacitacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Agregar Capacitación</center></h3><br>
                                <small>.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                    <form class="formulario4" id="formulario4" autocomplete="off"> 

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select name="add_capacitacion" id="add_capacitacion" class="form-control">
                                                    
                                                        <option value="">Seleccionar capacitación</option>
                                                        <option value="1">SI</option>
                                                        <option value="0">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <input type="hidden" id="capacitacion" name="capacitacion">
                                        <br><br>
                                        <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="eliminar">Cancelar</button>
                                        <button type="submit" class="btn btn-success btn-flat" id="enviar2">Guardar cambios <i class='fa fa-chevron-circle-right'></i></button>
                                </center>

                                    </form>
                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-agregar-observacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Agregar Observación</center></h3><br>
                                <small>Puede agregar todo tipo de información.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                    <form class="formulario4" id="formulario4" autocomplete="off"> 

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea name="obs" id="obs" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <input type="hidden" id="observacion" name="observacion">
                                        <br><br>
                                        <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="eliminar">Cancelar</button>
                                        <button type="submit" class="btn btn-success btn-flat" id="enviar3">Guardar cambios <i class='fa fa-chevron-circle-right'></i></button>
                                </center>

                                    </form>
                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="observaciones" style='overflow-y: scroll;max-height: auto !important;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Observaciones</center></h3><br>
                                <small>Historial de observación por hotel.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="informacion_todas">
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
     
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-agregar-contacto">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Agregar Contactos</center></h3><br>
                                <small>.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                    <form class="formulario4" id="formulario4" autocomplete="off"> 
                                        <div class="col-md-12">
                                        <div class="row">
                                            
                                                <div class="form-group">
                                                    <select name="tipo_usuario" id="tipo_usuario" class="form-control">
                                                    
                                                        <option value="">Seleccionar opción</option>
                                                        <option value="1">Comercial</option>
                                                        <option value="2">Operaciones</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="text" name="email" id="email" placeholder="Ingrese email" class="form-control" style="width:100%;">
                                                </div>
                                            
                                            <div class="form-group">
                                                    <input type="text" name="nombre" id="nombre" placeholder="Ingrese Nombre" class="form-control" style="width:100%;">
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <input type="text" name="fono" id="fono" placeholder="Ingrese fono" class="form-control" style="width:100%;">
                                                </div>
                                            
                                            
                                                
                                            </div>
                                            
                                            
                                        </div>
                                        

                                        <input type="hidden" id="contacto" name="contacto">
                                        <br><br>
                                        <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="eliminar">Cancelar</button>
                                        <button type="submit" class="btn btn-success btn-flat" id="enviar4">Guardar cambios <i class='fa fa-chevron-circle-right'></i></button>
                                </center>

                                    </form>
                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>

    
        <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="contactos">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Contactos</center></h3><br>
                                <small>Útimos contactos ingresados por operacion y comercial.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_contacto_comercial"></div>
                                <div id="informacion_contacto_operacional"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana2">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    
               
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para cts.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_cts"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas_veci" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para veci.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_veci"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas_otsi" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para otsi.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_otsi"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>

    
    
     <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas_turavion" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para turavion.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_turavion"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas_cocha" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para cocha.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_cocha"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
        

        <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-7" id="form-traer-tarifas_travel" style='overflow-y: scroll;max-height: 700px;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Tarifas</center></h3><br>
                                <small>Listado de tarifas para travel.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_tarifas_travel"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>
    
    
     <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-traer-usupass" style='overflow-x:auto !important;'>
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Usuarios</center></h3><br>
                                <small>Listado de usuarios para acceder al sistema.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form">
                                
                                <div id="informacion_usuarios"></div>
                               
                            </div>
                                 <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="cerrar_ventana7">Cerrar</button>
                                        </center>
                            
                            <br><br>
                         </div>
                    </div>
                
                    <div class="md-overlay"></div>

    
    
    
    
    <!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="form-agregar-recordatorio">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Agregar Recordatorio</center></h3><br>
                                <small> Ver recordatorios, recordar que solo se mostrará el último.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                                <br><br>
                                    <form class="formulario4" id="formulario_record" autocomplete="off"> 
                                        <div class="col-md-12">
                                        <div class="row">
                                            
                        
                                                <div class="form-group">
                                                    <input type="text" name="record" id="record" placeholder="Ingrese fecha de recordatorio" class="form-control datepicker" data-date-format="dd-mm-yyyy" style="width:100%;" maxlength="0">
                                                </div>
                                       
                                            
                                                
                                            </div>
                                            
                                            
                                            <div class="row">
                                            
                        
                                                <div class="form-group">
                                                    <textarea id="recordatorio_texto" placeholder="Escriba el recordatorio" class="form-control"></textarea>
                                                </div>
                                       
                                            
                                                
                                            </div>
                                            
                                            
                                        </div>
                                        

                                        <input type="hidden" id="id_pk" name="id_pk">
                                        <br><br>
                                        <center>
                                        <button type="button" class="btn btn-danger btn-flat md-close" data-dismiss="modal" id="eliminar">Cancelar</button>
                                        <button type="submit" class="btn btn-success btn-flat" id="enviar_recordatorio">Guardar cambios <i class='fa fa-chevron-circle-right'></i></button>
                                </center>

                                    </form>
                                <br>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <i class="fa fa-warning fa-fw"></i> Último Recordatorio del <span id="fecha_de_recordatorio"></span></h3></div>
                                    
                                    <div class="panel-body">
                                        
                                        
                                        <div id="informacion_recordatorio"></div>
                                    
                                    
                                    </div>
                                
                                
                            </div>

                         </div>
                    </div>
                         
    </div>
                
                    <div class="md-overlay"></div>

      
 
 <script src="prueba/js/jquery.js" type="text/javascript"></script>
 <script src="prueba/js/bootstrap.js" type="text/javascript"></script>
 <script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
 <script src="prueba/js/notificaciones.js" type="text/javascript"></script>
 <script src="prueba/js/jquery.numeric.js"></script>
 <script src="prueba/js/tabla_hoteles.js"></script>
 <script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
 <script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
 <script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
 <script src="prueba/js/bootstrap-datepicker.js" type="text/javascript"></script>
        
    
 </body>
</html>