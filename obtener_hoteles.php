<?php

$traerhoteles = new TraerHoteles();


class TraerHoteles{

	//protected $link;
	protected $sql_con;
	protected $datos = array();
	protected $datos_usuario = array();

	
	
	public function __construct(){
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
        //ini_set('memory_limit', '512M');
        session_start();
        require_once('Connections/db1.php');
        $this->set_conexion($db1);
        $this->obtener_parametros();
		//$this->revisar_existe();
        //$this->tipo();
		
	}
	
	
	protected function set_conexion($db1){
	
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
	}
    
    
    public function obtener_parametros(){
        
        extract($_POST);
        $this->datos_usuario["sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] =  $tipo;
        $this->datos_usuario["tipo_operacion"] =  $tipo_operacion;
        $this->datos_usuario["operacion"] =  $operacion;
        $this->datos_usuario["tipo_capacitacion"] =  $tipo_capacitacion;
        $this->datos_usuario["capacitado"] =  $capacitado;
        $this->datos_usuario["obs"] =  $obs;
        $this->datos_usuario["observacion"] =  $observacion;
        $this->datos_usuario["id"] =  $id;
        $this->datos_usuario["contacto"] = $contacto;
        $this->datos_usuario["email"] = $email;
        $this->datos_usuario["fono"] = $fono;
        $this->datos_usuario["tipo_usuario"] = $tipo_usuario;
        $this->datos_usuario["nombre"] = $nombre;
        $this->datos_usuario["id_hotel"] = $id_hotel;
        $this->datos_usuario["nombre_hotel"] = $nombre_hotel;
        $this->datos_usuario["id_pk"] = $id_pk;
        $this->datos_usuario["fec_record"] = $fec_record;
        $this->datos_usuario["recordatorio"] = $recordatorio;
        $this->datos_usuario["id_usuario"] = $_SESSION["id"];
        $this->datos_usuario["bd"] = $bd;
        $this->quitar_caracteres();
        $this->revisar_tipo();
        
     
    }
    
    
    public function revisar_tipo(){
        
       
        switch($this->datos_usuario["tipo"]){
 
            case 1:
                
                $this->guardar_operacion();
                
            break;
            
            case 2:
                
                $this->guardar_capacitacion();
                
            break;
                
                
            case 3:
                
                $this->guardar_observacion();
                
            break;
                
                
             case 4:
                
                $this->traer_hoteles_tabla();
                
            break;
                
             case 5:
                
                $this->traer_observaciones();
                
            break;
                
                
            case 6:
                
                $this->guardar_contacto();
                
            break;
                
                
            case 7:
                
                $this->traer_contacto_comercial();
                $this->traer_contacto_operacion();
                
            break;
                
                
                
            case 8:
                
                $this->traer_tarifa_cts();
                
            break;
                
                
            case 9:
                
                $this->traer_tarifa_veci();
                
            break;
                
             case 10:
                
                $this->traer_tarifa_otsi();
                
            break;
                
             case 11:
                
                $this->traer_tarifa_turavion();
                
            break;
                
              case 12:
                
                $this->traer_tarifa_cocha();
                
            break;
                
            case 13:
                
                $this->traer_usuarios();
                
            break;
                
                
            case 14:
                
                $this->traer_recordatorio();
                
            break;
                
                
            case 15:
                
                $this->guardar_recordatorio();
                
            break;

            case 16:
                
                $this->traer_tarifa_travel();
                
            break;
			
			case 17:
                
                $this->traer_tarifa_carlson();
                
            break;
                
        
        
        }
        
    }
    
    
    
   
    
    protected function traer_hoteles_tabla(){
        
        if(isset($this->datos_usuario["sitio"])){

            $consulta="
                            
                        SELECT 
            						  hm.id_pk,
            						  hm.opera_or,
            						  hm.capacitado,
            						  hm.id_hotel_cts,
            						  hm.id_hotel_veci,
            						  hm.id_hotel_otsi,
            						  hm.id_hotel_turavion,
            						  hm.id_hotel_travelclub,
            						  hm.id_hotel_cocha,
									  hm.id_hotel_carlson,
            						  c.nom_cadena,
            						  hm.`release`,
            						  release_cts,
            						  release_otsi,
            						  release_veci,
            						  release_turavion,
            						  release_cocha,
            						  release_travelclub
									   release_carlson
            						   
            						FROM
            						  hotelesmerge hm 
            						  LEFT JOIN cadena c 
            							ON c.id_cadena = hm.id_cadena 
            
                    ";
            $enviar = $this->sql_con->SelectLimit($consulta);

               $this->datos["hoteles2"] = array();
               $this->datos_usuario["bd"] = "";
             

                while (!$enviar->EOF){
                    

                      $hoteles = array(
                              "distantis" => $enviar->Fields("id_hotel_cts"),
                              "veci" => $enviar->Fields("id_hotel_veci"),
                              "otsi" => $enviar->Fields("id_hotel_otsi"),
                              "touravion_dev" => $enviar->Fields("id_hotel_turavion"),
                              "cocha" => $enviar->Fields("id_hotel_cocha"),
                              "travelclub" => $enviar->Fields("id_hotel_travelclub"),
							  "carlson" => $enviar->Fields("id_hotel_carlson")
                      );

                   

                     $nombre_hotel = $this->nombre_hotel($hoteles);


                    
                    //$total = $this->revisar_usuarios($enviar->Fields("id_hotel_cts"),$enviar->Fields("id_hotel_veci"),$enviar->Fields("id_hotel_otsi"),$enviar->Fields("id_hotel_turavion"),$enviar->Fields("id_hotel_cocha"));
                   
                    //echo $veci;
                    //$veci = $this->veci($enviar->Fields("id_ho,$enviar->Fields("id_hotel_turavion")el_veci"));
                    //$otsi = $this->revisar_usuarios(3,$enviar->Fields("id_hotel_otsi"));
                    //$turavion = $this->revisar_usuarios(4,$enviar->Fields("id_hotel_turavion"));
                    //$cocha = $this->revisar_usuarios(5,$enviar->Fields("id_hotel_cocha"));
                    
                    //$si_existen = $cts+$veci+$otsi+turavion+$cocha;

                    $id_hotel_cts = $enviar->Fields("id_hotel_cts");
                    $id_hotel_otsi = $enviar->Fields("id_hotel_otsi");
                    $id_hotel_veci = $enviar->Fields("id_hotel_veci");
                    $id_hotel_turavion = $enviar->Fields("id_hotel_turavion");
                    $id_hotel_cocha = $enviar->Fields("id_hotel_cocha");
                    $id_hotel_travelclub = $enviar->Fields("id_hotel_travelclub");
					 $id_hotel_carlson = $enviar->Fields("id_hotel_carlson");

                    if(!isset($id_hotel_cts))
                        $id_hotel_cts = 0;

                    if(!isset($id_hotel_otsi))
                        $id_hotel_otsi = 0;

                    if(!isset($id_hotel_veci))
                        $id_hotel_veci = 0;

                    if(!isset($id_hotel_turavion))
                        $id_hotel_turavion = 0;

                    if(!isset($id_hotel_cocha))
                        $id_hotel_cocha = 0;

                    if(!isset($id_hotel_travelclub))
                        $id_hotel_travelclub = 0;
					
                     if(!isset($id_hotel_carlson))
                        $id_hotel_carlson = 0;
                  
                  if($nombre_hotel != "NADA"){
                    
                    
                          $a = array(
                            
                                  "nombre_hotel" => utf8_encode($nombre_hotel),
                                  "cts"  => $enviar->Fields("id_hotel_cts"),
                                  "veci"  => $enviar->Fields("id_hotel_veci"),
                                  "otsi"  => $enviar->Fields("id_hotel_otsi"),
                                  "turavion"  => $enviar->Fields("id_hotel_turavion"),
                                  "cocha"  => $enviar->Fields("id_hotel_cocha"),
                                  "travel"  => $enviar->Fields("id_hotel_travelclub"),
								   "carlson"  => $enviar->Fields("id_hotel_carlson"),
                                  "nom_cadena" => $enviar->Fields("nom_cadena"),
                                  "opera" => $enviar->Fields("opera_or"),
                                  "capacitado" => $enviar->Fields("capacitado"),
                                  "id" => $enviar->Fields("id_pk"),
                                  "fec_record" => $this->traer_recordatorio_normal($enviar->Fields("id_pk")),
                                  "cont_cts" => $this->cts($enviar->Fields("id_hotel_cts")),
                                  "cont_veci" => $this->veci($enviar->Fields("id_hotel_veci")),
                                  "cont_otsi" => $this->otsi($enviar->Fields("id_hotel_otsi")),
                                  "cont_turavion" => $this->turavion($enviar->Fields("id_hotel_turavion")),
                                  "cont_cocha" => $this->cocha($enviar->Fields("id_hotel_cocha")),
                                  "cont_travel" => $this->travelclub($enviar->Fields("id_hotel_travelclub")),
								   "cont_carlson" => $this->travelclub($enviar->Fields("id_hotel_carlson")),
                                  "bd"=>$this->datos_usuario["bd"],
                    							"release" =>$enviar->Fields("release"),
                    							"release_cts" =>$this->r_cts($id_hotel_cts),
                    							"release_otsi" =>$this->r_otsi($id_hotel_otsi),
                    							"release_veci" =>$this->r_veci($id_hotel_veci),
                    							"release_turavion"=>$this->r_turavion($id_hotel_turavion),
                    							"release_cocha" =>$this->r_cocha($id_hotel_cocha),
                    							"release_travelclub" =>$this->r_travelclub($id_hotel_travelclub),
												"release_carlson" =>$this->r_carlson($id_hotel_carlson)
                          
                              );

                              

                          array_push($this->datos["hoteles2"], $a);
                    //echo "paso";


                    }

                    $enviar->MoveNext();
					
					
                }
            
            
        }
        
    }
    

    
    protected function cts($cts){
         
         $consulta = "select count(*) as cts from distantis.usuarios du where du.id_empresa='".$cts."' and usu_estado = 0 ";
         $enviar = $this->sql_con->SelectLimit($consulta);
         $cantidad = $enviar->Fields("cts");
         
         return $cantidad;
        
    }
    
    
     protected function veci($veci){
         
         $consulta1 = "select count(*) as veci from veci.usuarios vu where vu.id_empresa='".$veci."'  and usu_estado = 0";
         $enviar1 = $this->sql_con->SelectLimit($consulta1);
         $cantidad1 = $enviar1->Fields("veci");
         
         return $cantidad1;
        
    }
    
    protected function otsi($otsi){
         
         $consulta2 = "select count(*) as otsi from otsi.usuarios ou where ou.id_empresa='".$otsi."'  and usu_estado = 0";

         $enviar2 = $this->sql_con->SelectLimit($consulta2);
         $cantidad2 = $enviar2->Fields("otsi");
         
         return $cantidad2;
        
    }
    
     protected function turavion($turavion){
         
         $consulta3 = "select count(*) as turavion from touravion_dev.usuarios tu where tu.id_empresa='".$turavion."'  and usu_estado = 0";
         $enviar3 = $this->sql_con->SelectLimit($consulta3);
         $cantidad3 = $enviar3->Fields("turavion");
         
         return $cantidad3;
        
    }
    
    protected function cocha($cocha){
         
         $consulta4 = "select count(*) as cocha from cocha.usuarios cu where cu.id_empresa='".$cocha."'  and usu_estado = 0";
         $enviar4= $this->sql_con->SelectLimit($consulta4);
         $cantidad4 = $enviar4->Fields("cocha");
         
         return $cantidad4;
        
    }

    protected function travelclub($travelclub){
         
         $consulta5 = "select count(*) as travel from travelclub.usuarios cu where cu.id_empresa='".$travelclub."'  and usu_estado = 0";
         $enviar5= $this->sql_con->SelectLimit($consulta5);
         $cantidad5 = $enviar5->Fields("travel");
         
         return $cantidad5;
        
	}
	
	 protected function carlson($carlson){
         
         $consulta6 = "select count(*) as carlson from carlson.usuarios cu where cu.id_empresa='".$carlson."'  and usu_estado = 0";
         $enviar5= $this->sql_con->SelectLimit($consulta6);
         $cantidad6 = $enviar5->Fields("carlson");
         
         return $cantidad6;
        
	}
	
	
	protected function r_cts($id_hotel){
		$consultar = "SELECT h.release FROM distantis.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		
		return $relea;
	}
	protected function r_veci($id_hotel){
		$consultar = "SELECT h.release FROM veci.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		
		return $relea;
	}
	protected function r_otsi($id_hotel){
		$consultar = "SELECT h.release FROM otsi.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		
		return $relea;
	}
	protected function r_turavion($id_hotel){
		$consultar = "SELECT h.release FROM touravion_dev.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		
		return $relea;
	}
	protected function r_cocha($id_hotel){
		$consultar = "SELECT h.release FROM cocha.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		
		return $relea;
	}
	protected function r_travelclub($id_hotel){
		$consultar = "SELECT h.release FROM travelclub.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		//echo $relea."  .";die();
		return $relea;
	}
	
	protected function r_carlson($id_hotel){
		$consultar = "SELECT h.release FROM carlson.hotel h WHERE id_hotel = ".$id_hotel;
		$consultar= $this->sql_con->SelectLimit($consultar);
		if($consultar->RecordCount()>0){
			$relea = $consultar->Fields("release");
		}else{
			$relea = 0;
		}
		//echo $relea."  .";die();
		return $relea;
	}
	
	
	
	
	
	
	
    
    protected function nombre_hotel($hoteles){
        
        $bd = "";
        $id_hotel = "";
        $revisar = 0;
        foreach($hoteles as $key=>$datos){
         
            if($datos != 0 and $revisar == 0){
              $bd = $key;
              $id_hotel = $datos;
              $revisar = 1;
            }

        }


       if($bd!= "" and $id_hotel != ""){
        
         $consulta = "select hot_nombre from $bd.hotel where id_hotel = $id_hotel ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $this->datos_usuario["bd"] = $bd;

         $nombre_hotel = $traer->Fields("hot_nombre");

       }else
          $nombre_hotel = "NADA";


       return $nombre_hotel;
        
        
        
       
    }
    
    /*protected function nombre_hotel($id){
        
        $consulta = "select * from hotelcts where id_hotel='".$id."'";
        $revisar = $this->sql_con->SelectLimit($consulta);
        
        if($revisar->RecordCount() > 0){
            
            $nombre = $revisar->Fields("hot_nombre");
            
        }else{
            
            $consulta1="select * from hotelveci where id_hotel='".$id."'";
            $revisar1= $this->sql_con->SelectLimit($consulta1);
            
              if($revisar1->RecordCount() > 0){
                  
                  $nombre = $revisar1->Fields("hot_nombre");
                  
              }else{
                  
                    $consulta2="select * from hotelotsi where id_hotel='".$id."'";
                    $revisar2= $this->sql_con->SelectLimit($consulta2);

                      if($revisar2->RecordCount() > 0){

                          $nombre = $revisar2->Fields("hot_nombre");

                      }else{

                            $consulta3="select * from hotelturavion where id_hotel='".$id."'";
                            $revisar3= $this->sql_con->SelectLimit($consulta3);

                              if($revisar3->RecordCount() > 0){

                                  $nombre = $revisar3->Fields("hot_nombre");

                              }else{

                                    $consulta4="select * from hotelcocha where id_hotel='".$id."'";
                                    $revisar4= $this->sql_con->SelectLimit($consulta4);

                                      if($revisar4->RecordCount() > 0){

                                          $nombre = $revisar4->Fields("hot_nombre");

                                      }else{


                                            $nombre = "Sin nombre";
                                      }

                              }

                      }
                  
              }
            
        }
        
        return $nombre;
        
    }*/
    
    
    protected function guardar_operacion(){
    
        
        
        $actualizar = "update hotelesmerge set opera_or='".$this->datos_usuario["tipo_operacion"]."' where id_pk='".$this->datos_usuario["operacion"]."'";
        
        $enviar = $this->sql_con->Execute($actualizar);
        
        if($enviar)
            $this->datos["respuesta"] = 1;
        else
            $this->datos["respuesta"] = 0;
        
    }
    
    protected function guardar_capacitacion(){
    
        
        
        $actualizar = "update hotelesmerge set capacitado='".$this->datos_usuario["tipo_capacitacion"]."' where id_pk='".$this->datos_usuario["capacitado"]."'";
        
        $enviar = $this->sql_con->Execute($actualizar);
        
        if($enviar)
            $this->datos["respuesta"] = 1;
        else
            $this->datos["respuesta"] = 0;
        
    }
    
    
    protected function guardar_observacion(){
    
        
        $fecha = date('Y-m-d H:m:s');
        $insertar = "insert into merge_observacion (id_hotel, obs, fch_obs, usu_id)";
        $insertar.= " values('".$this->datos_usuario["observacion"]."','".$this->datos_usuario["obs"]."','".$fecha."','".$this->datos_usuario["id_usuario"]."')";
    
        $enviar = $this->sql_con->Execute($insertar);
        
        if($enviar)
            $this->datos["respuesta"] = 1;
        else
            $this->datos["respuesta"] = 0;
        
    }
    
    
    
    protected function traer_observaciones(){
    
        
        $consulta = "select * from merge_observacion where id_hotel = '".$this->datos_usuario["id"]."'";
    
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["observaciones"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "obs"=>utf8_encode($enviar->Fields("obs")),
                                "fecha"=>$enviar->Fields("fch_obs")
                        
                            );
                    array_push($this->datos["observaciones"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 2;   
            
        }
    }
    
    
    protected function guardar_contacto(){
    
        
        $fecha = date('Y-m-d H:m:s');
        $insertar = "insert into merge_contacto (id_hotel, email, fono,nombre, tipo_usu, fch_contacto,usu_id)";
        $insertar.= " values('".$this->datos_usuario["contacto"]."','".$this->datos_usuario["email"]."','".$this->datos_usuario["fono"]."','".$this->datos_usuario["nombre"]."','".$this->datos_usuario["tipo_usuario"]."','".$fecha."','".$this->datos_usuario["id_usuario"]."')";
    
        $enviar = $this->sql_con->Execute($insertar);
        
        if($enviar)
            $this->datos["respuesta"] = 1;
        else
            $this->datos["respuesta"] = 0;
        
    }
    
    
    
    protected function traer_contacto_comercial(){
    
        
        $consulta = "select * from merge_contacto where id_hotel = '".$this->datos_usuario["id"]."' and tipo_usu= 1 order by fch_contacto desc limit 1";
   
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["contacto_comercial"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "email"=>utf8_encode($enviar->Fields("email")),
                                "fono"=>$enviar->Fields("fono"),
                                "nombre"=>utf8_encode($enviar->Fields("nombre")),
                                "fecha"=>$enviar->Fields("fch_contacto")
                        
                            );
                    array_push($this->datos["contacto_comercial"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 4;   
            
        }
    }
    
    
    protected function traer_contacto_operacion(){
    
        
        $consulta = "select * from merge_contacto where id_hotel = '".$this->datos_usuario["id"]."' and tipo_usu=2 order by fch_contacto desc limit 1";
      
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["contacto_operacion"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "email"=>utf8_encode($enviar->Fields("email")),
                                "fono"=>$enviar->Fields("fono"),
                                "nombre"=>utf8_encode($enviar->Fields("nombre")),
                                "fecha"=>$enviar->Fields("fch_contacto")
                        
                            );
                    array_push($this->datos["contacto_operacion"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 5;   
            
        }
    }
    
    
    
    protected function traer_tarifa_cts(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM distantis.hotdet hd left join distantis.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join distantis.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_cts"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_cts"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    
     protected function traer_tarifa_veci(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM veci.hotdet hd left join veci.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join veci.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_veci"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_veci"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    
    protected function traer_tarifa_otsi(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM otsi.hotdet hd left join otsi.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join otsi.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";

        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_otsi"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_otsi"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    
     protected function traer_tarifa_turavion(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM touravion_dev.hotdet hd left join touravion_dev.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join touravion_dev.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_turavion"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_turavion"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    
    protected function traer_tarifa_cocha(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM cocha.hotdet hd left join cocha.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join cocha.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_cocha"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_cocha"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }


    protected function traer_tarifa_travel(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM travelclub.hotdet hd left join travelclub.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join travelclub.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_travel"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_travel"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
	
	 protected function traer_tarifa_carlson(){
        
        $fecha = date('Y-m-d');
        $consulta = "SELECT * FROM carlson.hotdet hd left join carlson.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion left join carlson.tipotarifa tt on tt.id_tipotarifa = hd.id_tipotarifa WHERE id_hotel='".$this->datos_usuario["id_hotel"]."' AND hd_fechasta>='$fecha' and hd_estado = 0";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["tarifa_carlson"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "desde"=>date('d-m-Y', strtotime($enviar->Fields("hd_fecdesde"))),
                                "hasta"=>date('d-m-Y', strtotime($enviar->Fields("hd_fechasta"))),
                                "sgl"=>$enviar->Fields("hd_sgl"),
                                "dbl"=>$enviar->Fields("hd_dbl"),
                                "tpl"=>$enviar->Fields("hd_tpl"),
                                "tt"=>utf8_encode($enviar->Fields("tt_nombre")),
                                "th"=>utf8_encode($enviar->Fields("th_nombre"))
                        
                            );
                    array_push($this->datos["tarifa_carlson"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    protected function traer_usuarios(){
        
        $consulta = "
        
                SELECT u.usu_login,u.usu_password,c.ciu_nombre FROM ".$this->datos_usuario["bd"].".hotel h
                LEFT JOIN ".$this->datos_usuario["bd"].".usuarios u 
                ON u.id_empresa = h.id_hotel
                LEFT JOIN ".$this->datos_usuario["bd"].".ciudad c
                ON c.id_ciudad = h.id_ciudad
                WHERE h.hot_nombre='".$this->datos_usuario["nombre_hotel"]."'
                and usu_estado = 0
    
                ";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["usuarios"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "usuario"=>$enviar->Fields("usu_login"),
                                "password"=>$enviar->Fields("usu_password"),
                                "ciudad"=>utf8_encode($enviar->Fields("ciu_nombre"))
                        
                            );
                    array_push($this->datos["usuarios"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
        
    }
    
    
     protected function traer_recordatorio(){
    
        
        $consulta = "select * from merge_recordatorio where id_pk='".$this->datos_usuario["id_pk"]."' order by mr_pk desc LIMIT 1";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
        $this->datos["record"] = array();
        
        if($enviar->RecordCount() > 0){
        
                while (!$enviar->EOF){

                    $dato = array(
                            
                                "recordatorio"=>utf8_encode($enviar->Fields("recordatorio")),
                                "fecha"=>$enviar->Fields("fec_record"),
                                "id"=>$enviar->Fields("id_pk")
                        
                            );
                    array_push($this->datos["record"], $dato);
                    
                    $enviar->MoveNext();
                }
            
            
            
        }else{
            
            $this->datos["respuesta"] = 6;   
            
        }
    }
    
    
    
    protected function guardar_recordatorio(){
    
        
        $fecha = date('Y-m-d', strtotime($this->datos_usuario["fec_record"]));
        $insertar = "insert into merge_recordatorio (id_pk, recordatorio, fec_record, usu_id)";
        $insertar.= " values('".$this->datos_usuario["id_pk"]."','".$this->datos_usuario["recordatorio"]."','".$fecha."','".$this->datos_usuario["id_usuario"]."')";
    
        $enviar = $this->sql_con->Execute($insertar);
        
        if($enviar)
            $this->datos["respuesta"] = 1;
        else
            $this->datos["respuesta"] = 0;
        
        
    }
    
    
    protected function traer_recordatorio_normal($id){
        
         $consulta = "select * from merge_recordatorio where id_pk=$id order by mr_pk desc LIMIT 1";
        
        $enviar = $this->sql_con->SelectLimit($consulta);
        
       
        
        if($enviar->RecordCount() > 0){
        
            $fecha = $enviar->Fields("fec_record");
            
            
            
        }else{
            
            $fecha = "";
            
        }
        
        
        return $fecha;
        
    }
    
    protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["tipo_operacion"]); 
        $this->mssql_escape($this->datos_usuario["operacion"]);
        $this->mssql_escape($this->datos_usuario["tipo_capacitacion"]);
        $this->mssql_escape($this->datos_usuario["capacitado"]);
        $this->mssql_escape($this->datos_usuario["obs"]);
        $this->mssql_escape($this->datos_usuario["observacion"]);
        $this->mssql_escape($this->datos_usuario["id"]);
        $this->mssql_escape($this->datos_usuario["contacto"]);
        $this->mssql_escape($this->datos_usuario["email"]);
        $this->mssql_escape($this->datos_usuario["fono"]);
        $this->mssql_escape($this->datos_usuario["tipo_usuario"]);
        $this->mssql_escape($this->datos_usuario["nombre"]);
        $this->mssql_escape($this->datos_usuario["id_hotel"]);
        $this->mssql_escape($this->datos_usuario["nombre_hotel"]);
        $this->mssql_escape($this->datos_usuario["id_pk"]);
        $this->mssql_escape($this->datos_usuario["fec_record"]);
        $this->mssql_escape($this->datos_usuario["recordatorio"]);
        $this->mssql_escape($this->datos_usuario["bd"]);
    }
    

   
    protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }
    
    
    function __destruct(){
         echo json_encode($this->datos);
        
    }
    

}

?>