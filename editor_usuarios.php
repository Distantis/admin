<?php
session_start();

if(!isset($_SESSION['ses_editor_usuario_activo']) || $_SESSION['ses_editor_usuario_activo'] != true) {
    header('location: ingreso_editor_usuarios.php');
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>MODIFICADOR USUARIOS</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--meta content="60" http-equiv="REFRESH"> </meta-->
	<link href="prueba/css/bootstrap.css" rel="stylesheet">
	<link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="prueba/css/jquery.gritter.css" rel="stylesheet">
	<link href="prueba/css/style_gritter.css"  rel="stylesheet">
	<link href="prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.niftymodals/css/component.css"/>
    <link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
    <link rel="stylesheet" href="prueba/css/protip.min.css">
</head>
<body>

<br><br>
<div class="col-md-12">

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-7">
					<select id="hoteles" name="hotel" class="form-control select2" style="width:50%;"></select>
			</div>

		</div>
	</div>

</div>

<div class="col-md-12">
	<div class="row">
	<br>
	              <div class="col-md-12">
		              	<div class="col-md-11"> 
		              			
				               <div class="panel panel-success" id="mostrar" style="display:none;">
				                   <div class="panel-heading" ><center><b>Detalle usuarios</b><i class="fa fa-spinner fa-spin pull-right cargando"></i></center>

				                   </div>
				  				<div class="panel-body">
				  				<span class="pull-right"><input type="text" id="correo" class="form-control buscar_correo" placeholder="Buscar Correo"><br></span>
				  					<div class='col-md-12'>
				                		<div id="respuesta" class='table-responsive'></div>
				                		<br><br>
				                	</div>

				                </div>
				        	</div>



			        </div>
	            </div>
	  </div>     
</div> 


<input type="hidden" id="hotel">
<input type="hidden" id="bd">
<input type="hidden" id="pk">


      
<!-- Nifty Modal -->
                     <div class="md-modal  custom-width md-effect-5" id="mostrar-informacion">
                        <div class="md-content" style="background-color:#ffffff !important; color: #555;">
                            <div class="modal-header colored-header">
                                <div class="modal-title"><h3><center>Operadores</center></h3><br>
                                <small>Replicar correos para los siguientes operadores.</small><br>
                                </div>
                            </div>
                            <div class="modal-body form" id="form-agregar-promocion-body">
                               
                                <br><br>
            
                                        <div class="row" style="height:70% !important;">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <div class='responsive'>

                                                        <input type="hidden" id="id_empresa">
                                                        <input type="hidden" id="usu_login">
                                                        <input type="hidden" id="usu_password">
                                                        <input type="hidden" id="usu_nombre">
                                                        <input type="hidden" id="usu_pat">
                                                        <input type="hidden" id="usu_mat">
                                                        <input type="hidden" id="usu_idioma">
                                                        <input type="hidden" id="usu_mail">

                                                    	
                                                    	CTS <input type="checkbox" class="op" id="cts" value="1">
                                                    	OTSI <input type="checkbox" class="op" id="otsi" value="2">
                                                    	VECI <input type="checkbox" class="op" id="veci" value="3">
                                                    	TURAVION <input type="checkbox" class="op" id="turavion" value="4">
                                                    	COCHA <input type="checkbox" class="op" id="cocha" value="5">
                                                    	TRAVEL <input type="checkbox" class="op" id="travel" value="6">
                                                    	CARLSON <input type="checkbox" class="op" id="carlson" value="7">
                                                    	MUNDOTOUR <input type="checkbox" class="op" id="mundotour" value="8">
                                                    	TOURMUNDIAL <input type="checkbox" class="op" id="tourmundial" value="9">
                                                    	EUROANDINO <input type="checkbox" class="op" id="euroandino" value="10">
                                                    	<br><br><br>

                                                    	<center><button class="btn btn-success pull-right" id="guardar_replicar">Guardar Información</button></center>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <br><br>
                                      

                                <br><br>
                            </div>

                         </div>
                    </div>
                
                    <div class="md-overlay"></div>


<script src="prueba/js/jquery.js" type="text/javascript"></script>
<script src="prueba/js/protip.min.js"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
<script src="prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>
<script src="prueba/js/jquery.numeric.js" type="text/javascript"></script>
<script type="text/javascript" src="prueba/js/jquery.niftymodals/js/jquery.modalEffects.js"></script>
<script>
		
		$(document).on("ready", function(){

			$.protip();

			$.post("modificador_usuarios.php", {
                        verificando_sitio: 1, tipo:1
                    },
                    function (data) {

                        data = $.parseJSON(data);
                        var hotel = '<option value=\'\'></option>';

                        $.each(data.hoteles, function (i, datos) {

                        	var channel = "";

                        	if(datos.channel != "")
                        		channel = " ("+datos.channel+")";

                          hotel += '<option  value="' + datos.id_pk + '" data-id="'+datos.bd+'" data-hotel = "'+datos.id_hotel+'">(' + datos.id_pk + ') ' + datos.nombre_hotel + channel + '</option>';

                        });

                      
                        $("#hoteles").html(hotel);


                    }).done(function () {

              
                      $('select#hoteles').select2({

                            placeholder: "Seleccione Hotel"

                      });


                 });


		});






		

		 $("#hoteles").on("change", function(){

		 			$("#respuesta").html("");
		 			$(".cargando").addClass("fa fa-spinner fa-spin");
		 			

        			var valor = $(this).find(':selected').attr("data-hotel");
        			var bd = $(this).find(':selected').attr("data-id");
        			var pk = $(this).val();

        			$("#hotel").val(valor);
        			$("#bd").val(bd);
        			$("#pk").val(pk);

        			traer_usuarios(pk);

    		});


		
		function traer_usuarios(pk){

				$("#mostrar").css('display','block');

				$.post("modificador_usuarios.php", {
                        verificando_sitio: 1, tipo: 2, pk : pk
                    },
                    function (data) {

                        data = $.parseJSON(data);

                          var tabla ='<table class="table table-bordered" id="usuarios" width="100%" style="height : 60% !important;"><thead>'
                                +'<tr>'
                                    +'<th>Op</th>'
                                    +'<th>Nombre</th>'
                                    +'<th>Pat</th>'
                                    +'<th>Usuario</th>'
                                    +'<th>Pass</th>'
                                    +'<th>Mail</th>'
                                    +'<th>-</th>'
                                    +'<th>-</th>'
                                    +'<th>-</th>'
                                    +'<th>-</th>'
                                +'</tr></thead><tbody>';

                        if(data.usuarios!==undefined){

                        	$.each(data.usuarios, function(i,datos){

                        		var operador = "";

                        		if(datos.cliente == "distantis")
                        			operador = "cts";
                        		else if(datos.cliente == "touravion_dev")
                        			operador = "turavion";
                        		else if(datos.cliente == "travelclub")
                        			operador = "travel"
                        		else
                        			operador = datos.cliente
                        		

                        		if(datos.correo == 1)
                        			color = "class='bg-warning'";
                        		else
                        			color = "";

				                    tabla += "<tr id='usu"+datos.id_usuario+datos.cliente+"' "+color+">"
				                    		+"<td align='left'>"+operador.toUpperCase()+"</td>"
		                                    +"<td align='right'><div class='col-xs-8'><input type='text' value='"+datos.nombre+"'' class='buscar form-control' id='nombre"+datos.cliente+datos.id_usuario+"'></div></td>"
		                                    +"<td align='right'><div class='col-xs-8'><input type='text' value='"+datos.pat+"'' class='buscar form-control' id='pat"+datos.cliente+datos.id_usuario+"'></div></td>"   
		                                    +"<td align='right'><div class='col-xs-8'><input type='text' value='"+datos.login+"''  class='buscar form-control' id='login"+datos.cliente+datos.id_usuario+"'></div></td>"
		                                    +"<td align='left'><div class='col-xs-6'><input type='text' value='"+datos.pass+"''  class='buscar form-control' id='pass"+datos.cliente+datos.id_usuario+"'></div></td>"
		                                    +"<td align='left'><div class='col-xs-8'><input type='text' value='"+datos.mail+"'' class='buscar form-control' id='mail"+datos.cliente+datos.id_usuario+"'></div><i class='fa fa-info-circle text-warning pull-right protip' data-pt-title='"+datos.correos_todos+"' data-pt-scheme='black'></i><span style='display:none'>"+datos.mail+"</span></td>"
		                                    +"<td align='left'><button  class='btn btn-warning  editar' data-id='"+datos.id_usuario+"' data-operador='"+datos.cliente+"'><i class='fa fa-edit'></button></td>"
		                                    +"<td align='left'><button id='replicar' class='btn btn-success modales' data-modal='mostrar-informacion'" 
		                                    	+"data-empresa='"+datos.id_empresa+"' "
		                                    	+"data-login='"+datos.login+"' "
		                                    	+"data-pass='"+datos.pass+"' "
		                                    	+"data-mail='"+datos.mail+"' "
		                                    	+"data-nombre='"+datos.nombre+"' "
		                                    	+"data-app='"+datos.pat+"' "
		                                    	+"data-apm='"+datos.mat+"' "
		                                    	+"data-idioma='"+datos.idioma+"' "
		                                    	+">"
		                                    	+"<i class='fa fa-repeat'></button></td>"
		                                    +"<td align='left'><button  class='btn btn-danger eliminar' data-id='"+datos.id_usuario+"' data-operador='"+datos.cliente+"'><i class='fa fa-trash'></button></td>"
		                                     +"<td align='left'><button  class='btn btn-primary iniciar_session' data-parametros='"+datos.url_credenciales_codificados+"'><i class='fa fa-user'></button></td>"
		                                  +"</tr>"  

	                    	});
									
									
	                	}


	                	tabla += "</thead></tbody>";
	                	$("#respuesta").html(tabla);

                       
                    }


                    ).done(function(){

                    		$(".modales").modalEffects();

                    		$(".modales").on("click", function(){

                    			$('#cts').attr('checked',false);
			    				$('#otsi').attr('checked',false);
			    				$('#veci').attr('checked',false);
			    				$('#turavion').attr('checked',false);
			    				$('#cocha').attr('checked',false);
			    				$('#travel').attr('checked',false);
			    				$('#carlson').attr('checked',false);
			    				$('#mundotour').attr('checked',false);
			    				$('#tourmundial').attr('checked',false);
			    				$('#euroandino').attr('checked',false);


                    			  $("#id_empresa").val($(this).attr("data-empresa"));
                    			  $("#usu_login").val($(this).attr("data-login"));
                    			  $("#usu_password").val($(this).attr("data-pass"));
                    			  $("#usu_nombre").val($(this).attr("data-nombre"));
                    			  $("#usu_pat").val($(this).attr("data-app"));
                    			  $("#usu_mat").val($(this).attr("data-apm"));
                    			  $("#usu_idioma").val($(this).attr("data-idioma"));
                    			  $("#usu_mail").val($(this).attr("data-mail"));

                    		});


                    		$("#guardar_replicar").on("click", function(){


								$(".op:checked").each(function() {
					    			 
									var cliente = $(this).val();
									var id_empresa = $("#id_empresa").val();
									var login = $("#usu_login").val();
									var pass = $("#usu_password").val();
									var nombre = $("#usu_nombre").val();
									var pat = $("#usu_pat").val();
									var mat = $("#usu_mat").val();
									var idioma = $("#usu_idioma").val();
									var mail = $("#usu_mail").val();

									var pk = $("#pk").val();

									if(mail == "" || login == "" || pass == ""){

										mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>Mail/Usuario/Password sin información.</label>", "warning", "bottom-left");

									}else{

										$.post("modificador_usuarios.php", {
						                        verificando_sitio: 1, tipo: 3, cliente:cliente,id_empresa:id_empresa,login:login,pass:pass,nombre:nombre,
						                        pat:pat,mat:mat,idioma:idioma,mail:mail,pk:pk
						                    },
						                    function (data) {

						                        data = $.parseJSON(data);

						                       	switch (data.respuesta){

						                       		case 1:
						                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Replicado en "+data.operador+" Correctamente.</label>", "success", "bottom-left");
						                       			$("#mostrar-informacion").removeClass("md-show");
						                       		break;

						                       		case 2:
						                       			mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>El usuario en "+data.operador+" ya existe.</label>", "warning", "bottom-left");
						                       		break;

						                       		case 3:
						                       			mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>No existe hotel del operador en el pk seleccionado.</label>", "warning", "bottom-left");
						                       		break;

						                       		default:
						                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar guardar el usuario.</label>", "danger", "bottom-left");
						                       		break;
						                       	}


						                });

									}


								});

							});


							$(".editar").on("click", function(){

								var operador = $(this).attr("data-operador");
								var id_usuario = $(this).attr("data-id");

								var login = $("#login"+operador+id_usuario).val();
								var pass = $("#pass"+operador+id_usuario).val();
								var mail = $("#mail"+operador+id_usuario).val();
								var nombre = $("#nombre"+operador+id_usuario).val();
								var pat = $("#pat"+operador+id_usuario).val();

								$.post("modificador_usuarios.php", {
						                        verificando_sitio: 1, tipo: 4, cliente:operador,id_usuario:id_usuario,login:login,pass:pass,mail:mail,nombre:nombre,pat:pat
						                    },
						                    function (data) {

						                        data = $.parseJSON(data);

						                        switch (data.respuesta){

						                       		case 1:
						                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Actualizado Correctamente.</label>", "success", "bottom-left");
						                       		break;

						                       		
						                       		default:
						                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar editar el usuario.</label>", "danger", "bottom-left");
						                       		break;
						                       	}

						                 });

								


							});


							$(".eliminar").on("click", function(){

								var operador = $(this).attr("data-operador");
								var id_usuario = $(this).attr("data-id");

								$.post("modificador_usuarios.php", {
						                        verificando_sitio: 1, tipo: 5, cliente:operador,id_usuario:id_usuario
						                    },
						                    function (data) {

						                        data = $.parseJSON(data);

						                        switch (data.respuesta){

						                       		case 1:
						                       			mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Eliminado Correctamente.</label>", "success", "bottom-left");
						                       			$("#usu"+id_usuario+operador).html("");
						                       		break;

						                       		
						                       		default:
						                       			mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un problema al intentar eliminar el usuario.</label>", "danger", "bottom-left");
						                       		break;
						                       	}

						                 });

								


							});


							$(".iniciar_session").on("click", function(){

                                let parametros_codificados = $(this).data('parametros');
								 window.open("http://h2o.distantis.com/h2o/ingresando.php?"+parametros_codificados, '_blank');

							});



                    	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-left");
							$(".cargando").removeClass("fa fa-spinner fa-spin");






                    });

			}


		$(document).on("keyup",".buscar_correo",function(){
		
			if($(this).val() != ""){
				$("#usuarios tbody>tr").hide();
				$("#usuarios td:contains-ci('" + $(this).val() + "')").parent("tr").show();
			}else
				$("#usuarios tbody>tr").show();
			

		});


		$.extend($.expr[":"], 
		{
		    "contains-ci": function(elem, i, match, array) 
			{
				return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
			}
		});

</script>

</body>
</html>