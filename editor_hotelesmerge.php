
<!DOCTYPE html>
<html>
<head>
	<title>MODIFICADOR HOTELESMERGE</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--meta content="60" http-equiv="REFRESH"> </meta-->
	<link href="prueba/css/bootstrap.css" rel="stylesheet">
	<link href="prueba/fonts/font-awesome-4/css/font-awesome.css" rel="stylesheet" type="text/css">
	<link href="prueba/css/jquery.gritter.css" rel="stylesheet">
	<link href="prueba/css/style_gritter.css"  rel="stylesheet">
	<link href="prueba/js/select2-4.0.0-beta.3/dist/css/select2.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="prueba/js/jquery.datatables/bootstrap-adapter/css/datatables.css" />
</head>
<body>

<br><br>
<div class="col-md-12">

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-7">
					<select id="hoteles" name="hotel" class="form-control select2" style="width:50%;"></select>
			</div>

			<div class="col-md-3">
					<select id="cadena" name="cadena" class="form-control select2" style="width:50%;"></select>
			</div>
		</div>
	</div>

</div>

<div class="col-md-12">
	<div class="row">
	<br>
	              <div class="col-md-12">
		              	<div class="col-md-12"> 
		              			
				               <div class="panel panel-success" id="mostrar" style="display:none;">
				                   <div class="panel-heading" ><center><b>Detalle pk</b><i class="fa fa-spinner fa-spin pull-right cargando"></i></center>

				                   </div>
				  				<div class="panel-body">

				  					<div class='col-md-12'>
				                		<div id="respuesta" class='table-responsive'></div>
				                		<br><br>
				                	</div>

				                	<div class="row">
					                	<div class="col-md-10">

					                		<button class="btn btn-success" id="global">Pasar a global</button>
					                		<button class="btn btn-warning" id="global_quitar">Quitar global</button>
					                		<button class="btn btn-success" id="dispo_normal_agregar">Agregar pestaña dipo</button>
					                		<button class="btn btn-warning" id="dispo_normal_quitar">Quitar pestaña dispo</button>
					                	</div>

				                	</div>

				                </div>
				        	</div>



				        	<div class="panel panel-success" id="mostrar_cadena" style="display:none;">
				                   <div class="panel-heading" ><center><b>Detalle Cadena</b><i class="fa fa-spinner fa-spin pull-right cargando"></i></center>

				                   </div>
				  				<div class="panel-body">

				  					<div class='col-md-12'>
				                		<div id="respuesta_cadena" class='table-responsive'></div>
				                		<br><br>
				                	</div>


				                </div>
				        	</div>


			        </div>
	            </div>
	  </div>     
</div> 


<input type="hidden" id="hotel">
<input type="hidden" id="cadena">


<script src="prueba/js/jquery.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" src="prueba/js/jquery.datatables/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="prueba/js/jquery.datatables/bootstrap-adapter/js/datatables.js"></script>
<script src="prueba/js/jquery.gritter.js" type="text/javascript"></script>
<script src="prueba/js/notificaciones.js" type="text/javascript"></script>
<script src="prueba/js/select2-4.0.0-beta.3/dist/js/select2.min.js"></script>
<script src="prueba/js/jquery.numeric.js" type="text/javascript"></script>
<script>
		
		$(document).on("ready", function(){


			$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo:1
                    },
                    function (data) {

                        data = $.parseJSON(data);
                        var hotel = '<option value=\'\'></option>';

                        $.each(data.hoteles, function (i, datos) {

                          hotel += '<option  value="' + datos.id_pk + '">(' + datos.id_pk + ') ' + datos.nombre_hotel + '</option>';

                        });

                      
                        $("#hoteles").html(hotel);


                    }

                 ).done(function () {

              
                      $('select#hoteles').select2({

                            placeholder: "Seleccione Hotel"

                      });


                 });



                 $.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo:8
                    },
                    function (data) {

                        data = $.parseJSON(data);
                        var cadena = '<option value=\'\'></option>';

                        $.each(data.cadenas, function (i, datos) {

                          cadena += '<option  value="' + datos.id_cadena + '">(' + datos.id_cadena + ') ' + datos.nombre_cadena + '</option>';

                        });

                      
                        $("#cadena").html(cadena);


                    }

                 ).done(function () {

              
                      $('select#cadena').select2({

                            placeholder: "Seleccione Cadena"

                      });


                 });


		});


		$("#global").on("click", function(){

			$(this).html("<i class='fa fa-spinner fa-spin'></i>");
			var id_hotel = $("#hotel").val();

			$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 4, pk: id_hotel
                    },
                    function (data) {

                        data = $.parseJSON(data);

                        switch(data.respuesta){

                        	case 1:
                        		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Hotel ahora es global.</label>", "success", "bottom-left");
                        		$("#global").html("Pasar a Global");
                        	break;

                        	case 2:
                        		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>EL hotel ya se encuentra como global.</label>", "warning", "bottom-left");
                        		$("#global").html("Pasar a Global");
                        	break;

                        	default:
                        		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-left");
                        		$("#global").html("Pasar a Global");
                        	break;

                        }




                     });



		});



		$("#global_quitar").on("click", function(){

			$(this).html("<i class='fa fa-spinner fa-spin'></i>");
			var id_hotel = $("#hotel").val();

			$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 5, pk: id_hotel
                    },
                    function (data) {

                        data = $.parseJSON(data);

                        switch(data.respuesta){

                        	case 1:
                        		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>El hotel ya no es global.</label>", "success", "bottom-left");
                        		$("#global_quitar").html("Quitar Global");
                        	break;

                        	default:
                        		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-left");
                        		$("#global_quitar").html("Quitar Global");
                        	break;

                        }


                     });



		});



		$("#dispo_normal_agregar").on("click", function(){

			//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
			var id_hotel = $("#hotel").val();

			$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 6, pk: id_hotel
                    },
                    function (data) {

                        data = $.parseJSON(data);

                        switch(data.respuesta){

                        	case 1:
                        		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Pestaña normal agregada.</label>", "success", "bottom-left");
                        	break;

                        	case 2:
                        		mostrar_notificacion("Advertencia", "<label style='color:white !important;font-size:13px'>EL hotel ya se encuentra con la pestaña normal activa.</label>", "warning", "bottom-left");
                        	break;

                        	default:
                        		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-left");
                        	break;

                        }


                     });



		});
	

		$("#dispo_normal_quitar").on("click", function(){

			//$(this).html("<i class='fa fa-spinner fa-spin'></i>");
			var id_hotel = $("#hotel").val();

			$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 7, pk: id_hotel
                    },
                    function (data) {

                        data = $.parseJSON(data);

                        switch(data.respuesta){

                        	case 1:
                        		mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Pestaña normal eliminada.</label>", "success", "bottom-left");
                        	break;

                        	default:
                        		mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Error al intentar actualizar.</label>", "danger", "bottom-left");
                        	break;

                        }


                     });



		});


		 $("#hoteles").on("change", function(){

		 			$("#respuesta").html("");
		 			$(".cargando").addClass("fa fa-spinner fa-spin");
		 			$("#mostrar_cadena").css("display", "none");


        			var valor = $(this).val();

        			$("#hotel").val(valor);

        			traer_pk(valor);

    		});


		 $("#cadena").on("change", function(){

		 			$("#respuesta_cadena").html("");
		 			$(".cargando").addClass("fa fa-spinner fa-spin");
		 			$("#mostrar").css("display", "none");


        			var valor = $(this).val();

        			$("#cadena").val(valor);


        			traer_cadena(valor);

    		});



		function traer_pk(id_hotel){

				var revisar = "";
				var guardar = "";

				$("#mostrar").css('display','block');

				$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 2, pk: id_hotel
                    },
                    function (data) {

                        data = $.parseJSON(data);

                          var tabla ='<table class="table table-bordered" id="editor" width="100%" style="height : 60% !important;"><thead>'
                                +'<tr>'
                                    +'<th>CTS</th>'
                                    +'<th>VECI</th>'
                                    +'<th>OTSI</th>'
                                    +'<th>TURAVION</th>'
                                    +'<th>COCHA</th>'
                                    +'<th>TRAVEL</th>'
                                    +'<th>CARSLON</th>'
                                    +'<th>MUNDOTOUR</th>'
                                    +'<th>TOURMUNDIAL</th>'
                                    +'<th>EUROANDINO</th>'
                                    +'<th>Cadena</th>'
                                +'</tr></thead><tbody>';

                        if(data.detalle!==undefined){

                        	$.each(data.detalle, function(i,datos){
                        
	                     
				                    tabla += "<tr>"
		                                    +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.cts+"'' class='numeric buscar form-control' id='cts'></div></td>"
		                                    +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.veci+"'' class='numeric buscar form-control' id='veci'></div></td>"
		                                    +"<td align='left'><div class='col-xs-5'><input type='text' value='"+datos.otsi+"'' class='numeric buscar form-control' id='otsi'></div></td>"
		                                    +"<td align='left'><div class='col-xs-5'><input type='text' value='"+datos.turavion+"'' class='numeric buscar form-control' id='turavion'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.cocha+"'' class='numeric buscar form-control' id='cocha'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.travel+"'' class='numeric buscar form-control' id='travelclub'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.carlson+"'' class='numeric buscar form-control' id='carlson'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.mundotour+"'' class='numeric buscar form-control' id='mundotour'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.tourmundial+"'' class='numeric buscar form-control' id='tourmundial'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.euroandino+"'' class='numeric buscar form-control' id='euroandino'></div></td>"
		                                    +"<td align='center'><div class='col-xs-5'><input type='text' value='"+datos.cadena+"'' class='numeric buscar form-control' id='cadena'></div></td>"	
		                                  +"</tr>"  

	                    	});
									
									
	                	}


	                	tabla += "</thead></tbody>";
	                	$("#respuesta").html(tabla);

                       
                    }


                    ).done(function(){

                    		$(".numeric").numeric();

                    	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-left");
							$(".cargando").removeClass("fa fa-spinner fa-spin");
							


							$(".buscar").on("blur", function(){


								var id = $(this).attr("id");
								var valor = $(this).val();


									$.post("modificador_hotelesmerge.php", {
	                        				verificando_sitio: 1, tipo: 3, id: id, valor:valor,pk: id_hotel
	                    			},

	                    			function (data) {

	                        			data = $.parseJSON(data);


	                        			if(data.respuesta == 1)
	                        				mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente.</label>", "success", "bottom-left");
	                        			else
	                        				mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar actualizar la información.</label>", "danger", "bottom-left");



	                        		});





							});



                    });

			}



			function traer_cadena(id_cadena){

				$("#mostrar_cadena").css('display','block');

				$.post("modificador_hotelesmerge.php", {
                        verificando_sitio: 1, tipo: 9, id_cadena: id_cadena
                    },
                    function (data) {

                        data = $.parseJSON(data);

                          var tabla ='<table class="table table-bordered" id="editor_cadena" width="100%" style="height : 60% !important;"><thead>'
                                +'<tr>'
                                    +'<th>Nombre</th>'
                                    +'<th>Modo</th>'
                                +'</tr></thead><tbody>';

                        if(data.detalle_cadena!==undefined){

                        	$.each(data.detalle_cadena, function(i,datos){
                        
	                     
				                    tabla += "<tr>"
		                                    +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.nom_cadena+"'' class='buscar form-control' id='nom_cadena'></div></td>"
		                                    +"<td align='right'><div class='col-xs-5'><input type='text' value='"+datos.modo+"'' class='numeric buscar form-control' id='modo'></div></td>"
		                                  +"</tr>"  

	                    	});
									
									
	                	}


	                	tabla += "</thead></tbody>";
	                	$("#respuesta_cadena").html(tabla);

                       
                    }


                    ).done(function(){

                    		$(".numeric").numeric();


                    	    mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Detalle correcto.</label>", "success", "bottom-left");
							$(".cargando").removeClass("fa fa-spinner fa-spin");
							


							$(".buscar").on("blur", function(){


								var id = $(this).attr("id");
								var valor = $(this).val();


									$.post("modificador_hotelesmerge.php", {
	                        				verificando_sitio: 1, tipo: 10, id: id, valor:valor,id_cadena: id_cadena
	                    			},

	                    			function (data) {

	                        			data = $.parseJSON(data);


	                        			if(data.respuesta == 1)
	                        				mostrar_notificacion("Éxito", "<label style='color:white !important;font-size:13px'>Se ha actualizado correctamente.</label>", "success", "bottom-left");
	                        			else
	                        				mostrar_notificacion("ERROR", "<label style='color:white !important;font-size:13px'>Ocurrio un error al intentar actualizar la información.</label>", "danger", "bottom-left");



	                        		});





							});



                    });

			}




</script>

</body>
</html>