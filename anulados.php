<?php
include("secure.php");
//require_once("includes/mailing.php");
require_once("Connections/db1.php");

error_reporting(E_ALL);

$qclientes = "SELECT * FROM clientes WHERE estado = 0";
$clientes=$db1->SelectLimit($qclientes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_fechas = "SELECT 
				  DATE_FORMAT(NOW(), '%Y-%m-%d') AS desde,
				  DATE_FORMAT(
					DATE_ADD(NOW(), INTERVAL 30 DAY),
					'%Y-%m-%d'
				  ) AS hasta";
$fechas=$db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$desde = $fechas->Fields('desde');
$hasta = $fechas->Fields('hasta');

$query_cadenas = "SELECT 
					  * 
					FROM
					  (SELECT 
						* 
					  FROM
						cadena 
					  WHERE cad_estado = 0 
					  ORDER BY nom_cadena ASC) tb1 
					  INNER JOIN (SELECT 
					  IFNULL(hm.`id_hotel_cts`, 0) AS id_hotel_cts,
					  hcts.`hot_nombre` AS cts_nom,
					  IFNULL(hm.`id_hotel_otsi`, 0) AS id_hotel_otsi,
					  hotsi.`hot_nombre` AS otsi_nom,
					  IFNULL(hm.`id_hotel_veci`, 0) AS id_hotel_veci,
					  hveci.`hot_nombre` AS veci_nom,
					  IFNULL(hm.`id_hotel_turavion`, 0) AS id_hotel_turavion,
					  hturavion.`hot_nombre` AS turavion_nom,
					  IFNULL(hm.`id_hotel_cocha`, 0) AS id_hotel_cocha,
					  hcocha.`hot_nombre` AS cocha_nom,
					  hm.`id_cadena`,
					  hm.id_pk 
					FROM
					  hotelesmerge hm 
					  LEFT JOIN hotelcts hcts 
						ON hm.`id_hotel_cts` = hcts.`id_hotel` 
					  LEFT JOIN hotelotsi hotsi 
						ON hm.`id_hotel_otsi` = hotsi.`id_hotel` 
					  LEFT JOIN hotelveci hveci 
						ON hm.`id_hotel_veci` = hveci.`id_hotel` 
					  LEFT JOIN hotelturavion hturavion 
						ON hm.`id_hotel_turavion` = hturavion.`id_hotel`
					LEFT JOIN hotelcocha hcocha 
						ON hm.`id_hotel_cocha` = hcocha.`id_hotel`
						) tb2 
						ON tb1.id_cadena = tb2.id_cadena
						order by nom_cadena";
$cadenas=$db1->Execute($query_cadenas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());





if(isset($_POST['aplicar'])){
	
	$desde = $_POST['desde1'];
	$hasta = $_POST['hasta1'];
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--meta content="90" http-equiv="REFRESH"> </meta!-->
	<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
	<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
	<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	
	<script>
		$(function(){
			$("#desde1").datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$("#hasta1").datepicker({
				dateFormat: 'yy-mm-dd'
			});
			
		});
	</script>
	
</head>


<body>
	<div id="container" class="inner">
		<div id="header">
			<? include("bienvenida.php");?>
			<ul id="nav">
				<li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
				<li class="servicios activo"><a href="nums_conf.php" title="<? echo $servind_tt;?>" class="tooltip">N Conf Pendiente</a></li>
				<li class="servicios activo"><a href="anulados.php" title="<? echo $servind_tt;?>" class="tooltip">Reservas anuladas</a></li> 
			</ul>

			<ol id="pasos">

            </ol>
		</div>
		<div style="margin-left:auto;margin-right:auto;width: 1024px;">
			<div id="nueva" width = '1024px' >
			<form method="post"> <center>
		<table>
		
		<tr>
			<th colspan="4"><center>Rango de fechas</center></th>
		</tr>
		 <tr> 
			<th>Desde:</th> 
			<td><center><input type="text" id="desde1" name="desde1" value="<?=$desde?>"></center></td> 
			<th>Hasta:</th>
			<td><center><input type="text" id="hasta1" name="hasta1" value="<?=$hasta?>"></center></td>
		 </tr>
		 
		 </table></center>
		 	<table>
				<tr>
					<th>Hotel</th>
					<td>
					<select id="cadena">
						<option value ="0">Selecciona un Hotel</option>
						<?
						while (!$cadenas->EOF){

							if($cadenas -> Fields('cts_nom')!=""){
							$hotel_nom = $cadenas -> Fields('cts_nom');
							}else{
							if($cadenas -> Fields('otsi_nom')!=""){
							$hotel_nom = $cadenas -> Fields('otsi_nom');
							}else{
							if($cadenas -> Fields('veci_nom')!=""){
							$hotel_nom = $cadenas -> Fields('veci_nom');
							}else{
							if($cadenas -> Fields('turavion_nom')!=""){
							$hotel_nom = $cadenas -> Fields('turavion_nom');
							}else{
							if($cadenas -> Fields('cocha_nom')!=""){
							$hotel_nom = $cadenas -> Fields('cocha_nom');
							}
							}
							}
							}
							}						
						?>
						<option value="<?=$cadenas -> Fields('id_pk')?>"><?=$cadenas -> Fields('nom_cadena')."-".$hotel_nom?></option>
						<?
						$cadenas -> MoveNext();
						} 
						?>
					</select>
					</td>
					
					
				</tr>
				<tr>
			<td colspan="2"><center><input type="submit" id="aplicar" name="aplicar" value="Aplicar"></center></td>
		 </tr>
				
			</table>
		 
		 </form>

<table class="programa" align="center">
	
<?php

while (!$clientes->EOF) {
	
$query_anulados = "SELECT 
					  c.id_cot,
					  id_cotdes,
					  cd.`cd_fecdesde` as desde,
					  cd.`cd_fechasta` as hasta,
					  cd.`cd_hab1`,
					  cd.`cd_hab2`,
					  cd.`cd_hab3`,
					  cd.`cd_hab4`,
					  h.`hot_nombre`,
					  h.`hot_fono` 
					FROM 
					  ".$clientes->Fields('bd').".cot c 
					  INNER JOIN ".$clientes->Fields('bd').".cotdes cd 
						ON c.id_cot = cd.id_cot 
					  INNER JOIN ".$clientes->Fields('bd').".hotel h 
						ON h.id_hotel = cd.id_hotel 
					WHERE (
						cd.`cd_estado` = 1 
						OR c.cot_estado = 1
					  ) 
					  AND c.id_seg = 7 
					  AND cd.`cd_fecdesde` BETWEEN '".$desde."' 
					  AND '".$hasta."'
					  order by hot_nombre asc";
		
//echo $query_anulados."<br><br>"; 	
$anulados=$db1->SelectLimit($query_anulados) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		$respuesta = "<tr>";
		$respuesta.= "<th colspan='7'><center> Cliente ".$clientes->Fields('nombre')."</center></th>";
		$respuesta.= "</tr>";
		$respuesta.= "<tr>";
		$respuesta.= "<th>Cot</th>";
		$respuesta.= "<th>Hotel</th>";
		$respuesta.= "<th>Fono</th>";
		$respuesta.= "<th>Nombre Pax</th>";
		$respuesta.= "<th>Habs</th>";
		$respuesta.= "<th>Fec in</th>";
		$respuesta.= "<th>Fec out</th>";
		$respuesta.= "</tr>";
		
	while (!$anulados->EOF) {
	
		$respuesta.= "<tr>";
		$respuesta.= "<td><center>".$anulados->Fields('id_cot')."</center></td>";
		$respuesta.= "<td><center>".$anulados->Fields('hot_nombre')."</center></td>";
		$respuesta.= "<td><center>".$anulados->Fields('hot_fono')."</center></td>";
		$respuesta.= "<td><center>S:".$anulados->Fields('cd_hab1')." - tw:".$anulados->Fields('cd_hab2')." - ma:".$anulados->Fields('cd_hab3')." - tpl:".$anulados->Fields('cd_hab4')."</center></td>";
		$respuesta.= "<td><center>S:".$anulados->Fields('cd_hab1')." - tw:".$anulados->Fields('cd_hab2')." - ma:".$anulados->Fields('cd_hab3')." - tpl:".$anulados->Fields('cd_hab4')."</center></td>";
		$respuesta.= "<td><center>".$anulados->Fields('desde')."</center></td>";
		$respuesta.= "<td><center>".$anulados->Fields('hasta')."</center></td>";
		$respuesta.= "</tr>";
		
		
	$anulados->MoveNext();
	}
	
	echo $respuesta;
$clientes->MoveNext();
}
 ?>

 </table>
  </div></div></div>
 </body>
 </html>