<?php	 	 
include ("secure.php");
//require_once ('includes/mailing.php');
require_once('Connections/db1.php');
$clientes_query = "select * from clientes where estado = 0";
$clientes=$db1->Execute($clientes_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$sonando = false;

$query_hoteles = "SELECT 
					  id_pk,
					  id_hotel_cts,
					  hcts.hot_nombre AS cts,
					  id_hotel_veci,
					  hveci.hot_nombre AS veci,
					  id_hotel_otsi,
					  hotsi.hot_nombre AS otsi,
					  id_hotel_turavion,
					  htura.hot_nombre AS turavion,
					  id_hotel_cocha,
					  hcocha.hot_nombre AS cocha,
					  hm.id_cadena,
					  c.`nom_cadena` as cadena
					FROM
					  hotelesmerge hm
					  LEFT JOIN distantis.hotel hcts
					  ON hm.`id_hotel_cts` = hcts.id_hotel
					  LEFT JOIN veci.hotel hveci
					  ON hm.`id_hotel_veci` = hveci.`id_hotel`
					  LEFT JOIN otsi.hotel hotsi
					  ON hm.id_hotel_otsi = hotsi.`id_hotel`
					  LEFT JOIN touravion_dev.hotel htura
					  ON hm.`id_hotel_turavion` = htura.`id_hotel`
					  LEFT JOIN cocha.hotel hcocha
					  ON hm.`id_hotel_cocha` = hcocha.`id_hotel`
					  INNER JOIN cadena c
					  ON hm.`id_cadena` = c.`id_cadena`
					  ORDER BY nom_cadena";
$hoteles = $db1->SelectLimit($query_hoteles) or die("Error: <br>".$query_hoteles);

$query_cts = "SELECT 
				  * 
				FROM
				  distantis.hotel h 
				  LEFT JOIN hotelesmerge hm
				  ON h.id_hotel = hm.`id_hotel_cts`
				WHERE h.`id_tipousuario` = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  AND id_hotel_cts IS NULL";
$cts = $db1->SelectLimit($query_cts) or die("Error: <br>".$query_cts);


$rcts = "<option value='0'>Sin Asignar</option>";
while (!$cts->EOF){
	$rcts.= "<option value='".$cts -> Fields('id_hotel')."'>".$cts -> Fields('hot_nombre')."</option>";
$cts -> MoveNext();
				}

$query_veci = "SELECT 
				  * 
				FROM
				  veci.hotel h 
				  LEFT JOIN hotelesmerge hm
				  ON h.id_hotel = hm.`id_hotel_veci`
				WHERE h.`id_tipousuario` = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  AND id_hotel_cts IS NULL";
$veci = $db1->SelectLimit($query_veci) or die("Error: <br>".$query_veci);

$rveci = "<option value='0'>Sin Asignar</option>";
while (!$veci->EOF){
	$rveci.= "<option value='".$veci -> Fields('id_hotel')."'>".$veci -> Fields('hot_nombre')."</option>";
$veci -> MoveNext();
				}

$query_otsi = "SELECT 
				  * 
				FROM
				  otsi.hotel h 
				  LEFT JOIN hotelesmerge hm
				  ON h.id_hotel = hm.`id_hotel_otsi`
				WHERE h.`id_tipousuario` = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  AND id_hotel_cts IS NULL";
$otsi = $db1->SelectLimit($query_otsi) or die("Error: <br>".$query_otsi);

$rotsi = "<option value='0'>Sin Asignar</option>";
while (!$otsi->EOF){
	$rotsi.= "<option value='".$otsi -> Fields('id_hotel')."'>".$otsi -> Fields('hot_nombre')."</option>";
$otsi -> MoveNext();
				}

$query_tura = "SELECT 
				  * 
				FROM
				  touravion_dev.hotel h 
				  LEFT JOIN hotelesmerge hm
				  ON h.id_hotel = hm.`id_hotel_turavion`
				WHERE h.`id_tipousuario` = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  AND id_hotel_cts IS NULL";
$tura = $db1->SelectLimit($query_tura) or die("Error: <br>".$query_tura);

$rtura = "<option value='0'>Sin Asignar</option>";
while (!$tura->EOF){
	$rtura.= "<option value='".$tura -> Fields('id_hotel')."'>".$tura -> Fields('hot_nombre')."</option>";
$tura -> MoveNext();
				}

$query_cocha = "SELECT 
				  * 
				FROM
				  cocha.hotel h 
				  LEFT JOIN hotelesmerge hm
				  ON h.id_hotel = hm.`id_hotel_cocha`
				WHERE h.`id_tipousuario` = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  AND id_hotel_cts IS NULL";
$cocha = $db1->SelectLimit($query_cocha) or die("Error: <br>".$query_cocha);

$rcocha = "<option value='0'>Sin Asignar</option>";
while (!$cocha->EOF){
	$rcocha.= "<option value='".$cocha -> Fields('id_hotel')."'>".$cocha -> Fields('hot_nombre')."</option>";
$cocha -> MoveNext();
				}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"

    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">  

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 

	
	</head>
	<script>
	
		function aplicar(contenido){
		
		cocha = $("#cocha_"+contenido).val();
		//alert(cocha);
		actual = contenido;
		$('#td_cocha_'+contenido).html('<div><center><img src="images/loading2.gif" width="70px" height="70px"/></center></div>');
		
		$.ajax({
					type: 'POST',
					url: 'solicitar_hoteles2.php',
					data: {
						flag: "aplicar",
						id_pk: contenido,
						hotelc: cocha
					},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#td_cocha_"+contenido).html(divOtra);
						
					},
					error:function(){
						alert("Error 1!!")
					}
			    });
		}
	</script>


<body onLoad="document.form.txt_numpas.focus();">

    <div id="container" class="inner">

        <div id="header">
			
			
		<?php	 	 include ("bienvenida.php");?>



            <ul id="nav">

                <li class="servicios activo"><a href="or_main.php" title="<? echo $servind_tt;?>" class="tooltip">On Request</a></li>
				<li class="servicios activo"><a href="ntarifa.php" title="<? echo $servind_tt;?>" class="tooltip">Nueva Tarifa</a></li>
				<li class="servicios activo"><a href="m_empresas.php" title="<? echo $servind_tt;?>" class="tooltip">Empresas</a></li>
		   </ul>

            <ol id="pasos">
				<li class="paso1 activo"><a href="hoteles.php" title="<? echo $progdest;?>">Cadena de hoteles</a></li>
				<li class="paso1 activo"><a href="change_password.php" title="<? echo $progdest;?>">Cambiar contrase&ntilde;a</a></li>
				<li class="paso1 activo"><a href="hoteles2.php" title="<? echo $progdest;?>">Manejo de hoteles</a></li> 
            </ol>													    

        </div>

        <!-- INICIO Contenidos principales-->

        <div style="margin-left:auto;margin-right:auto;width: 1024px;">

			<table>
				<tr>
					<td>Id PK</td>
					<td>CTS</td>
					<td>VECI</td>
					<td>OTSI</td>
					<td>TURAVION</td>
					<td>COCHA</td>
					<td>CADENA</td>
					<td>ACCION</td>
				</tr>
				<?while (!$hoteles->EOF){?>
				<tr>
					<td><?=$hoteles -> Fields('id_pk')?></td>
					<td id="td_cts_<?=$hoteles -> Fields('id_pk')?>">
					<?
					if($hoteles -> Fields('id_hotel_cts')==0){
						echo "<select id='cts_".$hoteles -> Fields('id_pk')."'>".$rcts."</select>";
					}else{
						echo $hoteles -> Fields('cts');
					}
					?>
					</td>
					<td id="td_veci_<?=$hoteles -> Fields('id_pk')?>">
					<?
					if($hoteles -> Fields('id_hotel_veci')==0){
						echo "<select id='veci_".$hoteles -> Fields('id_pk')."'>".$rveci."</select>";
					}else{
						echo $hoteles -> Fields('veci');
					}
					?>
					</td>
					<td id="td_otsi_<?=$hoteles -> Fields('id_pk')?>">
					<?if($hoteles -> Fields('id_hotel_otsi')==0){
						echo "<select id='otsi_".$hoteles -> Fields('id_pk')."'>".$rotsi."</select>";
					}else{
						echo $hoteles -> Fields('otsi');
					}?>
					</td>
					<td id="td_tura_<?=$hoteles -> Fields('id_pk')?>">
					<?if($hoteles -> Fields('id_hotel_turavion')==0){
						echo "<select id='tura_".$hoteles -> Fields('id_pk')."'>".$rtura."</select>";
					}else{
						echo $hoteles -> Fields('turavion');
					}?>
					</td>
					<td id="td_cocha_<?=$hoteles -> Fields('id_pk')?>">
					<?if($hoteles -> Fields('id_hotel_cocha')==0){
						echo "<select id='cocha_".$hoteles -> Fields('id_pk')."'>".$rcocha."</select>";
					}else{
						echo $hoteles -> Fields('cocha');
					}?>
					</td>
					<td ><?=$hoteles -> Fields('cadena')?></td> 
					<td><button onclick="aplicar(<?=$hoteles -> Fields('id_pk')?>)">Aplicar</button></td>

				</tr>
				<?$hoteles -> MoveNext();
				}?>
			</table>

        <!-- FIN Contenidos principales -->





        

    

    </div>
    <!-- FIN container -->

</body>

</html>

