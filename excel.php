<?php

$excel = new Excel();


class Excel{


    protected $sql_con;
    protected $datos = array();
    protected $datos_usuario = array();

    
    
    public function __construct(){
    
        require_once('Connections/db1.php');
        require_once('lib/PHPExcel/PHPExcel.php');
        $this->set_conexion($db1);
        $this->obtener_parametros();        
    }
    
    
    protected function set_conexion($db1){
    
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
    }
    
   
    
    protected function obtener_parametros(){
        
        extract($_GET);
        $this->datos_usuario["sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] = $tipo;
        $this->datos_usuario["id_pk"] = $id_pk;
        $this->datos_usuario["desde"] = date('Y-m-d',strtotime($desde));
        $this->datos_usuario["hasta"] = date('Y-m-d',strtotime($hasta));
        $this->quitar_caracteres();
        //$this->traer_creditos();
        //$this->generar_excel();
        //$this->datos_usuario["imagen"] = mysqli_real_escape_string($this->sql_con, $imagen);
        $this->tipo();
     
    }
    
    
     protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["id_pk"]);
        $this->mssql_escape($this->datos_usuario["desde"]);
        $this->mssql_escape($this->datos_usuario["hasta"]);

        
    }
    
    
    protected function tipo(){
        
        
        switch($this->datos_usuario["tipo"]){
            
            
            case 1:
                

               $this->generar_excel_nmr_confirmacion();
            
            break;


            
            
        }
        
        
        
    }
    
   
    
    
    protected function generar_excel_nmr_confirmacion(){
        
        $objPHPExcel = new PHPExcel();
                        $objPHPExcel->getActiveSheet()->setTitle("Confirmacion");
                            $objPHPExcel->getProperties()
                            ->setCreator("")
                            ->setLastModifiedBy("")
                            ->setTitle("")
                            ->setSubject("")
                            ->setDescription("")
                            ->setKeywords("")
                            ->setCategory("");    


                        $tituloReporte = "Nmr Confirmacion";
                        $titulosColumnas = array('Operador','Cot','Desde','Hotel','Nemo','Fono','Nombre Pax','Habs','Fec In','Fec Out');

                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1',$tituloReporte) 
                            ->setCellValue('A3',  $titulosColumnas[0])
                            ->setCellValue('B3',  $titulosColumnas[1])
                            ->setCellValue('C3',  $titulosColumnas[2])
                            ->setCellValue('D3',  $titulosColumnas[3])
                            ->setCellValue('E3',  $titulosColumnas[4])
                            ->setCellValue('F3',  $titulosColumnas[5])
                            ->setCellValue('G3',  $titulosColumnas[6])
                            ->setCellValue('H3',  $titulosColumnas[7])
                            ->setCellValue('I3',  $titulosColumnas[8])
                            ->setCellValue('J3',  $titulosColumnas[9]);

                        $objPHPExcel->getActiveSheet()->getStyle("A3:J3")->getFont()->setBold(true);

                    $clientes_query = "select * from clientes where estado = 0";
                    $clientes=$this->sql_con->SelectLimit($clientes_query);
                  
                    $this->datos["cliente"] = array();

                    $i=0;
                    while (!$clientes->EOF){

                    $i+=1;

                  
                    if($i==5){

                          $agregar_nemo_select = ",n.nemo, pag_dir, plataxplata";
                          $agregar_nemo_join = "inner join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
                    }else{

                          $agregar_nemo_select = "";
                          $agregar_nemo_join = "";
                    }



                          $consulta = "

                                  SELECT 
                                    c.id_cot,
                                    id_cotdes,
                                    hot_nombre,
                                    hot_fono,
                                    c.id_usuario,
                                    CONCAT(
                                      TRIM(c.cot_pripas),
                                      ' ',
                                      TRIM(c.cot_pripas2)
                                    ) AS nombre_pax,
                                    cd_hab1,
                                    cd_hab2,
                                    cd_hab3,
                                    cd_hab4,
                                    DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                                    DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                                    cd_fecdesde,
                                    cd_fechasta,
                                    cd_numreserva ".$agregar_nemo_select." 
                                  FROM
                                    ".$clientes -> Fields('bd').".cot c 
                                    INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                                      ON c.id_cot = cd.id_cot 
                                    INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                                      ON cd.id_hotel = h.id_hotel 
                                    ".$agregar_nemo_join."
                                  WHERE cot_estado = 0 
                                    AND c.id_seg = 7 
                                    AND cd.id_seg = 7 
                                    AND (
                                      cd_numreserva IS NULL 
                                      OR TRIM(cd_numreserva) = ''
                                    ) 
                                    AND cd.cd_estado = 0 
                                    
                                  ORDER BY cd_fecdesde,
                                    nombre_pax ASC 

                          ";

                          //AND cd_fecdesde BETWEEN DATE_ADD(NOW(), INTERVAL - 4 DAY) 
                          // AND DATE_ADD(NOW(), INTERVAL 10 DAY) 
                           // echo consulta;
                          $enviar = $this->sql_con->SelectLimit($consulta);

                          $operador = $clientes -> Fields('bd');
                          while(!$enviar->EOF){


                              $num_reserva = $enviar->Fields("cd_numreserva");

                              $reserva = "";
                              if($num_reserva == "")
                                  $reserva = "";
                              else
                                  $reserva = $num_reserva;


                              $fono = $enviar->Fields("hot_fono");

                              $telefono = "";
                              if($fono == "")
                                  $telefono = "";
                              else
                                  $telefono = $fono;


                              if($i==5){

                                  $nemo_enviar = "";
                                  
                                  if($enviar->Fields("nemo") == "")
                                      $nemo_enviar = "";
                                  else
                                      $nemo_enviar = $enviar->Fields("nemo");


                                  $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                                  "plataxplata"=>$enviar->Fields("plataxplata"),
                                                  "nemo"=>utf8_encode($nemo_enviar));
                              }else
                                  $nuevos_datos = "";


                              $a = array(

                                      "cot"  => $enviar->Fields("id_cot"),
                                      "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                                      "fono" => $telefono,
                                      "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                                      "desde"  => $enviar->Fields("fec_in"),
                                      "hasta"  => $enviar->Fields("fec_out"),
                                      "cd_fecdesde"  => $enviar->Fields("cd_fecdesde"),
                                      "cd_fechasta"  => $enviar->Fields("cd_fechasta"),
                                      "hab1"  => $enviar->Fields("cd_hab1"),
                                      "hab2"  => $enviar->Fields("cd_hab2"),
                                      "hab3"  => $enviar->Fields("cd_hab3"),
                                      "hab4"  => $enviar->Fields("cd_hab4"),
                                      "cotdes" => $enviar->Fields("id_cotdes"),
                                      "operador" =>$operador,
                                      "num_reserva"=>$reserva,
                                      "cliente"=>$i,
                                      "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                                      $nuevos_datos

                                  );

                                  

                              array_push($this->datos["cliente"], $a);


                              $enviar->MoveNext();

                          }


                          $clientes->MoveNext();

                      }



                      $i = 4;

                        foreach($this->datos["cliente"] as $tipo=>$datos){


                          if($datos["nemo"] != "")
                            $nemo = $datos["nemo"];
                          else
                            $nemo = "Sin";

                                         $objPHPExcel->setActiveSheetIndex(0)
                                                ->setCellValue('A'.$i, $datos["operador"])
                                                ->setCellValue('B'.$i, $datos["cot"])
                                                #->setCellValue('C'.$i, $datos["hace_cuanto"])
                                                ->setCellValue('C'.$i, $datos["cd_fecdesde"])
                                                ->setCellValue('D'.$i, $datos["hotel"])
                                                ->setCellValue('E'.$i, $nemo)
                                                ->setCellValue('F'.$i, $datos["fono"])
                                                ->setCellValue('G'.$i, $datos["pax"])
                                                ->setCellValue('H'.$i, "S: ".$datos["hab1"]."Twn: ".$datos["hab2"]."Dbl: ".$datos["hab3"]."Tpl: ".$datos["hab1"])
                                                ->setCellValue('I'.$i, $datos["desde"])
                                                ->setCellValue('J'.$i, $datos["hasta"]);
                                    
                                           $i++;



                        }
                        
                        $fecha = date('d-m-Y');
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="Nmr_Reservas"'.$fecha.'".xlsx"');
                        header('Cache-Control: max-age=0');

                        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                        $objWriter->save('php://output');
        
        
    }


    protected function calcular_fecha($bd,$cot){

      if(isset($this->datos_usuario["sitio"])){

          /*$consulta = "select TIMEDIFF(NOW(),cot_fec) as diferencia from $bd.cot where id_cot = $cot";
          $traer = $this->sql_con->SelectLimit($consulta);
          $diferencia = $traer->Fields("diferencia");

          return $diferencia;*/

          $consulta = "select cot_fec from $bd.cot where id_cot = $cot";
          //echo $consulta."<br>";
          $traer = $this->sql_con->SelectLimit($consulta);

          $fecha_bd = $traer->Fields("cot_fec");
          $fecha_del_dia = date('Y-m-d H:i:s',strtotime("-2 hour"));

          //if($cot == 354313)
             //echo $fecha_bd."<=>".date('Y-m-d H:i:s',strtotime("-2 hour"))."<br>";


          $fecha1 = new DateTime("$fecha_bd");
          $fecha2 = new DateTime("$fecha_del_dia");
          $fecha = $fecha1->diff($fecha2);

          $ano = $fecha->y;
          $meses = $fecha->m;
          $dias = $fecha->d;
          $horas = $fecha->h;
          $minutos = $fecha->i;


          $diferencia = "A:".$ano." M:".$meses." D:".$dias." Hrs:".$horas." Min:".$minutos;

          return $diferencia;

      }



    }

    
    
    protected function getdia($ndia){

     if($ndia != ""){
      
           $nombre = "";
           
            if($ndia==1){
                $nombre = "LUN";
            }else if($ndia==2){
                $nombre = "MAR";
            }else if($ndia==3){
                $nombre = "MIE";
            }else if($ndia==4){
                $nombre = "JUE";
            }else if($ndia==5){
               $nombre = "VIE"; 
            }else if($ndia==6){
                $nombre = "SAB";
            }else if($ndia==7){
                $nombre = "DOM";
            }
            
            return $nombre;
        
        }
    
    }

    
    
    
     protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }
    
    
    
     protected function cambiar_fecha($fecha){
        
        $nueva_fecha = "";
        
        $nueva_fecha = date('d-m-Y', strtotime($fecha));
        
        return $nueva_fecha;
        
        
        
    }
    
    
    protected function devolver_entero($rut){
        
        $entero = "";
        
            $entero = preg_replace('/^0+/', '', $rut);

        return $entero;
        
    }
    
    
    

}
       


?>