<?php

$traernumconf = new TraerNumConf();


class TraerNumConf{

	//protected $link;
	protected $sql_con;
	protected $datos = array();
	protected $datos_usuario = array();

	
	
	public function __construct(){
        error_reporting(0);
        //ini_set("display_errors", 1);
        //ini_set('memory_limit', '512M');
        session_start();
        require_once('Connections/db1.php');
        $this->set_conexion($db1);
        $this->obtener_parametros();
		//$this->revisar_existe();
        //$this->tipo();
		
	}
	
	
	protected function set_conexion($db1){
	
        $this->sql_con = $db1;
        //$this->sql_con->set_charset('utf8');
	}
    
    
    public function obtener_parametros(){
        
        extract($_POST);
        $this->datos_usuario["sitio"] = $verificando_sitio;
        $this->datos_usuario["tipo"] =  $tipo;
        $this->datos_usuario["desde"] =  date('Y-m-d',strtotime($desde));
        $this->datos_usuario["hasta"] =  date('Y-m-d',strtotime($hasta));
        $this->datos_usuario["cot"] = $cot;
        $this->datos_usuario["fono"] = $fono;
        $this->datos_usuario["cadena"] = $cadena;
        $this->datos_usuario["id_usuario"] = $_SESSION['id'];
        $this->quitar_caracteres();
        $this->revisar_tipo();
        
     
    }
    
    
    public function revisar_tipo(){
        
       
        switch($this->datos_usuario["tipo"]){

            case 1:
                
                $this->traer_numconf();
                
            break;


            case 2:
                
                $this->traer_numconf_filtro();
                
            break;


            case 3:
                
                $this->traer_numconf_buscar();
                
            break;

             case 4:
                
                $this->agrgear_fono();
                
            break;
 
         
            
        
        }
        
    }

    protected function agrgear_fono(){
        
        if(isset($this->datos_usuario["sitio"])){

            $guardar = "insert into hoteles.fono_hotel_por_cadena (fono,cadena) values ('".$this->datos_usuario["fono"]."','".$this->datos_usuario["cadena"]."')";
            $insertar = $this->sql_con->Execute($guardar);

            if($insertar)
              $this->datos["respuesta"] = 1;
            else
              $this->datos["respuesta"] = 0;

        }

    }


    
    
    protected function traer_numconf(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          //$i=0;
          while (!$clientes->EOF){

          $nombre = $clientes->Fields("nombre");
          $id_cliente = $clientes->Fields("id_cliente");

          $i=$id_cliente;

          $this->datos["cliente$i"] = array();



              /*if($i==3){

                    $agregar_select = ",hv.hot_nombre as nombre_hv";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hv on c.id_opcts = hv.id_hotel";

               }elseif($i==4){

                    $agregar_select = ",hcc.hot_nombre as nombre_opcts";
                    $join = "left join ".$clientes -> Fields('bd').".hotel hcc on c.id_opcts = hcc.codigo_cliente";

               }elseif($i==5){

                    $agregar_select = ",n.razon_social";
                    $join = "left join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
                    
               }else{

                    $agregar_select = "";
                    $join = "";
                    
               }*/



                if($nombre=="cocha"){

                      $agregar_select = ",n.nemo, pag_dir, plataxplata";
                      $join = "inner join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";

                }else{

                      $agregar_select = "";
                      $join = "";
                }

                if($this->datos_usuario["id_usuario"] == 2444)
                  $mostrar = "";
                else
                  $mostrar = "AND h.id_hotel not in (4321,3973,7303,6048,6448,6448,7338,6538,6852,6855)";


                $consulta = "

                        SELECT 
                          c.id_cot,
                          cd.id_cotdes,
                          ho.id_cot as cot_otas,
                          h.hot_nombre,
                          ag_nombre,
                          h.hot_fono,
                          h.id_hotel,
                          c.id_usuario,
                          c.cot_numfile,
                          c.cot_correlativo,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_fecdesde,
                          cd_fechasta,
                          c.id_opcts,
                          cd_numreserva ".$agregar_select." 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                          left JOIN otas.hotocu ho
                            ON ho.id_cotcli = c.id_cot
                          left join otas.cot co_o 
                          on co_o.id_cot = ho.id_cot
                          left join otas.agencia ag 
                          on ag.id_agencia = co_o.id_agencia
                          ".$join."
                        WHERE c.cot_estado = 0 
                          AND (c.id_seg = 7)
                          AND cd.id_seg = 7 
                          AND (
                            cd_numreserva IS NULL 
                            OR TRIM(cd_numreserva) = ''
                          )
                          AND (cd_fecdesde  BETWEEN '".date("Y-m-d")."'
                                  AND '".date("Y-m-d",strtotime("+5 years"))."'
                                  OR cd_fechasta  BETWEEN '".date("Y-m-d")."'
                                  AND '".date("Y-m-d",strtotime("+5 years"))."'
                                  OR '".date("Y-m-d")."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                                  OR '".date("Y-m-d",strtotime("+5 years"))."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                                )
                          AND cd.cd_estado = 0 
                          $mostrar

                       
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";

                
                //AND cd_fecdesde BETWEEN DATE_ADD(NOW(), INTERVAL - 4 DAY) 
                // AND DATE_ADD(NOW(), INTERVAL 10 DAY) 
                //echo $consulta."<br><br>";
                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = trim(utf8_encode($enviar->Fields("cd_numreserva")));

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;


                   if($nombre=="cocha"){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $arreglo = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));

                    }elseif($nombre == "turavion"){

                       $nemo_enviar = "";

                       $empresa = $this->buscar_empresa(1,$enviar->Fields("id_opcts"));

                       if($empresa!="")
                            $nemo_enviar = $empresa;


                        $arreglo = array("nemo"=>utf8_encode($nemo_enviar));


                    }elseif($nombre == "carlson"){

                       $nemo_enviar = "";

                       $empresa = $this->buscar_empresa(2,$enviar->Fields("id_opcts"));

                       if($empresa!="")
                            $nemo_enviar = $empresa;


                        $arreglo = array("nemo"=>utf8_encode($nemo_enviar));


                    }else
                        $arreglo = "";


                      /*if($i==3){

                          $arreglo = array(
                              "nemo"=>utf8_encode(trim($enviar->Fields("nombre_hv")))
                              );

                      }elseif($i==4){
                          $arreglo = array(
                              "nemo"=>utf8_encode(trim($enviar->Fields("nombre_opcts")))
                              );

                      }elseif($i==5){
                           
                          $arreglo = array(
                                  "nemo"=>utf8_encode(trim($enviar->Fields("razon_social")))
                                  );
                      }else{

                          $arreglo = "";
                      }*/


                    //$telefono;


                    $tma = $enviar->fields("cot_correlativo");

                    if($nombre == "otsi" or $nombre == "cts")
                      $tma = $enviar->fields("cot_numfile");


                    $obs_existe = $this->buscar_obs_numconf($enviar->Fields("id_cot"));

                    $a = array(

                            "cot"  => $enviar->Fields("id_cot"),
                            "id_comprador"  => $enviar->Fields("cot_otas"),
                            "agencia"  => $enviar->Fields("ag_nombre"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono" => $this->buscar_fono_formulario($enviar->Fields("id_hotel"),$nombre),
                            "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                            "desde"  => $enviar->Fields("fec_in"),
                            "hasta"  => $enviar->Fields("fec_out"),
                            "hab1"  => $enviar->Fields("cd_hab1"),
                            "hab2"  => $enviar->Fields("cd_hab2"),
                            "hab3"  => $enviar->Fields("cd_hab3"),
                            "hab4"  => $enviar->Fields("cd_hab4"),
                            "cd_fecdesde"  => $enviar->Fields("cd_fecdesde"),
                            "cd_fechasta"  => $enviar->Fields("cd_fechasta"),
                            "cotdes" => $enviar->Fields("id_cotdes"),
                            "operador" =>$operador,
                            "num_reserva"=>str_replace(" ", "", $reserva),
                            "cliente"=>$i,
                            "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                            "cadena"=>$this->datos_usuario["cadena"],
                            "estado_dia"=>$this->datos_usuario["estado_dia"],
                            "empresa"=>$arreglo,
                            "verificar_obs"=>$obs_existe,
                            "nombre"=>$nombre,
                            "tma"=>$tma

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }

    protected function buscar_empresa($tipo,$opcts){
      
      $consulta = "";
      
      switch ($tipo) {
        case 1:
            $consulta = "select hot_nombre as nombre from touravion_dev.hotel where codigo_cliente = '$opcts' ";
          break;

        case 2:
            $consulta = "select hot_nombre as nombre from carlson.hotel where id_hotel = '$opcts' ";
          break;

      }
        
      $traer = $this->sql_con->SelectLimit($consulta);
      return $traer->Fields("nombre");

    }

    protected function buscar_fono_formulario($id_hotel,$nombre){

      $consulta = "select fh.`tel_hotel` AS telefono,id_cadena,hpc.fono as fono_cadena from hoteles.hotelesmerge hm left join hoteles.formulario_hotel fh on fh.cadena = hm.id_cadena left join hoteles.fono_hotel_por_cadena hpc on hpc.cadena = hm.id_cadena where hm.id_hotel_$nombre = $id_hotel ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $fono = $traer->Fields("telefono");
      $this->datos_usuario["cadena"] = $traer->Fields("id_cadena");

      if($fono == "")
          $fono = $traer->Fields("fono_cadena");
      else
          $fono = $fono;


      if($fono == "null" or $fono == NULL)
          $fono = "";



        return $fono;
    }



    protected function buscar_obs_numconf($id_cot){

      if(isset($this->datos_usuario["sitio"])){

         $consulta = "select * from hoteles.obs_confirm where id_cot = $id_cot ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $retornar = 0;

         if($traer->RecordCount() > 0)
          $retornar = 1;


         return $retornar;

      }

    }


    protected function calcular_fecha($bd,$cot){

      if(isset($this->datos_usuario["sitio"])){

          /*$consulta = "select TIMEDIFF(NOW(),cot_fec) as diferencia from $bd.cot where id_cot = $cot";
          $traer = $this->sql_con->SelectLimit($consulta);
          $diferencia = $traer->Fields("diferencia");

          return $diferencia;*/

          $consulta = "select cot_fec from $bd.cot where id_cot = $cot";
          //echo $consulta."<br>";
          $traer = $this->sql_con->SelectLimit($consulta);

          $fecha_bd = $traer->Fields("cot_fec");
          $fecha_del_dia = date('Y-m-d H:i:s',strtotime("-2 hour"));

          //if($cot == 354313)
             //echo $fecha_bd."<=>".date('Y-m-d H:i:s',strtotime("-2 hour"))."<br>";


          $fecha1 = new DateTime("$fecha_bd");
          $fecha2 = new DateTime("$fecha_del_dia");
          $fecha = $fecha1->diff($fecha2);

          $ano = $fecha->y;
          $meses = $fecha->m;
          $dias = $fecha->d;
          $horas = $fecha->h;
          $minutos = $fecha->i;


          $diferencia = "A:".$ano." M:".$meses." D:".$dias." Hrs:".$horas." Min:".$minutos;

          $this->datos_usuario["estado_dia"] = 0;

          if($dias > 7 or $meses > 0 or $ano > 0)
            $this->datos_usuario["estado_dia"] = 1;

          return $diferencia;

      }



    }

    protected function traer_numconf_filtro(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          //$i=0;
          while (!$clientes->EOF){

          $nombre = $clientes->Fields("nombre");
          $id_cliente = $clientes->Fields("id_cliente");

          $i=$id_cliente;

          $this->datos["cliente$i"] = array();


          if($nombre=="cocha"){

                $agregar_nemo_select = ",n.nemo, pag_dir, plataxplata";
                $agregar_nemo_join = "inner join ".$clientes -> Fields('bd').".nemo n on c.nemo = n.id_nemo";
          }else{

                $agregar_nemo_select = "";
                $agregar_nemo_join = "";
          }


          $mostrar = "";

                /*if($this->datos_usuario["id_usuario"] == 2444)
                  $mostrar = "";
                else
                  $mostrar = "AND h.id_hotel not in (4321,3973,7303,6048,6448,6448,7338,6538,6852,6855)";*/



                $consulta = "

                        SELECT 
                          c.id_cot,
                          cd.id_cotdes,
                          hot_nombre,
                          ho.id_cot as cot_otas,
                          h.id_hotel,
                          ag_nombre,
                          hot_fono,
                          c.cot_correlativo,
                          c.cot_numfile,
                          c.id_usuario,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_fecdesde,
                          cd_fechasta,
                          cd_numreserva  ".$agregar_nemo_select."
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                        left JOIN otas.hotocu ho
                            ON ho.id_cotcli = c.id_cot
                          left join otas.cot co_o 
                          on co_o.id_cot = ho.id_cot
                          left join otas.agencia ag 
                          on ag.id_agencia = co_o.id_agencia
                            ".$agregar_nemo_join."
                        WHERE c.cot_estado = 0 
                          AND (c.id_seg = 7) 
                          AND cd.id_seg = 7 
                          AND (
                            cd_numreserva IS NULL 
                            OR TRIM(cd_numreserva) = ''
                          ) 
                          AND cd.cd_estado = 0 
                          AND (cd_fecdesde  BETWEEN '".$this->datos_usuario["desde"]."'
                                  AND '".$this->datos_usuario["hasta"]."'
                                  OR cd_fechasta  BETWEEN '".$this->datos_usuario["desde"]."'
                                  AND '".$this->datos_usuario["hasta"]."'
                                  OR '".$this->datos_usuario["desde"]."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                                  OR '".$this->datos_usuario["hasta"]."'   BETWEEN  cd_fecdesde
                                  AND cd_fechasta 
                          )
                          $mostrar
                        
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 



                ";

          
                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = trim(utf8_encode($enviar->Fields("cd_numreserva")));

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;

                    if($nombre=="cocha"){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));
                    }else
                        $nuevos_datos = "";



                    $tma = $enviar->fields("cot_correlativo");

                    if($nombre == "otsi" or $nombre == "cts")
                      $tma = $enviar->fields("cot_numfile");

                    $obs_existe = $this->buscar_obs_numconf($enviar->Fields("id_cot"));

                    $a = array(

                            "cot"  => $enviar->Fields("id_cot"),
                            "id_comprador"  => $enviar->Fields("cot_otas"),
                            "agencia"=>$enviar->Fields("ag_nombre"),
                            "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                            "fono" => $this->buscar_fono_formulario($enviar->Fields("id_hotel"),$nombre),
                            "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                            "desde"  => $enviar->Fields("fec_in"),
                            "hasta"  => $enviar->Fields("fec_out"),
                            "cd_fecdesde"  => $enviar->Fields("cd_fecdesde"),
                            "cd_fechasta"  => $enviar->Fields("cd_fechasta"),
                            "hab1"  => $enviar->Fields("cd_hab1"),
                            "hab2"  => $enviar->Fields("cd_hab2"),
                            "hab3"  => $enviar->Fields("cd_hab3"),
                            "hab4"  => $enviar->Fields("cd_hab4"),
                            "cotdes" => $enviar->Fields("id_cotdes"),
                            "operador" =>$operador,
                            "num_reserva"=>str_replace(" ", "", $reserva),
                            "cliente"=>$i,
                            "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                            "cadena"=>$this->datos_usuario["cadena"],
                            "estado_dia"=>$this->datos_usuario["estado_dia"],
                            "verificar_obs"=>$obs_existe,
                            "empresa"=>$nuevos_datos,
                            "tma"=>$tma

                        );

                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }



      protected function traer_numconf_buscar(){
        
        if(isset($this->datos_usuario["sitio"])){


          $clientes_query = "select * from clientes where estado = 0";
          $clientes=$this->sql_con->SelectLimit($clientes_query);
          $cantidad = $clientes->RecordCount();

          $this->datos["cantidad"] = $cantidad;


          //$i=0;
          while (!$clientes->EOF){

          $nombre = $clientes->Fields("nombre");
          $id_cliente = $clientes->Fields("id_cliente");

          $i=$id_cliente;

          $this->datos["cliente$i"] = array();



          if($nombre == "cts" or $nombre=="otsi"){

              $consulta = "

                        SELECT 
                          c.id_cot,
                          cd.id_cotdes,
                          ho.id_cot as cot_otas,
                          hot_nombre,
                          ag_nombre,
                          h.id_hotel,
                          hot_fono,
                          c.id_usuario,
                          c.cot_numfile,
                          c.cot_pripas AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_numreserva 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                          left JOIN otas.hotocu ho
                            ON ho.id_cotcli = c.id_cot
                          left join otas.cot co_o 
                          on co_o.id_cot = ho.id_cot
                          left join otas.agencia ag 
                          on ag.id_agencia = co_o.id_agencia
                           WHERE c.cot_estado = 0 
                           AND (c.id_cot = '".$this->datos_usuario["cot"]."')
                           AND cd_estado = 0 
                           AND (c.id_seg = 7)
                           AND cd.id_seg = 7
                        
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";


          }else{

                $consulta = "

                        SELECT 
                          c.id_cot,
                          cd.id_cotdes,
                          ho.id_cot as cot_otas,
                          hot_nombre,
                          ag_nombre,
                          h.id_hotel,
                          hot_fono,
                          c.id_usuario,
                          c.cot_correlativo,
                          CONCAT(
                            TRIM(c.cot_pripas),
                            ' ',
                            TRIM(c.cot_pripas2)
                          ) AS nombre_pax,
                          cd_hab1,
                          cd_hab2,
                          cd_hab3,
                          cd_hab4,
                          DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') AS fec_in,
                          DATE_FORMAT(cd_fechasta, '%d-%m-%Y') AS fec_out,
                          cd_numreserva 
                        FROM
                          ".$clientes -> Fields('bd').".cot c 
                          INNER JOIN ".$clientes -> Fields('bd').".cotdes cd 
                            ON c.id_cot = cd.id_cot 
                          INNER JOIN ".$clientes -> Fields('bd').".hotel h 
                            ON cd.id_hotel = h.id_hotel 
                          left JOIN otas.hotocu ho
                            ON ho.id_cotcli = c.id_cot
                          left join otas.cot co_o 
                          on co_o.id_cot = ho.id_cot
                          left join otas.agencia ag 
                          on ag.id_agencia = co_o.id_agencia
                           WHERE c.cot_estado = 0 
                           AND (c.id_cot = '".$this->datos_usuario["cot"]."')
                           
                           AND cd_estado = 0 
                           AND (c.id_seg = 7)
                           AND cd.id_seg = 7
                        ORDER BY cd_fecdesde,
                          nombre_pax ASC 

                ";


              }


                $enviar = $this->sql_con->SelectLimit($consulta);

                $operador = $clientes -> Fields('bd');
                while(!$enviar->EOF){


                    $num_reserva = trim(utf8_encode($enviar->Fields("cd_numreserva")));

                    $reserva = "";
                    if($num_reserva == "")
                        $reserva = "";
                    else
                        $reserva = $num_reserva;


                    $fono = $enviar->Fields("hot_fono");

                    $telefono = "";
                    if($fono == "")
                        $telefono = "";
                    else
                        $telefono = $fono;

                    if($i==5){

                        $nemo_enviar = "";
                        
                        if($enviar->Fields("nemo") == "")
                            $nemo_enviar = "";
                        else
                            $nemo_enviar = $enviar->Fields("nemo");


                        $nuevos_datos = array("pag_dir"=>$enviar->Fields("pag_dir"),
                                        "plataxplata"=>$enviar->Fields("plataxplata"),
                                        "nemo"=>utf8_encode($nemo_enviar));
                    }else
                        $nuevos_datos = "";



                    $tma = $enviar->fields("cot_correlativo");

                    if($nombre == "otsi" or $nombre == "cts")
                      $tma = $enviar->fields("cot_numfile");


                    $obs_existe = $this->buscar_obs_numconf($enviar->Fields("id_cot"));


                    

                        $a = array(

                                "cot"  => $enviar->Fields("id_cot"),
                                "id_comprador"  => $enviar->Fields("cot_otas"),
                                "agencia"=>$enviar->Fields("ag_nombre"),
                                "hotel"  => utf8_encode($enviar->Fields("hot_nombre")),
                                "fono" => $this->buscar_fono_formulario($enviar->Fields("id_hotel"),$nombre),
                                "pax"  => utf8_encode($enviar->Fields("nombre_pax")),
                                "desde"  => $enviar->Fields("fec_in"),
                                "hasta"  => $enviar->Fields("fec_out"),
                                "hab1"  => $enviar->Fields("cd_hab1"),
                                "hab2"  => $enviar->Fields("cd_hab2"),
                                "hab3"  => $enviar->Fields("cd_hab3"),
                                "hab4"  => $enviar->Fields("cd_hab4"),
                                "cotdes" => $enviar->Fields("id_cotdes"),
                                "operador" =>$operador,
                                "num_reserva"=>str_replace(" ", "", $reserva),
                                "cliente"=>$i,
                                "hace_cuanto"=>$this->calcular_fecha($operador,$enviar->Fields("id_cot")),
                                "cadena"=>$this->datos_usuario["cadena"],
                                "estado_dia"=>$this->datos_usuario["estado_dia"],
                                "verificar_obs"=>$obs_existe,
                                "empresa"=>$nuevos_datos,
                                "tma"=>$tma

                            );


                        

                    array_push($this->datos["cliente$i"], $a);


                    $enviar->MoveNext();

                }


                $clientes->MoveNext();

            }


        }


      }
    
   
    
    
    
    protected function quitar_caracteres(){
        
        $this->mssql_escape($this->datos_usuario["sitio"]); 
        $this->mssql_escape($this->datos_usuario["tipo"]);
        $this->mssql_escape($this->datos_usuario["desde"]);
        $this->mssql_escape($this->datos_usuario["hasta"]);
        $this->mssql_escape($this->datos_usuario["cot"]);
    }
    

   
    protected function mssql_escape($str){
        
            if(get_magic_quotes_gpc())
            {
                $str= stripslashes($str);
            }
            return str_replace("'", "''", $str);
    }
    
    
    function __destruct(){
         echo json_encode($this->datos);
        
    }
    

}

?>