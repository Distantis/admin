<?php

$modificador_hotelesmerge = new ModificadorHotelesmerge();

class ModificadorHotelesmerge{
    
    private $datos,$datos_usuario = array();
    private $markup;

    public function __construct(){
        
        //error_reporting(E_ALL);
        //ini_set("display_errors", 1);
        //include_once("secure.php");
        include('Connections/db1.php');
        $this->set_conectar($db1);
        $this->post();
        

        
    }
    
    protected function set_conectar($valor){
  
        $this->sql_con = $valor;
   }

    


   protected function post(){

      extract($_POST);

      $this->datos_usuario["tipo"] = $tipo;
      $this->datos_usuario["pk"] = $pk;
      $this->datos_usuario["valor"] = $valor;
      $this->datos_usuario["id"] = $id;
      $this->datos_usuario["id_cadena"] = $id_cadena;
      $this->datos_usuario["tipohab"] = $tipohab;
      $this->datos_usuario["id_tipohabitacion"] = $id_tipohabitacion;
      $this->datos_usuario["bd"] = $bd;
      $this->traer($tipo);

   }
    

    
    public function traer($tipo){
        
        
        switch($tipo){
            
            
            case 1:
                 
              $this->traer_hoteles();
            
            break;

            case 2:
                 
              $this->traer_detalle_pk();
            
            break;

            case 3:
                 
              $this->actualizar_datos_hotelesmerge();
            
            break;


            case 4:
                 
              $this->pasar_global();
            
            break;


            case 5:
                 
              $this->quitar_global();
            
            break;

            case 6:
                 
              $this->agregar_pestana_normal();
            
            break;

            case 7:
                 
              $this->quitar_pestana_normal();
            
            break;


            case 8:
                 
              $this->traer_cadenas();
            
            break;

            case 9:
                 
              $this->traer_detalle_cadena();

            break;

            case 10:
                 
              $this->actualizar_datos_cadena();

            break;

            case 11:
                 
              $this->traer_hab();

            break;

            case 12:
                 
              $this->traer_habitaciones_op();

            break;

            case 13:
                 
              //$this->act_habglobal();

            break;

            
        }
        
   
        
    }


    protected function act_habglobal(){

        $actualizar = "update ".$this->datos_usuario["bd"].".tipohabitacion set id_tipohab = ".$this->datos_usuario["valor"]." where id_tipohabitacion = '".$this->datos_usuario["id_tipohabitacion"]."' ";
        /*$act = $this->sql_con->Execute($actualizar);

        

        if($act)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;*/

    }

    protected function traer_hab(){

      $consulta = "select th.th_nombre,th.id_tipohabitacion from hoteles.hotdet hd join hoteles.tipohabitacion th on th.id_tipohabitacion = hd.id_tipohabitacion where id_pk = ".$this->datos_usuario["pk"]." and hd_estado = 0 ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hab"] = array();

      while(!$traer->EOF){

        $nombre_hab = trim(utf8_encode($traer->Fields("th_nombre")));

        $datos = array(
                        "th_nombre"=>$nombre_hab,
                        "id_tipohabitacion"=>$traer->Fields("id_tipohabitacion")
                        //"hab"=>$this->hab($bd)
                      );

        array_push($this->datos["hab"], $datos);

        $traer->MoveNext();
      }


    }

    protected function traer_habitaciones_op(){

      $traer_clientes = "select * from hoteles.clientes";
      $clientes = $this->sql_con->SelectLimit($traer_clientes);

      $this->datos["habitaciones"] = array();

      while(!$clientes->EOF){

        $nombre = $clientes->Fields("nombre");
        $bd = $clientes->Fields("bd");

        $datos = array(
                        "cliente"=>$nombre,
                        "hab"=>$this->hab(1,$bd),
                        "id_tipohabitacion"=>$this->hab(2,$bd),
                        "cliente_b"=>$bd
                      );

        array_push($this->datos["habitaciones"], $datos);

        $clientes->MoveNext();
      }


    }


    protected function hab($tipo,$bd){

      $consulta = "";

      switch ($tipo) {
        
        case 1:
          $consulta = "select id_tipohab as tip from $bd.tipohabitacion where id_tipohab = ".$this->datos_usuario["tipohab"]." ";
          break;
        
        case 2:
          $consulta = "select id_tipohabitacion as tip from $bd.tipohabitacion where id_tipohab = ".$this->datos_usuario["tipohab"]." ";
          break;
      
      }

      echo $consulta.";<br>";
      $traer = $this->sql_con->SelectLimit($consulta);

      return $traer->Fields("tip");

    }


    protected function traer_cadenas(){


      $consulta = "select * from hoteles.cadena";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["cadenas"] = array();

      while(!$traer->EOF){


             $datos = array(
                    
                    "nombre_cadena" => utf8_encode($traer->Fields("nom_cadena")),
                    "id_cadena"=>$traer->Fields("id_cadena")

                  );            


            array_push($this->datos["cadenas"], $datos);

        

        $traer->MoveNext();
      }


    }


    protected function quitar_pestana_normal(){

      $actualizar = "update hoteles.hotelesmerge set mira = 1 where id_pk = '".$this->datos_usuario["pk"]."' ";
      $act = $this->sql_con->Execute($actualizar);



        if($act)
          $this->datos["respuesta"] = 1;
        else
          $this->datos["respuesta"] = 0;


    }


    protected function agregar_pestana_normal(){

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' and mira = 0 ";
      $revisar = $this->sql_con->SelectLimit($consulta);

      if($revisar->RecordCount() > 0)
          $this->datos["respuesta"] = 2;
      else{

          $actualizar = "update hoteles.hotelesmerge set mira = 0 where id_pk = '".$this->datos_usuario["pk"]."' ";
          $act = $this->sql_con->Execute($actualizar);

        

            if($act)
              $this->datos["respuesta"] = 1;
            else
              $this->datos["respuesta"] = 0;

      }

    }


    protected function quitar_global(){

      $actualizar = "update hoteles.hotelesmerge set ver = 1 where id_pk = '".$this->datos_usuario["pk"]."' ";
      $act = $this->sql_con->Execute($actualizar);

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' ";
      $revisar = $this->sql_con->SelectLimit($consulta);



        if($revisar->RecordCount() > 0){

           $con = "select * from distantis.hotelbeds_merge_hoteles where id_hotel_cts = ".$revisar->FIelds("id_hotel_cts")." ";
           $t = $this->sql_con->SelectLimit($con);

              if($t->RecordCount() > 0){

                 $goblal_hotelbeds = "UPDATE
                                        distantis.hotelbeds_merge_hoteles hmh 
                                        JOIN distantis.hotelbeds_merge_tarifas hmt 
                                          ON hmt.id_tarifa_hb = hmh.id_hotel_hb 
                                        SET hmh.usa_global = 0, hmt.usa_global = 0  
                                      WHERE id_hotel_cts = ".$revisar->FIelds("id_hotel_cts")."
                                    ";
                                   
                  $this->sql_con->Execute($goblal_hotelbeds) or $this->errores(__LINE__);

              }
        }



       if($act){

          /*$this->datos_usuario["cont"] = 0;
          $this->datos_usuario["cont_act"] = 0;
          $this->buscar_tarifas_vigentes(0);

          if($this->datos_usuario["cont"] == $this->datos_usuario["cont_act"])
            $this->datos["respuesta"] = 1;
          else
            $this->datos["respuesta"] = 0;*/

          $this->datos["respuesta"] = 1;  

        }else
          $this->datos["respuesta"] = 0;


    }


    protected function pasar_global(){

      $consulta = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."' and ver = 0 ";
      $revisar = $this->sql_con->SelectLimit($consulta);

      if($revisar->RecordCount() > 0)
          $this->datos["respuesta"] = 2;
      else{

          $actualizar = "update hoteles.hotelesmerge set ver = 0 where id_pk = '".$this->datos_usuario["pk"]."' ";
          $act = $this->sql_con->Execute($actualizar);


          $consulta1 = "select * from hoteles.hotelesmerge where id_pk = '".$this->datos_usuario["pk"]."'";
          $revisar1 = $this->sql_con->SelectLimit($consulta1);

          if($revisar1->RecordCount() > 0){

                $goblal_hotelbeds = "UPDATE
                                    distantis.hotelbeds_merge_hoteles hmh 
                                    JOIN distantis.hotelbeds_merge_tarifas hmt 
                                      ON hmt.id_tarifa_hb = hmh.id_hotel_hb 
                                    SET hmh.usa_global = 1, hmt.usa_global = 1  
                                  WHERE id_hotel_cts = ".$revisar1->FIelds("id_hotel_cts")."
                                  ";
                             
                $this->sql_con->Execute($goblal_hotelbeds);

          }



            if($act){

              $this->datos["respuesta"] = 1;

              /*$this->datos_usuario["cont"] = 0;
              $this->datos_usuario["cont_act"] = 0;
              $this->buscar_tarifas_vigentes(1);

              if($this->datos_usuario["cont"] == $this->datos_usuario["cont_act"])
                $this->datos["respuesta"] = 1;
              else
                $this->datos["respuesta"] = 0;*/



            }else
              $this->datos["respuesta"] = 0;

      }

    }


    protected function buscar_tarifas_vigentes($tipo){


      $consulta_clientes =  "select * from hoteles.clientes";
      $traer_clientes = $this->sql_con->SelectLimit($consulta_clientes);
      $fecha = date("Y-m-d");
      while(!$traer_clientes->EOF){

        $nombre = $traer_clientes->Fields("nombre");
        $bd = $traer_clientes->Fields("bd");

        $consulta = "
                  SELECT 
                     hd.id_hotdet
                  FROM
                    hoteles.hotelesmerge hm
                    JOIN $bd.hotdet hd
                    ON hd.id_hotel = hm.id_hotel_$nombre
                  WHERE id_pk = ".$this->datos_usuario["pk"]."
                  AND hd.hd_estado = 0
                  AND hd.hd_fechasta>='$fecha'
                  ";
             
        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

          if($traer->RecordCount() > 0){
              
              while(!$traer->EOF){
                $hotdet = $traer->Fields("id_hotdet");
                $this->actualizar_stock($bd,$hotdet,$tipo);
                $traer->MoveNext();
              }
          }

        $traer_clientes->MoveNext();
      }


    }


    protected function actualizar_stock($bd,$hotdet,$tipo){

       $this->datos_usuario["bd"] = $bd;
       $fecha = date("Y-m-d");
       $consulta = "select sc_fecha,id_stock from $bd.stock where id_hotdet = $hotdet and sc_fecha>='$fecha' order by sc_fecha";
       $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

       if($traer->RecordCount() > 0){

          while(!$traer->EOF){

            $this->datos_usuario["cont"]+=1;

            $sc_fecha = $traer->Fields("sc_fecha");

            if($tipo == 1){
              $hc_hab1 = $this->ocupacion(1,$hotdet,$sc_fecha);
              $hc_hab2 = $this->ocupacion(2,$hotdet,$sc_fecha);
              $hc_hab3 = $this->ocupacion(3,$hotdet,$sc_fecha);
              $hc_hab4 = $this->ocupacion(4,$hotdet,$sc_fecha);


              if($hc_hab1>0)
                $hc_hab1 = $hc_hab1 * -1 ;


              if($hc_hab2>0)
                $hc_hab2 = $hc_hab2 * -1 ;


              if($hc_hab3>0)
                $hc_hab3 = $hc_hab3 * -1 ;


              if($hc_hab4>0)
                $hc_hab4 = $hc_hab4 * -1 ;

            }else{
              $hc_hab1 = 0;
              $hc_hab2 = 0;
              $hc_hab3 = 0;
              $hc_hab4 = 0; 
            }


            $actualizar = "update $bd.stock set sc_hab1 = $hc_hab1, sc_hab2 = $hc_hab2, sc_hab3 = $hc_hab3, sc_hab4 = $hc_hab4 where id_hotdet = $hotdet and sc_fecha = '$sc_fecha' ";
            echo $actualizar."<br>";
            /*$act = $this->sql_con->Execute($actualizar) or $this->errores(__LINE__);
    

            if($act)
              $this->datos_usuario["cont_act"]+=1;*/

            $traer->MoveNext();
          }

       }


    }


    protected function ocupacion($tipo,$hotdet,$fecha){

        switch ($tipo) {

                case 1:
                  
                   $consulta = "select sum(hc_hab1) as valor from ".$this->datos_usuario["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";
                   
                break;

                case 2:
                  
                   $consulta = "select sum(hc_hab2) as valor from ".$this->datos_usuario["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 3:
                  
                   $consulta = "select sum(hc_hab3) as valor from ".$this->datos_usuario["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;

                case 4:
                  
                   $consulta = "select sum(hc_hab4) as valor from ".$this->datos_usuario["bd"].".hotocu where hc_fecha = '$fecha' and id_hotdet = $hotdet and hc_estado = 0";

                break;
            
            
        }

        $traer = $this->sql_con->SelectLimit($consulta) or $this->errores(__LINE__);

        $valor = $traer->Fields("valor");

        if($valor == "")
            $valor = 0;

        return $valor;

    }


    protected function actualizar_datos_hotelesmerge(){


       if($this->datos_usuario["id"] != "cadena"){

           $actualizar = "update hoteles.hotelesmerge set 
                              id_hotel_".$this->datos_usuario["id"]." = ".$this->datos_usuario["valor"]." 
                              where id_pk = '".$this->datos_usuario["pk"]."' 
                         ";
      
           $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          

      }else{

         $actualizar = "update hoteles.hotelesmerge set id_cadena = ".$this->datos_usuario["valor"]." 
                              where id_pk = '".$this->datos_usuario["pk"]."' 
                         ";

         $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          
        

      }




    }


     protected function actualizar_datos_cadena(){


       if($this->datos_usuario["id"] != "nom_cadena"){

           $actualizar = "update hoteles.cadena set modo = ".$this->datos_usuario["valor"]." 
                              where id_cadena = '".$this->datos_usuario["id_cadena"]."' 
                         ";

           $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          

      }else{

         $actualizar = "update hoteles.cadena set nom_cadena = '".$this->datos_usuario["valor"]."'
                              where id_cadena = '".$this->datos_usuario["id_cadena"]."' 
                         ";


         $act = $this->sql_con->Execute($actualizar);

           if($act)
               $this->datos["respuesta"] = 1;
           else
               $this->datos["respuesta"] = 0;          
        

      }




    }


    protected function traer_detalle_pk(){


      $consulta = "select * from hoteles.hotelesmerge where id_pk =  '".$this->datos_usuario["pk"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["detalle"] = array();

      while(!$traer->EOF){


        $datos = array(
                          "cts" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "turavion" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travel" => $traer->Fields("id_hotel_travelclub"),
                          "carlson" => $traer->Fields("id_hotel_carlson"),
                          "mundotour" => $traer->Fields("id_hotel_mundotour"),
                          "tourmundial" => $traer->Fields("id_hotel_tourmundial"),
                          "euroandino" => $traer->Fields("id_hotel_euroandino"),
                          "cadena" =>$traer->Fields("id_cadena")
                  );


        array_push($this->datos["detalle"], $datos);


        $traer->MoveNext();



      }



    }

    protected function traer_detalle_cadena(){


      $consulta = "select * from hoteles.cadena where id_cadena =  '".$this->datos_usuario["id_cadena"]."'  ";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["detalle_cadena"] = array();

      while(!$traer->EOF){


        $datos = array(
                          "nom_cadena" => utf8_encode($traer->Fields("nom_cadena")),
                          "modo" => $traer->Fields("modo")
                  );


        array_push($this->datos["detalle_cadena"], $datos);


        $traer->MoveNext();



      }



    }

    protected function traer_hoteles(){


      $consulta = "select * from hoteles.hotelesmerge";
      $traer = $this->sql_con->SelectLimit($consulta);

      $this->datos["hoteles"] = array();

      while(!$traer->EOF){

      $hoteles = array(
                          "distantis" => $traer->Fields("id_hotel_cts"),
                          "veci" => $traer->Fields("id_hotel_veci"),
                          "otsi" => $traer->Fields("id_hotel_otsi"),
                          "touravion_dev" => $traer->Fields("id_hotel_turavion"),
                          "cocha" => $traer->Fields("id_hotel_cocha"),
                          "travelclub" => $traer->Fields("id_hotel_travelclub"),
                          "carlson" => $traer->Fields("id_hotel_carlson"),
                          "mundotour" => $traer->Fields("id_hotel_mundotour"),
                          "tourmundial" => $traer->Fields("id_hotel_tourmundial"),
                          "euroandino" => $traer->Fields("id_hotel_euroandino")
                  );


        $nombre_hotel = $this->nombre_hotel($hoteles);



        if($nombre_hotel != "NADA"){

             $datos = array(
                    
                    "nombre_hotel" => utf8_encode($nombre_hotel),
                    "id_pk"=>$traer->Fields("id_pk")

                  );            


            array_push($this->datos["hoteles"], $datos);

        }


        $traer->MoveNext();
      }


    }


    protected function nombre_hotel($hoteles){
        
        $bd = "";
        $id_hotel = "";
        $revisar = 0;
        foreach($hoteles as $key=>$datos){
         
            if($datos != 0 and $revisar == 0){
              $bd = $key;
              $id_hotel = $datos;
              $revisar = 1;
            }

        }


       if($bd!= "" and $id_hotel != ""){
        
         $consulta = "select hot_nombre from $bd.hotel where id_hotel = $id_hotel ";
         $traer = $this->sql_con->SelectLimit($consulta);

         $nombre_hotel = $traer->Fields("hot_nombre");

       }else
          $nombre_hotel = "NADA";


       return $nombre_hotel;
       
    }



     protected function errores($linea){

        die($_SERVER['REQUEST_URI']." - ".$linea." : ".$this->sql_con->ErrorMsg());
    }
   

    function __destruct(){
         $this->sql_con->close();
         echo json_encode($this->datos); 
    }
    


}


?>